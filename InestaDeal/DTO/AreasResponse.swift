//
//  AreasResponse.swift
//  InestaDEal
//
//  Created by Muhammad Mrzok on 5/19/19.
//  Copyright © 2019 Waleed. All rights reserved.
//
import ObjectMapper

class AreasResponse: BaseResponse
{
    //MARK:- Properties
    var id      :String?
    var name    :String?
    var child   :[AreasResponse]?
    var data    :[AreasResponse]?
    var price   :Float?
    
    // Custom Addition
    var isExpanded : Bool = false 
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id      <- map["id"]
        name    <- map["name"]
        child   <- map["child"]
        data    <- map["data"]
        price   <- map["price"]
        
        // Custom Addition
        isExpanded <- map["isExpanded"]
    }
}

class AreasPricesResponse: BaseResponse
{
    //MARK:- Properties
    var data    :Int?
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data    <- map["data"]
    }
}
