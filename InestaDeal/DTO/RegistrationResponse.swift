//
//  RateResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//
import ObjectMapper

class RegistrationResponse: BaseResponse
{
    
    //MARK:- Properties
    var id: String?
    var status:Int?
    var utype:String?

    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["data.id"]
        status <- map["data.status"]
        utype <- map["data.utype"]

    }
}
