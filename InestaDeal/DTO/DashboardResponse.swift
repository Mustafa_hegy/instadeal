//
//  DashboardResponse.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 12/16/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import ObjectMapper

class DashboardResponse: BaseResponse {

    //MARK:- Properties
    var slider              :[SlidersResponse]?
    var cats                :[CategoriesResponse]?
    var offers              :[SlidersResponse]?
    var topSales            :[CatsProducts]?
    var newProducts         :[CatsProducts]?
    var selectedProducts    :[CatsProducts]?
    var lastId              :String?
    var limit               : Int = 0
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        slider              <- map["slider"]
        cats                <- map["cats"]
        offers              <- map["offers"]
        topSales            <- map["top_sales"]
        newProducts         <- map["new_products"]
        selectedProducts    <- map["selected"]
        lastId              <- map["lastId"]
        limit               <- map["limit"]
    }
}
