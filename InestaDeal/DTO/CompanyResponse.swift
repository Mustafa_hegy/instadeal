//
//  RateResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//
import ObjectMapper

class CompanyResponse: BaseResponse
{
    
    //MARK:- Properties
    var id: String?
    var name:String?
    var photo:String?
    var photo1:String?
    var delivery_time:String?
    var delivery_price:String?
    var deliveryPrice:Float?
    var gmmaly :String?
    var data:[CompanyResponse]?
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        photo <- map["photo"]
        photo1 <- map["photo1"]
        delivery_time <- map["delivery_time"]
        delivery_price <- map["delivery_price"]
        deliveryPrice <- map["delivery_price"]
        gmmaly <- map["gamm3"]
        data <- map["data"]
        

    }
}
