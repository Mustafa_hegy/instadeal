//
//  RateResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//
import ObjectMapper

class GetSupport: BaseResponse
{
    required init?(map: Map) {
        super.init()
    }
    var id:String?
    var mobile:String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        mobile <- map["mobile"]
        
        
        
    }
}

class GetSupportResponse: BaseResponse
{
    required init?(map: Map) {
        super.init()
    }
    var data:[GetSupport]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
        
        
        
    }
}

class GetOffersResponse: BaseResponse
{
    
    //MARK:- Properties
    
    var data: [OffersResponse]?
    
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
        
        
        
    }
}

class OffersResponse: BaseResponse
{
    
    //MARK:- Properties
    
    var id: String?
    var name: String?
    var FeedData: String?
    var pdate: String?
    var price: String?
    var photo: String?
    var photo1: String?
    var photo2: String?
    var photo3: String?
    var photo4: String?
    var photo5: String?
    var photo6: String?
    var cat_id: String?
    var active: String?
    var mobile: String?
    var views: String?
    var client_id: String?
    var cat_name: String?
    var company_id: String?
    var watermark: String?
    var offer_big: String?
    var offer_small: String?
    var offer: String?
    var favChecked: Int?
    var likes: Int?
    
    
    var discount        :String?
    var newPrice        : Double?
    var counts          :String?
    var companyId       :String?
    var deliveryPrice   :String?
    var gmmaly          :String?
    var freeDelivery    :Int?
    
    required init?(map: Map) {
        super.init()
    }
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        FeedData <- map["FeedData"]
        pdate <- map["pdate"]
        price <- map["price"]
        photo <- map["photo"]
        photo1 <- map["photo1"]
        photo2 <- map["photo2"]
        photo3 <- map["photo3"]
        photo4 <- map["photo4"]
        photo5 <- map["photo5"]
        photo6 <- map["photo6"]
        offer_big <- map["offer_big"]
        offer_small <- map["offer_small"]
        
        cat_id <- map["cat_id"]
        cat_name <- map["cat_name"]
        active <- map["active"]
        mobile <- map["mobile"]
        views <- map["views"]
        client_id <- map["client_id"]
        company_id <- map["company_id"]
        watermark <- map["watermark"]
        likes <- map["likes"]
        offer <- map["offer"]
        favChecked <- map[" favChecked"]
        
        
        discount        <- map["discount"]
        newPrice        <- map["new_price"]
        counts          <- map["counts"]
        companyId       <- map["company_id"]
        deliveryPrice   <- map["delivery_price"]
        gmmaly          <- map["gamm3"]
        freeDelivery    <- map["free_delivery"]
    }
}

class ProductDetailsResponse: BaseResponse
{
    //MARK:- Properties
    
    var id: String?
    var name: String?
    var FeedData: String?
    var pdate: String?
    var price: String?
    var photo: String?
    var photo1: String?
    var photo2: String?
    var photo3: String?
    var photo4: String?
    var photo5: String?
    var photo6: String?
    var cat_id: String?
    var counts: String?
    var active: String?
    var mobile: String?
    var views: String?
    var client_id: String?
    var cat_name: String?
    var company_id: String?
    var watermark: String?
    var offer_photo: String?
    var offer: String?
    var favChecked: Int?
    var newPrice: Double?
    var discount: String?
    
    var delivery_time: String?
    var more_feats: [ProductFeats]?
    var deliveryPrice   :String?
    var companyName    :String?
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["data.id"]
        name <- map["data.name"]
        FeedData <- map["data.FeedData"]
        pdate <- map["data.pdate"]
        price <- map["data.price"]
        photo <- map["data.photo"]
        photo1 <- map["data.photo1"]
        photo2 <- map["data.photo2"]
        photo3 <- map["data.photo3"]
        photo4 <- map["data.photo4"]
        photo5 <- map["data.photo5"]
        photo6 <- map["data.photo6"]
        cat_id <- map["data.cat_id"]
        counts <- map["data.counts"]
        cat_name <- map["data.cat_name"]
        active <- map["data.active"]
        mobile <- map["data.mobile"]
        views <- map["data.views"]
        client_id <- map["data.client_id"]
        company_id <- map["data.company_id"]
        watermark <- map["data.watermark"]
        offer_photo <- map["data.offer_photo"]
        offer <- map["data.offer"]
        favChecked <- map["data.favChecked"]
        newPrice <- map["data.new_price"]
        discount <- map["data.discount"]
        
        delivery_time   <- map["data.delivery_time"]
        more_feats      <- map["data.more_feats"]
        deliveryPrice   <- map["data.delivery_price"]
        companyName     <- map["data.company_name"]
    }
}

class ProductFeats: BaseResponse
{
    //MARK:- Properties
    
    var name: String?
    var feat_required: String?
    var FeatsItem: [featItem]?
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        name <- map["name"]
        feat_required <- map["feat_required"]
        FeatsItem <- map["FeatsItem"]
    }
}

class featItem: BaseResponse
{
    //MARK:- Properties
    /*
    feat_value: "0",
    feat_id: "4",
    feat_name: "احمر",
    feat_add_to_cart: "0_4_احمر",
    selected: true,
     */
    var feat_value: String?
    var feat_id: String?
    var feat_name: String?
    var feat_add_to_cart: String?
    var selected: Bool?

    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        feat_value <- map["feat_value"]
        feat_id <- map["feat_id"]
        feat_name <- map["feat_name"]
        feat_add_to_cart <- map["feat_add_to_cart"]
        selected <- map["selected"]
    }
}
/*
class ProductsListResponse: BaseResponse{
    
    var data: [ProductDetailsResponse]?
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
    
}
 */
