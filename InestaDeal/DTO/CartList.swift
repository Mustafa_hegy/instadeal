//
//  RateResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//
import ObjectMapper

import Foundation
import Alamofire

struct CartList: Codable {
    let result: String
    let data: [CartListData]
    let message: String
}

struct CartListData: Codable {
    let id, name, gamm3, deliveryPrice: String
    var products: [Product]
    var isChecked = false
    
    
    enum CodingKeys: String, CodingKey {
        case id, name, gamm3
        case deliveryPrice = "delivery_price"
        case products
    }
}

struct Product: Codable {
    let id, prod, count, price: String
    let totalPrice, date, companyID, gamm3: String
    let prodID: String
    let photo: String
    let companyDeliveryPrice: String
    var isChecked = false
    
    // New Edit
    let status      :String?
    let order_id    :String?
    let product_name:String?
    let status_text :String?
    let company_name:String?
    let deliveryTime:String?
    let options:[itemOptions]?
    
    enum CodingKeys: String, CodingKey {
        case id, prod, count, price
        case totalPrice = "total_price"
        case date
        case companyID = "company_id"
        case gamm3
        case prodID = "prod_id"
        case photo
        case companyDeliveryPrice = "company_delivery_price"
        
        // New Edit
        case status
        case order_id
        case product_name
        case status_text
        case company_name
        case deliveryTime
        case options
    }
}

struct itemOptions: Codable {
    let option_name, option_value, option_price_txt, option_price: String
    
    enum CodingKeys: String, CodingKey {
        case option_name, option_value, option_price_txt,option_price
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// Alamofire 

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseCartList(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<CartList>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}





class GetCartProducte: BaseResponse
{
    
    //MARK:- Properties
    
    var id: String?
    var prod: String?
    var count: String?
    var price: String?
    var total_price: String?
    var date: String?
    var company_id: String?
    var photo: String?
    
    

    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        prod <- map["prod"]
        count <- map["count"]
        total_price <- map["total_price"]
        price <- map["price"]
        photo <- map["photo"]
        date <- map["date"]
        company_id <- map["company_id"]
        
        

    }
}

class GetCartResponse: BaseResponse
{
    
    //MARK:- Properties
    
    var data: [GetCartProducte]?

    
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
      
        
        
        
    }
}
