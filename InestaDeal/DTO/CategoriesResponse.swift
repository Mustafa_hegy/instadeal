//
//  RateResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//
import ObjectMapper

class CategoriesResponse: BaseResponse
{
    
    //MARK:- Properties
    var id: String?
    var name:String?
    var photo:String?
    var lvl:String?
    var parent:String?
    var childs:[CategoriesResponse]?
    var data:[CategoriesResponse]?
    var favChecked: Int = 0
    
    var file:String?
    var icon:String?
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        photo <- map["photo"]
        parent <- map["parent"]
        lvl <- map["lvl"]
        childs <- map["child"]
        data <- map["data"]
        favChecked <- map["favChecked"]
        
        file <- map["file"]
        icon <- map["icon"]
        

    }
}
