//
//  AreasResponse.swift
//  InestaDEal
//
//  Created by Muhammad Mrzok on 5/19/19.
//  Copyright © 2019 Waleed. All rights reserved.
//
import ObjectMapper

class DeliveryTimeResponse: BaseResponse{
    
    var data: [DeliveryTimes]?
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
    
    
}

class DeliveryTimes: BaseResponse
{
    //MARK:- Properties
    var id          :String?
    var ctime1      :String?
    var ctime2      :String?
    var area_id     :String?
    var weeknum     :String?
    var status      :String?
    var check       :String?
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id          <- map["id"]
        ctime1      <- map["ctime1"]
        ctime2      <- map["ctime2"]
        area_id     <- map["area_id"]
        weeknum     <- map["weeknum"]
        status      <- map["status"]
        check       <- map["check"]
    }
}

