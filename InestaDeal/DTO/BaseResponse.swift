//
//  BaseResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import ObjectMapper

class singleNotification: BaseResponse{
 
    
    var id: String?
    var client_id: String?
    var title: String?
    var text2: String?
    var ndate: String?
    var page: String?
    var readed: String?
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        client_id <- map["client_id"]
        title <- map["title"]
        text2 <- map["text2"]
        ndate <- map["ndate"]
        page <- map["page"]
        readed <- map["readed"]
    }
    
    
}

class NotificationsResponse: BaseResponse{
    
    var data: [singleNotification]?
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
    
    
}

class GetUserResponse: BaseResponse{
    
    var id: String?
    var name: String?
    var mobile: String?
    var email: String?
    var active: String?
    var password: String?
    var utype: String?
    var faceid: String?
    var device_token: String?
    var rdate: String?
    var photo: String?
    var cats: String?
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["data.id"]
        name <- map["data.name"]
        email <- map["data.email"]
        active <- map["data.active"]
        password <- map["data.password"]
        utype <- map["data.utype"]
        faceid <- map["data.faceid"]
        device_token <- map["data.device_token"]
        rdate <- map["data.rdate"]
        photo <- map["data.photo"]
        cats <- map["data.cats"]
        mobile <- map["data.mobile"]
    }
    
    
}

class ProductForEditResponse: BaseResponse{
    
    var cat_id: String?
    var cat_name: String?
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        cat_id <- map["data.cat_id"]
        cat_name <- map["data.cat_name"]
    }
    
    
}

class Order: BaseResponse{
    
    var id: String?
    var client_id : String?
    var odate: String?
    var company_id: String?
    var status: String?
    var otime: String?
    var neworder: String?
    var payment: String?
    var address_id: String?
    var count: String? //Int?
    var sum: String?
    var area_name: String?
    var company_name: String?
    var company_mobile: String?
    var client_mobile: String?
    var client_email: String?
    var client_name: String?
    var gammaly : String?
    var deliveryCost : String?
    var companies: [CompanyDetailsForOrdersResponse]?
    
    
    // New Edit
    var prod_id         : String?
    var date            : String?
    var order_id        : String?
    var price           : String?
    var gamm3           : String?
    var product_name    : String?
    var photo           : String?
    var status_text     : String?
    var deliv_text      : String?
    var deliveryTime    : String?
    
    var client          : String?
    var time_id         : String?
    var time_txt        : String?
    var time_id_date    : String?
    var address         : String?
    var payment_id      : String?
    var Ref             : String?
    var TrackID         : String?
    var TranID          : String?
    var amount          : String?
    var post_date       : String?
    var Result          : String?
    var pay             : String?
    var products        : [OrderProduct]?
    var delivery        : String?
    var lat             : Double?
    var lng             : Double?
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        client_id <- map["client_id"]
        odate <- map["odate"]
        company_id <- map["company_id"]
        status <- map["status"]
        otime <- map["otime"]
        neworder <- map["neworder"]
        payment <- map["payment"]
        address_id <- map["address_id"]
        count <- map["count"]
        sum <- map["sum"]
        area_name <- map["area_name"]
        company_name <- map["company_name"]
        company_mobile <- map["company_mobile"]
        client_name <- map["client_name"]
        client_email <- map["client_email"]
        client_mobile <- map["client_mobile"]
        gammaly <- map["gamm3"]
        deliveryCost <- map["delivery"]
        companies <- map["companies"]
        
        // New Edit
        prod_id             <- map["prod_id"]
        date                <- map["date"]
        order_id            <- map["order_id"]
        price               <- map["price"]
        gamm3               <- map["gamm3"]
        product_name        <- map["product_name"]
        photo               <- map["photo"]
        status_text         <- map["status_text"]
        deliv_text          <- map["deliv_text"]
        deliveryTime        <- map["deliveryTime"]
        client              <- map["client"]
        time_id             <- map["time_id"]
        time_txt            <- map["time_txt"]
        time_id_date        <- map["time_id_date"]
        address             <- map["address"]
        payment_id          <- map["payment_id"]
        Ref                 <- map["Ref"]
        TrackID             <- map["TrackID"]
        TranID              <- map["TranID"]
        amount              <- map["amount"]
        post_date           <- map["post_date"]
        Result              <- map["Result"]
        pay                 <- map["pay"]
        products            <- map["products"]
        lat                 <- map["lat"]
        lng                 <- map["lng"]
    }
}

class OrderProduct: BaseResponse{
    var id              : String?
    var prod_id         : String?
    var count           : String?
    var status          : String?
    var date            : String?
    var total_price     : String?
    var price           : String?
    var company_id      : String?
    var order_id        : String?
    var gamm3           : String?
    var product_name    : String?
    var photo           : String?
    var status_text     : String?
    var company_name    : String?
    var deliveryTime    : String?
    
    var options         : [ordersItemOptions]?
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        // New Edit
        id              <- map["id"]
        prod_id         <- map["prod_id"]
        count           <- map["count"]
        status          <- map["status"]
        date            <- map["date"]
        total_price     <- map["total_price"]
        price           <- map["price"]
        company_id      <- map["company_id"]
        order_id        <- map["order_id"]
        gamm3           <- map["gamm3"]
        product_name    <- map["product_name"]
        photo           <- map["photo"]
        status_text     <- map["status_text"]
        company_name    <- map["company_name"]
        deliveryTime    <- map["deliveryTime"]
        options         <- map["options"]
    }
}

class ordersItemOptions: BaseResponse{
    var option_name         : String?
    var option_value        : String?
    var option_price_txt    : String?
    var option_price        : String?
    
    required init?(map: Map) {
        super.init()
    }
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        option_name         <- map["option_name"]
        option_value        <- map["option_value"]
        option_price_txt    <- map["option_price_txt"]
        option_price        <- map["option_price"]
        
    }
}

class OrdersResponse: BaseResponse{
    
    var data: [Order]?
    
    required init?(map: Map) {
        super.init()
    }
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class OrderDetailsResponse: BaseResponse{
    
    var data: Order?
    
    required init?(map: Map) {
        super.init()
    }
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class addProductResponse: BaseResponse{
    
    var id: String = ""
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["data.id"]
    }
}
class BaseResponse: NSObject, Mappable
{

    //MARK:- Properties
    var result: String = ""
    var message: String = ""
    
    
    //MARK:- Intializer
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    //MARK:- Map
    func mapping(map: Map) {
        result <- map["result"]
        message <- map["message"]
    }
}



class CompanyDetailsForOrdersResponse: BaseResponse
{

    //MARK:- Properties
    var id: String?
    var name:String?
    var mobile:String?
    var delivery_price:String?

    
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        delivery_price <- map["data.delivery_price"]
        mobile <- map["mobile"]
    }
}
