//
//  RateResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//
import ObjectMapper

class GetContactsResponse: BaseResponse{
    
    var mail: String?
    var mobile:String?

    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        mail <- map["data.email"]
        mobile <- map["data.mobile"]

    }
    
}

class CommentsResponse: BaseResponse{
    
    var data: [Comments]?
    
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
        
    }
    
}

class Comments: BaseResponse{

    var id: String?
    var comment:String?
    var rate:String?
    var company_id:String?
    var client_id:String?
    var cdate:String?
    var name:String?
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        comment <- map["comment"]
        rate <- map["rate"]
        company_id <- map["company_id"]
        client_id <- map["client_id"]
        cdate <- map["cdate"]
        name <- map["name"]
    }
    
}

class Cats: BaseResponse{
    var id: String?
    var name:String?
    
    required init?(map: Map) {
        super.init()
    }

    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
    }
    
}
class CompanyDetailsResponse: BaseResponse
{
    
    //MARK:- Properties
    var id: String?
    var name:String?
    var feedData:String?
    var cats:[Cats]?
    var photo:String?
    var photo1:String?
    var mobile:String?
    var rdate:String?
    var pay:String?
    var pay_name:String?
    var delivery_time:String?
    var active:String?
    var delivery_price:String?
    var workFrom:String?
    var workTo:String?
    var clientId:String?
    var instgram:String?
    var catsName: String?
    var product_counts: Int?
    var rate: Int?
    var comments: Int?
    var account_name: String?
    var iban: String?
    var bank: String?
    var collect: String?
    var gamm3: String?
    
    var delivery_time_from:String?
    var delivery_time_to:String?
    
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["data.id"]
        name <- map["data.name"]
        feedData <- map["data.FeedData"]
        cats <- map["data.cats"]
        mobile <- map["data.mobile"]
        photo <- map["data.photo"]
        photo1 <- map["data.photo1"]
        rdate <- map["data.rdate"]
        pay <- map["data.pay"]
        pay_name <- map["data.pay_name"]
        delivery_time <- map["data.delivery_time"]
        delivery_price <- map["data.delivery_price"]
        active <- map["data.active"]
        workFrom <- map["data.work_from"]
        workTo <- map["data.work_to"]
        clientId <- map["data.client_id"]
        instgram <- map["data.instagram"]
        catsName <- map["data.cats_name"]
        product_counts <- map["data.product_counts"]
        rate <- map["data.rate"]
        comments <- map["data.comments"]
        account_name <- map["data.account_name"]
        iban <- map["data.iban"]
        bank <- map["data.bank"]
        collect <- map["data.collect"]
        gamm3 <- map["data.gamm3"]
        
        delivery_time_from <- map["data.delivery_time_from"]
        delivery_time_to <- map["data.delivery_time_to"]
    }
}
