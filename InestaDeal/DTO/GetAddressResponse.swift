//
//  RateResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//
import ObjectMapper

class GetAddress: BaseResponse
{
    
    //MARK:- Properties

    
    var id: String?
    var client_id: String?
    var name: String?
    var area: String?
    var street: String?
    var house: String?
    var mobile: String?
    var phone: String?
    var notes: String?
    var block: String?
    var gada: String?
    var floor: String?
    var room: String?
    var lat : String?
    var lng : String?
    var area_id : String?
    
    

    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        client_id <- map["client_id"]
        name <- map["name"]
        area <- map["area"]
        street <- map["street"]
        house <- map["house"]
        mobile <- map["mobile"]
        phone <- map["phone"]
        notes <- map["notes"]
        block <- map["block"]
        gada <- map["gada"]
        floor <- map["floor"]
        room <- map["room"]
        lat <- map["lat"]
        lng <- map["lng"]
        area_id <- map["area_id"]
        
        

    }
}

class GetAddressResponse: BaseResponse
{
    
    //MARK:- Properties
    
    var data: [GetAddress]?

    
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
      
        
        
        
    }
}
