//
//  CompanyCatsResponse.swift
//  InestaDeal
//
//  Created by Waleed on 4/18/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import Foundation
import ObjectMapper

class CatsProducts: BaseResponse{
    
    var id: String?
    var name: String?
    var photo: String?
    var price: String?
    var discount:String?
    var newPrice : Double?
    var favChecked : Int?
    
    var counts          :String?
    var companyId       :String?
    var deliveryPrice   :String?
    var gmmaly          :String?
    var freeDelivery    :Int?
    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        photo <- map["photo"]
        price <- map["price"]
        discount <- map["discount"]
        newPrice <- map["new_price"]
        favChecked <- map["favChecked"]
        
        counts          <- map["counts"]
        companyId       <- map["company_id"]
        deliveryPrice   <- map["delivery_price"]
        gmmaly          <- map["gamm3"]
        freeDelivery    <- map["free_delivery"]
    }
    
}

class CompanyCatsResponse: BaseResponse{
    
    var data: [CatsProducts]?
//    var productsListdata: [ProductDetailsResponse]? // For products list screen
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
//        productsListdata <- map["data"]
    }
    
}

