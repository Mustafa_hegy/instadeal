//
//  RateResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//
import ObjectMapper

class RelatedProduct: BaseResponse{
    
    //MARK:- Properties
    
    var id: String?
    var name: String?
    var FeedData: String?
    var cat_id: String?
    var price: String?
    var photo: String?
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        FeedData <- map["FeedData"]
        cat_id <- map["cat_id"]
        price <- map["price"]
        photo <- map["photo"]
        
    }
    
}
class RelatedProductsResponse: BaseResponse
{
    var data: [RelatedProduct]?
    
    required init?(map: Map) {
        super.init()
    }
    
    
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }

}
