//
//  RateResponse.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//
import ObjectMapper

class SlidersResponse: BaseResponse
{
   

    //MARK:- Properties
    var data: [SlidersResponse]?
    var id: String?
    var name:String?
    var photo:String?
    var link:String?
    var text:String?
    var mobile:String?
    var atype:String?
    var cat_id:String?
    var slider_type:String?

    
    required init?(map: Map) {
        super.init()
    }
    
    //MARK:- Map
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        photo <- map["photo"]
        link <- map["link"]
        text <- map["text2"]
        mobile <- map["mobile"]
        atype <- map["atype"]
        cat_id <- map["cat_id"]
        slider_type <- map["slider_type"]
        data <- map["data"]

    }
}
