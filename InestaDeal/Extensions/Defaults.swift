//
//  Defaults.swift
//  Tutoring
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//


import SwiftyUserDefaults



extension DefaultsKeys
{
    //MARK:- SignalR
    static let parentCategoryId = DefaultsKey<String>("parentCategoryId")
    static let subCategoryId = DefaultsKey<String>("subCategoryId")
    static let subCategoryName = DefaultsKey<String>("subCategoryName")
    static let selectedCatID = DefaultsKey<String>("selectedCatID")
    static let companyId = DefaultsKey<String>("companyId")
    static let deliveryFees = DefaultsKey<String>("deliveryFees")
    static let tagerCompanyId = DefaultsKey<String>("tagerCompanyId")
    static let orderId = DefaultsKey<String>("orderId")
    static let catId = DefaultsKey<String>("catId")
    static let compId = DefaultsKey<String>("compId")
    static let flag = DefaultsKey<Bool>("flag")
    static let linkPageFlag = DefaultsKey<Bool>("linkPageFlag")
    static let flagAddProd = DefaultsKey<Bool>("flagAddProd")
    static let dismissed = DefaultsKey<Bool>("dismissed")
    static let cartId = DefaultsKey<String>("cartId")
    
    static let userId = DefaultsKey<String>("userId")
    static let userName = DefaultsKey<String>("userName")
    static let userEmail = DefaultsKey<String>("userEmail")
    static let userMobile = DefaultsKey<String>("userMobile")
    
    static let userType = DefaultsKey<String>("userType")
    static let logged = DefaultsKey<Bool>("logged")
    static let uploadedProdId = DefaultsKey<String>("uploadedProdId")
    static let prodIdIndexPath = DefaultsKey<IndexPath>("prodIdIndexPath")
    static let prodId = DefaultsKey<String>("prodId")
    static let deliveryPrice = DefaultsKey<String>("deliveryPrice")
    static let numberOfProducts = DefaultsKey<String>("numberOfProducts")
    static let globalCompanyID = DefaultsKey<String>("globalCompanyID")
    static let tagerProdId = DefaultsKey<String>("tagerProdId")
    static let amountOfProducts = DefaultsKey<[String]>("amountOfProducts")
    static let productsIds = DefaultsKey<[String]>("productsIds")
    static let totalPrices = DefaultsKey<String>("totalPrices")
    static let totalPriceWithFees = DefaultsKey<Double>("totalPriceWithFees")
    static let catSelectedAddProduct = DefaultsKey<String>("catSelectedAddProduct")
    static let compSelectedAddProduct = DefaultsKey<String>("compSelectedAddProduct")
    static let selectedCatstring = DefaultsKey<String>("selectedCatstring")
    static let selectedCatIds = DefaultsKey<String>("selectedCatIds")
    static let selectedCompsString = DefaultsKey<String>("selectedCompsString")
    static let selectedCompsIds = DefaultsKey<String>("selectedCompsIds")
    static let search = DefaultsKey<Bool>("search")
    
    //address
    static let addressId = DefaultsKey<String>("addressId")
    static let addressName = DefaultsKey<String>("addressName")
    static let flatNum = DefaultsKey<String>("flatNum")
    static let floor = DefaultsKey<String>("floor")
    static let benayaNum = DefaultsKey<String>("benayaNum")
    static let street = DefaultsKey<String>("street")
    static let ket3a = DefaultsKey<String>("ket3a")
    static let gada = DefaultsKey<String>("gada")
    static let area = DefaultsKey<String>("area")
    
    // Area
    static let areaId = DefaultsKey<String>("areaId")
    static let areaName = DefaultsKey<String>("areaName")
    static let langId = DefaultsKey<String>("langId")
    
    // Product Options
    static let selectedOptionsArray = DefaultsKey<[String]>("selectedOptionsArray")
    
    // Area
    static let PNToken = DefaultsKey<String>("PNToken")
    

    
        
}
