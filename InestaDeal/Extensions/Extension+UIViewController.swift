//
//  Extension+UIViewController.swift
//  InestaDeal
//
//  Created by Waleed on 4/11/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

extension String {
    
    func convertHtmlToString() -> NSAttributedString? {
        
        
//        let myAttribute = [ NSAttributedStringKey.font: UIFont(name: "Chalkduster", size: 18.0)! ]
//        let myAttrString = NSAttributedString(string: self, attributes: myAttribute)
        
        // set attributed text on a UILabel
        
        
        let data = self.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        
        return attrStr
    }
}
    
    

extension UIViewController{
    
    func addNavBar(isBack: Bool){
        self.navigationController?.navigationBar.isHidden = true
        let navViewController = NavBarViewController()
        addChildViewController(navViewController)
        var height = 70
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
            case 1334:
                print("iPhone 6/6S/7/8")
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                if let _ = navViewController.parent as? PrdouctDetailsViewController {
                    
                }
                print("iPhone X/XS")
            case 1792:
                if let _ = navViewController.parent as? PrdouctDetailsViewController {
                    
                }
                print("iPhone XR")
            case 2688:
                if let _ = navViewController.parent as? PrdouctDetailsViewController {
                    
                }
                print("iPhone XS Max")
            default:
                print("Unknown Device")
            }
        }
        
        navViewController.view.frame = CGRect(x: 0, y: 0, width: Int(screenWidth), height: height)
        navViewController.backBtn.isHidden = !isBack
        navViewController.menuIcon.isHidden = isBack
//        if Defaults[.amountOfProducts].count == 0 {
//            navViewController.itemsCount = 0
//        }
//        else{
//            navViewController.numberOfProducts.text =  "\(Defaults[.amountOfProducts].count)"
//        }
        
        view.addSubview(navViewController.view)
        navViewController.view.tag = 999
        navViewController.didMove(toParentViewController: self)
    }
    
    func addNavBarNotification(isBack: Bool){
        self.navigationController?.navigationBar.isHidden = true
        let navViewController = NavBarViewController()
        addChildViewController(navViewController)
        navViewController.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 70)
        navViewController.backBtn.isHidden = !isBack
        navViewController.menuIcon.isHidden = isBack
        //        if Defaults[.amountOfProducts].count == 0 {
        //            navViewController.itemsCount = 0
        //        }
        //        else{
        //            navViewController.numberOfProducts.text =  "\(Defaults[.amountOfProducts].count)"
        //        }
        
        view.addSubview(navViewController.view)
        navViewController.view.tag = 999
        navViewController.didMove(toParentViewController: self)
    }
    
    func addNavBarCart(isBack: Bool = true ){
        self.navigationController?.navigationBar.isHidden = true
        let navViewController = NavBarCartViewController()
        addChildViewController(navViewController)
        navViewController.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 70)
        navViewController.backBtn.isHidden = !isBack
        navViewController.menuIcon.isHidden = isBack
        
        view.addSubview(navViewController.view)
        navViewController.view.tag = 999
        navViewController.didMove(toParentViewController: self)
    }
    
    func addNavBarFinishOrder(isBack: Bool = true ,showMenue:Bool = true){
        self.navigationController?.navigationBar.isHidden = true
        let navViewController = NavBarCartViewController()
        addChildViewController(navViewController)
        navViewController.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 70)
        navViewController.backBtn.isHidden = !isBack
        navViewController.menuIcon.isHidden = !showMenue
        
        view.addSubview(navViewController.view)
        navViewController.view.tag = 999
        navViewController.didMove(toParentViewController: self)
        
    }
    
    func addNavBarProductList(isBack: Bool = true ,showMenue:Bool = true){
        self.navigationController?.navigationBar.isHidden = true
        let navViewController = NavBarViewController()
        addChildViewController(navViewController)
        navViewController.view.frame = CGRect(x: 0, y: 0, width: Int(screenWidth), height: 70)

        navViewController.backBtn.isHidden = !isBack
        navViewController.menuIcon.isHidden = isBack
        
        navViewController.areaBtn.setTitle(Defaults[.areaName], for: .normal)
        navViewController.areaBtn.isHidden = false
        
        navViewController.appSloganImage.isHidden = true
        
        view.addSubview(navViewController.view)
        navViewController.view.tag = 999
        navViewController.didMove(toParentViewController: self)
    }
    
    func present(vc:UIViewController, style:String) {
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        view.window?.layer.add(transition, forKey: kCATransition)
        present(vc, animated: false, completion: nil)
    }
    
    func setStepperStatusForView(cell:TagerOrdersCells,status:String?,statusText:String?){
        /*
         if 1    = "قيد التنفيذ"
         if 2    = "تجهيز المنتج"
         if 3    = "خدمة التوصيل"
         if 4    = "تم التسليم"
         if 5    = "ملغي"
         if 6    = "ملغي"
         if 7    = "مسترجع"
         if 8    = "فاشل"
         if 9    = "مرفوض"
         if 10   = "ملغي من الادارة"
         */
        
        switch status {
        case "1":
            cell.statusIcon1.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon2.image          = #imageLiteral(resourceName: "uncheckedCircle")
            cell.statusIcon3.image          = #imageLiteral(resourceName: "uncheckedCircle")
            cell.statusIcon4.image          = #imageLiteral(resourceName: "uncheckedCircle")
            cell.statusTitleLable1.text     = statusText
            cell.statusTitleLable2.text     = ""
            cell.statusTitleLable3.text     = ""
            cell.statusTitleLable4.text     = ""
            
        case "2","5","6":
            cell.statusIcon1.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon2.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon3.image          = #imageLiteral(resourceName: "uncheckedCircle")
            cell.statusIcon4.image          = #imageLiteral(resourceName: "uncheckedCircle")
            cell.statusTitleLable1.text     = "inProgress".localized()
            cell.statusTitleLable2.text     = statusText
            cell.statusTitleLable3.text     = ""
            cell.statusTitleLable4.text     = ""
            
        case "3":
            cell.statusIcon1.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon2.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon3.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon4.image          = #imageLiteral(resourceName: "uncheckedCircle")
            cell.statusTitleLable1.text     = "inProgress".localized()
            cell.statusTitleLable2.text     = "prepare".localized()
            cell.statusTitleLable3.text     = statusText
            cell.statusTitleLable4.text     = ""
            
        case "4","8","9","10":
            cell.statusIcon1.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon2.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon3.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon4.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusTitleLable1.text     = "inProgress".localized()
            cell.statusTitleLable2.text     = "prepare".localized()
            cell.statusTitleLable3.text     = "deliveryService".localized()
            cell.statusTitleLable4.text     = statusText
            
        default:
            cell.statusIcon1.image          = #imageLiteral(resourceName: "checkedCircle.png")
            cell.statusIcon2.image          = #imageLiteral(resourceName: "uncheckedCircle")
            cell.statusIcon3.image          = #imageLiteral(resourceName: "uncheckedCircle")
            cell.statusIcon4.image          = #imageLiteral(resourceName: "uncheckedCircle")
            cell.statusTitleLable1.text     = "inProgress".localized()
            cell.statusTitleLable2.text     = ""
            cell.statusTitleLable3.text     = ""
            cell.statusTitleLable4.text     = ""
            
        }
    }
    
    func handleTabBarVisibility( hide : Bool ){
        tabBarController?.tabBar.isHidden = hide
//        UIView.transition(with: tabBarController!.tabBar, duration: 0.7, options: .transitionCrossDissolve, animations: nil)
    }
}

extension UIViewController
{
    //MARK:- Factory method
    class func instanceFromStoryboard<T:UIViewController>(storyboardName:String)->T? {
        let vc = viewControllerWithIdentifier(storyboardId: String(describing: classForCoder()), onStoryboardWithName:storyboardName) as? T
        return vc
    }
    
    
    //MARK:- init from xib
    static func instanceWithDefaultNib() -> Self {
        let className = String(describing: classForCoder())
        let bundle = Bundle(for: self as AnyClass)
        return self.init(nibName: className, bundle: bundle)
    }
    
    
    
    //MARK:- ViewController from storyboard
    class func viewControllerWithIdentifier(storyboardId:String, onStoryboardWithName name:String)->UIViewController{
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: storyboardId)
        return viewController
    }
    
    
    //MARK:- Show alert views
    func showThreeActionsAlert(title:String?,firstActionButtontitle:String?, secondActionButtontitle:String?,thirdActionButtontitle:String?,message:String?,onCancel:(()->Void)? = nil,secondActionClosure:@escaping ()->Void,thirdActionClosure: @escaping ()->Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let alertSecondAction = UIAlertAction(title:secondActionButtontitle, style: .default) { (action) in
            secondActionClosure()
        }
        
        alertController.addAction(alertSecondAction)
        
        let alertThirdAction = UIAlertAction(title:thirdActionButtontitle, style: .default) { (action) in
            thirdActionClosure()
        }
        
        alertController.addAction(alertThirdAction)
        
        
        let cancelAction = UIAlertAction(title:firstActionButtontitle, style: .default) { (action) in
            onCancel?()
        }
        
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true) {
        }
    }
    
    func showTwoActionsAlert(title:String?,firstActionButtontitle:String?, secondActionButtontitle:String?,mesage:String?,onCancel:(()->Void)?
        = nil,actionClosure:@escaping ()->Void) {
        
        let alertController = UIAlertController(title: title, message: mesage, preferredStyle: .alert)
        
        
        let alertAction = UIAlertAction(title:firstActionButtontitle, style: .default) { (action) in
            actionClosure()
        }
        
        alertController.addAction(alertAction)
        
        
        let cancelAction = UIAlertAction(title:secondActionButtontitle, style: .default) { (action) in
            onCancel?()
        }
        
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true) {
        }
    }
    
    func showDefaultAlert(title:String?,message:String?, actionBlock:(()->Void)? = nil){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel
        ) { (action) in
            alertController.dismiss(animated: true){
            }
            actionBlock?()
        }
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    
    func showSingleInputForm(title:String?, firstActionTitle:String? , secondActionTitle:String?,placeHolder:String?, message:String? ,actionBlock:(()->Void)?) {
        
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let firstAction = UIAlertAction(title: firstActionTitle, style: .default) { [weak alertVC] _ in
            if let alert = alertVC {
                let _ = alert.textFields![0] as UITextField
            }
        }
        
        firstAction.isEnabled = false
        
        let cancelAction = UIAlertAction(title: secondActionTitle, style: .default) { _ in }
        
        alertVC.addTextField { textField in
            textField.placeholder = placeHolder
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main) { notification in
                firstAction.isEnabled = textField.text != ""
            }
        }
        
        alertVC.addAction(cancelAction)
        alertVC.addAction(firstAction)
        
        
        self.present(alertVC, animated: true) {}
    }
}

extension UIButton {
    func alignVertical(spacing: CGFloat = 6.0) {
        guard let imageSize = self.imageView?.image?.size,
            let text = self.titleLabel?.text,
            let font = self.titleLabel?.font
            else { return }
        self.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0.0)
        let labelString = NSString(string: text)
        let titleSize = labelString.size(withAttributes: [NSAttributedStringKey.font: font])
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.contentEdgeInsets = UIEdgeInsets(top: edgeOffset, left: 0.0, bottom: edgeOffset, right: 0.0)
    }
}

class TextField: UITextField ,UITextFieldDelegate{
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }

}
