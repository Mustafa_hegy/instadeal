//
//  HomeModelView.swift
//  InestaDeal
//
//  Created by Waleed on 4/12/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyUserDefaults

class TagerCompanyProdyctModelView{
    
    unowned var scene: TagerCompanyProductsViewController
    init(scene: TagerCompanyProductsViewController) {
        
        self.scene = scene
    }
    
    func getCompanyCommentsApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getCompanyComments/\(Defaults[.companyId] )?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CommentsResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
    func getCompanyDetailsApi(successClosure: @escaping SuccessClosure){
        let params: Parameters = [
            "area": "\(Defaults[.areaId])"
        ]
//        Alamofire.request("https://www.instadeal.co/home/getCompany/\(Defaults[.companyId])/0", method: .get, parameters: params).responseJSON
        Alamofire.request("https://www.instadeal.co/home/getCompany/\(Defaults[.companyId])?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get, parameters: params).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyDetailsResponse(JSON: data)
//                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
    func deleteProductApi( successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/deleteProduct/\(Defaults[.tagerProdId] )?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
               
                   
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(json)
                
            }
        }
        
    }
    
    func getCatsProductApi(successClosure: @escaping SuccessClosure){
        let catId = scene.catId?.replacingOccurrences(of: " ", with: "")
        Alamofire.request("https://www.instadeal.co/home/getCompanyProducts/\(Defaults[.companyId])?cat=\(catId ?? "")&lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyCatsResponse(JSON: data)
//                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
            
        }
        
    }
    
    func getCatsProductApiFirst(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getCompanyProducts/\(Defaults[.companyId])?cat=0&lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyCatsResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
}
