//
//  HomeModelView.swift
//  InestaDeal
//
//  Created by Waleed on 4/12/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyUserDefaults


class companyTableModelView{
    
    unowned var scene: CompanyViewController
    init(scene: CompanyViewController) {
        
        self.scene = scene
    }
    
    func getCompaniesApi(successClosure: @escaping SuccessClosure){
        let params: Parameters = [
            "lang": "\(Defaults[.langId])" ,
            "area": "\(Defaults[.areaId])"
        ]
        
        print("from getCompaniesApi \(Defaults[.subCategoryId])")
        
        Alamofire.request("https://www.instadeal.co/home/getCompanies/\(Defaults[.subCategoryId])?lang=\(Defaults[.langId])", method: .get, parameters: params).responseJSON { response in
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyResponse(JSON: data)
                    
                    
                   successClosure(dataModel)
                }
            }
        }
    }
    
    func getUserCompaniesApi(successClosure: @escaping SuccessClosure){
        let params: Parameters = [
            "lang": "\(Defaults[.langId])" ,
            "area": "\(Defaults[.areaId])"
        ]
        
        print("from getUserCompaniesApi \(Defaults[.userId])")
        Alamofire.request("https://www.instadeal.co/home/getUserCompanies/\(Defaults[.userId])?lang=\(Defaults[.langId])", method: .get, parameters: params).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyResponse(JSON: data)
                    
                    
                    successClosure(dataModel)
                }
            }
        }
    }

}
