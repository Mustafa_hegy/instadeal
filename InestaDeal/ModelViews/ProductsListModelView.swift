//
//  ProductsListModelView.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 8/5/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyUserDefaults

class ProductsListModelView {

    unowned var scene: ProductsListViewController
    init(scene: ProductsListViewController) {
        
        self.scene = scene
    }
    
    func getCatsProductsApi(limit: Int, start: Int , filterParameters:String! , successClosure: @escaping SuccessClosure){
        
//        var params: Parameters = [
//            "lang": "\(Defaults[.langId])"
//        ]
        var extraParameters:String
        if filterParameters != "" {
            extraParameters = "&" + filterParameters
        } else {
            extraParameters = ""
        }
        print("from getCatProductsApi : https://www.instadeal.co/home/getCatProducts/\(Defaults[.subCategoryId])/\(limit)/\(start)?lang=\(Defaults[.langId])\(extraParameters)")
        
        Alamofire.request("https://www.instadeal.co/home/getCatProducts/\(Defaults[.subCategoryId])/\(limit)/\(start)?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])\(extraParameters)", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyCatsResponse(JSON: data)
                    successClosure(dataModel)
                }else {
                    successClosure([])
                }
            }else {
                successClosure([])
            }
        }
    }
    
    func getGamm3SubCatsProductsApi(successClosure: @escaping SuccessClosure){
        
        print("from getGamm3SubCatsProductsApi : https://www.instadeal.co/home/getCatProducts/\(Defaults[.subCategoryId])?ga=1&lang=\(Defaults[.langId])&area=\(Defaults[.areaId])")
        Alamofire.request("https://www.instadeal.co/home/getCatProducts/\(Defaults[.subCategoryId])?ga=1&lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyCatsResponse(JSON: data)
                    successClosure(dataModel)
                }else {
                    successClosure([])
                }
            }else {
                successClosure([])
            }
        }
    }
    
    func getGamm3ParentCatProductsApi(successClosure: @escaping SuccessClosure){
        
        print("from getGamm3ParentCatProductsApi : https://www.instadeal.co/home/getParentCatProducts/\(scene.parentCategoryId ?? "0")?ga=1&lang=\(Defaults[.langId])&area=\(Defaults[.areaId])")
        Alamofire.request("https://www.instadeal.co/home/getParentCatProducts/\(scene.parentCategoryId ?? "0")?ga=1&lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyCatsResponse(JSON: data)
                    successClosure(dataModel)
                }else {
                    successClosure([])
                }
            }else {
                successClosure([])
            }
        }
    }
    
    func getParentCatProductsApi(limit: Int, start: Int , filterParameters:String! , successClosure: @escaping SuccessClosure){
        
        //        var params: Parameters = [
        //            "lang": "\(Defaults[.langId])"
        //        ]
        var extraParameters:String
        if filterParameters != "" {
            extraParameters = "&" + filterParameters
        } else {
            extraParameters = ""
        }
        print("from getCatProductsApi : https://www.instadeal.co/home/getParentCatProducts/\(scene.parentCategoryId ?? "")/\(limit)/\(start)?lang=\(Defaults[.langId])\(extraParameters)")
        
        Alamofire.request("https://www.instadeal.co/home/getParentCatProducts/\(scene.parentCategoryId ?? "")/\(limit)/\(start)?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])\(extraParameters)", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyCatsResponse(JSON: data)
                    successClosure(dataModel)
                }else {
                    successClosure([])
                }
            }else {
                successClosure([])
            }
        }
    }
    
    func getSubCategoriesApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getCats/\(scene.parentCategoryId ?? "")?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CategoriesResponse(JSON: data)
                    
                    successClosure(dataModel)
                }
            }
        }
    }
}

class ProductModelView {
    
    // Fevorite Action
    func addToFavApi(successClosure: @escaping SuccessClosure){
        let params: Parameters = [
            "client_id": Defaults[.userId] ,
            "prod_id": Defaults[.prodId] ,
            "lang": "\(Defaults[.langId])"
        ]
        
        Alamofire.request("https://www.instadeal.co/home/addFavorite?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
        }
    }
    
    func addToCartApi(price : String , count : Int ,successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "client_id": Defaults[.userId] ,
            "prod_id": Defaults[.prodId] ,
            "count": count ,
            "price": price ,
            "options":Defaults[.selectedOptionsArray]
        ]
        
        Alamofire.request("https://www.instadeal.co/home/addCart?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
}

