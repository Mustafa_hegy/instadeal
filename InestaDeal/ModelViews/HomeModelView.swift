//
//  HomeModelView.swift
//  InestaDeal
//
//  Created by Waleed on 4/12/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyUserDefaults

typealias SuccessClosure = (Any)->Void
typealias SuccessClosureUpload = ()->Void
typealias FailureClosure = (String)->Void
class HomeModelView{
    
    unowned var scene: UIViewController
    init(scene: UIViewController) {
        
        self.scene = scene
    }
    
    func getCategoriesApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "lang": "\(Defaults[.langId])"
        ]
        Alamofire.request("https://www.instadeal.co/home/getCatsWithSub", method: .get, parameters: params).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CategoriesResponse(JSON: data)
                    
                   successClosure(dataModel)
                }
            }
        }
    }
    
    func getHomeSliderApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "lang": "\(Defaults[.langId])"
        ]
        Alamofire.request("https://www.instadeal.co/home/getAdsSlider", method: .get, parameters: params).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = SlidersResponse(JSON: data)
                    
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
    func getHomeDashboardApi(successClosure: @escaping SuccessClosure){
        
        print("getHomeDashboardApi: https://www.instadeal.co/home/getHome/0?lang=\(Defaults[.langId])&user_id=\(Defaults[.userId])")
        Alamofire.request("https://www.instadeal.co/home/getHome/0?lang=\(Defaults[.langId])&user_id=\(Defaults[.userId])&area=\(Defaults[.areaId])", method: .get ).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = DashboardResponse(JSON: data)
                    
                    successClosure(dataModel)
                }
            }
            else{
                successClosure("errorHint".localized())
            }
        }
    }
    
    func getSelectedProductsApi(limit: Int, start: Int , successClosure: @escaping SuccessClosure){
        
        print("from getSelectedProducts : https://www.instadeal.co/home/getSelectedProducts/\(limit)/\(start)?lang=\(Defaults[.langId])&user=\(Defaults[.userId])&area=\(Defaults[.areaId])")
        
        Alamofire.request("https://www.instadeal.co/home/getSelectedProducts/\(limit)/\(start)?lang=\(Defaults[.langId])&user=\(Defaults[.userId])&area=\(Defaults[.areaId])", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyCatsResponse(JSON: data)
                    successClosure(dataModel)
                }else {
                    successClosure([])
                }
            }else {
                successClosure([])
            }
        }
    }
}
