//
//  HomeModelView.swift
//  InestaDeal
//
//  Created by Waleed on 4/12/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyUserDefaults


class SubCategoriesModelView{
    
    unowned var scene: SubCategoriesViewController
    init(scene: SubCategoriesViewController) {
        
        self.scene = scene
    }
    
    func getSubCategoriesApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getCats/\(scene.parentCategoryId ?? "")&lang=\(Defaults[.langId]))", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CategoriesResponse(JSON: data)
                    
                   successClosure(dataModel)
                }
            }
        }
    }
    
    

}
