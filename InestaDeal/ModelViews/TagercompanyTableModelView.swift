//
//  HomeModelView.swift
//  InestaDeal
//
//  Created by Waleed on 4/12/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyUserDefaults


class TagercompanyTableModelView{
    
    unowned var scene: TagerCompanyViewController
    init(scene: TagerCompanyViewController) {
        
        self.scene = scene
    }
    
    func getCompaniesApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "lang": "\(Defaults[.langId])" ,
            "area": "\(Defaults[.areaId])"
        ]
        
        Alamofire.request("https://www.instadeal.co/home/getCompanies/\(Defaults[.subCategoryId])?lang=\(Defaults[.langId])", method: .get , parameters: params).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyResponse(JSON: data)
                    
                    
                   successClosure(dataModel)
                }
            }
        }
    }
    
    func getUserCompaniesApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "lang": "\(Defaults[.langId])" ,
            "area": "\(Defaults[.areaId])"
        ]
        
        print("getUserCompaniesApi: \("https://www.instadeal.co/home/getUserCompanies/\(Defaults[.userId])")")
        
        Alamofire.request("https://www.instadeal.co/home/getUserCompanies/\(Defaults[.userId])?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get , parameters: params).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyResponse(JSON: data)
                    
                    
                    successClosure(dataModel)
                }
            }
        }
    }

    
    

}
