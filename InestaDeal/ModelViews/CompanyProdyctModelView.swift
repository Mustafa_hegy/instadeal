//
//  HomeModelView.swift
//  InestaDeal
//
//  Created by Waleed on 4/12/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyUserDefaults

class CompanyProductModelView{
    
    unowned var scene: CompnyProductsViewController
    init(scene: CompnyProductsViewController) {
        
        self.scene = scene
    }
    

    func getCompanyCommentsApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getCompanyComments/\(Defaults[.companyId] )?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CommentsResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
    func getCompanyDetailsApi(successClosure: @escaping SuccessClosure){
        let params: Parameters = [
            "area": "\(Defaults[.areaId])"
        ]
        print("companyID:-\(Defaults[.companyId])")
//        Alamofire.request("https://www.instadeal.co/home/getCompany/\(Defaults[.companyId])/0", method: .get, parameters: params).responseJSON
        Alamofire.request("https://www.instadeal.co/home/getCompany/\(Defaults[.companyId])?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get, parameters: params).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyDetailsResponse(JSON: data)
//                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
    func getCatsProductApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getCompanyProducts/\(Defaults[.companyId])?cat=\(Defaults[.subCategoryId])&lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyCatsResponse(JSON: data)
//                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
    func getCatsProductApiFirst(successClosure: @escaping SuccessClosure){
        Alamofire.request("https://www.instadeal.co/home/getCompanyProducts/\(Defaults[.companyId])?cat=\(Defaults[.subCategoryId])&lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyCatsResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
}
