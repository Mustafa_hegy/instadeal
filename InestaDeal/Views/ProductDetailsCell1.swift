//
//  ProductDetailsCell1.swift
//  InestaDeal
//
//  Created by Waleed on 4/27/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit

class ProductDetailsCell2: UITableViewCell {
    
    @IBOutlet weak var maxAmount: UILabel!
    @IBOutlet weak var amountOfProductsLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class ProductDetailsCell1: UITableViewCell {

    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productDescLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
