//
//  MenuCell.swift
//  InestaDeal
//
//  Created by Waleed on 5/11/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit

class SelectCategoryCell: UITableViewCell{
    
    @IBOutlet weak var categoryLbl: UILabel!
    
    @IBOutlet weak var imgIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        self.accessoryType = .none
    }
}

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellIconImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class deliveryTimeCell: UITableViewCell {
    
    @IBOutlet weak var cellLabel    : UILabel!
    @IBOutlet weak var cellView     : UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class areaCell: UITableViewCell{
    
    @IBOutlet weak var titleLbl     : UILabel!
    @IBOutlet weak var imgIcon      : UIImageView!
    @IBOutlet weak var cellButton   : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        //        self.accessoryType = .disclosureIndicator
    }
}

protocol TextFieldInTableViewCellDelegate: class {
    func textField(editingDidEndIn cell:areaPriceCell)
}

class areaPriceCell: UITableViewCell{
    
    var delegate: TextFieldInTableViewCellDelegate?
    
    @IBOutlet weak var titleLbl     : UILabel!
    @IBOutlet var cellInputField    : UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cellInputField.addTarget(self, action: #selector(didEndChangeTextFieldValue(sender:)), for: UIControlEvents.editingDidEnd)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func prepareForReuse() {
//        self.accessoryType = .disclosureIndicator
    }
    
    @objc func didEndChangeTextFieldValue(sender: AnyObject?) {
        self.delegate?.textField(editingDidEndIn: self)
    }
}
