//
//  MyAdressTableViewCell.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/14/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class MyAdressTableViewCell: UITableViewCell {
    @IBOutlet weak var addressName : UILabel!
    @IBOutlet weak var addressDescription : UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var selectAddressButton: UIButton!
    
    var editTapped: (() -> Void)?
    var deleteTapped: (() -> Void)?
    var deliveryTapped: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(_ address: GetAddress) {
        deleteButton.setTitle("delete".localized(), for: .normal)
        editButton.setTitle("edit".localized() , for: .normal)
        selectAddressButton.setTitle("deliverToThisAddress".localized() , for: .normal)
        
        addressName.text = address.name ?? ""
        let name = "" + (address.name ?? "")
        let house = "\("house".localized()):" + (address.house ?? "-")
        //let block = "" + (addressModel?.data![indexPath.row].block ?? "")
        let street = "\("street".localized()):" + (address.street?.replacingOccurrences(of: ",", with: " ") ?? "")
        let area  = "\("area".localized()):" + (address.area ?? "-")
        let gada = "\("gada".localized()):" + (address.gada ?? "-")
        let floor = "\("floor".localized()):" + (address.floor ?? "-")
        //let notes = "" + (addressModel?.data![indexPath.row].notes ?? "")
        let room = "\("room".localized()):" + (address.room ?? "-")
        let mobile = "\("recieverPhoneNumber".localized()):" + (address.mobile ?? "-")
        addressDescription.text = "\("address".localized()):\(name) \(area) \(street) \(house) \(floor) \(gada) \(room) \(mobile)"
        
        if Defaults[.langId] == "1" {
            addressName.textAlignment           = .left
            addressDescription.textAlignment    = .left
        } else if Defaults[.langId] == "2" {
            addressName.textAlignment           = .right
            addressDescription.textAlignment    = .right
        }
    }
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        editTapped?()
    }
    
    @IBAction func deleteButtonTapped(_ sender:UIButton) {
        deleteTapped?()
    }
    @IBAction func deliveyButtonTapped(_ sender:UIButton){
        deliveryTapped?()
    }

}
