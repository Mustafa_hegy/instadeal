//
//  TagerOrdersCells.swift
//  InestaDeal
//
//  Created by Waleed on 5/17/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class TagerOrdersCells: UITableViewCell {
    var moveToOrderDetails : (()->())?
    var cancelOrderProduct : (()->())?
    
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var statusIcon1: UIImageView!
    @IBOutlet weak var statusIcon2: UIImageView!
    @IBOutlet weak var statusIcon3: UIImageView!
    @IBOutlet weak var statusIcon4: UIImageView!
    
    @IBOutlet weak var statusTitleLable1: UILabel!
    @IBOutlet weak var statusTitleLable2: UILabel!
    @IBOutlet weak var statusTitleLable3: UILabel!
    @IBOutlet weak var statusTitleLable4: UILabel!
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var currentStatusLable: UILabel!
    @IBOutlet weak var quantityLable: UILabel!
    @IBOutlet weak var deliveryTime: UILabel!
    @IBOutlet weak var deliveryBy: UILabel!
    
    @IBOutlet weak var showOptionsLbl: UILabel!
    @IBOutlet weak var gammalyView: UIView!
    @IBOutlet weak var deliverDoneView: UIView!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var deliveryCost: UILabel!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var btnChangeStatus: UIButton!
    @IBOutlet weak var headTitlesLbl: UILabel!
    @IBOutlet weak var qtyTitleLbl: UILabel!
    @IBOutlet weak var sellerTitleLBl: UILabel!
    @IBOutlet weak var deliverByTitleLbl: UILabel!
    @IBOutlet weak var deliverTimeTitleLbl: UILabel!
    
    @IBOutlet weak var itemOptionTitle: UILabel!
    @IBOutlet weak var itemOptionValue: UILabel!
    @IBOutlet weak var showOrderDetailsLabel: UILabel!
    
    @IBOutlet weak var currentStatusTitleLabel: UILabel!
    @IBOutlet weak var quantityTitleLabel: UILabel!
    
    @IBAction func didSelectMoveToOrderDetails(_ sender:Any){
        moveToOrderDetails! ()
    }
    
    @IBAction func changeStatusAction(_ sender: Any) {
        cancelOrderProduct! ()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code        
        localizeUI()
    }
    
    func localizeUI(){
        btnChangeStatus?.setTitle("cancelOrder".localized(), for: .normal)
        statusTitleLable1.text          = "inProgress".localized()
        statusTitleLable2.text          = "prepare".localized()
        statusTitleLable3.text          = "deliveryService".localized()
        statusTitleLable4.text          = "delivered".localized()
        
        quantityTitleLabel?.text         = "quantity".localized()
        currentStatusTitleLabel?.text    = "\("status".localized())\n\("price".localized()) \n\("deliverBy".localized())\n\("deliveryTime".localized())"
        showOrderDetailsLabel?.text      = "showOrderDetails".localized()
        
        if Defaults[.langId] == "1" {
            quantityTitleLabel?.textAlignment        = .left
            currentStatusTitleLabel?.textAlignment   = .left
            showOrderDetailsLabel?.textAlignment     = .left
            sellerTitleLBl?.textAlignment            = .left
            itemTitle.textAlignment                 = .left
            currentStatusLable.textAlignment        = .left
            quantityLable.textAlignment             = .left
            
            companyName?.textAlignment             = .left
            headTitlesLbl?.textAlignment             = .left
            qtyTitleLbl?.textAlignment             = .left
            showOptionsLbl?.textAlignment             = .left
            
        } else if Defaults[.langId] == "2" {
            quantityTitleLabel?.textAlignment        = .right
            currentStatusTitleLabel?.textAlignment   = .right
            showOrderDetailsLabel?.textAlignment     = .right
            sellerTitleLBl?.textAlignment           = .right
            itemTitle.textAlignment                 = .right
            currentStatusLable.textAlignment        = .right
            quantityLable.textAlignment             = .right
            
            companyName?.textAlignment             = .right
            headTitlesLbl?.textAlignment             = .right
            qtyTitleLbl?.textAlignment             = .right
            showOptionsLbl?.textAlignment             = .right
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
