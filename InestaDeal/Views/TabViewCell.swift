//
//  TabViewCell.swift
//  InestaDeal
//
//  Created by Waleed on 4/16/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit

class TabViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellLabel: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    
}
