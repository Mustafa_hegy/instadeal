//
//  tagerCell0.swift
//  InestaDeal
//
//  Created by Waleed on 4/20/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Cosmos
import ImageSlideshow

class ImageSliderCell: UITableViewCell{
    
    @IBOutlet weak var imgsNumber: UILabel!
    @IBOutlet weak var slideShow: ImageSlideshow!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func prepareForReuse() {
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func prepareSlider(){
        slideShow.pageControl.removeFromSuperview()
        slideShow.contentScaleMode = .scaleAspectFit
        slideShow.scrollView.isPagingEnabled = false
        slideShow.scrollView.isScrollEnabled = false
        slideShow.circular = false
        slideShow.setCurrentPage(0, animated: true)
    }
}

class ProdNameCell: UITableViewCell {
    
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var prodNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}

class NotificationsCell: UITableViewCell {
    

    @IBOutlet weak var notificationBody: UILabel!
    @IBOutlet weak var notificationDate: UILabel!
    @IBOutlet weak var notificationName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func prepareForReuse() {
        notificationBody.text = ""
        self.layoutIfNeeded()
        self.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var unitPrice: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var cartAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}

class tagerCellEvaluation: UITableViewCell {
    
    @IBOutlet weak var rateView: CosmosView!
    
    @IBOutlet weak var commect: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}

class tagerCell1: UITableViewCell {
    
    @IBOutlet weak var cell2HeadLbl: UILabel!
    @IBOutlet weak var cell2BodyLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class tagerCell0: UITableViewCell {

    @IBOutlet weak var descBodyLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
