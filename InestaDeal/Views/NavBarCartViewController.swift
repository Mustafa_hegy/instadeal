//
//  NavBarViewController.swift
//  InestaDeal
//
//  Created by Waleed on 4/11/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Alamofire
let notificationName = Notification.Name("editBtn")
class NavBarCartViewController: UIViewController {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var menuIcon: UIButton!
    var itemsCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if Defaults[.langId] == "1" {
            backBtn.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editBtnAction(_ sender: UIButton ) {
        NotificationCenter.default.post(name: notificationName, object: nil)
        
    }
    @IBAction func menuIconAction(_ sender: UIButton) {
        
    }

    @IBAction func bacBtnAction(_ sender: UIButton) {
        if let vc = parent as? AreasListViewController {
            if parent?.presentingViewController?.presentedViewController == vc && vc.nextViewFlag == "productsList" {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        } else {
            self.navigationController?.popViewController()
            return
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
