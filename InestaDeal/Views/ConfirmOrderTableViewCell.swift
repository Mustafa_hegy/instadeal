//
//  ConfirmOrderTableViewCell.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/26/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyUserDefaults

class ConfirmOrderTableViewCell: UITableViewCell {
    @IBOutlet weak var itemImage : UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemCount: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var itemOptionTitle: UILabel!
    @IBOutlet weak var itemOptionValue: UILabel!
    @IBOutlet weak var quantityTitleLabel: UILabel!
    @IBOutlet weak var priceTitleLabel: UILabel!
    @IBOutlet weak var totalTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(item: Product){
        quantityTitleLabel.text = "quantity".localized()
        priceTitleLabel.text    = "price".localized()
        totalTitleLabel.text    = "total".localized()
        
        itemCount.text = item.count
        itemPrice.text = "\(Double(item.price) ?? 0.0)"
        totalPrice.text = "\(Double(item.totalPrice) ?? 0.0)"
        itemName.text = item.prod
        itemImage.sd_setImage(with: URL(string: item.photo), placeholderImage: #imageLiteral(resourceName: "iconApp"))
        
        if !(item.options!.isEmpty) {
            for option in item.options! {
                itemOptionTitle.text! += "\(itemOptionTitle.text!.isEmpty ? "" : "\n")\(option.option_name) : \(option.option_value)"
                itemOptionValue.text! += "\(itemOptionValue.text!.isEmpty ? "" : "\n")\(option.option_price_txt)"
                
                if Defaults[.langId] == "1" {
                    itemOptionTitle.textAlignment     = .left
                    itemOptionValue.textAlignment     = .right
                } else if Defaults[.langId] == "2" {
                    itemOptionTitle.textAlignment     = .right
                    itemOptionValue.textAlignment     = .left
                }
            }
        }
    }
    
}
