//
//  CartTagerTableViewCell.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/21/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyUserDefaults


class CartTagerTableViewCell: UITableViewCell{
    
    var productSelection    : (()->())?
    var productDeletion     : (()->())?
    
    @IBOutlet weak var singleItemButton : UIButton!
    @IBOutlet weak var itemImage : UIImageView!
    @IBOutlet weak var itemName : UILabel!
    @IBOutlet weak var itemPrice : UILabel!
    @IBOutlet weak var countOfItemsLable: UILabel!
    @IBOutlet weak var itemOptionTitle: UILabel!
    @IBOutlet weak var itemOptionValue: UILabel!
    
    var productSelected:Bool!
    
    var itemSelected: ((_ itemSelected: Bool)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configure(item: Product){
        itemImage.sd_setImage(with: URL(string: item.photo), placeholderImage: #imageLiteral(resourceName: "iconApp"))
        itemName.text = item.prod
        countOfItemsLable.text = "\("quantity".localized()): \(item.count)"
        itemPrice.text = "\(Double(item.price) ?? 0.0) \("currency".localized())"
        
        if !(item.options!.isEmpty) {
            for option in item.options! {
                itemOptionTitle.text! += "\(itemOptionTitle.text!.isEmpty ? "" : "\n")\(option.option_name) : \(option.option_value)"
                itemOptionValue.text! += "\(itemOptionValue.text!.isEmpty ? "" : "\n")\(option.option_price_txt)"
                
                if Defaults[.langId] == "1" {
                    itemOptionTitle.textAlignment     = .left
                    itemOptionValue.textAlignment     = .right
                } else if Defaults[.langId] == "2" {
                    itemOptionTitle.textAlignment     = .right
                    itemOptionValue.textAlignment     = .left
                }
            }
        }
        
    }
    
    @IBAction func itemSelectedButtonTapped(_ sender: UIButton){
        productSelection! ()
        /*
        if !productSelected{
            itemSelected?(true)
            //productSelected = false
        } else {
            itemSelected?(false)
            //productSelected = true
        }
        */
    }
    
    @IBAction func itemDeleteButtonTapped(_ sender: Any) {
        productDeletion! ()
    }
    
}
