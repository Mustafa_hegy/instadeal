//
//  CompanyTableCell.swift
//  InestaDeal
//
//  Created by Waleed on 4/13/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit

class OffersTableCell: UITableViewCell{
    
    @IBOutlet weak var offersImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

class CompanyTableCell: UITableViewCell {

    @IBOutlet weak var bottomDeliveryTimeLbl: UILabel!
    @IBOutlet weak var bottomDeliveryLbl: UILabel!
    @IBOutlet weak var bottomCompanyName: UILabel!
    
    
    @IBOutlet weak var topDeliveryTimeLbl:UILabel!
    @IBOutlet weak var topDeliveryLbl:UILabel!
    @IBOutlet weak var topCompanyName:UILabel!

    
    
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var mainPhoto: UIImageView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var gmmalyView: UIView!
    @IBOutlet weak var mainView : UIView!
    var gradient: CAGradientLayer?
    override func awakeFromNib() {
        super.awakeFromNib()
        mainPhoto.roundCorners(corners: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner], radius: 25.0)
        mainView.roundCorners(corners: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner], radius: 25.0)
        topView.roundCorners(corners: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner], radius: 25.0)
        setImageShadowLayer()
        // Initialization code
    }
    func setImageShadowLayer(){
        let view = UIView(frame: mainPhoto.frame)
        
         gradient = CAGradientLayer()
        
        gradient?.frame = CGRect(x: 0, y: 0, width: self.width * 2, height: self.height)
        
        gradient?.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        
        gradient?.locations = [0.0, 1.0]
        
        view.layer.insertSublayer(gradient!, at: 0)
        
        mainPhoto.addSubview(view)
        view.alpha = 0.5
        mainPhoto.bringSubview(toFront: view)
        
    }

    override func layoutSubviews() {
        self.layoutIfNeeded()
       
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
public extension UIView {
    func roundCorners(corners: CACornerMask, radius: CGFloat) {
        layer.maskedCorners = corners
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    func setShadow() { // targetView:UIView
        layer.masksToBounds      = false
        layer.shadowColor        = UIColor.black.cgColor
        layer.shadowOpacity      = 0.65
        layer.shadowOffset       = .zero
        layer.shadowRadius       = 8
//        layer.shadowPath         = UIBezierPath(rect: layer.bounds).cgPath
        layer.shouldRasterize    = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}
