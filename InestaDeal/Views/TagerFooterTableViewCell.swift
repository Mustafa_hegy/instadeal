//
//  TagerFooterTableViewCell.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/23/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class TagerFooterTableViewCell: UITableViewCell {
    @IBOutlet weak var sumOfProductsLabel: UILabel!
    @IBOutlet weak var tagerDeliveryCostLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var sumTitleLabel: UILabel!
    @IBOutlet weak var totalTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    var sumOfProducts = 0.0
    var total = 0.0
    func configure(items:[Product], gammaly: Int){
        localizeUI()
        
        for item in items{
            sumOfProducts += Double(item.totalPrice)!
            tagerDeliveryCostLabel.text = "\(Double(item.companyDeliveryPrice)!) \("currency".localized())"
            
            // Add item option price ====>> New Code
            if !(item.options!.isEmpty) {
                for option in item.options! {
                    let optionPrice = Double(option.option_price)!
                    let itemCount   = Double(item.count)!
                    sumOfProducts += optionPrice * itemCount
                }
            }
            /* ***** */
            
        }
        sumOfProductsLabel.text = "\(sumOfProducts) \("currency".localized())"
        totalLabel.text = "\(sumOfProducts + Double(items[0].companyDeliveryPrice)!) \("currency".localized())"
        if gammaly == 1 {
            tagerDeliveryCostLabel.isHidden = true
            deliveryLabel.isHidden = true
            totalLabel.text = "\(sumOfProducts) \("currency".localized())"
        }
    }
    
    func localizeUI(){
        deliveryLabel.text      = "deliveryCost".localized()
        sumTitleLabel.text      = "sum".localized()
        totalTitleLabel.text    = "total".localized()
        
        if Defaults[.langId] == "1" {
            deliveryLabel.textAlignment     = .left
            sumTitleLabel.textAlignment     = .left
            totalTitleLabel.textAlignment   = .left
        } else if Defaults[.langId] == "2" {
            deliveryLabel.textAlignment     = .right
            sumTitleLabel.textAlignment     = .right
            totalTitleLabel.textAlignment   = .right
        }
    }
    
  
}
