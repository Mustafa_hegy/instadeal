//
//  ShowAddressTableViewCell.swift
//  InestaDeal
//
//  Created by Waleed on 5/6/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit

class ShowAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var addressName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
