//
//  NavBarViewController.swift
//  InestaDeal
//
//  Created by Waleed on 4/11/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Alamofire

class NavBarViewController: UIViewController , UITabBarDelegate {
    
    @IBOutlet weak var numberOfProducts: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var menuIcon: UIButton!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var areaBtn: UIButton!
    @IBOutlet weak var appSloganImage: UIImageView!
    var itemsCount = 0
    var totalPrices = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
//       areaBtn.alignVertical()
        if Defaults[.langId] == "1" {
            backBtn.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let params: Parameters = [
            "lang": "\(Defaults[.langId])" ,
            "area": "\(Defaults[.areaId])"
        ]
        Alamofire.request("https://www.instadeal.co/home/getCarts/\(Defaults[.userId])", method: .get , parameters: params ).responseCartList { response in
            if let getCartResponse = response.result.value {
                if getCartResponse.result == "true"{
                    var count = 0
                    for data in getCartResponse.data{
                        count += data.products.count
                    }
                    self.numberOfProducts.text = String(count)
                }
            }}
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func menuIconAction(_ sender: UIButton) {
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "menuID") as! MenuViewController
        // .instantiatViewControllerWithIdentifier() returns AnyObject! this must be downcast to utilize it
        
        self.navigationController?.show(viewController, sender: sender)
        
    }

    @IBAction func bacBtnAction(_ sender: UIButton) {

         if let vc = parent as? SelectCategoryViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        
        handleTabBarVisibility(hide: false)
        
//        if Defaults[.linkPageFlag] == false{
//            self.dismiss(animated: true, completion: nil)
//        }
//        if Defaults[.dismissed] && ((parent as? LinkViewController) != nil) {
//            if let vc = (parent as? LinkViewController){
//                if vc.url.contains("knet/buy.php"){
//                    let viewController:UITabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarRoot") as! UITabBarController
//
//                    self.present(viewController, animated: true, completion: nil)
//                }
//            }
//            else{
//                self.dismiss(animated: true, completion: nil)
//                return
//            }
//        }
        if let vc = parent as? LinkViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        else if let vc = parent as? CartListViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        else if let vc = parent as? AddAddressViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        else if Defaults[.flag] {
           self.tabBarController?.selectedIndex = 0
            Defaults[.flag] = false
            return
        }
        
        else if let vc = parent as? AddAddressViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        else if let vc = parent as? PrdouctDetailsViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        else if let vc = parent as? AddAddressViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        else if let vc = parent as? MyAddressViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        else if let vc = parent as? MyLocationViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        else if let vc = parent as? AddressViewController {
            if parent?.presentingViewController?.presentedViewController == vc {
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.navigationController?.popViewController()
            }
            return
        }
        else if let _ = parent as? OffersViewController {
            self.tabBarController?.selectedIndex = 0
            return
        }
        else if let _ = parent as? SearchViewController {
            
            let viewController:MainTabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarRoot") as! MainTabBarController
            let appDelegate = UIApplication.shared.delegate
            appDelegate?.window??.rootViewController = viewController
            
            return
        }
        else if let _ = parent as? CatsViewController {
            self.tabBarController?.selectedIndex = 0
            return
        }
        else if self.navigationController != nil {
            self.navigationController?.popViewController()
            return
        }
        else if (self.tabBarController?.selectedIndex) != 0 {
            self.tabBarController?.selectedIndex = 0
            return
        }
         else{
            self.dismiss(animated: true, completion: nil)
            return
        }
        
       
    }
    
    @IBAction func goToListOfCart(_ sender: UIButton) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CartListViewController") as? CartListViewController {
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
        
    }
    
    @IBAction func goToListOfAreas(_ sender: Any) {
        print("area name")
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AreasListViewController") as? AreasListViewController {
            viewController.nextViewFlag = "productsList"
            if let navigator = navigationController {
//                navigator.pushViewController(viewController, animated: true)
                navigator.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
}
