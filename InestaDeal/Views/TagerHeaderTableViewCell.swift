//
//  TagerHeaderTableViewCell.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/23/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit

class TagerHeaderTableViewCell: UITableViewCell {
    
    var tagerSelection : (()->())?
    
    @IBOutlet weak var tagerTitleLabel: UILabel!
    @IBOutlet weak var tagerName:UILabel!
    @IBOutlet weak var selectItemsOfTagerButton : UIButton!
    @IBOutlet weak var gammalyView: UIView!
    @IBOutlet weak var gammalyTitleLabel: UILabel!
    var selectAllItemsOfTager: ((_ selected:Bool)-> Void)?
    var tagerSelected:Bool!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(cartList: CartListData){
        tagerTitleLabel.text    = "seller".localized() + " : "
        gammalyTitleLabel.text  = "gmma3ly".localized()
        tagerName.text = cartList.name
        if cartList.gamm3 == "1"{
            gammalyView.isHidden = false
        } else {
            gammalyView.isHidden = true
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func allItemsOfTagerButtonTapped(_ sender:UIButton){
        tagerSelection! ()
        /*
        if !tagerSelected{
            selectAllItemsOfTager?(true)
            //tagerSelected = false
        } else {
            selectAllItemsOfTager?(false)
            //tagerSelected = true
        }
        */
    }
    
}
