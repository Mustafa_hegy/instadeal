//
//  CatsProductCell.swift
//  InestaDeal
//
//  Created by Waleed on 4/18/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import Foundation

import UIKit
import SwiftyUserDefaults

class CatsProductCell : UICollectionViewCell {
    
    @IBOutlet weak var addCartBtn: UIButton!
    @IBOutlet weak var productHead: UILabel!
    @IBOutlet weak var delivery: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var favProductButton:UIButton!
    @IBOutlet weak var favView:UIView!
    @IBOutlet weak var salaView: UIView!
    @IBOutlet weak var oldPrice: UILabel!
    
    @IBOutlet weak var discountPercentage: UILabel!
    @IBOutlet weak var gmma3lyIcon: UIImageView!
    @IBOutlet weak var shippingCostLabel: UILabel!
    var productFavAction : (()->())?
    var productAddCartAction : (()->())?
    
    override func awakeFromNib() {
        lineOnOldPrice()
//        salaView.backgroundColor = UIColor(white: 1, alpha: 0.85)
//        favView.backgroundColor = UIColor(white: 1, alpha: 0.85)
        
        shippingCostLabel?.text      = "freeShipping".localized()
        
        if Defaults[.langId] == "1" {
            productHead.textAlignment           = .left
            shippingCostLabel?.textAlignment    = .right
        } else if Defaults[.langId] == "2" {
            productHead.textAlignment           = .right
            shippingCostLabel?.textAlignment    = .left
        }
    }

    func lineOnOldPrice(){
        let lineView = UIView()
        lineView.backgroundColor = UIColor.lightGray
        oldPrice.tintColor = UIColor.lightGray
        oldPrice.addSubview(lineView)
        
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.leadingAnchor.constraint(equalTo: oldPrice.leadingAnchor).isActive = true
        lineView.trailingAnchor.constraint(equalTo: oldPrice.trailingAnchor).isActive = true
        lineView.centerYAnchor.constraint(equalTo: oldPrice.centerYAnchor).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
    }
    
    @IBAction func productFavButtonAction(_ sender: Any) {
        productFavAction! ()
    }
    
    @IBAction func productAddCartButtonAction(_ sender: Any) {
        productAddCartAction! ()
    }
}
