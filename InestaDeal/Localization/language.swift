//
//  language.swift
//  Sabeel
//
//  Created by Mohamed Mrzok on 4/27/19.
//  Copyright © 2019 dd. All rights reserved.
//

import Foundation

class language {
    
    func currentLanguage () ->String {
        let def = UserDefaults.standard
        let langs = def.object(forKey: "AppleLanguages") as! NSArray
        let firstLang = langs.firstObject as! String
        return firstLang
    }
    
    func setAppLanguage(lang :String)
    {
        let def = UserDefaults.standard
        
        def.set([lang ,currentLanguage()], forKey: "AppleLanguages")
        def.synchronize()
        
    }
}
