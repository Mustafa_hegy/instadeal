//
//  Localizer.swift
//  Sabeel
//
//  Created by Mohamed Mrzok on 4/27/19.
//  Copyright © 2019 dd. All rights reserved.
//

import Foundation
import UIKit

class Localizer {
    
    class func DoTheExchange ()
    {
        ExchangeMethodsForClass(className:Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.customeLocalizedStringForKey(key:value:table:)))
    }
    
}


extension Bundle{
    @objc func customeLocalizedStringForKey(key:String , value :String?,
                                            table tableName:String) ->String
    {
        let currentLanguage = language().currentLanguage()
        var bundle = Bundle()
        
        if let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj")
        {
            bundle = Bundle(path: path)!
        } else {
            let path = Bundle.main.path(forResource: "Base", ofType: "lproj")
            bundle = Bundle(path: path!)!
        }
        return bundle.customeLocalizedStringForKey(key:key , value:value, table:tableName)
    }
}



func ExchangeMethodsForClass(className :AnyClass,originalSelector:Selector
    ,overrideSelector:Selector)
    
{
    let originalMethod = class_getInstanceMethod(className, originalSelector)
    
    let overideMethod = class_getInstanceMethod(className, overrideSelector)
    
    if class_addMethod(className, originalSelector, method_getImplementation(overideMethod!), method_getTypeEncoding(overideMethod!))
    {
        class_replaceMethod(className, overrideSelector, method_getImplementation(originalMethod!), method_getTypeEncoding(originalMethod!))
    } else {
        method_exchangeImplementations(originalMethod!, overideMethod!)
    }
}
