//
//  ConfirmOrderViewController.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/26/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Alamofire


class ConfirmOrderViewController: UIViewController {
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var deliveryTime : UILabel!
    @IBOutlet weak var cashIndicatorButton:UIButton!
    @IBOutlet weak var kNetIndicatorButton:UIButton!
    @IBOutlet weak var nowDateView: UIView!
    @IBOutlet weak var nowLabel: UILabel!
    @IBOutlet weak var totalDeliveryCostaLabel: UILabel!
    @IBOutlet weak var deliveryTimeView: UIView!
    @IBOutlet var deliveryTimeStackViewHieghtLayoutConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var totalPriceTitleLabel: UILabel!
    @IBOutlet weak var deliveryCostLabel: UILabel!
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var deliveryTimeButton: UIButton!
    @IBOutlet weak var orderDetailsLabel: UILabel!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var confirmOrderButton: UIButton!
    @IBOutlet weak var completrOrderHintLabel: UILabel!
    
    
    
    var orders:[Product] = []
    var address: GetAddress?
    var model: ProductDetailsResponse?
    var catsProductObject:CatsProducts?
//    var productDetailsObject:ProductDetailsResponse?
//    var model: Any?
    var totalPrice: Double?
    var totalDelivery: Double?
    var numberOfProducts:Int?
    var currentDate:String = ""
    var selectedDate:String = ""
    var selectedTimeId:String = ""
    var paymentMethod:Int = 1
    var gmmaly = 0
    var totalDeliveryCost: Double?
    var addressId:String = ""
    var selectedProducts = ""
    var finalTotalPrice = 0.0
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    override func viewDidLoad() {
        super.viewDidLoad()
//        addNavBar(isBack: true)
        localizeUI()
        
        /*
        if productDetailsObject != nil {
            model = productDetailsObject as ProductDetailsResponse?
        }
        if catsProductObject != nil {
            model = catsProductObject as CatsProducts?
        }
        */
        
        addNavBarCart(isBack: true)
        if orders.count != 0 {
            for order in orders {
                selectedProducts += "\(order.prodID)-"
            }
            selectedProducts.remove(at: selectedProducts.index(before: selectedProducts.endIndex))
            print("selectedProducts: \(selectedProducts)")
        } else {
            if model != nil {
                if let singleItem = model {
                    selectedProducts = "\(singleItem.id ?? "0")"
                    print("selectedProducts: \(selectedProducts)")
                }
            }
            else if catsProductObject != nil {
                if let singleItem = catsProductObject {
                    selectedProducts = "\(singleItem.id ?? "0")"
                    print("selectedProducts: \(selectedProducts)")
                }
            }
        }
        
        if gmmaly == 0 {
            deliveryTimeView.isHidden   = true
            deliveryTimeStackViewHieghtLayoutConstraint.constant = 0
        }
        
        //print("orders: \(orders)")
        //print("address: \(address)")
        
        // Old Delivery time way
        /*
        dateAndTimeNow()
         */
        if let address = address {
            addressId = address.id ?? "0"
            let name = "" + (address.name ?? "")
            let house = "\("house".localized()):" + (address.house ?? "-")
            //let block = "" + (addressModel?.data![indexPath.row].block ?? "")
            let street = "\("street".localized()):" + (address.street?.replacingOccurrences(of: ",", with: "") ?? "")
            let area  = "\("area".localized()):" + (address.area ?? "-")
            let gada = "\("gada".localized()):" + (address.gada ?? "-")
            let floor = "\("floor".localized()):" + (address.floor ?? "-")
            //let notes = "" + (addressModel?.data![indexPath.row].notes ?? "")
            let room = "\("room".localized()):" + (address.room ?? "-")
            let mobile = "\("recieverPhoneNumber".localized()):" + (address.mobile ?? "-")
            addressLabel.text = "\("address".localized()):\(name) \(area) \(street) \(house) \(floor) \(gada) \(room) \(mobile)"
        }
        
        if let totalPrice = totalPrice{
            totalPriceLabel.text = "\(totalPrice) \("currency".localized())"
            finalTotalPrice = totalPrice
            finalTotalPrice = Double(round(1000*finalTotalPrice)/1000)
        }
        
        if let totalDeliveryCost = totalDeliveryCost {
            totalDeliveryCostaLabel.text = "\(totalDeliveryCost) \("currency".localized())"
        }
        
        if model != nil {
            totalDeliveryCostaLabel.text = "\(Double(Defaults[.deliveryPrice]) ?? 0.0) \("currency".localized())"
            totalPriceLabel.text = "\(((model?.newPrice ?? 0.0) * (Double(Defaults[.numberOfProducts]) ?? 0.0)) + (Double(Defaults[.deliveryPrice]) ?? 0.0)) \("currency".localized())"
            
            finalTotalPrice = (((model?.newPrice ?? 0.0) * (Double(Defaults[.numberOfProducts]) ?? 0.0)) + (Double(Defaults[.deliveryPrice]) ?? 0.0))
            finalTotalPrice = Double(round(1000*finalTotalPrice)/1000)
        }
        else if catsProductObject != nil {
            totalDeliveryCostaLabel.text = "\(Double(Defaults[.deliveryPrice]) ?? 0.0) \("currency".localized())"
            totalPriceLabel.text = "\(((catsProductObject?.newPrice ?? 0.0) * (Double(Defaults[.numberOfProducts]) ?? 0.0)) + (Double(Defaults[.deliveryPrice]) ?? 0.0)) \("currency".localized())"
            
            finalTotalPrice = (((catsProductObject?.newPrice ?? 0.0) * (Double(Defaults[.numberOfProducts]) ?? 0.0)) + (Double(Defaults[.deliveryPrice]) ?? 0.0))
            finalTotalPrice = Double(round(1000*finalTotalPrice)/1000)
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        handleTabBarVisibility(hide: true)
    }
    
    
    func localizeUI(){
        pageTitleLabel.text             = "orderDetails".localized()
        orderDetailsLabel.text          = "orderDetails".localized()
        deliveryTime.text               = "deliveryTime".localized()
        paymentMethodLabel.text         = "selectPayMethod".localized()
        completrOrderHintLabel.text     = "completrOrderHint".localized()
        deliveryCostLabel.text          = "deliveryCost".localized() + " : "
        totalPriceTitleLabel.text       = "totalCost".localized() + " : "
        confirmOrderButton.setTitle("confirmOrder".localized(), for: .normal)
        
        if Defaults[.langId] == "1" {
            addressLabel.textAlignment  = .left
        } else if Defaults[.langId] == "2" {
            addressLabel.textAlignment  = .right
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDatePopup"{
            nowDateView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            nowLabel.textColor = .darkGray

            let datePopUp = segue.destination as! DateFromPopUpViewController
            datePopUp.getDate = { (date) in
                self.selectedDate = date
                self.deliveryTime.text = date.replacingOccurrences(of: "_", with: " ")
            }
            
        }
    }
    
    @IBAction func dateAndTimeNowTapped(_ sender:UIButton){
        nowDateView.backgroundColor = #colorLiteral(red: 0.8078431373, green: 0.02745098039, blue: 0.3333333333, alpha: 1)
        nowLabel.textColor = .white
        dateAndTimeNow()
    }
    
    func dateAndTimeNow () {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat    = "yyyy-MM-dd_HH-mm"
        dateFormatter.locale        = NSLocale(localeIdentifier: "en") as Locale
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
//        selectedDate = dateInFormat
        currentDate = dateInFormat
//        deliveryTime.text = dateInFormat.replacingOccurrences(of: "_", with: " ")
        print("date: \(dateInFormat)")
    }
    
    @IBAction func finishOrderButtonTapped(_ sender:UIButton){
        if gmmaly == 1 && ( selectedDate.isEmpty || selectedTimeId.isEmpty) {
            // Validation Alert
            self.showDefaultAlert(title: "deliveryTimeRequired".localized(), message: nil)
        }
        else {
            dateAndTimeNow()
            if paymentMethod == 1 {
                print("finishOrder: https://www.instadeal.co/home/finishOrder/\(Defaults[.userId])/\(addressId)/\(paymentMethod)?ga=\(gmmaly)&prods=\(selectedProducts)&date=\(currentDate)&time_id=\(selectedTimeId)&time_date=\(selectedDate)")
                activity.center = view.center
                activity.color = #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
                activity.startAnimating()
                view.addSubview(activity)
                payNowApi { (data) in
                    if let datamodel = data as? BaseResponse{
                        if datamodel.result == "true"
                        {
                            self.activity.stopAnimating()
                            self.activity.isHidden = true
                            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let finishOrderVC = storyBoard.instantiateViewController(withIdentifier: "FinishOrderVC") as! FinishOrderViewController
                            finishOrderVC.paymentType = self.paymentMethod
//                            self.navigationController?.pushViewController(finishOrderVC)
                            
                            UIApplication.shared.delegate?.window??.rootViewController = finishOrderVC
                            
                            /* Edited Code */
//                            let nav1 = UINavigationController.init(rootViewController: finishOrderVC)
//                            self.navigationController?.pushViewController(nav1)
                        }
//                        else {
//                            self.navigationController?.popToRootViewController(animated: true)
//                        }
                    }
                }
                
            }else {
                // http://instadeal.co/knet/buy.php?price=100&ga=1&address=1&userid=1&date=2019-04-22_10-28&prods=1-2-3
                
                // http://instadeal.co/knet/buy.php?price=" + Total + "&ga=" + Ga + "&address=" + AddressID+ "&userid=" + FaceIdHolder.getInstance().getData() +                "&date=" + Date + "&prods=" + prods
                
                
                //KNET: http://instadeal.co/knet/buy.php?price=12.52&ga=0&address=7&userid=1&date=2019-04-22_20-16&prods=32
                
                
                
                let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
                let url = "http://instadeal.co/knet/buy.php?price=\(finalTotalPrice)&ga=\(gmmaly)&address=\(addressId)&userid=\(Defaults[.userId])&date=\(selectedDate.replacingOccurrences(of: ":", with: "-"))&prods=\(selectedProducts)&time_id=\(selectedTimeId)"
                print ("KNET: \(url)")
                viewController.url = url
                self.navigationController?.show(viewController, sender: self)
                
            }
        }
    }
    
    func payNowApi(successClosure: @escaping SuccessClosure){ Alamofire.request("https://www.instadeal.co/home/finishOrder/\(Defaults[.userId])/\(addressId)/\(paymentMethod)?ga=\(gmmaly)&prods=\(selectedProducts)&date=\(currentDate)&time_id=\(selectedTimeId)&time_date=\(selectedDate)&lang=\(Defaults[.langId])", method: .get,  encoding: JSONEncoding.default)
            .responseJSON { response in
                
                if let json = response.result.value {
                    /* Basic Code
                    if let data = json as? [String : Any]{
                        Defaults[.orderId] = data["data"] as! String
                        print("orderId:-\(data["data"] as! String)")
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                    */
                    
                    if var data = json as? [String : Any]{
                        if let orderId = data["data"] , orderId is Int && !(orderId as! Bool) {
                            self.showDefaultAlert(title: "errorHint".localized(), message: nil, actionBlock: nil)
//                            data["result"] = false
                        } else {
                            Defaults[.orderId] = data["data"] as! String
                            print("orderId:-\(data["data"] as! String)")
                        }
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
        }
    }
    
    @IBAction func conditonalAndTerms(_ sender: Any) {
        let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
        viewController.url = "https://www.instadeal.co/conditions.html"
        self.navigationController?.show(viewController, sender: self)
    }
    @IBAction func cashButtonTapped(_ sender:UIButton){
        paymentMethod = 1
        cashIndicatorButton.setBackgroundImage(#imageLiteral(resourceName: "filledBtn"), for: .normal)
        kNetIndicatorButton.setBackgroundImage(#imageLiteral(resourceName: "emptyBtn"), for: .normal)

        
    }
    
    @IBAction func kNetButtonTapped(_ sender:UIButton){
        paymentMethod = 3
        cashIndicatorButton.setBackgroundImage(#imageLiteral(resourceName: "emptyBtn"), for: .normal)
        kNetIndicatorButton.setBackgroundImage(#imageLiteral(resourceName: "filledBtn"), for: .normal)
    }
    


}


extension ConfirmOrderViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orders.count == 0{
            return 1
        }else{
            return orders.count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let orderItemsCell = Bundle.main.loadNibNamed("ConfirmOrderTableViewCell", owner: self, options: nil)?.first as! ConfirmOrderTableViewCell
        if orders.count == 0{
            if model != nil {
                if let model = model{
                    orderItemsCell.itemName.text = model.name
                    if let count = numberOfProducts{
                        orderItemsCell.itemCount.text = "\(count)"
                        orderItemsCell.totalPrice.text = ("\(Double(model.newPrice ?? 0) * Double(count))")
                    }
                    orderItemsCell.itemImage.sd_setImage(with: URL(string: model.photo!), placeholderImage: #imageLiteral(resourceName: "iconApp"))
                    orderItemsCell.itemPrice.text = "\(model.newPrice ?? 0)"
                    orderItemsCell.totalPrice.text = "\(model.newPrice ?? 0)"
                }
            }
            else if catsProductObject != nil {
                if let model = catsProductObject{
                    orderItemsCell.itemName.text = model.name
                    if let count = numberOfProducts{
                        orderItemsCell.itemCount.text = "\(count)"
                        orderItemsCell.totalPrice.text = ("\(Double(model.newPrice ?? 0) * Double(count))")
                    }
                    orderItemsCell.itemImage.sd_setImage(with: URL(string: model.photo!), placeholderImage: #imageLiteral(resourceName: "iconApp"))
                    orderItemsCell.itemPrice.text = "\(model.newPrice ?? 0)"
                    orderItemsCell.totalPrice.text = "\(model.newPrice ?? 0)"
                }
            }
        } else {
            orderItemsCell.configure(item: orders[indexPath.row])

        }
        return orderItemsCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(110)
    }
    
    // MARK: - Navigation
    @IBAction func unwindToConfirmOrderView(_ unwindSegue: UIStoryboardSegue) {
        if unwindSegue.identifier == "unwindFromCalenderView" {
            let sourceViewController = unwindSegue.source as! CalenderViewController
            // Use data from the view controller which initiated the unwind segue
            selectedDate        = sourceViewController.confirmOrderSelectedDate
            selectedTimeId      = sourceViewController.selectedTimeId
            deliveryTime.text   = sourceViewController.selectedDate
        }
    }
}
