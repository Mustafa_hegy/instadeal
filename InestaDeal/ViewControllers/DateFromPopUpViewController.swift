//
//  DateFromPopUpViewController.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/31/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit

class DateFromPopUpViewController: UIViewController {
    @IBOutlet weak var datePicker: UIDatePicker!
    var getDate: ((_ date:String) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.minimumDate = Date()

    }
    
    @IBAction func selectDateDone(_ sender: UIButton){
        print(datePicker.date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd_HH:mm"
        let dateInFormat = dateFormatter.string(from: datePicker.date)
        getDate?(dateInFormat)
        dismiss(animated: true)
    }
}
