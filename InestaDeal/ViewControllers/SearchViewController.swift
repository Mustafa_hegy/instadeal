//
//  SearchViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/28/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class SearchViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var searchInBtn: UIButton!
    @IBOutlet weak var to: UITextField!
    @IBOutlet weak var from: UITextField!
    @IBOutlet weak var category: UITextField!
    @IBOutlet weak var searchQuery: UITextField!
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var searchHint: UILabel!
    
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    var results = [OffersResponse]()
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        
        Defaults[.catSelectedAddProduct] = ""
        addNavBar(isBack: true)
  
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Defaults[.search] = false
        if Defaults[.catSelectedAddProduct] == ""{
            
        }else{
            self.category.text = Defaults[.catSelectedAddProduct]
            
        }
        self.view.endEditing(true)
        self.category.endEditing(true)
    }
    
    func localizeUI(){
        searchButton.setTitle("search".localized(), for: .normal)
//        categoryButton.setTitle("selectCategory".localized(), for: .normal)
        category.text           = "selectCategory".localized()
        searchHint.text         = "searchHint".localized()
        pageTitleLabel.text     = "advancedSearch".localized()
        from.placeholder        = "priceFrom".localized()
        to.placeholder          = "priceTo".localized()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 10{
            
        }
        
    }

    @IBAction func searchBtnAction(_ sender: UIButton) {
        Defaults[.search] = true
        performSegue(withIdentifier: "searchCats", sender: self)
        self.view.endEditing(true)
        
    }
    @IBAction func searchAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if category.isEmpty{
            showDefaultAlert(title: "categoryRequired".localized(), message: nil)
        }
        else{
            searchApi { (data) in
                if let dataModel = data as? GetOffersResponse{
                    if dataModel.data?.count != 0{
                        self.results = dataModel.data ?? []
                        self.performSegue(withIdentifier: "goResults", sender: self)
                    }
                    else{
                        self.showDefaultAlert(title: "noSearchResult".localized() , message: nil)
                    }
                }
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goResults"{
            
            if let nextVC = segue.destination as? SearchResultViewController{
                nextVC.results = self.results
            }
        }
    }
    
    func searchApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "title": searchQuery.text ?? "" ,
            "cat": Defaults[.catId] ,
            "from": from.text ?? "" ,
            "to": to.text ?? ""
        ]
        
        
        Alamofire.request("https://www.instadeal.co/home/getSearch?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = GetOffersResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
    



}
