//
//  SearchResultViewController.swift
//  InestaDeal
//
//  Created by Waleed on 6/8/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Alamofire

class SearchResultViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate , UICollectionViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tableResults: UICollectionView!

    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var isFiltered = false
//    var results = [OffersResponse]()
    var flag = true
    
    var productModelView: ProductModelView? = ProductModelView()
    
    var results = [OffersResponse]()
    {
        didSet{
//
            if !flag{
//            self.performSegue(withIdentifier: "goFavs", sender: self)
            tableResults.reloadData()
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Defaults[.prodId] = results[indexPath.row].id ?? ""
        performSegue(withIdentifier: "goProdDetailsSegue", sender: self)
    }
    
    func getFavApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getMyFav/\(Defaults[.userId] )?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = GetOffersResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    override func viewDidLoad() {
        tableResults.contentInset.top = 10
        tableResults.contentInset.bottom = 20

        if !flag{
            activity.center = view.center
            activity.color = #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
            activity.startAnimating()
            view.addSubview(activity)
            getFavApi(successClosure: { (data) in
                
                if let dataModel = data as? GetOffersResponse{
                    
                    self.results = dataModel.data ?? []
                    self.activity.stopAnimating()
                    self.activity.isHidden = true
                    
                }
            })
        }
        super.viewDidLoad()
        tableResults.reloadData()
        addNavBar(isBack: true)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    
    //MARK:- Class Methods
    private func startLoading() {
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
    }
    
    private func stopLoading() {
        self.activity.stopAnimating()
        self.activity.isHidden = true
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let itemsPerRow:CGFloat = 3
//        let hardCodedPadding:CGFloat = 8
//        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
//        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
//        return CGSize(width: itemWidth, height: 175)
//    }
//
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//       return CGSize(width: collectionView.frame.width / 2, height: 230)
        
        return CGSize(width: (collectionView.frame.width - 4) / 2, height: 274)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        /*
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productDetailsSearchCell", for: indexPath) as! SimilarProductCell
        cell.prodName.text = results[indexPath.row].name
        cell.price.text = (results[indexPath.row].price ?? "") + " \("currency".localized()) "
//        cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
        cell.imageProduct.sd_setImage(with: URL(string: results[indexPath.row].photo ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
       // cell.addCartBtn.tag = indexPath.row
        
        //        cell.imageView.sd_setImage(with: URL(string: (categories?.data![section].childs![indexPath.row].photo) ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        
        
        return cell
        */
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productDetailsSearchCell", for: indexPath) as? CatsProductCell
        
        let rowData  = results[indexPath.row]
        cell?.productHead.text  = rowData.name
        cell?.delivery.text     = "\(rowData.newPrice ?? 0) \("currency".localized())"
        
        if rowData.photo == "" { // rowData.photo!.isEmptyrowData.photo!.isEmpty
            cell?.productImage.image = #imageLiteral(resourceName: "instaLogo")
        } else {
            cell?.productImage.sd_setImage(with: URL(string: (rowData.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "instaLogo"))
        }
        
        cell?.addCartBtn.tag    = indexPath.row
        
        print("price: \(rowData.newPrice)")
        
        if let fav = rowData.favChecked {
            if fav == 1 {
                cell?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
            } else {
                cell?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
            }
        }
        
        if let discount = Int(rowData.discount ?? "0") {
            if discount > 0 {
                cell?.discountPercentage.isHidden   = false
                cell?.discountPercentage.text       = "\("discountKey".localized()) \(rowData.discount ?? "0")%"
            } else {
                cell?.discountPercentage.isHidden = true
            }
        }
        
        if let gmma3 = Int(rowData.gmmaly ?? "0") {
            if gmma3 == 1 {
                cell?.gmma3lyIcon.isHidden = false
            } else {
                cell?.gmma3lyIcon.isHidden = true
            }
        }
        
        if let freeShipping = rowData.freeDelivery {
            if freeShipping == 1 {
                cell?.shippingCostLabel.isHidden = false
            } else {
                cell?.shippingCostLabel.isHidden = true
            }
        }
        
        cell?.productFavAction = {
            if Defaults[.logged]{
                // Load Data From API
                Defaults[.prodId] = rowData.id ?? ""
                
                self.startLoading()
                self.productModelView?.addToFavApi { (data) in
                    
                    self.stopLoading()
                    
                    if let dataModel = data as? BaseResponse{
                        if dataModel.result == "true"
                        {
                            if dataModel.message == "تمت الاضافة للمفضلة"
                            {
                                cell?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                            } else {
                                cell?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                            }
                        }
                        self.view.layoutSubviews()
                        self.view.layoutIfNeeded()
                    }
                }
            }
            else{
                self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
            }
        }
        
//        cell?.productAddCartAction = {
//            Defaults[.prodId] = rowData.id ?? ""
//            let requiredQuantity: Int   = 1
//            let existCountity: Int      = Int(rowData.counts ?? "0") ?? 0
//            if requiredQuantity <= existCountity && requiredQuantity != 0 {
//                if !Defaults[.logged]{
//                    self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
//                }
//                else{
//                    // Edit Code => Add to cart first and then move to Addresses List
//                    
//                    self.startLoading()
//                    self.productModelView?.addToCartApi(price: rowData.price ?? "0", count: requiredQuantity /* => required quantity */ ) { (data) in
//                        
//                        self.stopLoading()
//                        
//                        if let dataModel = data as? BaseResponse{
//                            
//                            if dataModel.message == "تم الاضافة للسلة" {
//                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                let myAddressVC = storyBoard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressViewController
//                                myAddressVC.catsProductObject = rowData
//                                myAddressVC.numberOfProducts = requiredQuantity
//                                Defaults[.numberOfProducts] = "\(requiredQuantity)"
//                                self.navigationController?.pushViewController(myAddressVC, animated: true)
//                                //performSegue(withIdentifier: "directBuy", sender: self)
//                            }
//                        }
//                    }
//                }
//            }
//            else{
//                self.showDefaultAlert(title: "noEnoughQuantity".localized(), message: nil)
//            }
//        }
        
        if let newPrice = rowData.newPrice{
            if let oldPrice = rowData.price{
                if Double(newPrice) == Double(oldPrice){
                    cell?.oldPrice.isHidden = true
                } else {
                    cell?.oldPrice.isHidden = false
                    cell?.oldPrice.text = "\(oldPrice) \("currency".localized())"
                }
            }
        }
        return cell!
        
    }

}
