//
//  AreasShippingCostViewController.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 5/26/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class AreasShippingCostViewController: UIViewController, UITableViewDelegate, UITableViewDataSource ,TextFieldInTableViewCellDelegate {

    @IBOutlet var areasTableView    : UITableView!
    @IBOutlet weak var hintLbl: UILabel!
    @IBOutlet weak var btnSendData: UIButton!
    
    let activity                    = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var companyId                   = ""
    var areasPricesString           = ""
    var areasListResponse           : AreasResponse?
    {
        didSet{
            areasTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        localizeUI()
        
        // Add nav bar => addNavBarCart for nav bar with back button and without cart button
        addNavBarCart(isBack: true)
        
        // Add Activity Indicator
        startLoadingActivity()
        
        // Load Data From API
        getAreasApi(successClosure: {  (data) in
            if let dataModel = data as? AreasResponse{
                self.areasListResponse = dataModel
            }
        })
    }
    
    // MARK: - Class Custom Methods
    func localizeUI(){
        btnSendData.setTitle("save".localized(), for: .normal)
        hintLbl.text        = "areasCostHint".localized()
    }
    
    func startLoadingActivity() {
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
    }
    
    @objc func handelExpendClose(button : UIButton) {
        let section = button.tag
        
        // Set All index pathes of rows for a section to be deleted
        var indexPaths = [IndexPath]()
        for row in (areasListResponse?.data?[section].child!.indices)! {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        let isExpanded = areasListResponse?.data?[section].isExpanded
        areasListResponse?.data?[section].isExpanded    = !isExpanded!
        
        if isExpanded! {
            areasTableView.deleteRows(at: indexPaths, with: .fade)
        }
        else{
            areasTableView.insertRows(at: indexPaths, with: .fade)
        }
        
        areasTableView.reloadData()
    }
    
    // MARK: - Class Actions
    @IBAction func btnSendData(_ sender: Any) {
         // Rretrive input Fields text from cells
        for section in (areasListResponse?.data!.indices)! {
//            print("Section \(section)")
            for row in (areasListResponse?.data?[section].child!.indices)! {
//                print("Row \(row)")
                let areaPrice = ((areasListResponse?.data?[section].child?[row].price)! != 0 || (areasListResponse?.data?[section].child?[row].price) != nil) ? "\((areasListResponse?.data?[section].child?[row].price)!)" : "0"
                
                areasPricesString = (areasPricesString.isEmpty ? "" : "\(areasPricesString),") + (areasListResponse?.data?[section].child?[row].id!)! + "_" + areaPrice
            }
        }
        print("areasPricesString \(areasPricesString)")
        
        // Add Activity Indicator
        startLoadingActivity()
        // Save Data to API
        saveAreasPricesApi(successClosure: {  (data) in
            if let dataModel = data as? AreasPricesResponse{
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    // MARK: - TableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return areasListResponse?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !(areasListResponse?.data?[section].isExpanded)! {
            return 0
        }
        
        return areasListResponse?.data?[section].child?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView: areaCell  = tableView.dequeueReusableCell(withIdentifier: "SectionCell") as! areaCell
        let sectionItem                 = areasListResponse?.data?[section]
        
        if (areasListResponse?.data?.count)! > 0 {
            headerView.titleLbl.text        = sectionItem?.name
            headerView.cellButton.tag       = section
            headerView.cellButton.addTarget(self, action: #selector(handelExpendClose), for: .touchUpInside)
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell        = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! areaPriceCell
        cell.delegate   = self
        
        let row                     = areasListResponse?.data?[indexPath.section].child?[indexPath.row]
        cell.titleLbl.text          = row?.name
        cell.cellInputField.text    = row?.price != 0 ? "\(row?.price! ?? 0)" : ""
        cell.cellInputField.placeholder = "deliveryCost".localized()
        
        if Defaults[.langId] == "1" {
            cell.titleLbl.textAlignment     = .left
        } else if Defaults[.langId] == "2" {
            cell.titleLbl.textAlignment     = .right
        }
        
        return cell
    }
    
    // MARK: - TextFieldInTableViewCellDelegate
    func textField(editingDidEndIn cell: areaPriceCell) {
        if let indexPath    = areasTableView?.indexPath(for: cell) {
            let row         = areasListResponse?.data?[indexPath.section].child?[indexPath.row]
            // Update area price in main list with new entered value
            row?.price      = !(cell.cellInputField.text)!.isEmpty ? Float(cell.cellInputField.text!) : 0.0
            
            // If row is All cities => index 0 , set all cities price in the same section to same price
            if indexPath.row == 0 {
                for (index, element) in (areasListResponse?.data?[indexPath.section].child?.enumerated())! {
                    if index != 0 {
                        element.price               = row?.price
                    }
                }
                // reload table to show updates
                areasTableView.reloadData()
            }
        }
    }
    
    // MARK: - Api Connection
    func getAreasApi(successClosure: @escaping SuccessClosure){
        Alamofire.request("https://www.instadeal.co/home/getAreaWithPrice/\(companyId)?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = AreasResponse(JSON: data)
                    successClosure(dataModel)
                    
                    self.activity.stopAnimating()
                    self.activity.isHidden = true
                }
            }
        }
    }
    
    func saveAreasPricesApi(successClosure: @escaping SuccessClosure){
        let params: Parameters = [
            "Price": "\(areasPricesString)"
        ]
        Alamofire.request("https://www.instadeal.co/home/editCompanyPrice/\(companyId)?lang=\(Defaults[.langId])", method: .post, parameters: params).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = AreasPricesResponse(JSON: data)
                    successClosure(dataModel)
                }
            }
            self.activity.stopAnimating()
            self.activity.isHidden = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
