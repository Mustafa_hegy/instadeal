//
//  CalenderViewController.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 6/8/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import FSCalendar
import SwiftyUserDefaults

class CalenderViewController: UIViewController , UITableViewDelegate, UITableViewDataSource , FSCalendarDelegate , FSCalendarDataSource {

    @IBOutlet var calenderView              : FSCalendar!
    @IBOutlet var deliveryTimesTableView    : UITableView!
    @IBOutlet weak var btnSendData: UIButton!
    
    let activity                            = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var curDate                             : Date?
    var formatter                           = DateFormatter()
    var weekDayNumber                       : String = ""
    var selectedDate                        : String = ""
    var confirmOrderSelectedDate            : String = ""
    var selectedTimeId                      : String = ""
    var deviceCurrentLanguage               = Locale.current.languageCode
    
    
    
    var deliveryTimesListResponse           : DeliveryTimeResponse?
    {
        didSet{
            deliveryTimesTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        localizeUI()
        addNavBarCart(isBack: true)
        
        setDateAndFormatter()
    }
    
    func localizeUI(){
        btnSendData.setTitle("save".localized() , for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (deviceCurrentLanguage == "en" && Defaults[.langId] == "1") {
            // English Language Selected
            UIView.appearance().semanticContentAttribute = .forceLeftToRight // to show the calender dates in correct direction
        } else if (deviceCurrentLanguage == "ar" && Defaults[.langId] == "2")  {
            // Arabic Language Selected
            UIView.appearance().semanticContentAttribute = .forceRightToLeft // to show the calender dates in correct direction
        }
//        if Defaults[.langId] == "1"{
//            UIView.appearance().semanticContentAttribute = .forceLeftToRight // to show the calender dates in correct direction
//        }
//        calenderView.semanticContentAttribute = .forceLeftToRight
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Set layout directions
//        if Defaults[.langId] == "1"{
        if (deviceCurrentLanguage == "en" && Defaults[.langId] == "1"){
            // English Language Selected
            UIView.appearance().semanticContentAttribute = .forceRightToLeft  //Design is created for Ar =>default
        } else if (deviceCurrentLanguage == "ar" && Defaults[.langId] == "2")  {
            // Arabic Language Selected
            UIView.appearance().semanticContentAttribute = .forceLeftToRight // to show the calender dates in correct direction
        }
    }
    
    // MARK: - Class Custom Methods
    func setDateAndFormatter () {
        curDate = NSDate() as Date
        // Set CurDate to contain next date => client reqiremnet to disaple current date
        curDate = Calendar.current.date(byAdding: .day, value: 1, to: curDate!)

        formatter.dateFormat = "yyyy-MM-dd"
        
        // Set Required Params to Call API
        // Get Working Times for specific Current Date
        calenderView.select(curDate!)
        calendar(calenderView, didSelect: curDate!, at: FSCalendarMonthPosition.current)
//        print("date: \(curDate)")
    }
    
    func getWorkingTimeForDate(date : Date) {
        // Formatter For calling delivery times list
        formatter.dateFormat    = "yyyy-MM-dd"
        formatter.locale        = Locale.init(identifier: "en")
        selectedDate = formatter.string(from: date )
        
        // Formatter For being used in finishing Order
//        formatter.dateFormat = "yyyy-MM-dd" // "yyyy-MM-dd_HH-mm" //
        confirmOrderSelectedDate = formatter.string(from: date )
        
        // Initiated Calendar
        let calendar = Calendar.current
        
        //Fetch the day number in the week , week number in the year
        weekDayNumber   = "\(calendar.component(.weekday, from: date ))"
        
        startLoadDataFromApi()
    }
    
    func startLoadDataFromApi(){
        // Add Activity Indicator
        startLoading()
        
        // Load Data From API
        getDeliveryTimesApi(successClosure: {  (data) in
            if let dataModel = data as? DeliveryTimeResponse{
                self.deliveryTimesListResponse = dataModel
            }
            
            self.stopLoading()
        })
    }
    
    private func startLoading() {
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
    }
    
    private func stopLoading() {
        self.activity.stopAnimating()
        self.activity.isHidden = true
    }
    // MARK: - Class Actions
    
    @IBAction func btnSendData(_ sender: Any) {
        if selectedTimeId.isEmpty {
            // Validation Alert
            self.showDefaultAlert(title: "deliveryTimeRequired".localized(), message: nil)
        } else {
            // Return to Previous page
            performSegue(withIdentifier: "unwindFromCalenderView", sender: nil)
        }
        
    }
    
    // MARK: - TableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deliveryTimesListResponse?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: deliveryTimeCell  = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! deliveryTimeCell
        
        let row                     = deliveryTimesListResponse?.data?[indexPath.row]
        cell.cellLabel.text         = "\("between".localized()) \((row?.ctime1)!) \("and".localized()) \((row?.ctime2)!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell: deliveryTimeCell  = tableView.cellForRow(at: indexPath) as! deliveryTimeCell
        
        cell.cellLabel.textColor        = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.cellView.backgroundColor   = #colorLiteral(red: 0.6985644036, green: 0.4381865343, blue: 0.009566354181, alpha: 1)
        let row                         = deliveryTimesListResponse?.data?[indexPath.row]
        selectedTimeId                  = (row?.id!)!
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell: deliveryTimeCell  = tableView.cellForRow(at: indexPath) as! deliveryTimeCell
        
        cell.cellLabel.textColor        = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.cellView.backgroundColor   = #colorLiteral(red: 1, green: 0.5647058824, blue: 0, alpha: 1)
    }
    
    // MARK: - FSCalendarDataSource
    func minimumDate(for calendar: FSCalendar) -> Date {
        return curDate!
    }
    
    // MARK: - FSCalendarDelegate
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // Set Required Params to Call API
        // Get Working Times for specific Date
        getWorkingTimeForDate(date: date)
    }
    
    // MARK: - Api Connection
    func getDeliveryTimesApi(successClosure: @escaping SuccessClosure){
        Alamofire.request("https://www.instadeal.co/home/getTimes2/0/\(weekDayNumber)/\(selectedDate)?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = DeliveryTimeResponse(JSON: data)
                    successClosure(dataModel)
                } else {
                    successClosure([])
                }
            } else {
                successClosure([])
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
