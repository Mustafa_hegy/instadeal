//
//  MyAddressViewController.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/13/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Alamofire

class MyAddressViewController: UIViewController {
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var noAddressLabel: UILabel!
    var orders: [Product] = []
    var model: ProductDetailsResponse?
    var catsProductObject:CatsProducts?
    var productDetailsObject:ProductDetailsResponse?
//    var model: Any?
    var numberOfProducts:Int?
    var totalPrice: Double?
    var totalDelivery: Double?
    var gammaly = 0
    var totalDeliveryCost = 0.0
    
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var addAddressButton: UIButton!
    
    var addressModel: GetAddressResponse?{
        didSet{
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        handleTabBarVisibility(hide: true)
        /*
        if catsProductObject != nil {
            model?.id?              = catsProductObject!.id!
            model?.name?            = catsProductObject!.name!
            model?.photo?           = catsProductObject!.photo!
            model?.price?           = catsProductObject!.price!
            model?.discount?        = catsProductObject!.discount!
            model?.newPrice?        = catsProductObject!.newPrice!
            model?.favChecked?      = catsProductObject!.favChecked!
            model?.counts?          = catsProductObject!.counts!
            model?.company_id?      = catsProductObject!.companyId!
            model?.deliveryPrice?   = catsProductObject!.deliveryPrice!
        }
        */
//        addNavBar(isBack: true)
        addNavBarCart(isBack: true)
        activityIndicator.startAnimating()
        loadData()
        
    }
    
    func localizeUI(){
        pageTitleLabel.text     = "addresses".localized()
        noAddressLabel.text     = "noAddresses".localized()
        addAddressButton.setTitle("addNewAddress".localized(), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleTabBarVisibility(hide: true)
        
        loadData()
    }
    private func loadData() {
        getAddressesApi { (data) in
            if let model = data as? GetAddressResponse{
                if model.result == "true"{
                    self.addressModel = model
                        if self.addressModel?.data?.count == 0{
                            self.tableView.separatorStyle = .none
                            self.noAddressLabel.isHidden = false
                        }else {
                            self.tableView.separatorStyle = .singleLine
                            self.noAddressLabel.isHidden = true
                    }
                }
            }
            
            self.stopLoading()
        }
        
        
    }
    
    private func stopLoading() {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    func deleteAddress(addressId: String,at index: Int){
        deleteAddressApi(addressId: addressId, index: index)
    }
    
    func deleteAddressApi(addressId: String, index:Int){
        Alamofire.request("https://www.instadeal.co/home/deleteAddress/\(addressId)?lang=\(Defaults[.langId])", method: .get,  encoding: JSONEncoding.default)
            .responseJSON { response in
                if response.result.isSuccess{
                    self.showDefaultAlert(title: "تم حذف العنوان!", message: nil, actionBlock: {
                        self.addressModel?.data?.remove(at: index)
                        if self.addressModel?.data?.count == 0{
                            self.tableView.separatorStyle = .none
                            self.noAddressLabel.isHidden = false
                        }
                        self.tableView.reloadData()
                    })
                } else {
                    self.showDefaultAlert(title: "حاول مرة اخري!", message: nil, actionBlock: {
                    })
                }
                
        }
        
    }
    
    func getAddressesApi(successClosure: @escaping SuccessClosure){
        
//        let params: Parameters = [
//            "area": "\(Defaults[.areaId])"
//        ]
        
//        Alamofire.request("https://www.instadeal.co/home/getAddress/\(Defaults[.userId])",method: .get , encoding: JSONEncoding.default).responseJSON
//       , parameters: params
        Alamofire.request("https://www.instadeal.co/home/getAddresses/\(Defaults[.userId])?area=\(Defaults[.areaId])&lang=\(Defaults[.langId])",method: .get  , encoding: JSONEncoding.default)
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = GetAddressResponse(JSON: data)
                        self.tableView.separatorStyle = .singleLine
                        successClosure(dataModel!)
                    } else {
                        successClosure([])
                    }
                } else {
                    successClosure([])
                }
                
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let addressVC = segue.destination as? AddressViewController,
            let address = sender as? GetAddress {
            addressVC.editedData = address
            addressVC.edit = true
        }
    }
    
    
}



extension MyAddressViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell") as! MyAdressTableViewCell
        
        if let address = addressModel?.data![indexPath.row] {
            cell.configure(address)
            
            cell.editTapped = {
                self.performSegue(withIdentifier: "EditSegue", sender: address)
            }
            cell.deleteTapped = {
                if let id = address.id{
                     self.deleteAddress(addressId: id, at: indexPath.row)
                }
            }
            
            cell.deliveryTapped = {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let confirmOrdersVC = storyBoard.instantiateViewController(withIdentifier: "ConfirmOrdersVC") as! ConfirmOrderViewController
                confirmOrdersVC.address = address
                confirmOrdersVC.model = self.model
                
//                confirmOrdersVC.productDetailsObject = self.productDetailsObject
                confirmOrdersVC.catsProductObject = self.catsProductObject
                
                confirmOrdersVC.numberOfProducts = self.numberOfProducts
                confirmOrdersVC.orders = self.orders
                confirmOrdersVC.totalDelivery = self.totalDelivery
                confirmOrdersVC.totalPrice = self.totalPrice
                confirmOrdersVC.gmmaly = self.gammaly
                confirmOrdersVC.totalDeliveryCost = self.totalDeliveryCost
                self.navigationController?.pushViewController(confirmOrdersVC, animated: true)
            }
        }
        
        return cell
    }
    
    
}
