//
//  OptionCell.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 10/16/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit

class OptionCell: UICollectionViewCell {
    @IBOutlet weak var optionTitleLbl: UILabel!
}
