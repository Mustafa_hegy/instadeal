//
//  OffersViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/23/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import SwiftyUserDefaults

class OffersViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var offersTable: UITableView!
    var dataModel: GetOffersResponse?
    {
        didSet{
            offersTable.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Defaults[.prodId] = dataModel?.data![indexPath.row].id ?? ""
        Defaults[.flag] = true
//        let productVc : PrdouctDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productDetails") as! PrdouctDetailsViewController
//        let nav = UINavigationController(rootViewController: self)
//        self.navigationController?.ro
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : OffersTableCell = (tableView.dequeueReusableCell(withIdentifier: "OffersTableCell", for: indexPath) as? OffersTableCell)!
        // Mrzok Edit => change the offer image presented parameter path
        // Old Code - not showing offer image
//        cell.offersImage.sd_setImage(with: URL(string: dataModel?.data![indexPath.row].offer_big ?? ""), placeholderImage: nil)
        cell.offersImage.sd_setImage(with: URL(string: dataModel?.data![indexPath.row].photo ?? ""), placeholderImage: nil)
        return cell
    }
    
    func getOfferssApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getOffers?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = GetOffersResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBar(isBack: true)
        offersTable.tableFooterView = UIView()
        Defaults[.flag] = false
        getOfferssApi {[weak self] (data) in
            
            if let dataResponse = data as? GetOffersResponse{
                
                self?.dataModel = dataResponse
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
