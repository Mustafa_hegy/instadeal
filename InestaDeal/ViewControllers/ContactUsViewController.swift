//
//  ContactUsViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/24/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class ContactUsViewController: UIViewController {
    
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var pageTitleLabel2: UILabel!
    @IBOutlet weak var contactPhoneLabel: UILabel!
    @IBOutlet weak var contactEmailLabel: UILabel!
    @IBOutlet weak var commentsField: UITextField!
    @IBOutlet weak var mobileField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var sendBtnAction: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        getContactsApi { [unowned self] (data) in
            
            if let dataModel = data as? GetContactsResponse{
                self.email.text = dataModel.mail ?? ""
                self.phoneNumber.text = dataModel.mobile ?? ""
            }
        }
        addNavBar(isBack: true)
        
        // Do any additional setup after loading the view.
    }
    
    func localizeUI(){
        sendBtnAction.setTitle("send".localized(), for: .normal)
        pageTitleLabel.text         = "callUs".localized()
        pageTitleLabel2.text        = "contactWithUs".localized()
        contactPhoneLabel.text      = "mobileContact".localized() + " : "
        contactEmailLabel.text      = "emailContact".localized() + " : "
        nameField.placeholder       = "name".localized()
        mobileField.placeholder     = "mobileNumber".localized()
        commentsField.placeholder   = "message".localized()
        
        if Defaults[.langId] == "1" {
            contactPhoneLabel.textAlignment = .left
            contactEmailLabel.textAlignment = .left
            phoneNumber.textAlignment       = .left
            email.textAlignment             = .left
            nameField.textAlignment         = .left
            mobileField.textAlignment       = .left
            commentsField.textAlignment     = .left
        } else if Defaults[.langId] == "2" {
            contactPhoneLabel.textAlignment = .right
            contactEmailLabel.textAlignment = .right
            phoneNumber.textAlignment       = .right
            email.textAlignment             = .right
            nameField.textAlignment         = .right
            mobileField.textAlignment       = .right
            commentsField.textAlignment     = .right
        }
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        if nameField.isEmpty || mobileField.isEmpty || commentsField.isEmpty{
            showDefaultAlert(title: "allFieldsRequired".localized(), message: nil)
        }
        else{
            sendApi { (data) in
                
                if let dataModel = data as? BaseResponse{
                    
                    if dataModel.result == "true"
                    {
                        self.showDefaultAlert(title: "successSendMessage".localized(), message: nil)
                    }
                }
            }
        }
    }
    @IBAction func contactEmail(_ sender: UITapGestureRecognizer) {
        if let url = URL(string: "mailto:\(email.text ?? "")") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    @IBAction func contactPhone(_ sender: UITapGestureRecognizer) {
        
        if let url = URL(string: "tel://\(self.phoneNumber.text ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getContactsApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getContact?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = GetContactsResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
    func sendApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "mobile": mobileField.text ?? "" ,
            "text": commentsField.text ?? "" ,
            "name": nameField.text ?? "" ,
            "email": ""
        ]
        
        
        Alamofire.request("https://www.instadeal.co/home/addContact?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
    
    
    
}
