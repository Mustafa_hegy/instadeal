//
//  AddAddressViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/5/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class AddAddressViewController: UIViewController , UITextFieldDelegate , UIGestureRecognizerDelegate
{

    @IBOutlet weak var containingView: UIView!
    @IBOutlet weak var listAddressTable: UITableView!
    @IBOutlet weak var formTable: UITableView!
    var cell1: AddressTableViewCell?
    var name:String?
    var street:String?
    var house:String?
    var gada:String?
    var mobile:String?
    var notes:String?
    var block:String?
    var area:String?
    var floor:String?
    var room:String?
    var isAddressFilled: Bool = false
    var clearFormFirstTime: Bool = true
    var fieldDictionary = [Int : String]()
    var savedIndex: Int = 0
    {
        didSet{
            formTable.reloadData()
        }
    }
    var filledDataFlag: Bool = false
    var editFlag: Bool = false
    var addressModel: GetAddressResponse?
    {
        didSet{
            listAddressTable.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        containingView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight*2)
        listAddressTable.tableFooterView = UIView()
        self.addNavBar(isBack: true)
        
        // Do any additional setup after loading the view.
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

        
        editFlag = true
        switch textField.tag {
        case 11:
            
            name = textField.text
            fieldDictionary[0] = name
             Defaults[.addressName] = name ?? ""
        case 12:
            
            mobile = textField.text
            fieldDictionary[1] = mobile
        case 13:
            
            area = textField.text
            Defaults[.area] = area ?? ""
            fieldDictionary[2] = area
        case 14:
            
            block = textField.text
            Defaults[.ket3a] = block ?? ""
            fieldDictionary[3] = block
        case 15:
            
            street = textField.text
            Defaults[.street] = street ?? ""
            fieldDictionary[4] = street
        case 16:
            
            gada = textField.text
            Defaults[.gada] = gada ?? ""
            fieldDictionary[5] = gada
        case 17:
            
            house = textField.text
            Defaults[.benayaNum] = house ?? ""
            fieldDictionary[6] = house
        case 18:
            
            floor = textField.text
            Defaults[.floor] = floor ?? ""
            fieldDictionary[7] = floor
        case 19:
            
            room = textField.text
            Defaults[.flatNum] = room ?? ""
            fieldDictionary[8] = room
        case 20:
            
            notes = textField.text
            fieldDictionary[9] = notes
        default:
            break
        }
    }
    

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: listAddressTable))!{
            return false
        }
        return true
    }

    @IBAction func hideAddressTable(_ sender: UITapGestureRecognizer) {
        containingView.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleTabBarVisibility(hide: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showMyAddressAction(_ sender: UIButton) {
        containingView.isHidden = false
        getAddressesApi { (data) in
            
            if let model = data as? GetAddressResponse{
                if model.result == "true"{
                    self.addressModel = model
                    self.formTable.reloadData()
                    self.listAddressTable.reloadData()
                }
            }
            
            
        }
        
    }
    @IBAction func addAddressAtion(_ sender: UIButton) {
       
        view.endEditing(true)
        if (cell1?.textField.isEmpty)! && cell1?.textLabel?.tag != 8000{
            self.showDefaultAlert(title: "برجاء ملئ جميع الحقول", message: nil, actionBlock: {
                
            })
        }
        else{
            if isAddressFilled{
                performSegue(withIdentifier: "paymentSegue", sender: self)
                
            }
            else{
                
                addAddressApi { (data) in
                    if let dataJson = data as? [String : Any]{
                    let dataModel = BaseResponse(JSON: dataJson)
                    if let model = dataModel as? BaseResponse{
                        if model.result == "true"{
                            self.showDefaultAlert(title: model.message, message: nil, actionBlock: {
                                if let dat = dataJson as? [String : Any]{
                                     Defaults[.addressId] = dataJson["data"] as! String
                                }
                               
                                
                                self.performSegue(withIdentifier: "paymentSegue", sender: self)
                                
                                
                            })
                        }
                    }
                }
                }
            }
        }
            
    }

    
    
    func getAddressesApi(successClosure: @escaping SuccessClosure){
        
//        let params: Parameters = [
//            "area": "\(Defaults[.areaId])"
//        ]
        
////        Alamofire.request("https://www.instadeal.co/home/getAddress/\(Defaults[.userId])", method: .get,  encoding: JSONEncoding.default).responseJSON

//        Alamofire.request("https://www.instadeal.co/home/getAddresses/\(Defaults[.userId])",method: .get , parameters: params , encoding: JSONEncoding.default).responseJSON
        Alamofire.request("https://www.instadeal.co/home/getAddresses/\(Defaults[.userId])?area=\(Defaults[.areaId])&lang=\(Defaults[.langId])",method: .get  , encoding: JSONEncoding.default)
            .responseJSON{ response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = GetAddressResponse(JSON: data)
                        
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "paymentSegue"{
            if let VC = segue.destination as? CartListViewController{
                //VC.finalPay = true
            }
      
        }
    }
    func addAddressApi(successClosure: @escaping SuccessClosure){
        
        
        let params: Parameters = [
            "name": name ?? "",
            "street": street ?? "",
            "house": house ?? "",
            "gada": gada ?? "",
            "mobile": mobile ?? "",
            "desc": notes ?? "",
            "block": block ?? "",
            "client": Defaults[.userId] ,
            "area": area ?? "",
            "floor": floor ?? "",
            "room": room ?? ""
        ]
        
        
        Alamofire.request("https://www.instadeal.co/home/addAddress?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.httpBody , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
//                        let dataModel = BaseResponse(JSON: data)
                        successClosure(data)
                    }
                }
                
        }
        
    }


}

extension AddAddressViewController: UITableViewDataSource , UITableViewDelegate  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == formTable{
            return 12
        }
        else
        {
            return (addressModel?.data?.count ?? 0) - 1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == listAddressTable{
            containingView.isHidden = true
            
            if indexPath.row == 0{
                savedIndex = -1
                filledDataFlag = false
                editFlag = false
                self.isAddressFilled = false
                clearFormFirstTime = true
                Defaults[.addressId] = ""
                
                addressModel?.data?.removeAll()
                formTable.reloadData()
                
            }
            else{
                
                Defaults[.addressId] = addressModel?.data![indexPath.row - 1].id ?? ""
                filledDataFlag = true
                
                savedIndex = indexPath.row
                self.isAddressFilled = true
            }
            
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == formTable{
            if indexPath.row == 10{
                return 100
            }
            if indexPath.row == 11{
                return 50
            }
        }
        return 53
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == formTable{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myAddressesCell")
            cell1 = tableView.dequeueReusableCell(withIdentifier: "textFieldAddressCell") as? AddressTableViewCell
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "addressBtnCell")
            cell1?.textField.tag = indexPath.row + 10
            cell1?.textField.delegate = self
            
            if indexPath.row == 0{
                return cell ?? UITableViewCell()
            }
            if !filledDataFlag && clearFormFirstTime{
                cell1?.textField.clear()
                clearFormFirstTime = false
            }





            
            if indexPath.row == 1 {
                if !filledDataFlag {
                    
                    cell1?.textField.placeholder = "اسم العنوان"
                    cell1?.textField.keyboardType = .emailAddress
                    
                    
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil || filledDataFlag{
                        cell1?.textField.text = addressModel?.data![savedIndex - 1].name
                        Defaults[.addressName] = addressModel?.data![savedIndex - 1].name ?? ""
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
                
            }
            if indexPath.row == 2{
                cell1?.textField.keyboardType = .phonePad
                
                if !filledDataFlag{
                    cell1?.textField.text = mobile
                    cell1?.textField.placeholder = "رقم موبايل المستخدم"
                    
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil || filledDataFlag
                    {
                        cell1?.textField.text = addressModel?.data![savedIndex - 1].mobile
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
//                cell1?.textField.placeholder = filledDataFlag ? addressModel?.data![savedIndex - 1].mobile : "رقم موبايل المستخدم"
                
            }
            if indexPath.row == 3{
                
                if !filledDataFlag{
                    cell1?.textField.text = area
                    cell1?.textField.placeholder = "المنطقة"
                    cell1?.textField.keyboardType = .default
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil
                    {
                    cell1?.textField.text = addressModel?.data![savedIndex - 1].area
                     Defaults[.area] = addressModel?.data![savedIndex - 1].area ?? ""
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
                
            }
            if indexPath.row == 4{
                
                if !filledDataFlag{
                    cell1?.textField.text = block
                    cell1?.textField.placeholder = "قطعة"
                    cell1?.textField.keyboardType = .default
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil
                    {
                    cell1?.textField.text = addressModel?.data![savedIndex - 1].block
                     Defaults[.ket3a] = addressModel?.data![savedIndex - 1].block ?? ""
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
                
            }
            if indexPath.row == 5{
                
                if !filledDataFlag{
                    cell1?.textField.text = street
                    cell1?.textField.placeholder = "الشارع"
                    cell1?.textField.keyboardType = .default
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil
                    {
                    cell1?.textField.text = addressModel?.data![savedIndex - 1].street
                      Defaults[.street] = addressModel?.data![savedIndex - 1].street ?? ""
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
                
            }
            if indexPath.row == 6{
                if !filledDataFlag{
                    cell1?.textField.text = gada
                    cell1?.textField.placeholder = "جادة"
                    cell1?.textField.keyboardType = .default
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil
                    {
                    cell1?.textField.text = addressModel?.data![savedIndex - 1].gada
                     Defaults[.gada] = addressModel?.data![savedIndex - 1].gada ?? ""
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
               
            }
            if indexPath.row == 7{
                if !filledDataFlag{
                    cell1?.textField.text = house
                    cell1?.textField.placeholder = "البناية"
                    cell1?.textField.keyboardType = .default
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil
                    {
                    cell1?.textField.text = addressModel?.data![savedIndex - 1].house
                    Defaults[.benayaNum] = addressModel?.data![savedIndex - 1].house ?? ""
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
       
            }
            if indexPath.row == 8{
                cell1?.textField.keyboardType = .phonePad
                
                if !filledDataFlag{
                    cell1?.textField.text = floor
                    cell1?.textField.placeholder = "الطابق"
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil
                    {
                    cell1?.textField.text = addressModel?.data![savedIndex - 1].floor
                        Defaults[.floor] = addressModel?.data![savedIndex - 1].floor ?? ""
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
              
                
            }
            if indexPath.row == 9{
                cell1?.textField.keyboardType = .phonePad
                
                if !filledDataFlag{
                    cell1?.textField.text = room
                    cell1?.textField.placeholder =  "رقم الشقة او الغرفة"
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil
                    {
                    cell1?.textField.text = addressModel?.data![savedIndex - 1].room
                     Defaults[.flatNum] = addressModel?.data![savedIndex - 1].room ?? ""
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
               
                
            }
            if indexPath.row == 10{
                
                if !filledDataFlag{
                    cell?.textLabel?.tag = 8000
                    cell1?.textField.text = notes
                    cell1?.textField.placeholder =  "وصف إضافي"
                }
                else{
                    if !editFlag || fieldDictionary[indexPath.row - 1] == nil
                    {
                    cell1?.textField.text = addressModel?.data![savedIndex - 1].notes
                    }
                    else{
                        cell1?.textField.text = fieldDictionary[indexPath.row - 1]
                    }
                }
                
                
               
            }
            
            if indexPath.row == 11{
                return cell2!
            }
            
            return cell1 ?? UITableViewCell()
        }
        else
        {
               let cell = tableView.dequeueReusableCell(withIdentifier: "addressCellPopUp") as? ShowAddressTableViewCell
            if indexPath.row == 0{
                cell?.addressName.text = "إضافة عنوان جديد"
            }
            else{
             
                cell?.addressName.text = addressModel?.data![indexPath.row - 1].name
                
            }
            return cell ?? UITableViewCell()
        }
        
    }
    
    
}

