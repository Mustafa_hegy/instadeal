//
//  MainTabBarController.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 11/2/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tabBar.items?[0].title = "home".localized()
        tabBar.items?[1].title = "account".localized()
        tabBar.items?[2].title = "offers".localized()
        tabBar.items?[3].title = "search".localized()
        tabBar.items?[4].title = "categories".localized()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
