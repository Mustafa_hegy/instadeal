//
//  CompnyProductsViewController.swift
//  InestaDeal
//
//  Created by Waleed on 4/15/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyUserDefaults
import ImagePicker
import Alamofire
import TLPhotoPicker

class TagerCompanyProductsViewController: UIViewController, ImagePickerDelegate , TLPhotosPickerViewControllerDelegate , UIScrollViewDelegate {
    
    var selectedAssets = [TLPHAsset]()
    @IBOutlet weak var tabViewBottomConst: NSLayoutConstraint!
    @IBOutlet weak var instaView: UIWebView!
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.loader.startAnimating()
        imagePicker.dismiss(animated: true, completion: nil)
        let name = isThumb ? "photo" : "photo1"
        uploadProductPicsApi(with: name, image: images[0]) { (data) in
            
            self.loader.stopAnimating()
            self.showDefaultAlert(title: "successAddPhoto".localized(), message: nil)
        }
        
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    

    @IBOutlet weak var loader: UIActivityIndicatorView!
    var isThumb: Bool = true
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var ordersBtn: UIButton!
    @IBOutlet weak var productsBtn: UIButton!
    @IBOutlet weak var ordersBottomView: UIView!
    @IBOutlet weak var instaBottomView: UIView!
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var commentsTable: UITableView!
    @IBOutlet weak var tagerTable: UITableView!
    @IBOutlet weak var companyNameLbl: UILabel!
    
    @IBOutlet weak var companyTotalProducts: UILabel!
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var tabTable: UICollectionView!
    @IBOutlet weak var productTable: UICollectionView!
    @IBOutlet weak var infoBottomView: UIView!
    @IBOutlet weak var productsBottomView: UIView!
    @IBOutlet weak var evaluationBottomView: UIView!
    @IBOutlet weak var tagerProducts: UIButton!
    @IBOutlet weak var tagerInfo: UIButton!
    @IBOutlet weak var tagerEvaluation: UIButton!
    @IBOutlet weak var selectOptionLbl: UILabel!
    @IBOutlet weak var wantToDoLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
    
    var productModelView: ProductModelView? = ProductModelView()
    
    var flag: Bool = false
    var deletedProdIndex = -1
    var gradient: CAGradientLayer?
    var companyId: String?
    var catId: String?
    var pickheight: CGFloat = 0.0
    
    var companyComments: CommentsResponse?
    {
        didSet{
            //            productTable.reloadData()
//            commentsTable.reloadData()
            
        }
    }
    var companyDetails: CompanyDetailsResponse?
    
    {
        didSet{
//            productTable.reloadData()
            tabTable.reloadData()
//            tagerTable.reloadData()
//            commentsTable.reloadData()
            flag = true
            
        }
    }
    
    var companyCats: CompanyCatsResponse?
    {
        didSet{
            productTable.reloadData()
//            tagerTable.reloadData()
//            tabTable.reloadData()
        }
    }
    var modelView: TagerCompanyProdyctModelView?
    
    @IBAction func gestureAction(_ sender: UITapGestureRecognizer) {
        popUpView.isHidden = true
    }
    var imagePicker = ImagePickerController()
    var configuration = Configuration()
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        activity.center = view.center
        activity.color = #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
        productTable.contentInset.top = 10
        productTable.contentInset.bottom = 20
        configuration.allowMultiplePhotoSelection = false
        imagePicker = ImagePickerController(configuration: configuration)
        Defaults[.catSelectedAddProduct] = ""
        popUpView.isHidden = true
        
        commentsTable.rowHeight = UITableViewAutomaticDimension
        tagerTable.rowHeight = UITableViewAutomaticDimension
        
        commentsTable.tableFooterView = UIView()
        tagerTable.tableFooterView = UIView()
        
        commentsTable.isHidden = true
        productsBottomView.isHidden = false
//        infoBottomView.isHidden = true
//        evaluationBottomView.isHidden = true
        tagerTable.isHidden = true
        tagerTable.rowHeight = UITableViewAutomaticDimension
        setImageShadowLayer()
        
        modelView = TagerCompanyProdyctModelView(scene: self)
        addNavBar(isBack: true)
        modelView?.getCompanyCommentsApi(successClosure: { [unowned self](data) in
             if let dataModel = data as? CommentsResponse{
//                self.companyComments = dataModel
            }
        })
        modelView?.getCompanyDetailsApi(successClosure: { [unowned self](data) in
        
            if let dataModel = data as? CompanyDetailsResponse{
                
//                self.companyDetails = dataModel
                guard let url2 = URL(string: "https://www.instagram.com/" + (dataModel.instgram!.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ) else { return }
                
                 self.instaView.loadRequest((URLRequest(url: url2)))
                self.companyDetails = dataModel
                  self.companyImage.sd_setImage(with: URL(string: (dataModel.photo1) ?? ""), placeholderImage: #imageLiteral(resourceName: "iconApp"))
                
                self.logoImage.sd_setImage(with: URL(string: (dataModel.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "iconApp"))
                self.companyNameLbl.text = dataModel.name
                self.companyTotalProducts.text = "\(dataModel.product_counts ?? 0) \("product".localized())"
                Defaults[.deliveryFees] = dataModel.delivery_price ?? ""
                self.modelView?.getCatsProductApiFirst(successClosure: { [weak self](data) in
                    
                    if let dataModel = data as? CompanyCatsResponse{
                        self?.companyCats = dataModel
                        self?.activity.stopAnimating()
                        self?.activity.isHidden = true
                    }
                    
                })
               
            }
        })
        tabTable.transform = CGAffineTransform(scaleX: -1, y: 1)

        // Do any additional setup after loading the view.
        
        
    }
    
    func localizeUI(){
        ordersBtn.setTitle("orders".localized(), for: .normal)
        productsBtn.setTitle("products".localized(), for: .normal)
        tagerInfo.setTitle("settings".localized(), for: .normal)
        tagerProducts.setTitle("addProduct".localized(), for: .normal)
        editBtn.setTitle("edit".localized(), for: .normal)
        deleteBtn.setTitle("delete".localized(), for: .normal)
        selectOptionLbl.text    = "L_selectOption".localized()
        wantToDoLbl.text        = "L_wantToDo".localized()
    }
    
    func uploadProductPicsApi(with name: String , image: UIImage , successClosure: @escaping SuccessClosure){
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            if let imageData = UIImageJPEGRepresentation(image, 0.6)?.base64EncodedData() {
                multipartFormData.append(imageData, withName: name)}
            
            
        },
                         
                         to:"https://www.instadeal.co/home/updateCompanyPhotos/\(Defaults[.tagerCompanyId] )",
            method:.post,
            headers: [:],
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                    
                        
                    })
                    upload.responseString { response in
                        debugPrint(response)
                        if response.error == nil{
                            
                        }
                        successClosure(response)
                        
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
        
        
    }

//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        productTable.reloadData()
//    }
    
    @IBAction func deleteActionBtn(_ sender: UIButton) {
        modelView?.deleteProductApi(successClosure: { (data) in
           
                self.popUpView.isHidden = true
            
            if self.deletedProdIndex != -1{
                self.companyCats?.data?.remove(at: self.deletedProdIndex)
                self.productTable.reloadData()
            }
        })
    }
    @IBAction func popUpActionBtn(_ sender: UIButton) {
        self.popUpView.isHidden = true
      
    }
    @IBAction func mainTabsBtnsAction(_ sender: UIButton) {
        let ordersVC :UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ordersVC")
        if sender.tag == 1{
            productsBottomView.isHidden = false
            ordersBottomView.isHidden = true
            instaBottomView.isHidden = true
            instaView.isHidden = true
            self.view.viewWithTag(33333)?.removeFromSuperview()
            

        }
        
        if sender.tag == 0{
            productsBottomView.isHidden = true
            ordersBottomView.isHidden = false
            instaBottomView.isHidden = true
            instaView.isHidden = true
            self.addChildViewController(ordersVC)
            ordersVC.view.frame = CGRect(x: instaView.frame.origin.x, y: instaView.frame.origin.y, width: instaView.frame.width, height: instaView.frame.height)
            ordersVC.view.tag = 33333
            self.view.addSubview(ordersVC.view)
            ordersVC.didMove(toParentViewController: self)

        }
        
        if sender.tag == 2{
            
            
            productsBottomView.isHidden = true
            ordersBottomView.isHidden = true
            instaBottomView.isHidden = false
            instaView.isHidden = false
            self.view.viewWithTag(33333)?.removeFromSuperview()

        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ordersAction(_ sender: UIButton) {
    }
    @IBAction func productsAction(_ sender: UIButton) {
    }
    func setImageShadowLayer(){
        let view = UIView(frame: companyImage.frame)
        
        gradient = CAGradientLayer()
        
        gradient?.frame = CGRect(x: 0, y: 0, width: companyImage.frame.width * 2, height: companyImage.frame.height)
        
        gradient?.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        
        gradient?.locations = [0.0, 1.0]
        
        view.layer.insertSublayer(gradient!, at: 0)
        
        companyImage.addSubview(view)
        view.alpha = 0.5
        companyImage.bringSubview(toFront: view)
        
    }
    

    @IBAction func addCartAction(_ sender: UIButton) {
        
        performSegue(withIdentifier: "goProdDetailsSegue", sender: sender)
        Defaults[.prodId] = companyCats?.data![sender.tag].id ?? ""
        
    }
    
    @IBAction func thumbnileImgGesture(_ sender: UITapGestureRecognizer) {
//        isThumb = true
//        addAlert()
    }
    
    
    @IBAction func changeCoverImg(_ sender: UITapGestureRecognizer) {
//        isThumb = false
//        addAlert()
        
    }
    
    func addAlert(){
        let alert = UIAlertController(title: "Choose type" , message: "Select image" , preferredStyle: .actionSheet);
        
        let photos = UIAlertAction(title : "Camera" , style: .default , handler: { (alert: UIAlertAction ) in
            
            self.openCamera()
            
        })
        
        let videos = UIAlertAction(title : "Gallery" , style: .default , handler: { (alert: UIAlertAction ) in
            
            self.openGallery()
            
        })
        
        
        
        let cancel = UIAlertAction(title : "Cancel" , style: .default , handler: nil)
        
        alert.addAction(photos)
        
        alert.addAction(videos)
        
        alert.addAction(cancel)
        
        present(alert, animated: true , completion: nil)
    }
    
    
    func openCamera () {
        
        
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.delegate = self
            
            imagePicker.delegate = self
            imagePicker.topView.isHidden = false
            imagePicker.topView.isHidden = false
            imagePicker.imageLimit = 6
            imagePicker.imageLimit = 6
            
            
            
            present(imagePicker, animated: true, completion: nil)
            
            
            
        }else{
            
            print("Camera not awailable")
            
        }
        
    }
    
    
    
    func openGallery() {
        
        
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            
            imagePicker.delegate = self
            
            imagePicker.imageLimit = 6
            imagePicker.expandGalleryView()
            imagePicker.cameraNotAvailable()
            imagePicker.imageToLibrary()
            
            present(imagePicker, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editProduct"
        {
            let destinationVC = segue.destination as! AddProductViewController
            
            destinationVC.flagEdit = true
            
            
        }
    }
    
}

extension TagerCompanyProductsViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == tabTable{
            
            let cell = tabTable.cellForItem(at: indexPath) as? TabViewCell
            for tempCell in collectionView.visibleCells as! [TabViewCell] {
                // do something
                
                tempCell.bottomView.isHidden = true
            }
            cell?.bottomView.isHidden = false
            
            
            if indexPath.row == 0{
                catId = "0"
            }
            else{
                catId = companyDetails?.cats![indexPath.row - 1].id
            }

            modelView?.getCatsProductApi(successClosure: { (data) in
                
                if let dataModel = data as? CompanyCatsResponse{
                    self.companyCats = dataModel
                }
                
            })
        }
        else if collectionView == productTable{
            
            Defaults[.tagerProdId] = companyCats?.data![indexPath.row].id ?? ""
            deletedProdIndex = indexPath.row
            popUpView.isHidden = false
            
            
        }
    }
    
}
extension TagerCompanyProductsViewController: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.productTable{
            return companyCats?.data?.count ?? 0
        }
        else{
            return (companyDetails?.cats?.count ?? 0) + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == productTable{
//            return CGSize(width: collectionView.frame.width / 2, height: 220)
            return CGSize(width: (collectionView.frame.width - 24) / 2, height: 274)
        } else {
            return CGSize(width: 107, height: 33)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TabViewCell?
        let cell2: CatsProductCell?
        
        if collectionView == self.tabTable{
            
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tabCell", for: indexPath) as? TabViewCell
            cell?.bottomView.isHidden = true
            if indexPath.row == 0{
                cell?.cellLabel.text = "allCategories".localized()
            }
            else{
            
            cell?.cellLabel.text = companyDetails?.cats![indexPath.row - 1].name
            }
            
            cell?.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
            return cell!
        }
        
        else{
            
            cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as? CatsProductCell
            
            let rowData  = companyCats?.data![indexPath.row]
            
            cell2?.productHead.text  = rowData!.name
            cell2?.delivery.text     = "\(rowData!.newPrice ?? 0) \("currency".localized())"
            
            if rowData!.photo == "" { // rowData.photo!.isEmptyrowData.photo!.isEmpty
                cell2?.productImage.image = #imageLiteral(resourceName: "instaLogo")
            } else {
                cell2?.productImage.sd_setImage(with: URL(string: (rowData!.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "instaLogo"))
            }
            
            cell2?.addCartBtn.tag    = indexPath.row
            
            print("price: \(rowData!.newPrice)")
            
            if let fav = rowData!.favChecked {
                if fav == 1 {
                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                } else {
                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                }
            }
            
            if let discount = Int(rowData!.discount ?? "0") {
                if discount > 0 {
                    cell2?.discountPercentage.isHidden   = false
                    cell2?.discountPercentage.text       = "\("discountKey".localized()) \(rowData!.discount ?? "0") %"
                } else {
                    cell2?.discountPercentage.isHidden = true
                }
            }
            
            if let gmma3 = Int(rowData!.gmmaly ?? "0") { 
                if gmma3 == 1 {
                    cell2?.gmma3lyIcon.isHidden = false
                } else {
                    cell2?.gmma3lyIcon.isHidden = true
                }
            }
            
            if let freeShipping = rowData!.freeDelivery {
                if freeShipping == 1 {
                    cell2?.shippingCostLabel.isHidden = false
                } else {
                    cell2?.shippingCostLabel.isHidden = true
                }
            }
            
            cell2?.productFavAction = {
                if Defaults[.logged]{
                    // Load Data From API
                    Defaults[.prodId] = rowData!.id ?? ""
                    
                    //                    self.startLoading()
                    self.productModelView?.addToFavApi { (data) in
                        
                        //                        self.stopLoading()
                        
                        if let dataModel = data as? BaseResponse{
                            if dataModel.result == "true"
                            {
                                if dataModel.message == "تمت الاضافة للمفضلة"
                                {
                                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                                } else {
                                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                                }
                            }
                            self.view.layoutSubviews()
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                else{
                    self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
                }
            }
            
            cell2?.productAddCartAction = {
                Defaults[.prodId] = rowData!.id ?? ""
                let requiredQuantity: Int   = 1
                let existCountity: Int      = Int(rowData!.counts ?? "0") ?? 0
                if requiredQuantity <= existCountity && requiredQuantity != 0 {
                    if !Defaults[.logged]{
                        self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
                    }
                    else{
                        // Edit Code => Add to cart first and then move to Addresses List
                        
                        //                        self.startLoading()
                        self.productModelView?.addToCartApi(price: rowData!.price ?? "0", count: requiredQuantity /* => required quantity */ ) { (data) in
                            
                            //                            self.stopLoading()
                            
                            if let dataModel = data as? BaseResponse{
                                
                                if dataModel.message == "تم الاضافة للسلة" {
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let myAddressVC = storyBoard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressViewController
                                    myAddressVC.catsProductObject = rowData
                                    myAddressVC.numberOfProducts = requiredQuantity
                                    Defaults[.numberOfProducts] = "\(requiredQuantity)"
                                    self.navigationController?.pushViewController(myAddressVC, animated: true)
                                    //performSegue(withIdentifier: "directBuy", sender: self)
                                }
                            }
                        }
                    }
                }
                else{
                    self.showDefaultAlert(title: "noEnoughQuantity".localized(), message: nil)
                }
            }
            
            if let newPrice = rowData!.newPrice{
                if let oldPrice = rowData!.price{
                    if Double(newPrice) == Double(oldPrice){
                        cell2?.oldPrice.isHidden = true
                    } else {
                        cell2?.oldPrice.isHidden = false
                        cell2?.oldPrice.text = "\(oldPrice) \("currency".localized())"
                    }
                }
            }
            return cell2!
            
        }
        /*
        else{
            cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as? CatsProductCell
            
            let rowData  = companyCats?.data![indexPath.row]
            
            cell2?.productHead.text = rowData!.name
            cell2?.delivery.text = "\(rowData!.newPrice ?? 0)  \("currency".localized())"
            cell2?.productImage.sd_setImage(with: URL(string: (rowData!.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "iconApp"))
            cell2?.addCartBtn.tag = indexPath.row
            cell2?.productHead.text = rowData!.name
//            cell2?.contentView.layer.cornerRadius = 4
//            cell2?.contentView.layer.borderWidth = 1
//            cell2?.contentView.layer.borderColor = UIColor.gray.cgColor
            
            if let fav = rowData!.favChecked {
                if fav == 1 {
                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                } else {
                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                }
            }
            
            cell2?.productFavAction = {
                if Defaults[.logged]{
                    // Load Data From API
                    Defaults[.prodId] = rowData!.id ?? ""
                    
                    self.activity.startAnimating()
                    self.view.addSubview(self.activity)
                    
                    self.productModelView?.addToFavApi { (data) in
                        
                        self.activity.stopAnimating()
                        self.activity.isHidden = true
                        
                        if let dataModel = data as? BaseResponse{
                            if dataModel.result == "true"
                            {
                                if dataModel.message == "addedFav".localized()
                                {
                                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                                } else {
                                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                                }
                            }
                            self.view.layoutSubviews()
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                else{
                    self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
                }
            }
            
            cell2?.productAddCartAction = {
                Defaults[.prodId] = rowData!.id ?? ""
                let requiredQuantity: Int   = 1
                let existCountity: Int      = Int(rowData!.counts ?? "0") ?? 0
                if requiredQuantity <= existCountity && requiredQuantity != 0 {
                    if !Defaults[.logged]{
                        self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
                    }
                    else{
                        // Edit Code => Add to cart first and then move to Addresses List
                        
                        self.loader.startAnimating()
                        self.productModelView?.addToCartApi(price: rowData!.price ?? "0", count: requiredQuantity /* => required quantity */ ) { (data) in
                            
                            self.loader.stopAnimating()
                            
                            if let dataModel = data as? BaseResponse{
                                
                                if dataModel.message == "تم الاضافة للسلة" {
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let myAddressVC = storyBoard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressViewController
                                    myAddressVC.catsProductObject = rowData!
                                    myAddressVC.numberOfProducts = requiredQuantity
                                    Defaults[.numberOfProducts] = "\(requiredQuantity)"
                                    self.navigationController?.pushViewController(myAddressVC, animated: true)
                                    //performSegue(withIdentifier: "directBuy", sender: self)
                                }
                            }
                        }
                    }
                }
                else{
                    self.showDefaultAlert(title: "noEnoughQuantity".localized(), message: nil)
                }
            }
            
            if let newPrice = rowData!.newPrice{
                if let oldPrice = rowData!.price{
                    if Double(newPrice) == Double(oldPrice){
                        cell2?.oldPrice.isHidden = true
                    } else {
                        cell2?.oldPrice.isHidden = false
                        cell2?.oldPrice.text = "\(oldPrice)  \("currency".localized())"
                    }
                }
                
            }
            return cell2!
        }
         */
    }
    
}

extension TagerCompanyProductsViewController: UITableViewDelegate{
    
}

extension TagerCompanyProductsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tagerTable{
            return 6
        }
        if tableView == commentsTable{
            return companyComments?.data?.count ?? 0
        }
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tagerTable{
            let cell = tableView.dequeueReusableCell(withIdentifier: "tagerCell0", for: indexPath) as? tagerCell0
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "tagerCell1", for: indexPath) as? tagerCell1
            
            if indexPath.row == 0  {
                
                
                cell?.descBodyLbl.text = "breif".localized() + "\n\n" + (companyDetails?.feedData ?? "")
                
                let myString:NSString = (cell?.descBodyLbl.text as? NSString)!
                var myMutableString = NSMutableAttributedString()
                myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedStringKey.font:UIFont(name: "DroidArabicKufi-Bold", size: 11.0)!])
                
                myMutableString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "DroidArabicKufi-Bold", size: 13.0)!, range: NSRange(location:0,length: 12))
                
                    // Edit Code
                myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: NSRange(location:13,length:((cell?.descBodyLbl.text?.count)! - 13)))
                    // Old Code
//                myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: NSRange(location:13,length:((cell?.descBodyLbl.text?.length)! - 13)))
                
                myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:12))
                cell?.descBodyLbl.attributedText = myMutableString
                return cell!
            }
            
            
                if indexPath.row == 1{
                    cell1?.cell2HeadLbl.text = "specialization".localized()
                    cell1?.cell2BodyLbl.text = companyDetails?.catsName
                }
                if indexPath.row == 2{
                    cell1?.cell2HeadLbl.text = "workHours".localized()
                    cell1?.cell2BodyLbl.text = "\("from".localized()) \(companyDetails?.workFrom ?? "") \("to".localized()) \(companyDetails?.workTo ?? "") "
                    
                }
                
                if indexPath.row == 3{
                    cell1?.cell2HeadLbl.text = "deliveryPeriod".localized()
                    cell1?.cell2BodyLbl.text = companyDetails?.delivery_time ?? ""
                }
                if indexPath.row == 4{
                    cell1?.cell2HeadLbl.text = "deliveryCost".localized()
                    cell1?.cell2BodyLbl.text = "\(companyDetails?.delivery_price ?? "")  \("currency".localized())"
                }
                
                if indexPath.row == 5{
                    cell1?.cell2HeadLbl.text = "payType".localized()
                    cell1?.cell2BodyLbl.text = companyDetails?.pay_name ?? ""
                }
                
                return cell1!
            
        }
        if tableView == commentsTable{
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "tagerCellEvaluation", for: indexPath) as? tagerCellEvaluation
            cell2?.dateLbl.text = companyComments?.data![indexPath.row].cdate
            cell2?.userName.text = companyComments?.data![indexPath.row].name
            cell2?.commect.text =  companyComments?.data![indexPath.row].comment ?? ""
            cell2?.rateView.rating = Double(companyComments?.data![indexPath.row].rate ?? "") ?? 0.0
            
            return cell2!
        }
        return UITableViewCell()
    }
    

    
    
}
