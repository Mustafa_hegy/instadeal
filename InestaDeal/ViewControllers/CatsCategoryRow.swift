//
//  CategoryRow.swift
//  TwoDirectionalScroller
//
//  Created by Robert Chen on 7/11/15.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit
import SwifterSwift
import SwiftyUserDefaults

class CatsCategoryRow : UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableCellHeaderLbl: UILabel!
    
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var moreLabel: UILabel!
    
    var subCategoryId: String?
    var categories: CategoriesResponse?
    {
        didSet{
            moreLabel.text = "more".localized()
            collectionView.reloadData()
            collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    var section: Int = 0
    
    @IBAction func moreButtonAction(_ sender: Any) {
        
        self.parentViewController?.performSegue(withIdentifier: "catProductsSeg", sender: self) // New Code
        Defaults[.parentCategoryId] = (categories?.data![section].id)!
    }
}

extension CatsCategoryRow : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("counts:\(categories?.data![self.section].childs?.count)")
        return categories?.data![self.section].childs?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
        cell.imageView.sd_setImage(with: URL(string: (categories?.data![section].childs![indexPath.row].photo) ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.subCategoryLbl.text = categories?.data![section].childs![indexPath.row].name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if Defaults[.logged]{
//            self.parentViewController?.performSegue(withIdentifier: "CatsCompanySeg", sender: self)

            self.parentViewController?.performSegue(withIdentifier: "catProductsSeg", sender: self) // New Code
        }
        else{
            self.parentViewController?.performSegue(withIdentifier: "CatsgoLogin", sender: self)
            
        }
       subCategoryId = categories?.data![section].childs![indexPath.row].id
        Defaults[.subCategoryId] = subCategoryId ?? ""
        Defaults[.parentCategoryId] = (categories?.data![section].childs![indexPath.row].parent)!
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.backgroundColor = appThemeColor
    }
    
}

extension CatsCategoryRow : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 4
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
}

