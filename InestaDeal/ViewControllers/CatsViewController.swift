//
//  FirstViewController.swift
//  InestaDeal
//
//  Created by Waleed on 3/31/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import ImageSlideshow
import SwifterSwift
import SDWebImage
import AlamofireImage
import SwiftyUserDefaults

class CatsViewController: UIViewController {
    
//    @IBOutlet weak var searchField: UISearchBar!
    var subCategoryId: String?
    var modelView: HomeModelView?
     var imgsArray = [AlamofireSource]()
    var categories: CategoriesResponse?
    {
        didSet{
            containerTable.reloadData()
        }
    }
    @IBOutlet weak var containerTable: UITableView!
    @IBOutlet weak var slideShowView: ImageSlideshow!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.layer.zPosition = 0
        modelView = HomeModelView(scene: self)
        containerTable.tableFooterView = UIView()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
//        slideShowView.addGestureRecognizer(gestureRecognizer)
        modelView?.getCategoriesApi(successClosure: { [unowned self] (data) in
            
            if let dataModel = data as? CategoriesResponse{
                
                self.categories = dataModel
                
            }
            
        })
        addNavBar(isBack: true)
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.imgsArray.removeAll()
        modelView?.getCategoriesApi(successClosure: { [unowned self] (data) in
            
            if let dataModel = data as? CategoriesResponse{
                
                self.categories = dataModel
                
            }
            
        })
 
//        slideShowView.pageControl.pageIndicatorTintColor =  UIColor(hex: 0xF08944)
//        slideShowView.contentScaleMode = .scaleAspectFill
//        modelView?.getHomeSliderApi(successClosure: { [unowned self] (data) in
//
//            if let dataModel = data as? SlidersResponse{
//                print("dattta:\(dataModel)")
//                for index in dataModel.data ?? []{
//                    print("index:\(index.photo ?? "")")
//                    self.imgsArray.append(AlamofireSource(urlString: index.photo ?? "")!)
//                }
//
//                self.slideShowView.setImageInputs(self.imgsArray)
//
//                }
//
//        })
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.hidesBottomBarWhenPushed = false
        if segue.identifier == "catsmoreSubCatSeg"{
            Defaults[.flag] = true
            let destinationVC = segue.destination as! SubCategoriesViewController
            destinationVC.parentCategoryId = "\((sender as! UIButton).tag)"
             destinationVC.parentCategoryId = categories?.data![(sender as! UIButton).tag].id
            destinationVC.categoryName = categories?.data![(sender as! UIButton).tag].name
        }
        if segue.identifier == "CatsCompanySeg"{
            
            let destinationVC = segue.destination as! CompanyViewController
            destinationVC.subCategoryId = subCategoryId
        }
        if segue.identifier == "catProductsSeg"{
            
            let destinationVC = segue.destination as! ProductsListViewController
            destinationVC.subCategoryId = subCategoryId
        }
    }
    @objc func didTap() {
//        slideShowView.presentFullScreenController(from: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension CatsViewController : UITableViewDelegate { }

extension CatsViewController : UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return categories[section]
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
//    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
//        header.textLabel?.font = UIFont(name: "YourFontname", size: 14.0)
//        header.textLabel?.textAlignment = NSTextAlignment.right
//        header.backgroundView?.backgroundColor = .clear
//    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CatsCategoryRow
        cell.tableCellHeaderLbl.text = categories?.data![indexPath.row].name
        cell.categories = categories
        cell.section = indexPath.row
        subCategoryId = cell.subCategoryId
        cell.moreBtn.tag = indexPath.row//Int(categories?.data![indexPath.row].id ?? "0")!
        return cell
    }
    

    
}

