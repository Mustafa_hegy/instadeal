//
//  OrdersViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/17/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults
class OrdersViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
   
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var ordersTable: UITableView!
    
    @IBOutlet weak var popUpBtnCall: UIButton!
    @IBOutlet weak var popUpBtnShow: UIButton!
    @IBOutlet weak var popUpBtnPrepare: UIButton!
    @IBOutlet weak var popUpBtnDelivery: UIButton!
    @IBOutlet weak var popUpBtnFinished: UIButton!
    @IBOutlet weak var popUpSeperator1: UIView!
    @IBOutlet weak var popUpSeperator2: UIView!
    @IBOutlet weak var popUpSeperator3: UIView!
    @IBOutlet weak var popUpSeperator4: UIView!
    
    @IBOutlet weak var selectOptionLbl: UILabel!
    @IBOutlet weak var wantToDoLbl: UILabel!
    
    let activity            = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var index : IndexPath?
    var orders: OrdersResponse?
    {
        didSet{
            ordersTable.reloadData()
        }
    }

    @IBAction func showOrderDetails(_ sender: UIButton) {
        let gesture = UITapGestureRecognizer.init()
        dismissPopupView(gesture)
        
        self.performSegue(withIdentifier: "OrderDetailsSegue", sender: orders?.data![(index?.row)!])
        /*
        // Old Code
         let linkViewController: LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
        print("https://www.instadeal.co/home/showOrders/\(orders?.data![(index?.row)!].client_id ?? "")/\(orders?.data![(index?.row)!].company_id ?? "").html")
        linkViewController.url = "https://www.instadeal.co/home/showOrders/\(orders?.data![(index?.row)!].id ?? "")/\(orders?.data![(index?.row)!].company_id ?? "").html"
        
        self.navigationController?.show(linkViewController, sender: sender)
         */
    }
    @IBAction func contactClient(_ sender: UIButton) {
        if let url = URL(string: "tel://\(orders?.data![(index?.row)!].client_mobile ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func dismissPopupView(_ sender: UITapGestureRecognizer) {
        popUpView.isHidden = true
        
        // Show all Pop Up Buttons
        popUpBtnCall.isHidden       = false
        popUpBtnPrepare.isHidden    = false
        popUpBtnDelivery.isHidden   = false
        popUpBtnFinished.isHidden   = false
        popUpSeperator1.isHidden    = false
        popUpSeperator3.isHidden    = false
        popUpSeperator4.isHidden    = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        popUpView.isHidden = true
        // Do any additional setup after loading the view.
        
        loadDataThroughInternet()
    }
    
    func localizeUI(){
        popUpBtnCall.setTitle("call".localized(), for: .normal)
        popUpBtnShow.setTitle("show".localized(), for: .normal)
        popUpBtnPrepare.setTitle("prepareProduct".localized(), for: .normal)
        popUpBtnDelivery.setTitle("deliveryService".localized(), for: .normal)
        popUpBtnFinished.setTitle("deliveryDone".localized(), for: .normal)
        selectOptionLbl.text    = "L_selectOption".localized()
        wantToDoLbl.text        = "L_wantToDo".localized()
    }
    
    func loadDataThroughInternet () {
        getCompanyOrdersApi { (data) in
            
            if let dataModel = data as? OrdersResponse{
                
                self.orders = dataModel
            }
        }
    }
    @IBAction func prepareProduct(_ sender: Any) {
        setOrderPrepareApi { (data) in
            
            self.showDefaultAlert(title: "preparing".localized(), message: nil)
            self.popUpView.isHidden = true
        }
    }
    
    @IBAction func deliveryService(_ sender: Any) {
        setOrderDeliveryApi { (data) in
            
            self.showDefaultAlert(title: "delivering".localized(), message: nil)
            self.popUpView.isHidden = true
        }
    }
    @IBAction func deliverIsFinish(_ sender: UIButton) {
        setOrderDoneApi { (data) in
            
            self.showDefaultAlert(title: "orderDeliverd".localized(), message: nil)
            self.popUpView.isHidden = true
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getCompanyOrdersApi(successClosure: @escaping SuccessClosure){
        print("getCompanyOrdersApi: https://www.instadeal.co/home/getCompanyOrders/\(Defaults[.tagerCompanyId] )")
        Alamofire.request("https://www.instadeal.co/home/getCompanyOrders/\(Defaults[.tagerCompanyId] )?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = OrdersResponse(JSON: data)
                    successClosure(dataModel)
                }
            }
        }
        
    }
    func setOrderDoneApi(successClosure: @escaping SuccessClosure){
        
        // Add Activity Indicator
        startLoading()
        Alamofire.request("https://www.instadeal.co/home/setOrderDone/\((orders?.data![(index?.row)!].id)!)/\(Defaults[.tagerCompanyId])?lang=\(Defaults[.langId])", method: .get).response { response in
            
            
            if let json = response.response {
                //                if let data = json as? [String : Any]{
                //                    let dataModel = OrdersResponse(JSON: data)
                successClosure(json)
                self.stopLoading()
                
                self.loadDataThroughInternet()
                //                }
                //            }
            }
        }
        
    }
    
    func setOrderDeliveryApi(successClosure: @escaping SuccessClosure){
        
        // Add Activity Indicator
        startLoading()
        Alamofire.request("https://www.instadeal.co/home/setDeliveryOrder/\((orders?.data![(index?.row)!].id)!)?lang=\(Defaults[.langId])", method: .get).response { response in
            
            if let json = response.response {
                //                if let data = json as? [String : Any]{
                //                    let dataModel = OrdersResponse(JSON: data)
                successClosure(json)
                self.stopLoading()
                
                self.loadDataThroughInternet()
                //                }
                //            }
            }
        }
    }
    
    func setOrderPrepareApi(successClosure: @escaping SuccessClosure){
        
        // Add Activity Indicator
        startLoading()
        Alamofire.request("https://www.instadeal.co/home/setReadyOrder/\((orders?.data![(index?.row)!].id)!)?lang=\(Defaults[.langId])", method: .get).response { response in
            
            if let json = response.response {
                //                if let data = json as? [String : Any]{
                //                    let dataModel = OrdersResponse(JSON: data)
                successClosure(json)
                self.stopLoading()
                
                self.loadDataThroughInternet()
                //                }
                //            }
            }
        }
    }
    
    //MARL: - Table Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (orders?.data?.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell: TagerOrdersCells = tableView.dequeueReusableCell(withIdentifier: "TagerOrdersCells", for: indexPath) as! TagerOrdersCells
        
        let rowData                     = orders?.data![indexPath.row]
        cell.dateTime.text              = rowData?.date!
        cell.itemImage.sd_setImage(with: URL(string: (rowData?.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "iconApp"))
        cell.itemTitle.text             = rowData?.product_name
        cell.currentStatusLable.text    = "\(rowData?.status_text ?? "")\n\(rowData?.price ?? "") \("currency".localized()) \n\(rowData?.gamm3 == "1" ? "instaGmma3ly".localized() : "direct".localized() ) \n\(rowData?.deliveryTime ?? "" )"
        cell.headTitlesLbl.text         = "\("status".localized()) :\n\("price".localized()) :\n\("deliverBy".localized()) :\n\("deliveryTime".localized()) :"
        cell.qtyTitleLbl.text           = "quantity".localized()
        cell.quantityLable.text         = rowData?.count
        cell.showOptionsLbl.text        = "showOptions".localized()
        // Stepper Status
        setStepperStatusForView(cell: cell, status: rowData?.status, statusText: rowData?.status_text)
        
        /*
        // Old Code
        cell.deliverDoneView.isHidden = true
        cell.companyName.text = orders?.data![indexPath.row].company_name
        cell.orderNumber.text = orders?.data![indexPath.row].id
        cell.totalPrice.text = (orders?.data![indexPath.row].sum ?? "0.0") + " KD"
        cell.dateTime.text = (orders?.data![indexPath.row].odate ?? "") + " " + (orders?.data![indexPath.row].otime ?? "")
        cell.isUserInteractionEnabled = true
        if orders?.data![indexPath.row].status == "2"{
            cell.deliverDoneView.isHidden = false
            //cell.isUserInteractionEnabled = true
        }
        
        if orders?.data![indexPath.row].gammaly == "0"{
          cell.gammalyView.isHidden = true
          cell.deliveryCost.text = (orders?.data![indexPath.row].deliveryCost ?? "0.0") + " KD"
        } else {
            cell.gammalyView.isHidden = false
            cell.deliveryCost.text = "جمع لى"
        }
         */
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath
        popUpView.isHidden  = false
        
        let rowData         = orders?.data![indexPath.row]
        Defaults[.orderId]  = rowData?.id ?? ""
//        companyName.text    = "( اسم الشركة :" + ((rowData?.company_name) ?? "") + " )"
        setPopUpActionsFor(status: rowData?.status)
    }

    private func startLoading() {
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
    }
    
    private func stopLoading() {
        self.activity.stopAnimating()
        self.activity.isHidden = true
    }
    
    func setPopUpActionsFor(status:String?){
        /*
         if 1    = "قيد التنفيذ"
         if 2    = "تجهيز المنتج"
         if 3    = "خدمة التوصيل"
         if 4    = "تم التسليم"
         if 5    = "ملغي"
         if 6    = "ملغي"
         if 7    = "مسترجع"
         if 8    = "فاشل"
         if 9    = "مرفوض"
         if 10   = "ملغي من الادارة"
         */
        
        switch status {
        case "2":
            popUpBtnPrepare.isHidden    = true
            popUpSeperator3.isHidden    = true
            
        case "3":
            popUpBtnPrepare.isHidden    = true
            popUpBtnDelivery.isHidden   = true
            popUpSeperator3.isHidden    = true
            popUpSeperator4.isHidden    = true
            
        case "4":
            popUpBtnPrepare.isHidden    = true
            popUpBtnDelivery.isHidden   = true
            popUpBtnFinished.isHidden   = true
            popUpSeperator3.isHidden    = true
            popUpSeperator4.isHidden    = true
            
        case "5","6","7","8","9","10":
            popUpBtnCall.isHidden       = true
            popUpBtnPrepare.isHidden    = true
            popUpBtnDelivery.isHidden   = true
            popUpBtnFinished.isHidden   = true
            popUpSeperator1.isHidden    = true
            popUpSeperator3.isHidden    = true
            popUpSeperator4.isHidden    = true
            
        default: break
            
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "OrderDetailsSegue" {
            let destinationView             = segue.destination as! OrderDetailsViewController
            let rowData                     = sender as! Order
            destinationView.orderId         = rowData.order_id!
            destinationView.companyId       = Defaults[.tagerCompanyId]
        }
    }
}
