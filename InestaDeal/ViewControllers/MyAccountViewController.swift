//
//  MyAccountViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/21/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class MyAccountViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var myAccountButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    var response: GetUserResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        let paddingView = UIView(frame: CGRect(x: 0,y:  0,width: self.nameTextField.frame.width / 8, height: self.nameTextField.frame.height))
        
        let paddingView1 = UIView(frame: CGRect(x: 0,y:  0,width: self.emailTextField.frame.width / 8, height: self.emailTextField.frame.height))
        
        let paddingView2 = UIView(frame: CGRect(x: 0,y:  0,width: self.mobileTextField.frame.width / 8, height: self.mobileTextField.frame.height))
        
        let paddingView3 = UIView(frame: CGRect(x: 0,y:  0,width: self.passwordTextField.frame.width / 8, height: self.passwordTextField.frame.height))
        
        
        if Defaults[.langId] == "1" {
            backButton.setImage( #imageLiteral(resourceName: "blackArrow") , for: .normal)
            nameTextField.textAlignment     = .left
            emailTextField.textAlignment    = .left
            mobileTextField.textAlignment   = .left
            passwordTextField.textAlignment = .left

            nameTextField.leftView = paddingView
            nameTextField.leftViewMode = UITextFieldViewMode.always
            
            emailTextField.leftView = paddingView1
            emailTextField.leftViewMode = UITextFieldViewMode.always
            //
            mobileTextField.leftView = paddingView2
            mobileTextField.leftViewMode = UITextFieldViewMode.always
            //
            passwordTextField.leftView = paddingView3
            passwordTextField.leftViewMode = UITextFieldViewMode.always
            
        }
        else if Defaults[.langId] == "2" {
            backButton.setImage( #imageLiteral(resourceName: "blackArrowAr") , for: .normal)
            nameTextField.textAlignment     = .right
            emailTextField.textAlignment    = .right
            mobileTextField.textAlignment   = .right
            passwordTextField.textAlignment = .right

            nameTextField.rightView = paddingView
            nameTextField.rightViewMode = UITextFieldViewMode.always
            
            emailTextField.rightView = paddingView1
            emailTextField.rightViewMode = UITextFieldViewMode.always
            //
            mobileTextField.rightView = paddingView2
            mobileTextField.rightViewMode = UITextFieldViewMode.always
            //
            passwordTextField.rightView = paddingView3
            passwordTextField.rightViewMode = UITextFieldViewMode.always
        }
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        handleTabBarVisibility(hide: true)
        tabBarController?.tabBar.isHidden = true
        
        getUserInfoApi { (data) in
            
            if let dataModel = data as? GetUserResponse{
                
                self.response = dataModel
                self.nameTextField.text     = dataModel.name
                self.mobileTextField.text   = dataModel.mobile
                self.emailTextField.text    = dataModel.email
                
            }
        }

    }
    
    func localizeUI(){
        myAccountButton.setTitle("myAccount".localized(), for: .normal)
        updateButton.setTitle("update".localized(), for: .normal)
        logoutAction.setTitle("logout".localized(), for: .normal)
        
        mobileTextField.placeholder    = "mobile".localized()
        passwordTextField.placeholder  = "password".localized()
        emailTextField.placeholder     = "email".localized()
        nameTextField.placeholder      = "name".localized()
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        if navigationController != nil{
            self.navigationController?.popViewController()
        }
        else{
            performSegue(withIdentifier: "accountGoHome", sender: self)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var logoutAction: UIButton!
    @IBAction func updateAccount(_ sender: UIButton) {
        updateApi { (data) in
            if let dataModel = data as? BaseResponse{
                if dataModel.result == "true"{
                    self.showDefaultAlert(title: "successUpdateAlert".localized(), message: nil)
                }
                else{
                    self.showDefaultAlert(title: dataModel.message, message: nil)
                }
            }
        }
        
    }
    @IBAction func logoutAction(_ sender: Any) {
////        performSegue(withIdentifier: "logOut", sender: self)
//        performSegue(withIdentifier:"accountGoHome" , sender: "logOut")
        
        Defaults[.userType] = ""
        Defaults[.logged] = false
        let viewController:MainTabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarRoot") as! MainTabBarController
        let appDelegate = UIApplication.shared.delegate
        appDelegate?.window??.rootViewController = viewController
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logOut"{
            
            Defaults[.userType] = ""
            Defaults[.logged] = false
        }
        else if segue.identifier == "accountGoHome"{
            let destinationVC = segue.destination as! MainTabBarController
            destinationVC.modalPresentationStyle = .fullScreen
            
            let index = sender
            if sender is String && index as! String == "logOut" {
                Defaults[.userType] = ""
                Defaults[.logged] = false
                destinationVC.selectedIndex = 1
            }
        }
        
    }
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        
        view.endEditing(true)
    }
    
    func updateApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "name": nameTextField.text ?? "",
            "email": emailTextField.text ?? "",
            "mobile": mobileTextField.text ?? "",
            "password": passwordTextField.text ?? ""
        ]
        
        
        Alamofire.request("https://www.instadeal.co/home/updateAccount/\(Defaults[.userId])?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }

    
    func getUserInfoApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getUser/\(Defaults[.userId] )?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = GetUserResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }

}
