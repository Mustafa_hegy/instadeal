//
//  CategoryRow.swift
//  TwoDirectionalScroller
//
//  Created by Robert Chen on 7/11/15.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit
import SwifterSwift
import SwiftyUserDefaults

class SimilarProductRow : UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableCellHeaderLbl: UILabel!
     var indexPathSelected: IndexPath?

    
    var subCategoryId: String?
    var relatedProds: RelatedProductsResponse?
    {
        didSet{
            collectionView.reloadData()
            collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    var section: Int = 0
    
    
}


extension SimilarProductRow : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return relatedProds?.data?.count ?? 0
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var tempIndexPath: IndexPath?
        tempIndexPath = self.indexPathSelected ?? indexPath
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productDetailsCell", for: indexPath) as! SimilarProductCell
        cell.prodName.text = relatedProds?.data![(tempIndexPath?.row)!].name
        cell.price.text = (relatedProds?.data![(tempIndexPath?.row)!].price ?? "") + "د ك"
        cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
        cell.imageProduct.sd_setImage(with: URL(string: relatedProds?.data![(indexPath.row)].photo ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.addCartBtn.tag = (indexPath.row)
        
//        cell.imageView.sd_setImage(with: URL(string: (categories?.data![section].childs![indexPath.row].photo) ?? ""), placeholderImage: UIImage(named: "placeholder.png"))

        
        return cell
    }
    

    
    
}

extension SimilarProductRow : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        indexPathSelected = indexPath
        Defaults[.prodId] = ""
        Defaults[.prodId] = relatedProds?.data![(indexPathSelected?.row)!].id ?? ""
        Defaults.synchronize()
        //collectionView.reloadData()
    }
    
    
    
}
extension SimilarProductRow : UICollectionViewDelegateFlowLayout {
    
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let itemsPerRow:CGFloat = 4
//        let hardCodedPadding:CGFloat = 5
//        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
//        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
//        return CGSize(width: itemWidth, height: itemHeight)
//    }
    
}

