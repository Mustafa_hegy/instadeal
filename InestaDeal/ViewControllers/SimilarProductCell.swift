//
//  VideoCell.swift
//  TwoDirectionalScroller
//
//  Created by Robert Chen on 7/11/15.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit

class SimilarProductCell: UICollectionViewCell {
    
    @IBOutlet weak var addCartBtn: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var prodName: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
}
