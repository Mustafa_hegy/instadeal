//
//  splashVideoViewController.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 1/2/20.
//  Copyright © 2020 Waleed. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

import Alamofire
import SwiftyUserDefaults

class splashVideoViewController: UIViewController {
    var player          : AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Check if user exist , else sign out
        getUserInfoApi { (data) in
            
            if let dataModel = data as? GetUserResponse{
                // Do Nothing
                
                Defaults[.userName] = dataModel.name ?? ""
                Defaults[.userEmail] = dataModel.email ?? ""
                Defaults[.userMobile] = dataModel.mobile ?? ""
            }
            else {
                Defaults[.logged] = false
                Defaults[.userId] = ""
                
                Defaults[.userName]     = ""
                Defaults[.userEmail]    = ""
                Defaults[.userMobile]   = ""
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Call method to setup AVPlayer & AVPlayerLayer to play video
        
        // First way
        self.setupAVPlayer()
//        setMovement()
    }

    // MARK: - Class Custom Methods
    // First way
    func setupAVPlayer() {
        let path        = Bundle.main.path(forResource: "Intro", ofType:"mp4")
        let videoURL    = URL(fileURLWithPath: path!)
        let avAssets    = AVAsset(url:  videoURL) // Create assets to get duration of video.
        player          = AVPlayer(url: videoURL)
        let playerLayer = AVPlayerLayer(player: player)
        //        playerLayer.frame = self.videoPlayerView.bounds
        
        playerLayer.backgroundColor = .none
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        //  playerLayer.zPosition = -1
        
        self.view.layer.addSublayer(playerLayer)
        
        player!.play()
        player!.addPeriodicTimeObserver(forInterval: CMTime(seconds: 1, preferredTimescale: 1) , queue: .main) { [weak self] time in
            if time == avAssets.duration {
                self!.setMovement()
            }
        }
    }
    
    func setMovement(){
        if Defaults[.areaId] != "" {
            performSegue(withIdentifier: "tabBarSegue", sender: nil)
        }else{
            performSegue(withIdentifier: "SelectAreaViewSegue", sender: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - API Connection Methods

    func getUserInfoApi(successClosure: @escaping SuccessClosure){
        Alamofire.request("https://www.instadeal.co/home/getUser/\(Defaults[.userId] )?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = GetUserResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                } else{
                    successClosure([])
                }
            } else{
                successClosure([])
            }
        }
        
    }
}
