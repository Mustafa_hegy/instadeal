//
//  CartListViewController.swift
//  InestaDeal

//  Created by Ibrahim Salah on 3/16/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults
import SDWebImage



class CartListViewController: UIViewController{
    @IBOutlet weak var tagerButton: UIButton!
    @IBOutlet weak var tagerSelectionIcon: UIImageView!
    @IBOutlet weak var gmmalyImage: UIImageView!
    @IBOutlet weak var gmmalyLabel: UILabel!
    @IBOutlet weak var gmmalySelectionIcon: UIImageView!
    @IBOutlet weak var gmmalyView: UIView!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var tagerTableView : UITableView!
    @IBOutlet weak var selectAllItemsButton: UIButton!
    @IBOutlet weak var allItemsSelectedLabel : UILabel!
    @IBOutlet weak var deliveryCostlabel: UILabel!
    @IBOutlet weak var toatalCostLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var cartLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var deliveryTypeTitleLabel: UILabel!
    @IBOutlet weak var deliveryTyepeHintContainer: UIStackView!
    @IBOutlet weak var deliveryTypeHint1: UILabel!
    @IBOutlet weak var deliveryTypeSelectedName: UILabel!
    @IBOutlet weak var deliveryTypeSelectedIcon: UIImageView!
    
    @IBOutlet weak var selectAllItemsTitleLabel: UILabel!
    @IBOutlet weak var deliveryCostTitleLabel: UILabel!
    @IBOutlet weak var totlaCostTitleLabel: UILabel!
    
    
    var deliveryTypeSelected:Bool = false
    
    var gammaly = 0
    var finalDeliveryCost = 0.0
    var finalCost = 0.0
    var selectedItems: [Product] = []
    var gammalySetting: Setting?
    var totalSelectedItems = 0.0
    var totalDelivery = 0.0 {
        didSet{
            updateUI()
        }
    }
    
    var deliveryCost:[Int:Double] = [:]{
        didSet{
            updateDliveryCost()
        }
        
    }
    
    func updateUI (){
        if gammaly == 0 {
            deliveryCostlabel.text = "\(totalDelivery) \("currency".localized())"
            finalDeliveryCost = totalDelivery
            toatalCostLabel.text = "\(totalDelivery + totalSelectedItems) \("currency".localized())"
            finalCost = totalDelivery + totalSelectedItems
        }
        
    }
    var gammalyCartList = [CartListData]()
    func gammalyCost () {
        
        gammalyCartList.removeAll()
        if gammaly == 1 {
            deliveryCostlabel.text = "\(0.0) \("currency".localized())"
            toatalCostLabel.text = "\(0.0) \("currency".localized())"
            for cart in cartList!{
                for product in (cart.products){
                    if product.isChecked {
                        gammalyCartList.append(cart)
                        print("gammalyCartList: \(gammalyCartList.count)")
                        break
                    }
                }
            }
            
            if gammalyCartList.count != 0{
                if let gammalySetting = gammalySetting?.data {
                    if gammalyCartList.count <= 2{// cost for first two seller then calculation => New code
//                    if gammalyCartList.count == 1{ // cost for one seller then calculation => Old code
                        deliveryCostlabel.text = "\((Double(gammalySetting.gamm3) ?? 0.0)) \("currency".localized())"
                        finalDeliveryCost = (Double(gammalySetting.gamm3) ?? 0.0)
                        toatalCostLabel.text = "\((Double(gammalySetting.gamm3) ?? 0.0) + totalSelectedItems) \("currency".localized())"
                        finalCost = (Double(gammalySetting.gamm3) ?? 0.0) + totalSelectedItems
                        
                    } else {
                        let addedGammaly = Double((gammalyCartList.count - 2)) * (Double(gammalySetting.addedGamm3) ?? 0.0) // cost for more than two seller then calculation => Old code
//                        let addedGammaly = Double((gammalyCartList.count - 1)) * (Double(gammalySetting.addedGamm3) ?? 0.0) // cost for more than one seller then calculation => Old code
                        let totalGammalyCost = addedGammaly + (Double(gammalySetting.gamm3) ?? 0.0)
                        deliveryCostlabel.text = "\(totalGammalyCost) \("currency".localized())"
                        finalDeliveryCost = totalGammalyCost
                        toatalCostLabel.text = "\((totalGammalyCost) + totalSelectedItems) \("currency".localized())"
                        finalCost = (totalGammalyCost) + totalSelectedItems
                    }
                    
                }
                
            }
        }
        
        
    }
    
    func updateDliveryCost(){
        var totalDel = 0.0
        if deliveryCost.count != 0 {
            for deliver in deliveryCost {
                totalDel += deliver.value
            }
        }
        totalDelivery = totalDel
    }
    
    
    var cartList: [CartListData]?{
        didSet{
            if cartList?.count != 0{
                
                selectedItems.removeAll()
                var countOfSelectedItems = 0
                var totalSelectedItemsPrice = 0.0
                
                // logic of selection and and unSelection of tagers and items
                let selectedTager = cartList?.filter{
                    $0.isChecked == true
                }
                if selectedTager?.count == cartList?.count {
                    selectAllItemsButton.setBackgroundImage(#imageLiteral(resourceName: "mark"), for: .normal)
                    allItemsSelected = false
                } else {
                    selectAllItemsButton.setBackgroundImage(#imageLiteral(resourceName: "emptyCircle"), for: .normal)
                    allItemsSelected = true
                }
                
                // Count of selected items
                for cart in cartList!{
                    for product in (cart.products){
                        if product.isChecked {
                            selectedItems.append(product)
                            countOfSelectedItems += 1
                            totalSelectedItemsPrice += Double(product.totalPrice)!

                            // Add item option price ====>> New Code
                            if !(product.options!.isEmpty) {
                                for option in product.options! {
                                    
                                    let optionPrice = Double(option.option_price)!
                                    let itemCount   = Double(product.count)!
                                    totalSelectedItemsPrice += optionPrice * itemCount
                                }
                            }
                            /* ***** */
                        }
                    }
                }
                
                totalSelectedItems = totalSelectedItemsPrice
                allItemsSelectedLabel.text = "( \(countOfSelectedItems) )"
                gammalyCost()
                cartLabel.text = "cart".localized() + " (\(countOfSelectedItems))"
                buyButton.setTitle("buy".localized() + " ( \(countOfSelectedItems) )", for: .normal)
                
            }
            
        }
    }
    
    var allCartList: [CartListData]?
    
    var attributedString = NSMutableAttributedString(string:"")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        addNavBarCart(isBack: true)
        handleTabBarVisibility(hide: true)
        
        deliveryTyepeHintContainer.isHidden = true
        
        activityIndicator.startAnimating()
        
        // start screen with no delivery type selected
        tagerButton.backgroundColor = .white
        tagerButton.setTitleColor(.black, for: .normal)
        
        gmmalyView.backgroundColor = .white
        gmmalyLabel.textColor = .black
        
        gmmalyImage.image = UIImage(named: "gmm3lyLogoColored.png")
        attributedString.append(NSMutableAttributedString(string:"details".localized(), attributes:[NSAttributedStringKey.underlineStyle : 1]))
        detailsButton.setAttributedTitle(attributedString, for: .normal)
        loadCartList()
        getGammalySetting()
        //print(gammalySetting?.data.addedGamm3)
    }
    
    
    func localizeUI(){
        buyButton.setTitle("buy".localized() + " ( 0 )", for: .normal)
        tagerButton.setTitle("direct".localized() , for: .normal)
        cartLabel.text                  = "cart".localized() + " ( 0 )"
        gmmalyLabel.text                = "gmma3ly".localized()
        deliveryTypeTitleLabel.text     = "chooseDeliveryType".localized()
        selectAllItemsTitleLabel.text   = "selectAllItems".localized()
        deliveryCostTitleLabel.text     = "deliveryCost".localized() + " : "
        totlaCostTitleLabel.text        = "totalCost".localized() + " : "
    }
    
    @IBAction func detailsButtonTapped(_ sender:UIButton){
        let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
        if gammaly == 1 {
            viewController.url = "https://www.instadeal.co/about.html?lang=\(Defaults[.langId])"
        } else {
            viewController.url = "https://www.instadeal.co/direct.html?lang=\(Defaults[.langId])"
        }
        self.navigationController?.show(viewController, sender: self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleTabBarVisibility(hide: true)
    }
    
    func loadCartList(){
        getcartListApi { (data) in
            self.cartList = data as? [CartListData]
            self.allCartList = self.cartList
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.tagerTableView.reloadData()
        }
    }
    
    func getcartListApi(successClosure: @escaping SuccessClosure){
        
//        let params: Parameters = [
//            "lang": "\(Defaults[.langId])" ,
//            "area": "\(Defaults[.areaId])"
//        ]
        
        print("getcartListApi: https://www.instadeal.co/home/getCarts/\(Defaults[.userId])?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])")
        Alamofire.request("https://www.instadeal.co/home/getCarts/\(Defaults[.userId])?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get).responseCartList { response in
            if let cartResponse = response.result.value {
                if cartResponse.result == "true"{
                    successClosure(cartResponse.data)
                    
                }
            }
        }
        
        
    }
    
    func getGammalySetting() {
            let url = "https://www.instadeal.co/home/getSetting/"
           Alamofire.request(url).responseSetting { response in
             if let setting = response.result.value {
                self.gammalySetting = setting
             }
           }
    }
    
    var allItemsSelected = true
    @IBAction func selectAllItemsButtonTapped(_ sender: UIButton){
        if self.deliveryTypeSelected {
            if allItemsSelected{
                print("allItemsSelected: \(allItemsSelected)")
                selectAllItemsButton.setBackgroundImage(#imageLiteral(resourceName: "mark"), for: .normal)
                allItemsSelected = false
                if cartList != nil{
                    for index in (cartList?.indices)!{
                        cartList?[index].isChecked = true
                        for productIndex in (cartList?[index].products.indices)!{
                            cartList?[index].products[productIndex].isChecked = true
                        }
                        self.deliveryCost[index] = Double((self.cartList?[index].deliveryPrice)!)
                        
                    }
                    tagerTableView.reloadData()
                }
            } else {
                print("allItemsSelected: \(allItemsSelected)")
                selectAllItemsButton.setBackgroundImage(#imageLiteral(resourceName: "emptyCircle"), for: .normal)
                allItemsSelected = true
                if cartList != nil{
                    for index in (cartList?.indices)!{
                        cartList?[index].isChecked = false
                        for productIndex in (cartList?[index].products.indices)!{
                            cartList?[index].products[productIndex].isChecked = false
                        }
                        self.deliveryCost.removeValue(forKey: index)
                    }
                    tagerTableView.reloadData()
                }
            }
        } else {
            self.showDefaultAlert(title: "selectDeliveryType".localized(), message: nil)
        }
    }
    
    @IBAction func tagerButtonTapped(_ sender: Any) {
        deliveryTypeSelected = true
        
        if cartList != nil{
            for index in (cartList?.indices)!{
                cartList?[index].isChecked = false
                for productIndex in (cartList?[index].products.indices)!{
                    cartList?[index].products[productIndex].isChecked = false
                }
                self.deliveryCost.removeValue(forKey: index)
            }
            tagerTableView.reloadData()
            
        }
        gammaly = 0
        tagerButton.backgroundColor = #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        tagerButton.setTitleColor(.white, for: .normal)
        tagerSelectionIcon.image    = #imageLiteral(resourceName: "checkedCircle")
        
        gmmalyView.backgroundColor  = .white
        gmmalyLabel.textColor       = .black
        gmmalyImage.image           = UIImage(named: "gmm3lyLogoColored.png")
        gmmalySelectionIcon.image   = #imageLiteral(resourceName: "emptyCircle")
        
        if let allcompanies = allCartList{
            cartList = allcompanies
            tagerTableView.reloadData()
        }
        
        
        deliveryTypeHint1.text = "deliveryTypeDirectHint".localized()
        deliveryTypeSelectedName.isHidden   = true
        deliveryTypeSelectedIcon.isHidden   = true
        deliveryTyepeHintContainer.isHidden = false
        
    }
    
    @IBAction func gmmalyButtonTapped(_ sender: Any) {
        deliveryTypeSelected = true
        
        if cartList != nil{
            for index in (cartList?.indices)!{
                cartList?[index].isChecked = false
                for productIndex in (cartList?[index].products.indices)!{
                    cartList?[index].products[productIndex].isChecked = false
                }
                self.deliveryCost.removeValue(forKey: index)
            }
            tagerTableView.reloadData()
            
        }
        gammaly = 1
        tagerButton.backgroundColor = .white
        tagerButton.setTitleColor(.black, for: .normal)
        tagerSelectionIcon.image    = #imageLiteral(resourceName: "emptyCircle")
        
        gmmalyView.backgroundColor = #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        gmmalyLabel.textColor = .white
        gmmalyImage.image = UIImage(named: "gmm3lyLogo.png")
        gmmalySelectionIcon.image   = #imageLiteral(resourceName: "checkedCircle")
        
        // Filter gmma3ly companies
        if let companies = cartList{
            self.cartList = companies.filter{
                $0.gamm3 == "1"
            }
            tagerTableView.reloadData()
        }
        
        deliveryTypeHint1.text = "deliveryTypeGmma3lyHint".localized()
        deliveryTypeSelectedName.text = "gmma3lyHint".localized()
        deliveryTypeSelectedName.isHidden   = false
        deliveryTypeSelectedIcon.isHidden   = false
        deliveryTyepeHintContainer.isHidden = false
    }
    
    
    
    @IBAction func buyButtonTapped(_ sender:UIButton){
        if selectedItems.count != 0{
            print("selected item: \(selectedItems.count)")
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let myAddressVC = storyBoard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressViewController
            myAddressVC.gammaly = gammaly
            myAddressVC.orders = selectedItems
            myAddressVC.totalDelivery = finalDeliveryCost
            myAddressVC.totalPrice =  finalCost
            myAddressVC.totalDeliveryCost = finalDeliveryCost
            self.navigationController?.pushViewController(myAddressVC, animated: true)
        }
        else {
            self.showDefaultAlert(title: "selectProducts".localized(), message: nil)
        }
    }
    
    
    
    @IBAction func deleteCartButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: "clearCart".localized(), message: "doClearCart?".localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "continue".localized(), style: .default, handler: { (action) in
            self.deleteCartApi()
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func deleteCartApi(){
        Alamofire.request("https://www.instadeal.co/home/deleteUserCart/\(Defaults[.userId])?lang=\(Defaults[.langId])").responseJSON { response in
            if response.result.isSuccess {
                self.showAlert(title: "", message: "cartEmptied".localized())
                self.cartList?.removeAll()
                self.tagerTableView.reloadData()
            }
        }
    }
    
    func deleteCartItemApi(cartId:String,successClosure: @escaping SuccessClosure){
        Alamofire.request("https://www.instadeal.co/home/deleteCart/\(cartId)?lang=\(Defaults[.langId])").responseJSON { response in
            if response.result.isSuccess {
                
                successClosure(response.result.isSuccess)
            }
            
//            if let json = response.result.value {
//                if let data = json as? [String : Any]{
//                    let dataModel = BaseResponse(JSON: data)
//                    successClosure(dataModel as Any)
//                }
//            }
            
        }
    }
}

extension CartListViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cartList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartList?[section].products.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tagerCell = tableView.dequeueReusableCell(withIdentifier: "TagerCell") as! CartTagerTableViewCell
        
        if let product =  cartList?[indexPath.section].products[indexPath.row]{
            tagerCell.productSelected = cartList?[indexPath.section].products[indexPath.row].isChecked
            tagerCell.configure(item: product)
            
            // Select or deselect item
            tagerCell.itemSelected = { (selected) in
                print("ItemSelected: \(selected)")
                if  selected{
                    tagerCell.productSelected = true
                    tagerCell.singleItemButton.setBackgroundImage(#imageLiteral(resourceName: "mark"), for: .normal)
                    self.cartList?[indexPath.section].products[indexPath.row].isChecked = true
                    self.deliveryCost[indexPath.section] = Double((self.cartList?[indexPath.section].deliveryPrice)!)
                    
                    // Selection Logic
                    let selectedItems = self.cartList?[indexPath.section].products.filter {
                        $0.isChecked == true
                    }
                    if selectedItems?.count == self.cartList?[indexPath.section].products.count{
                        self.cartList?[indexPath.section].isChecked = true
                        tableView.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet , with: .none)
                    }
                    
                    
                } else {
                    self.allItemsSelected = true
                    tagerCell.singleItemButton.setBackgroundImage(#imageLiteral(resourceName: "emptyCircle"), for: .normal)
                    self.cartList?[indexPath.section].products[indexPath.row].isChecked = false
                    self.cartList?[indexPath.section].isChecked = false
                    
                    // Unselection Logic
                    let unSelectedItems = self.cartList?[indexPath.section].products.filter {
                        $0.isChecked == true
                    }
                    if unSelectedItems?.count != 0 {
                        self.deliveryCost[indexPath.section] = Double((self.cartList?[indexPath.section].deliveryPrice)!)
                    } else {
                        self.deliveryCost.removeValue(forKey: indexPath.section)
                        self.cartList?[indexPath.section].isChecked = false
                    }
                    tableView.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet , with: .none)
                }
            }
            
            tagerCell.productSelection = {
                if self.deliveryTypeSelected {
                    if !tagerCell.productSelected{
                        tagerCell.itemSelected?(true)
                        //productSelected = false
                    } else {
                        tagerCell.itemSelected?(false)
                        //productSelected = true
                    }
                } else {
                    self.showDefaultAlert(title: "selectDeliveryType".localized(), message: nil)
                }
            }
            
            tagerCell.productDeletion = {
                let alert = UIAlertController(title: "delete".localized(), message: "doDeleteCartItem?".localized(), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "continue".localized(), style: .default, handler: { (action) in
                    
                    self.deleteCartItemApi(cartId: product.id,successClosure: { [weak self] (data) in
                        
                        if data is Bool{
                            // remove item from list
//                            self!.cartList?[indexPath.section].products.remove(at:indexPath.row)
//                            tableView.reloadData()
                            self?.cartList?.removeAll()
                            self?.loadCartList()
                        }
                    })
                }))
                
                alert.addAction(UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                
            }
            // fix cell reuse issues
            if (cartList?[indexPath.section].products[indexPath.row].isChecked)! {
                tagerCell.singleItemButton.setBackgroundImage(#imageLiteral(resourceName: "mark"), for: .normal)
            } else {
                tagerCell.singleItemButton.setBackgroundImage(#imageLiteral(resourceName: "emptyCircle"), for: .normal)
            }
        }
        
        return tagerCell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(140)
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(50)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tagerHeaderView = Bundle.main.loadNibNamed("TagerHeaderTableViewCell", owner: self, options: nil)?.first as! TagerHeaderTableViewCell
        if let cartSection = cartList?[section]{
            tagerHeaderView.configure(cartList: cartSection)
            tagerHeaderView.tagerSelected = self.cartList?[section].isChecked
            
            // fix section reuse isuues
            if (self.cartList?[section].isChecked)! {
                tagerHeaderView.selectItemsOfTagerButton.setBackgroundImage(#imageLiteral(resourceName: "mark"), for: .normal)
            } else {
                tagerHeaderView.selectItemsOfTagerButton.setBackgroundImage(#imageLiteral(resourceName: "emptyCircle"), for: .normal)
            }
            
            // select or deselect tager
            tagerHeaderView.tagerSelection = {
                if self.deliveryTypeSelected {
                    if !tagerHeaderView.tagerSelected{
                        tagerHeaderView.selectAllItemsOfTager?(true)
                        //tagerSelected = false
                    } else {
                        tagerHeaderView.selectAllItemsOfTager?(false)
                        //tagerSelected = true
                    }
                } else {
                    self.showDefaultAlert(title: "selectDeliveryType".localized() , message: nil)
                }
            }
            
            tagerHeaderView.selectAllItemsOfTager = { (selected) in
                print("TagerSelected: \(selected)")
                if selected{
                    tagerHeaderView.selectItemsOfTagerButton.setBackgroundImage(#imageLiteral(resourceName: "mark"), for: .normal)
                    self.cartList?[section].isChecked = true
                    tagerHeaderView.tagerSelected = false
                    for index in (self.cartList?[section].products.indices)! {
                            self.cartList?[section].products[index].isChecked = true
                            let indexPath = IndexPath(item: index, section: section)
                            tableView.reloadRows(at: [indexPath], with: .none)
                        }
                    self.deliveryCost[section] = Double((self.cartList?[section].deliveryPrice)!)
                     tableView.reloadSections(NSIndexSet(index: section) as IndexSet , with: .none)
                    
                } else {
                    tagerHeaderView.selectItemsOfTagerButton.setBackgroundImage(#imageLiteral(resourceName: "emptyCircle"), for: .normal)
                    self.cartList?[section].isChecked = false
                    for index in (self.cartList?[section].products.indices)! {
                            self.cartList?[section].products[index].isChecked = false
                            let indexPath = IndexPath(item: index, section: section)
                            tableView.reloadRows(at: [indexPath], with: .none)
                        }
                    self.deliveryCost.removeValue(forKey: section)
                    tableView.reloadSections(NSIndexSet(index: section) as IndexSet , with: .none)

                }
            }
        }
        
        return tagerHeaderView.contentView
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let tagerFooterView = Bundle.main.loadNibNamed("TagerFooterTableViewCell", owner: self, options: nil)?.first as! TagerFooterTableViewCell
        if let products =  cartList?[section].products{
            tagerFooterView.configure(items: products, gammaly: gammaly)
        }
        
        return tagerFooterView.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(100)
    }
    
}
