//
//  OptionRow.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 10/16/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class OptionRow: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableCellHeaderLbl: UILabel!
    
    var selectedOptionPrice = 0
    
    var Options: ProductDetailsResponse?
    {
        didSet{
            if Defaults[.langId] == "1" {
                collectionView.semanticContentAttribute   = .forceLeftToRight
            } else if Defaults[.langId] == "2" {
                collectionView.semanticContentAttribute   = .forceRightToLeft
            }
            collectionView.reloadData()
            collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    var sectionRow: Int = 0
}

extension OptionRow : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("counts:\(Options?.more_feats![sectionRow].FeatsItem?.count)")
        return Options?.more_feats![sectionRow].FeatsItem?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionCell", for: indexPath) as! OptionCell
        cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
        let rowData = Options?.more_feats![sectionRow].FeatsItem![indexPath.row]
        cell.optionTitleLbl.text = rowData!.feat_name
        
        if rowData!.selected! {
            self.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .top)
            cell.isSelected = true
            cell.contentView.backgroundColor = #colorLiteral(red: 0.7675911784, green: 0.7676092982, blue: 0.7675995827, alpha: 1)
            Defaults[.selectedOptionsArray].append(rowData!.feat_add_to_cart!)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! OptionCell
        
        let rowData = Options?.more_feats![sectionRow].FeatsItem![indexPath.row]
        
        cell.contentView.backgroundColor = #colorLiteral(red: 0.7675911784, green: 0.7676092982, blue: 0.7675995827, alpha: 1)
        cell.isSelected = true
        
        selectedOptionPrice = Int(rowData!.feat_value!)!
        if !(Defaults[.selectedOptionsArray].contains(rowData!.feat_add_to_cart!)) {
            Defaults[.selectedOptionsArray].append(rowData!.feat_add_to_cart!)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! OptionCell
        
        let rowData = Options?.more_feats![sectionRow].FeatsItem![indexPath.row]
        
        cell.contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.isSelected = false
        if let index = Defaults[.selectedOptionsArray].index(of: rowData!.feat_add_to_cart!) {
            Defaults[.selectedOptionsArray].remove(at: index)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.image = nil
        verticalIndicator?.backgroundColor = appThemeColor
    }
    
}

extension OptionRow : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
}

