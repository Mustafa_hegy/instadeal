//
//  CompnyProductsViewController.swift
//  InestaDeal
//
//  Created by Waleed on 4/15/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyUserDefaults
import Alamofire

class CompnyProductsViewController: UIViewController, UIGestureRecognizerDelegate ,UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var insideView: UIView!
    //@IBOutlet weak var popupView: UIView!
    @IBOutlet weak var productsBottomView2: UIView!
    @IBOutlet weak var productsBottomView: UIView!
    @IBOutlet weak var instaBottomView: UIView!
    @IBOutlet weak var commentsTable: UITableView!
    @IBOutlet weak var tagerTable: UITableView!
    @IBOutlet weak var companyNameLbl: UILabel!
    
    @IBOutlet weak var topHeaderView: UIView!
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var companyTotalProducts: UILabel!
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var tabTable: UICollectionView!
    @IBOutlet weak var productTable: UICollectionView!
    @IBOutlet weak var productTableLayoutConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var infoBottomView: UIView!
   
    @IBOutlet weak var evaluationBottomView: UIView!
    @IBOutlet weak var tagerProducts: UIButton!
    @IBOutlet weak var tagerInfo: UIButton!
    @IBOutlet weak var tagerEvaluation: UIButton!
    @IBOutlet weak var instaView: UIWebView!
    
    var productModelView: ProductModelView? = ProductModelView()
    
    var flag: Bool = false
    var gradient: CAGradientLayer?
    var companyId: String?
    var catId: String?
    var pickheight: CGFloat = 0.0
    var selectedIndexPath: IndexPath?
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var companyComments: CommentsResponse?
    {
        didSet{
            //            productTable.reloadData()
            commentsTable.reloadData()
            
        }
    }
    var companyDetails: CompanyDetailsResponse?
    
    {
        didSet{
//            productTable.reloadData()
            tagerTable.reloadData()
            commentsTable.reloadData()
            tabTable.reloadData()
            flag = true
            
            
        }
    }
    
    var companyCats: CompanyCatsResponse?
    {
        didSet{
            productTable.reloadData()
            
            // Update productTable Height and then update view to applay new constraints
            let newHeight = productTable.collectionViewLayout.collectionViewContentSize.height
            productTableLayoutConstraintHeight.constant = newHeight
            self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width, height:self.productTable.frame.origin.y + newHeight )
            self.view.layoutIfNeeded()
        }
    }
    var modelView: CompanyProductModelView?
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view?.isDescendant(of: insideView))!{
            
            return false
        }
      
        return true
    }
    func getRelatedProductsApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "lang": "\(Defaults[.langId])" ,
            "area": "\(Defaults[.areaId])"
        ]
        
        Alamofire.request("https://www.instadeal.co/home/getCarts/\(Defaults[.userId])?lang=\(Defaults[.langId])", method: .get , parameters: params ,  encoding: JSONEncoding.default)
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = GetCartResponse(JSON: data)
                        
                        successClosure(dataModel!)
                    }
                }
                
        }
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        localizeUI()
        
        //activity
        activity.center = view.center
        activity.color = #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
        
        topHeaderView.backgroundColor = UIColor(white: 1, alpha: 0.96)
        companyImage.roundCorners(corners: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner], radius: 25)
//        popupView.isHidden = true
        instaView.isHidden = true
        commentsTable.rowHeight = UITableViewAutomaticDimension
        tagerTable.rowHeight = UITableViewAutomaticDimension
        
        commentsTable.tableFooterView = UIView()
        tagerTable.tableFooterView = UIView()
        productTable.contentInset.top = 10
        productTable.contentInset.bottom = 20
        commentsTable.isHidden = true
        productsBottomView.isHidden = false
        infoBottomView.isHidden = true
        //evaluationBottomView.isHidden = true
        tagerTable.isHidden = true
        tagerTable.rowHeight = UITableViewAutomaticDimension
        setImageShadowLayer()
        modelView = CompanyProductModelView(scene: self)
        addNavBar(isBack: true)
        if  Defaults[.globalCompanyID] != Defaults[.companyId] && Defaults[.globalCompanyID] != "" {
//            popupView.isHidden = true
            getRelatedProductsApi { (data) in
                
                if let dataModel = data as? GetCartResponse{
                 
                    if dataModel.data?.count != 0{
//                        self.popupView.isHidden = false
                    }
                    
                }
            }
            
        }
        modelView?.getCompanyCommentsApi(successClosure: { (data) in
             if let dataModel = data as? CommentsResponse{
                self.companyComments = dataModel
            }
        })
        modelView?.getCompanyDetailsApi(successClosure: { [unowned self](data) in
        
            if let dataModel = data as? CompanyDetailsResponse{
                if self.tabTable != nil
                {
                    self.tabTable.isHidden = false
                }
                self.companyDetails = dataModel
                if let urL = URL(string : "https://www.instagram.com/\(dataModel.instgram ?? "")"){
                    let request = URLRequest(url: urL)
                    self.instaView.loadRequest(request)
                }
                  self.companyImage.sd_setImage(with: URL(string: (dataModel.photo1) ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                
                self.logoImage.sd_setImage(with: URL(string: (dataModel.photo) ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                //self.companyNameLbl.text = dataModel.name
                //self.companyTotalProducts.text = "\(dataModel.product_counts ?? 0) منتج"
                Defaults[.deliveryFees] = dataModel.delivery_price ?? ""
                self.modelView?.getCatsProductApiFirst(successClosure: { [unowned self](data) in
                    
                    if let dataModel = data as? CompanyCatsResponse{
                        self.companyCats = dataModel
                        print("prod:id: \(self.companyCats?.data)")
                        self.activity.stopAnimating()
                        self.activity.isHidden = true
                    }
                    
                })
               
                if Defaults[.langId] == "1" {
                    self.tabTable.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
                } else if Defaults[.langId] == "2" {
                    self.tabTable.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
                }
            }
        })
    }
    
    override func viewDidLayoutSubviews() {
//        productTableLayoutConstraintHeight.constant = productTable.contentSize.height
        productTableLayoutConstraintHeight.constant = productTable.collectionViewLayout.collectionViewContentSize.height
        
        productTable.collectionViewLayout.invalidateLayout()
    }
    
    func localizeUI(){
        tagerInfo.setTitle("companyInfo".localized(), for: .normal)
        tagerProducts.setTitle("companyProducts".localized(), for: .normal)
    }
    
    @IBAction func mainTabAction(_ sender: UIButton) {
        if sender.tag == 0{
            instaView.isHidden = true
            productsBottomView.isHidden = false
            instaBottomView.isHidden = true
            
        }
        else
        {
            instaView.isHidden = false
            productsBottomView.isHidden = true
            instaBottomView.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        handleTabBarVisibility(hide: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        handleTabBarVisibility(hide: false)
        
    }
    
//    @IBAction func gestureAction(_ sender: UITapGestureRecognizer) {
//        popupView.isHidden = true
//    }
//    @IBAction func action2(_ sender: UIButton) {
//        popupView.isHidden = true
//        if self.navigationController != nil{
//            self.navigationController?.popViewController()
//        }
//        else{
//            dismiss(animated: true, completion: nil)
//        }
//    }
//    @IBAction func action1(_ sender: UIButton) {
//        deletCartApi { (data) in
//
//
//                self.popupView.isHidden = true
//                Defaults[.globalCompanyID] = ""
//
//        }
//    }
    
    func deletCartApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/deleteUserCart/\(Defaults[.userId] )?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                successClosure(json)
            }
        }
        
    }
    
    @IBAction func mainTabsBtnsAction(_ sender: UIButton) {
        if sender.tag == 0{
                productsBottomView2.isHidden = false
                infoBottomView.isHidden = true
                //evaluationBottomView.isHidden = true
            tagerTable.isHidden = true
            commentsTable.isHidden = true
            tagerTable.isHidden = true
            productTable.isHidden = false
        }
        
        if sender.tag == 1{
            productsBottomView2.isHidden = true
            infoBottomView.isHidden = false
            //evaluationBottomView.isHidden = true
            tagerTable.isHidden = false
            tagerTable.reloadData()
            commentsTable.isHidden = true
            productTable.isHidden = true
        }
        
        if sender.tag == 2{
            productsBottomView2.isHidden = true
            infoBottomView.isHidden = true
            //evaluationBottomView.isHidden = false
            commentsTable.isHidden = false
            tagerTable.isHidden = false
            productTable.isHidden = true
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setImageShadowLayer(){
        let view = UIView(frame: companyImage.frame)
        
        gradient = CAGradientLayer()
        
        gradient?.frame = CGRect(x: 0, y: 0, width: companyImage.frame.width * 2, height: companyImage.frame.height)
        
        gradient?.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        
        gradient?.locations = [0.0, 1.0]
        
        view.layer.insertSublayer(gradient!, at: 0)
        
        companyImage.addSubview(view)
        view.alpha = 0.5
        companyImage.bringSubview(toFront: view)
        
    }
    

    @IBAction func addCartAction(_ sender: UIButton) {
        
        performSegue(withIdentifier: "goProdDetailsSegue", sender: sender)
        Defaults[.prodId] = companyCats?.data![sender.tag].id ?? ""
        
    }
    
}
extension CompnyProductsViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == productTable{
//            return CGSize(width: collectionView.frame.width / 2, height: 230)
            return CGSize(width: (collectionView.frame.width - 24) / 2, height: 274)
        } else {
            return CGSize(width: 107, height: 33)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    
}
extension CompnyProductsViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == tabTable{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tabCell", for: indexPath) as? TabViewCell
            //            for tempCell in collectionView.visibleCells as! [TabViewCell] {
            //                // do something
            //
            //                tempCell.bottomView.isHidden = true
            //            }
            cell?.bottomView.isHidden = true
            selectedIndexPath = indexPath
            if indexPath.row == 0{
                catId = "0"
                
            }
            else{
                catId = companyDetails?.cats![indexPath.row - 1].id
                
            }
            Defaults[.subCategoryId] = catId ?? "0"
            modelView?.getCatsProductApi(successClosure: {
                (data) in
                
                if let dataModel = data as? CompanyCatsResponse{
                    self.companyCats = dataModel
                    
                    self.tabTable.reloadData()
                }
                
            })
            
        }
        else if collectionView == productTable{
            print("CurrentCompanyDeliveryPrice: \(Defaults[.deliveryPrice])")
            Defaults[.prodId] = ""
            Defaults[.prodId] = companyCats?.data![indexPath.row].id ?? ""
            Defaults[.deliveryPrice] = companyDetails?.delivery_price ?? ""
            print("CurrentCompanyDeliveryPrice: \(Defaults[.deliveryPrice])")
            performSegue(withIdentifier: "goProdDetailsSegue", sender: self)
        }
    }
    
}
extension CompnyProductsViewController: UICollectionViewDataSource { // , UICollectionViewDataSourcePrefetching{
//    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
//        // Begin asynchronously fetching data for the requested index paths.
//        for indexPath in indexPaths {
//            let model = companyCats?.data![indexPath.row]
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.productTable{
            return companyCats?.data?.count ?? 0
        }
        else{
            return (companyDetails?.cats?.count ?? 0) + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TabViewCell?
        let cell2: CatsProductCell?
        if collectionView == self.tabTable{
            
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tabCell", for: indexPath) as? TabViewCell
            
            cell?.bottomView.isHidden = true
            if indexPath.row == 0{
                
                //                cell?.bottomView.isHidden = false
                cell?.cellLabel.text = "all".localized()
                
            }
            else{
                
                cell?.cellLabel.text = companyDetails?.cats![indexPath.row - 1].name
                
                if companyDetails?.cats![indexPath.row - 1].id == Defaults[.subCategoryId] {
                    cell?.bottomView.isHidden = false
                }
            }
            if let tempRow = selectedIndexPath?.row{
                if tempRow == indexPath.row{
                    cell?.bottomView.isHidden = false
                }
            }
            
            
            return cell!
        }
        else{
            
            cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as? CatsProductCell
            
            let rowData  = companyCats?.data![indexPath.row]
            
            cell2?.productHead.text  = rowData!.name
            cell2?.delivery.text     = "\(rowData!.newPrice ?? 0) \("currency".localized())"
            
            if rowData!.photo == "" { // rowData.photo!.isEmptyrowData.photo!.isEmpty
                cell2?.productImage.image = #imageLiteral(resourceName: "instaLogo")
            } else {
                cell2?.productImage.sd_setImage(with: URL(string: (rowData!.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "instaLogo"))
            }
            
            cell2?.addCartBtn.tag    = indexPath.row
            
            print("price: \(rowData!.newPrice)")
            
            if let fav = rowData!.favChecked {
                if fav == 1 {
                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                } else {
                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                }
            }
            
            if let discount = Int(rowData!.discount ?? "0") {
                if discount > 0 {
                    cell2?.discountPercentage.isHidden   = false
                    cell2?.discountPercentage.text       = "\("discountKey".localized()) \(rowData!.discount ?? "0") %"
                } else {
                    cell2?.discountPercentage.isHidden = true
                }
            }
            
            if let gmma3 = Int(companyDetails?.gamm3 ?? "0") { // rowData!.gmmaly
                if gmma3 == 1 {
                    cell2?.gmma3lyIcon.isHidden = false
                } else {
                    cell2?.gmma3lyIcon.isHidden = true
                }
            }
            
            if let freeShipping = rowData!.freeDelivery {
                if freeShipping == 1 {
                    cell2?.shippingCostLabel.isHidden = false
                } else {
                    cell2?.shippingCostLabel.isHidden = true
                }
            }
            
            cell2?.productFavAction = {
                if Defaults[.logged]{
                    // Load Data From API
                    Defaults[.prodId] = rowData!.id ?? ""
                    
//                    self.startLoading()
                    self.productModelView?.addToFavApi { (data) in
                        
//                        self.stopLoading()
                        
                        if let dataModel = data as? BaseResponse{
                            if dataModel.result == "true"
                            {
                                if dataModel.message == "تمت الاضافة للمفضلة"
                                {
                                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                                } else {
                                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                                }
                            }
                            self.view.layoutSubviews()
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                else{
                    self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
                }
            }
            
            cell2?.productAddCartAction = {
                Defaults[.prodId] = rowData!.id ?? ""
                let requiredQuantity: Int   = 1
                let existCountity: Int      = Int(rowData!.counts ?? "0") ?? 0
                if requiredQuantity <= existCountity && requiredQuantity != 0 {
                    if !Defaults[.logged]{
                        self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
                    }
                    else{
                        // Edit Code => Add to cart first and then move to Addresses List
                        
//                        self.startLoading()
                        self.productModelView?.addToCartApi(price: rowData!.price ?? "0", count: requiredQuantity /* => required quantity */ ) { (data) in
                            
//                            self.stopLoading()
                            
                            if let dataModel = data as? BaseResponse{
                                
                                if dataModel.message == "تم الاضافة للسلة" {
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let myAddressVC = storyBoard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressViewController
                                    myAddressVC.catsProductObject = rowData
                                    myAddressVC.numberOfProducts = requiredQuantity
                                    Defaults[.numberOfProducts] = "\(requiredQuantity)"
                                    self.navigationController?.pushViewController(myAddressVC, animated: true)
                                    //performSegue(withIdentifier: "directBuy", sender: self)
                                }
                            }
                        }
                    }
                }
                else{
                    self.showDefaultAlert(title: "noEnoughQuantity".localized(), message: nil)
                }
            }
            
            if let newPrice = rowData!.newPrice{
                if let oldPrice = rowData!.price{
                    if Double(newPrice) == Double(oldPrice){
                        cell2?.oldPrice.isHidden = true
                    } else {
                        cell2?.oldPrice.isHidden = false
                        cell2?.oldPrice.text = "\(oldPrice) \("currency".localized())"
                    }
                }
            }
            return cell2!
            
        }
    }
    
}

extension CompnyProductsViewController: UITableViewDelegate{
    
}

extension CompnyProductsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tagerTable{
            return 6
        }
        if tableView == commentsTable{
            return companyComments?.data?.count ?? 0
        }
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tagerTable{
            let cell = tableView.dequeueReusableCell(withIdentifier: "tagerCell0", for: indexPath) as? tagerCell0
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "tagerCell1", for: indexPath) as? tagerCell1
            
            if indexPath.row == 0  {
                
                
                cell?.descBodyLbl.text = "breif".localized() + "\n\n" + (companyDetails?.feedData ?? "")
                
                let myString:NSString = (cell?.descBodyLbl.text as? NSString)!
                var myMutableString = NSMutableAttributedString()
                myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedStringKey.font:UIFont(name: "DroidArabicKufi-Bold", size: 11.0)!])
                
                myMutableString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "DroidArabicKufi-Bold", size: 13.0)!, range: NSRange(location:0,length: 12))
                
                    // Edit Code
                myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: NSRange(location:13,length:((cell?.descBodyLbl.text?.count)! - 13)))
                    // Old Code
//                myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: NSRange(location:13,length:((cell?.descBodyLbl.text?.length)! - 13)))
                
                myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:12))
                cell?.descBodyLbl.attributedText = myMutableString
                return cell!
            }
            
            
                if indexPath.row == 1{
                    cell1?.cell2HeadLbl.text = "specialization".localized()
                    cell1?.cell2BodyLbl.text = companyDetails?.catsName
                }
                if indexPath.row == 2{
                    cell1?.cell2HeadLbl.text = "workHours".localized()
                    cell1?.cell2BodyLbl.text = "\("from".localized()) \(companyDetails?.workFrom ?? "") \("to".localized()) \(companyDetails?.workTo ?? "") "
                    
                }
                
                if indexPath.row == 3{
                    cell1?.cell2HeadLbl.text = "deliveryPeriod".localized()
                    cell1?.cell2BodyLbl.text = companyDetails?.delivery_time ?? ""
                }
                if indexPath.row == 4{
                    cell1?.cell2HeadLbl.text = "deliveryCost".localized()
                    cell1?.cell2BodyLbl.text = "\(companyDetails?.delivery_price ?? "") \("currency".localized())"
                }
                
                if indexPath.row == 5{
                    cell1?.cell2HeadLbl.text = "payType".localized()
                    cell1?.cell2BodyLbl.text = companyDetails?.pay_name ?? ""
                }
                
                return cell1!
            
        }
        if tableView == commentsTable{
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "tagerCellEvaluation", for: indexPath) as? tagerCellEvaluation
            cell2?.dateLbl.text = companyComments?.data![indexPath.row].cdate
            cell2?.userName.text = companyComments?.data![indexPath.row].name
            cell2?.commect.text =  companyComments?.data![indexPath.row].comment ?? ""
            cell2?.rateView.rating = Double(companyComments?.data![indexPath.row].rate ?? "") ?? 0.0
            
            return cell2!
        }
        return UITableViewCell()
    }
    

    
    
}
