//
//  WebViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/17/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import WebKit
import SwiftyUserDefaults

class LinkViewController: UIViewController  {
    
    @IBOutlet weak var conditionWebView: UIWebView!
    
    var url: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBar(isBack: true)
        guard let url = URL(string: url) else { return }
        conditionWebView.loadRequest(URLRequest(url:url))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Defaults[.linkPageFlag] = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Defaults[.dismissed] = false
        Defaults[.linkPageFlag] = true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
