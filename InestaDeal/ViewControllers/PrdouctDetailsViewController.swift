//
//  PrdouctDetailsViewController.swift
//  
//
//  Created by Waleed on 4/22/18.
//

import UIKit
import ImageSlideshow
import Alamofire
import SwiftyUserDefaults
import AlamofireImage

class PrdouctDetailsViewController: UIViewController {
    
    
    
    @IBOutlet weak var heartBottomConst: NSLayoutConstraint!
    @IBOutlet weak var favLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var prodPrice: UILabel!
    //@IBOutlet weak var prodName: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productNewPrice: UILabel!
    @IBOutlet weak var productOldPrice: UILabel!
    @IBOutlet weak var oldPriceView : UIView!
    @IBOutlet weak var numberOfProducts: UILabel!
    @IBOutlet weak var favIconImage: UIImageView!
    
    @IBOutlet weak var productDescLabel: UILabel!
    @IBOutlet weak var maxNumberOfProduct: UILabel!
    @IBOutlet weak var ImageSlideshow: ImageSlideshow!
    
    @IBOutlet weak var containerTable: UITableView!
    @IBOutlet weak var containerTableHieghtLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var deliveryTimeHint: UILabel!
    
    @IBOutlet weak var productDetailsLabel: UILabel!
    @IBOutlet weak var maxQuantityLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var directBuyButton: UIButton!
    @IBOutlet weak var addTotFavLabel: UILabel!
    @IBOutlet weak var sellarLabel: UILabel!
    
    var itemsCount = 0
     var amountOfProducts: String = "1"
    
    var productModelView    : ProductModelView? = ProductModelView()
    var imgsArray = [AlamofireSource]()
    var tempImgsArray = [AlamofireSource]()
    var cell: ImageSliderCell?
    var companyID = ""
    var bottomConstFlag = false
    var photoFlag1 = false
    var photoFlag2 = false
    var photoFlag3 = false
    var photoFlag4 = false
    var photoFlag5 = false
    var photoFlag6 = false
    var photoFlag7 = false
    
    var model: ProductDetailsResponse?
    {
        didSet{
            tableView.reloadData()
            containerTable.reloadData()
        }
    }
    var realtedProdModel: RelatedProductsResponse?
    {
        didSet{
            tableView.reloadData()
        }
    }
    var cell1: ProductDetailsCell2?
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    
    @IBAction func buyDirect(_ sender: UIButton) {
        let x: Int = Int(numberOfProducts.text ?? "0") ?? 0
        let y: Int = Int(model?.counts ?? "0") ?? 0
        if x <= y && x != 0 {
            if !Defaults[.logged]{
                performSegue(withIdentifier: "goLoginFromProdDetails", sender: self)
            }
            else{
                /*
                // Basic Code
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let myAddressVC = storyBoard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressViewController
                myAddressVC.model = model
                myAddressVC.numberOfProducts = Int(numberOfProducts.text ?? "0") ?? 0
                Defaults[.numberOfProducts] = numberOfProducts.text!
                self.navigationController?.pushViewController(myAddressVC, animated: true)
                //performSegue(withIdentifier: "directBuy", sender: self)
                */
                
                // Edit Code => Add to cart first and then move to Addresses List
                addToCartApi(price: model?.price ?? "0", count: x /* => required quantity */ ) { (data) in
                    if let dataModel = data as? BaseResponse{
                        
                        if dataModel.message == "تم الاضافة للسلة" {
                            
                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let myAddressVC = storyBoard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressViewController
                            myAddressVC.model = self.model
//                            myAddressVC.productDetailsObject = self.model
                            myAddressVC.numberOfProducts = Int(self.numberOfProducts.text ?? "0") ?? 0
                            Defaults[.numberOfProducts] = self.numberOfProducts.text!
                            Defaults[.deliveryPrice] = (self.model?.deliveryPrice!)! // New Add For updating delivery cost insted of getting this value from  company
                            self.navigationController?.pushViewController(myAddressVC, animated: true)
                            //performSegue(withIdentifier: "directBuy", sender: self)
                        }
                    }
                }
                
            }
        }
        else{
            self.showDefaultAlert(title: "noEnoughQuantity".localized(), message: nil)
        }
    }
 
 
    override func viewDidAppear(_ animated: Bool) {
//        selectedOptionsArray.append("0_4_احمر")
//        selectedOptionsArray.append("0_5_xl")
        getProductDetailsApi { (data) in
            
            if let dataModel = data as? ProductDetailsResponse{
                self.model = dataModel
                self.productName.text = self.model?.name
                self.productNewPrice.text = "\("price".localized()) : \(self.model?.newPrice ?? 0) \("currency".localized())"
                if let newPrice = self.model?.newPrice{
                    if let oldPrice = self.model?.price{
                        print("new \(newPrice)")
                        print("old \(oldPrice)")

                        if Double(newPrice) == Double(oldPrice){
                            self.productOldPrice.isHidden = true
                            self.oldPriceView.isHidden = true
                            
                        }else{
                            self.productOldPrice.isHidden = false
                            self.oldPriceView.isHidden = false
                            self.productOldPrice.text = "\("price".localized()) : \(oldPrice) \("currency".localized())"
                            
                        }
                    }
                }
                let str = self.model?.FeedData?.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                self.productDescLabel.text = str
                self.maxNumberOfProduct.text = "(\(self.model?.counts ?? ""))"
                
                self.favIconImage.image = dataModel.favChecked == 1 ? #imageLiteral(resourceName: "filled_heart") : #imageLiteral(resourceName: "emptyHart")
                
                self.tableView.reloadData()
                self.companyID = dataModel.company_id ?? ""
                self.sellarLabel.text = "\("seller".localized()) : \(dataModel.companyName ?? "")"
                
                if Defaults[.globalCompanyID] == "" ||  Defaults[.globalCompanyID] == dataModel.company_id ?? ""{
                    Defaults[.globalCompanyID] = dataModel.company_id ?? ""
                }
                else{
                    
                }
            }
            
            self.getRelatedProductsApi { (data) in
                if let dataModel = data as? RelatedProductsResponse{
                    
                    self.realtedProdModel = dataModel
                    
                }
            }
        }
    }
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        Defaults[.selectedOptionsArray] = []
        
        handleTabBarVisibility(hide: true)
        
        addNavBar(isBack: true)
        firstView.isHidden = true
        secondView.isHidden = true
        activity.center = view.center
        activity.color = #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
    }
    

    func localizeUI(){
        addToCartButton.setTitle("addToCart".localized(), for: .normal)
        directBuyButton.setTitle("directBuy".localized(), for: .normal)
        productDetailsLabel.text    = "productDetails".localized() + " : "
        quantityLabel.text          = "quantity".localized() + " : "
        maxQuantityLabel.text       = "maxQuantity".localized() + " : "
        addTotFavLabel.text         = "addToFav".localized()
        
        if Defaults[.langId] == "1" {
            productDescLabel.textAlignment = .left
        } else if Defaults[.langId] == "2" {
            productDescLabel.textAlignment = .right
        }
    }

        override func viewDidLayoutSubviews() {
            containerTableHieghtLayoutConstraint.constant = containerTable.contentSize.height
        }
    
    @IBAction func shareAction(_ sender: UIButton) {
        let textToShare = "InstaDeal Application. \n https://itunes.apple.com/us/app/instadeal/id1409473459?mt=8"
        if let myWebsite = NSURL(string: "") {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            //
            
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func addFavAction(_ sender: UIButton) {
        if Defaults[.logged]{
            // Load Data From API
            productModelView?.addToFavApi { (data) in
                
                if let dataModel = data as? BaseResponse{
                    if dataModel.result == "true"
                    {
                        if dataModel.message == "تمت الاضافة للمفضلة" {
//                            self.heartBottomConst.constant = -36
                            self.favIconImage.image = #imageLiteral(resourceName: "filled_heart")
                            self.favLbl.isHidden    = false
                        }
                        else if dataModel.message == "تم الحذف من المفضلة" {
//                            self.heartBottomConst.constant = 8
                            self.favIconImage.image = #imageLiteral(resourceName: "emptyHart")
                            self.favLbl.isHidden    = true
                        }
                    }
                    self.view.layoutSubviews()
                    self.view.layoutIfNeeded()
                }
            }
        }
        else{
            performSegue(withIdentifier: "goLoginFromProdDetails", sender: self)
        }
    }
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        cell?.slideShow.presentFullScreenController(from: self)
    }
    
    @IBAction func addToBasketAction(_ sender: UIButton) {
        let x: Int = Int(numberOfProducts.text ?? "0") ?? 0
        let y: Int = Int(model?.counts ?? "0") ?? 0
        if x <= y && x != 0{
            if Defaults[.logged]{
                for view in self.view.subviews {
                    let tempAmount = Int(amountOfProducts) ?? 0
                    if view.tag == 999
                    {

                        if Defaults[.productsIds].contains(Defaults[.prodId]){
                            let itemIndex = Defaults[.productsIds].index(of: Defaults[.prodId])
                            if let temp = Int (Defaults[.amountOfProducts][itemIndex!])
                            {
                                Defaults[.amountOfProducts][itemIndex!] =  "\(temp + tempAmount)"
                            }
                            
                        }
                        else{
                            Defaults[.productsIds].append(Defaults[.prodId])
                            Defaults[.amountOfProducts].append(amountOfProducts)
                        }
                    }
                }
                
                if Defaults[.productsIds].contains(Defaults[.prodId]){
                    let itemIndex = Defaults[.productsIds].index(of: Defaults[.prodId])
                    
                    if let temp = Int (Defaults[.amountOfProducts][itemIndex!]){
                        let tempAmount = Int(amountOfProducts) ?? 0
                        addToCartApi(price: model?.price ?? "0", count: tempAmount) { (data) in
                            if let dataModel = data as? BaseResponse{
                                
                                if dataModel.message == "تم الاضافة للسلة"{
                                    
                                    self.showDefaultAlert(title: "addedToCart".localized(), message: nil, actionBlock: {
                                        self.navigationController?.popViewController(animated: true)
                                    })
                                    
                                  
                                }
                            }
                        }
                    }
                    
                }
                
            }
            else{
                performSegue(withIdentifier: "goLoginFromProdDetails", sender: self)
            }
        }
        else{
            self.showDefaultAlert(title: "noEnoughQuantity".localized(), message: nil)
        }
 
    }
    
    func addToCartApi(price : String , count : Int ,successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "client_id": Defaults[.userId] ,
            "prod_id": Defaults[.prodId] ,
            "count": count ,
            "price": price ,
            "options":Defaults[.selectedOptionsArray]
        ]
        
        Alamofire.request("https://www.instadeal.co/home/addCart?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
//    
//    func addToFavApi(successClosure: @escaping SuccessClosure){
//        
//        let params: Parameters = [
//            "client_id": Defaults[.userId] ,
//            "prod_id": Defaults[.prodId] ,
//        ]
//        
//        
//        Alamofire.request("https://www.instadeal.co/home/addFavorite", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
//            .responseJSON { response in
//                
//                if let json = response.result.value {
//                    if let data = json as? [String : Any]{
//                        let dataModel = BaseResponse(JSON: data)
//                        successClosure(dataModel!)
//                    }
//                }
//                
//        }
//        
//    }
    
    func getProductDetailsApi(successClosure: @escaping SuccessClosure){
        
        if !Defaults[.logged]{
            Defaults[.userId] = "0"
        }
        
        let params: Parameters = [
            "lang": "\(Defaults[.langId])"
        ]

        let tempProdId = Defaults[.prodId]
        print("getProduct: https://www.instadeal.co/home/getProduct/\(tempProdId)/\(Defaults[.userId] )?lang=\(Defaults[.langId])")
        Alamofire.request("https://www.instadeal.co/home/getProduct/\(tempProdId)/\(Defaults[.userId] )", method: .get, parameters: params)
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = ProductDetailsResponse(JSON: data)
                        Defaults[.catId] = dataModel?.cat_id ?? ""
                        if dataModel?.photo != "" && !self.photoFlag1 {
                            self.photoFlag1 = true
                            self.imgsArray.append(AlamofireSource(urlString: dataModel?.photo ?? "")!)
                        }
                        if dataModel?.photo1 != "" && !self.photoFlag2 {
                            self.photoFlag2 = true
                            self.imgsArray.append(AlamofireSource(urlString: dataModel?.photo1 ?? "")!)
                        }
                        if dataModel?.photo2 != "" && !self.photoFlag3 {
                            self.imgsArray.append(AlamofireSource(urlString: dataModel?.photo2 ?? "")!)
                            self.photoFlag3 = true
                        }
                        if dataModel?.photo3 != "" && !self.photoFlag4 {
                            self.imgsArray.append(AlamofireSource(urlString: dataModel?.photo3 ?? "")!)
                            self.photoFlag4 = true
                        }
                        if dataModel?.photo4 != "" && !self.photoFlag5 {
                            self.imgsArray.append(AlamofireSource(urlString: dataModel?.photo4 ?? "")!)
                            self.photoFlag5 = true
                        }
                        if dataModel?.photo5 != "" && !self.photoFlag6{
                            self.imgsArray.append(AlamofireSource(urlString: dataModel?.photo5 ?? "")!)
                            self.photoFlag6 = true
                        }
                        if dataModel?.photo6 != "" && !self.photoFlag7 {
                            self.imgsArray.append(AlamofireSource(urlString: dataModel?.photo6 ?? "")!)
                            self.photoFlag7 = true
                        }
      
                        self.deliveryTimeHint.text = "\("expectedDeliveryTime".localized()) ( \(dataModel?.delivery_time ?? "") )"
                        
                        if (dataModel?.more_feats!.count)! > 0 {
//                            self.containerTable.reloadData()
//                            containerTableHieghtLayoutConstraint.constant = containerTable.contentSize.height
                        }else {
//                            self.containerTableHieghtLayoutConstraint.constant = 0
                        }
                        
                        self.activity.stopAnimating()
                        self.activity.isHidden = true
                        self.firstView.isHidden = false
                        self.secondView.isHidden = false
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
    
    func getRelatedProductsApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "lang": "\(Defaults[.langId])"
        ]
        Alamofire.request("https://www.instadeal.co/home/getRelatedProducts/\(Defaults[.catId])/\(Defaults[.prodId])", method: .get, parameters: params)
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = RelatedProductsResponse(JSON: data)
                        
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
    
    @IBAction func addToCartAction(_ sender: UIButton) {
        
        performSegue(withIdentifier: "selfSegueProduct", sender: sender)
        Defaults[.prodId] = realtedProdModel?.data![sender.tag].id ?? ""
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.bottomConstFlag = false
//        imgsArray.removeAll()
        
        handleTabBarVisibility(hide: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        handleTabBarVisibility(hide: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func minusBtnAction(_ sender: UIButton) {
        var amountValue = Int( numberOfProducts.text ?? "")
        if amountValue != 1{
            amountValue = (amountValue ?? 1) - 1
        }
        amountOfProducts = "\(amountValue ?? 1)"
        numberOfProducts.text = "\(amountValue ?? 0)"
    }
    @IBAction func plusBtnAction(_ sender: UIButton) {
        var amountValue = Int( numberOfProducts.text ?? "")
        
        amountValue = (amountValue ?? 1) + 1
        
        amountOfProducts = "\(amountValue ?? 1)"
        numberOfProducts.text = "\(amountValue ?? 0)"
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if Defaults[.logged]{
        if segue.identifier == "directBuy"{
            
                let tempAmount = Int(amountOfProducts) ?? 0
                addToCartApi(price: model?.price ?? "0", count: tempAmount) { (data) in
                    if let dataModel = data as? BaseResponse{
                        
                        if dataModel.message == "تم الاضافة للسلة"{
                            
                            self.showDefaultAlert(title: "addedToCart".localized(), message: nil, actionBlock: {
                                
                            })
                        }
                    }
                }
            }
            
        }
        if segue.identifier == "selfSegueProduct"{
            
        }
        
    }
}

extension PrdouctDetailsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == containerTable {
            return  model?.more_feats!.count ?? 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == containerTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! OptionRow
            cell.tableCellHeaderLbl.text = model?.more_feats![indexPath.row].name
            cell.Options = model
            cell.sectionRow = indexPath.row
            return cell
        }
        else {
            if indexPath.row == 0{
                cell = tableView.dequeueReusableCell(withIdentifier: "ImageSliderCell") as? ImageSliderCell
                
                cell?.prepareSlider()
                
                cell?.slideShow.setImageInputs(self.imgsArray)
                cell?.imgsNumber.text = "\(self.imgsArray.count)"
                
                return cell ?? UITableViewCell()
            }
            
            
            
            //        if indexPath.row == 3{
            //            let cell3: UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "ProductDetailsCell3"))!
            //
            //            return cell3
            //        }
            
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == containerTable {
//            return UITableViewAutomaticDimension
            return 100
        }
        else {
            if indexPath.row == 0
            {
                return 285
            }
            if indexPath.row == 2
            {
                return UITableViewAutomaticDimension
            }
            if indexPath.row == 3
            {
                return 100
            }
            if indexPath.row == 4{
                return 200
            }
            return 75
        }
    }
    
    
}

extension PrdouctDetailsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == containerTable {
        }
        else {
            if indexPath.row == 0{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ImageSliderCell") as! ImageSliderCell
                cell.slideShow.presentFullScreenController(from: self)
            }
        }
        
    }
    
}
