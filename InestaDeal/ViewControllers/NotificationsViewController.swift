//
//  NotificationsViewController.swift
//  InestaDeal
//
//  Created by Waleed on 6/21/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

extension UIApplication {
    var statusBarView: UIView? {
        
        if #available(iOS 13, *) {
            
        } else if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}


class NotificationsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataModel?.data?.count ?? 0
    }
    
    var incresingAmmount = 20
    var lastFrom = 0
    var index = 0
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if indexPath.row == (dataModel?.data?.count) ?? 0 - 1 {
            
            
            getNotificationsApi(start: "\(lastFrom)", limit: "\(incresingAmmount)") { (data) in
                
                if let dataa = data as? NotificationsResponse{
                    self.dataModel = dataa
                    self.lastFrom += self.incresingAmmount
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsCell") as? NotificationsCell
        

        
        index += 1
        cell?.notificationName.text = dataModel?.data?[indexPath.row].title
        cell?.notificationDate.text = dataModel?.data?[indexPath.row].ndate
        cell?.notificationBody.text = dataModel?.data?[indexPath.row].text2
        return cell! 
    }
    
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableViewAutomaticDimension
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 88
//    }
    
    @IBOutlet weak var goToAppBtn: UIButton!
    @IBOutlet weak var notificationsTable: UITableView!
    
    
    var dataModel: NotificationsResponse?
    {
        didSet{
            if dataModel?.data?.count != 0{
                notificationsTable.reloadData()
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        notificationsTable.rowHeight = UITableViewAutomaticDimension
//        notificationsTable.estimatedRowHeight = 88
        
        goToAppBtn.setTitle("goToApp".localized(), for: .normal)
        
        if #available(iOS 13, *) {
            
        } else {
            UIApplication.shared.statusBarView?.backgroundColor = UIColor(hex: 0xFF9000)
        }
        
        getNotificationsApi(start: "0", limit: "20") { (data) in
            
            if let dataa = data as? NotificationsResponse{
                self.dataModel = dataa
                self.lastFrom += self.incresingAmmount
            }
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        handleTabBarVisibility(hide: false)
        
        notificationsTable.layoutSubviews()
        notificationsTable.layoutIfNeeded()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getNotificationsApi(start: String, limit: String , successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/GetNotis/\(Defaults[.userId])/\(limit)/\(start)?lang=\(Defaults[.langId])", method: .get,  encoding: JSONEncoding.default)
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = NotificationsResponse(JSON: data)
                        
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }

}
