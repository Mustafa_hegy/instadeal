//
//  FinishOrderViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/11/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class FinishOrderViewController: UIViewController {

    @IBOutlet weak var paymentTypeLbl: UILabel!
    @IBOutlet weak var orderNumberLbl: UILabel!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var deliveryTimeLbl: UILabel!

    @IBOutlet weak var orderDetailsLabel: UILabel!
    @IBOutlet weak var successHintLabel: UILabel!
    @IBOutlet weak var paymentTypeTitleLbl: UILabel!
    @IBOutlet weak var orderNumberTitleLbl: UILabel!
    @IBOutlet weak var dateTimeTitleLbl: UILabel!
    @IBOutlet weak var deliveryTimeTitleLbl: UILabel!

    @IBOutlet weak var backHomeButton: UIButton!
    
    var paymentType: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        handleTabBarVisibility(hide: true)
        
        self.tabBarController?.tabBar.layer.zPosition = -1
        getOrderApi { (data) in
            if let model1 = data as? [String : Any]{
                if let model = model1 ["data"] as? [String : Any]{
                //self.paymentTypeLbl.text = model["pay"] as? String
                    if let paymentType = self.paymentType{
                        if paymentType == 1 {
                            self.paymentTypeLbl.text = "cash".localized()
                        } else {
                            self.paymentTypeLbl.text = "knet".localized()
                        }
                      
                    }
                    self.orderNumberLbl.text    = model["id"] as? String
                    self.dateTimeLbl.text       = (model["odate"] as! String) + " " + ((model["otime"] as! String))
                    self.deliveryTimeLbl.text   = (model["time_id_date"] as! String) + " " + ((model["time_txt"] as! String))
                }
            }
        }
//        addNavBar(isBack: false)
        addNavBarFinishOrder(isBack: false,showMenue: false)
        
        // Do any additional setup after loading the view.
    }
    
    func localizeUI(){
        orderDetailsLabel.text      = "orderDetails".localized()
        successHintLabel.text       = "successOrderHint".localized()
        paymentTypeTitleLbl.text    = "payType".localized() + " :  "
        orderNumberTitleLbl.text    = "orderNumber".localized() + " :  "
        dateTimeTitleLbl.text       = "orderDateTime".localized() + " :  "
        deliveryTimeTitleLbl.text   = "deliveryTime".localized() + " :  "
        backHomeButton.setTitle("backHome".localized(), for: .normal)
        
        if Defaults[.langId] == "1" {
            paymentTypeLbl.textAlignment            = .left
            orderNumberLbl.textAlignment            = .left
            dateTimeLbl.textAlignment               = .left
            deliveryTimeLbl.textAlignment           = .left
            
            paymentTypeTitleLbl.textAlignment       = .left
            orderNumberTitleLbl.textAlignment       = .left
            dateTimeTitleLbl.textAlignment          = .left
            deliveryTimeTitleLbl.textAlignment      = .left
        } else if Defaults[.langId] == "2" {
            paymentTypeLbl.textAlignment            = .right
            orderNumberLbl.textAlignment            = .right
            dateTimeLbl.textAlignment               = .right
            deliveryTimeLbl.textAlignment           = .right
            
            paymentTypeTitleLbl.textAlignment       = .right
            orderNumberTitleLbl.textAlignment       = .right
            dateTimeTitleLbl.textAlignment          = .right
            deliveryTimeTitleLbl.textAlignment      = .right
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToHomeButtonTapped(_ sender: Any) {
        /*
         // Old Code
        for controller in self.navigationController?.viewControllers ?? [] {
            if controller.isKind(of: HomeViewController.self) {
                // Show tab bar before go back to hoe screen
                self.tabBarController?.tabBar.layer.zPosition = 0
                handleTabBarVisibility(hide: true)
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        */
        // Edited Code
        
        let viewController:MainTabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarRoot") as! MainTabBarController
        let appDelegate = UIApplication.shared.delegate
        appDelegate?.window??.rootViewController = viewController
    }
    func getOrderApi(successClosure: @escaping SuccessClosure){
        Alamofire.request("https://www.instadeal.co/home/getOrder/\(Defaults[.orderId])?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    successClosure(data)
                }
            }
        }
        
    }
}
