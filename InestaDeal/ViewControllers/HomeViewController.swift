//
//  FirstViewController.swift
//  InestaDeal
//
//  Created by Waleed on 3/31/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import ImageSlideshow
import SwifterSwift
import SDWebImage
import AlamofireImage
import SwiftyUserDefaults
//import OneSignal
import Alamofire

class HomeViewController: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headTitle1: UILabel!
    @IBOutlet weak var headTitle2: UILabel!
    @IBOutlet weak var headTitle3: UILabel!
    @IBOutlet weak var headTitle4: UILabel!
    @IBOutlet weak var headTitle5: UILabel!
    @IBOutlet weak var catsCollectionShow: UICollectionView!
    @IBOutlet weak var offersCollectionShow: UICollectionView!
    @IBOutlet weak var topProductsCollectionShow: UICollectionView!
    @IBOutlet weak var newProductsCollectionShow: UICollectionView!
    @IBOutlet weak var selectedProductsCollectionShow: UICollectionView!
    @IBOutlet weak var catsCollectionShowAutoLayoutConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var offersCollectionShowAutoLayoutConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var topProductsCollectionShowAutoLayoutConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var newProductsCollectionShowAutoLayoutConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var selectedProductsCollectionShowAutoLayoutConstraintHeight: NSLayoutConstraint!
    
//    @IBOutlet weak var searchField: UISearchBar!
    let activity        = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var subCategoryId: String?
    var limit           = 10
    var start           = 0
    var enableLoadMore  : Bool  = true
    var modelView: HomeModelView?
    var productModelView: ProductModelView? = ProductModelView()
    var imgsArray = [AlamofireSource]()
    var imgsTypes = [String]()
    var sliderModel: SlidersResponse?
    var categories: [CategoriesResponse]?
    {
        didSet{
            headTitle1.text         = "cats".localized()
//            containerTable.reloadData()
            catsCollectionShow.reloadData()
        }
    }
    
    var offers: [SlidersResponse]?
    {
        didSet{
            headTitle2.text         = "offers".localized()
            offersCollectionShow.reloadData()
            
            if Defaults[.langId] == "1" {
                offersCollectionShow.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
            } else if Defaults[.langId] == "2" {
                offersCollectionShow.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
            }
        }
    }
    
    var topProducts: [CatsProducts]?
    {
        didSet{
            headTitle3.text         = "topSales".localized()
            topProductsCollectionShow.reloadData()
            
            if Defaults[.langId] == "1" {
                topProductsCollectionShow.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
            } else if Defaults[.langId] == "2" {
                topProductsCollectionShow.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
            }
        }
    }
    
    var newProducts: [CatsProducts]?
    {
        didSet{
            headTitle4.text         = "newProducts".localized()
            newProductsCollectionShow.reloadData()
            
            if Defaults[.langId] == "1" {
                newProductsCollectionShow.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
            } else if Defaults[.langId] == "2" {
                newProductsCollectionShow.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
            }
        }
    }
    
    var selectedProducts: [CatsProducts]?
    {
        didSet{
            headTitle5.text         = "recommended".localized()
            selectedProductsCollectionShow.reloadData()
        }
    }
    
    var dashboardList: DashboardResponse?
    {
        didSet{
            renderApiResponse()
        }
    }
//    @IBOutlet weak var containerTable: UITableView!
    @IBOutlet weak var slideShowView: ImageSlideshow!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        scrollView.isHidden  = true
//        self.tabBarController?.tabBar.layer.zPosition = 0
        
        handleTabBarVisibility(hide: false)
        
        modelView = HomeModelView(scene: self)
//        containerTable.tableFooterView = UIView()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        slideShowView.addGestureRecognizer(gestureRecognizer)
        /*
        modelView?.getCategoriesApi(successClosure: { [unowned self] (data) in
            if let dataModel = data as? CategoriesResponse{
                self.categories = dataModel
            }
        })
         */
        addNavBar(isBack: false)
        // Do any additional setup after loading the view, typically from a nib.
        
        //activity start loading
        startLoading()
        // get dashboard data
        modelView?.getHomeDashboardApi(successClosure: { [unowned self] (data) in
            if let dataModel = data as? DashboardResponse{
                self.dashboardList = dataModel
            }
            self.stopLoading()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if #available(iOS 13, *) {
            
        } else if responds(to: Selector(("statusBar"))) {
            UIApplication.shared.statusBarView?.backgroundColor = .clear
        }
        
        if  Defaults[.logged]{
//            let _playerId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
            let _playerId = Defaults[.PNToken]
            sendPlayerIdApi(playerId: _playerId ?? "", successClosure: { (data) in
            })
        }
        
        handleTabBarVisibility(hide: false)
        
//        modelView?.getCategoriesApi(successClosure: { [unowned self] (data) in
//
//            if let dataModel = data as? CategoriesResponse{
//
//                self.categories = dataModel
//
//            }
//
//        })
        
        self.imgsArray.removeAll()
        slideShowView.pageControl.pageIndicatorTintColor =  UIColor(hex: 0xF08944)
        slideShowView.contentScaleMode = .scaleToFill // .scaleAspectFill
        slideShowView.circular = true
        slideShowView.slideshowInterval = 5.0
        /*
         modelView?.getHomeSliderApi(successClosure: { [unowned self] (data) in
         
         if let dataModel = data as? SlidersResponse{
         print("dattta:\(dataModel)")
         for index in dataModel.data ?? []{
         print("index:\(index.photo ?? "")")
         self.imgsArray.append(AlamofireSource(urlString: index.photo ?? "")!)
         self.sliderModel = dataModel
         
         
         }
         
         self.slideShowView.setImageInputs(self.imgsArray)
         
         }
         
         })
         */
    }
    
    override func viewDidLayoutSubviews() {
        catsCollectionShowAutoLayoutConstraintHeight.constant               =  catsCollectionShow.collectionViewLayout.collectionViewContentSize.height // catsCollectionShow.contentSize.height //
        offersCollectionShowAutoLayoutConstraintHeight.constant             = offersCollectionShow.collectionViewLayout.collectionViewContentSize.height
        topProductsCollectionShowAutoLayoutConstraintHeight.constant   = topProductsCollectionShow.collectionViewLayout.collectionViewContentSize.height
        newProductsCollectionShowAutoLayoutConstraintHeight.constant   = newProductsCollectionShow.collectionViewLayout.collectionViewContentSize.height
        selectedProductsCollectionShowAutoLayoutConstraintHeight.constant   = selectedProductsCollectionShow.collectionViewLayout.collectionViewContentSize.height
    }
    
    func localizeUI(){
        searchTextField.text    = "search".localized()
        
        if Defaults[.langId] == "1" {
            headTitle1.textAlignment                                = .left
            headTitle2.textAlignment                                = .left
            headTitle3.textAlignment                                = .left
            headTitle4.textAlignment                                = .left
            headTitle5.textAlignment                                = .left
            catsCollectionShow.semanticContentAttribute             = .forceLeftToRight
            offersCollectionShow.semanticContentAttribute           = .forceLeftToRight
            topProductsCollectionShow.semanticContentAttribute      = .forceLeftToRight
            newProductsCollectionShow.semanticContentAttribute      = .forceLeftToRight
            selectedProductsCollectionShow.semanticContentAttribute = .forceLeftToRight
        } else if Defaults[.langId] == "2" {
            headTitle1.textAlignment                                = .right
            headTitle2.textAlignment                                = .right
            headTitle3.textAlignment                                = .right
            headTitle4.textAlignment                                = .right
            headTitle5.textAlignment                                = .right
            catsCollectionShow.semanticContentAttribute             = .forceRightToLeft
            offersCollectionShow.semanticContentAttribute           = .forceRightToLeft
            topProductsCollectionShow.semanticContentAttribute      = .forceRightToLeft
            newProductsCollectionShow.semanticContentAttribute      = .forceRightToLeft
            selectedProductsCollectionShow.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    @IBAction func homeSearchAction(_ sender: UIButton) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        searchTextField.endEditing(true)
       
            return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        searchTextField.textAlignment = .center
        return true
    }
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        searchTextField.textAlignment = .center
    }
    
    func sendPlayerIdApi(playerId: String ,successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/updateUserToken/\(Defaults[.userId])/\(playerId)?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
//                if let data = json as? [String : Any]{
//                    let dataModel = SlidersResponse(JSON: data)
//
                    successClosure(json)
//                }
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.hidesBottomBarWhenPushed = false
        if segue.identifier == "moreSubCatSeg"{
            let destinationVC = segue.destination as! SubCategoriesViewController
            destinationVC.parentCategoryId = "\((sender as! UIButton).tag)"
//             destinationVC.parentCategoryId = categories?.data![(sender as! UIButton).tag].id
//            destinationVC.categoryName = categories?.data![(sender as! UIButton).tag].name
        }
        if segue.identifier == "companySeg"{
            
            let destinationVC = segue.destination as! CompanyViewController
            destinationVC.subCategoryId = subCategoryId
        }
        if segue.identifier == "catProductsSeg"{
            
            let destinationVC = segue.destination as! ProductsListViewController
            destinationVC.subCategoryId = subCategoryId
        }
    }
    @objc func didTap() {
        if sliderModel?.data?[slideShowView.currentPage].slider_type == "link"{
            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
            viewController.url = (sliderModel?.data?[slideShowView.currentPage].link)!
            Defaults[.dismissed] = true
            self.present(viewController, animated: true, completion: nil)
        }
        else{
            Defaults[.prodId] = sliderModel?.data?[slideShowView.currentPage].link ?? ""
            performSegue(withIdentifier: "showFromHome", sender: self)
        }
//        slideShowView.presentFullScreenController(from: self)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Class Methods
    private func startLoading() {
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
    }
    
    private func stopLoading() {
        self.activity.stopAnimating()
        self.activity.isHidden = true
    }
    
    func renderApiResponse() {
        
        scrollView.isHidden  = false
        
        // Set Slider
        for index in dashboardList?.slider ?? []{
            // print("index:\(index.photo ?? "")")
            self.imgsArray.append(AlamofireSource(urlString: index.photo ?? "")!)
        }
        self.slideShowView.setImageInputs(self.imgsArray)
        
        // Set Categories
        categories          = dashboardList?.cats
        
        // Set Offers
        offers              = dashboardList?.offers
         
        // Set Products
        topProducts         = dashboardList?.topSales
        newProducts         = dashboardList?.newProducts
        selectedProducts    = dashboardList?.selectedProducts
        
    }
    
    func startLoadingSelectedProducts() {
        
        // Load Selected Products Data From API
        self.startLoading()
        modelView?.getSelectedProductsApi(limit:limit,start:start ,successClosure: { [weak self] (data) in
            // Remove Activity Indicator
            self?.stopLoading()
            
            if let dataModel = data as? CompanyCatsResponse {
                
                if (dataModel.data?.count)! > 0 {
                    self?.selectedProducts == nil ? self?.selectedProducts = dataModel.data : self?.selectedProducts?.append(contentsOf: dataModel.data!)
                    
                    self?.selectedProductsCollectionShow.reloadData()
                    
                    if (dataModel.data?.count)! >= self!.limit {
                        self?.start             = self!.start + self!.limit
                        self?.enableLoadMore    = true
                    } else {
                        self?.enableLoadMore    = false
                    }
                }else {
                    self?.enableLoadMore    = false
                }
            }
        })
    }

}

extension HomeViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var rowsCount = 0
        if collectionView == catsCollectionShow {
            rowsCount = categories?.count ?? 0
        }
        else if collectionView == offersCollectionShow {
            rowsCount = offers?.count ?? 0
        }
        else if collectionView == topProductsCollectionShow {
            rowsCount = topProducts?.count ?? 0
        }
        else if collectionView == newProductsCollectionShow {
            rowsCount = newProducts?.count ?? 0
        }
        else if collectionView == selectedProductsCollectionShow {
            rowsCount = selectedProducts?.count ?? 0
        }
        return rowsCount
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CategoryCell?
        let cell2: CatsProductCell?
        
        if collectionView == catsCollectionShow {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CategoryCell

            let rowData = categories![indexPath.row]
            
            if rowData.icon == "" {
                cell?.imageView.image = #imageLiteral(resourceName: "instaLogo")
            } else {
                cell?.imageView.sd_setImage(with: URL(string: (rowData.icon) ?? ""), placeholderImage: UIImage(named: "instaLogo"))
            }
            
            cell?.subCategoryLbl.text = rowData.name
            return cell!
        }
        else if collectionView == offersCollectionShow {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CategoryCell
           
            let rowData = offers![indexPath.row]
            
            if rowData.photo == "" {
                cell?.imageView.image = #imageLiteral(resourceName: "instaLogo")
            } else {
                cell?.imageView.sd_setImage(with: URL(string: (rowData.photo) ?? ""), placeholderImage: UIImage(named: "instaLogo"))
            }
            return cell!
        }
        else {
            cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CatsProductCell
            
            var rowData : CatsProducts?
            if collectionView == topProductsCollectionShow {
                rowData = topProducts?[indexPath.row]
            }
            else if collectionView == newProductsCollectionShow {
                rowData = newProducts?[indexPath.row]
            }
            else if collectionView == selectedProductsCollectionShow {
                rowData = selectedProducts?[indexPath.row]
            }
            
            cell2?.productHead.text  = rowData?.name
            cell2?.delivery.text     = "\(rowData?.newPrice ?? 0) \("currency".localized())"
            
            if rowData?.photo == "" { // rowData.photo!.isEmptyrowData.photo!.isEmpty
                cell2?.productImage.image = #imageLiteral(resourceName: "instaLogo")
            } else {
                cell2?.productImage.sd_setImage(with: URL(string: (rowData?.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "instaLogo"))
            }
            
            cell2?.addCartBtn.tag    = indexPath.row
            
            print("price: \(rowData?.newPrice ?? 0)")
            
            if let fav = rowData?.favChecked {
                if fav == 1 {
                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                } else {
                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                }
            }
            
            if let discount = Int(rowData?.discount ?? "0") {
                if discount > 0 {
                    cell2?.discountPercentage.isHidden   = false
                    cell2?.discountPercentage.text       = "\("discountKey".localized()) \(rowData?.discount ?? "0")%"
                } else {
                    cell2?.discountPercentage.isHidden = true
                }
            }
            
            if let gmma3 = Int(rowData?.gmmaly ?? "0") {
                if gmma3 == 1 {
                    cell2?.gmma3lyIcon.isHidden = false
                } else {
                    cell2?.gmma3lyIcon.isHidden = true
                }
            }
            
            if let freeShipping = rowData?.freeDelivery {
                if freeShipping == 1 {
                    cell2?.shippingCostLabel.isHidden = false
                } else {
                    cell2?.shippingCostLabel.isHidden = true
                }
            }
            cell2?.productFavAction = {
                if Defaults[.logged]{
                    // Load Data From API
                    Defaults[.prodId] = rowData?.id ?? ""
                    
                    self.startLoading()
                    self.productModelView?.addToFavApi { (data) in
                        
                        self.stopLoading()
                        
                        if let dataModel = data as? BaseResponse{
                            if dataModel.result == "true"
                            {
                                if dataModel.message == "تمت الاضافة للمفضلة"
                                {
                                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                                } else {
                                    cell2?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                                }
                            }
                            self.view.layoutSubviews()
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                else{
                    self.performSegue(withIdentifier: "goLogin", sender: self)
                }
            }
            
            cell2?.productAddCartAction = {
                Defaults[.prodId] = rowData?.id ?? ""
                let requiredQuantity: Int   = 1
                let existCountity: Int      = Int(rowData?.counts ?? "0") ?? 0
                if requiredQuantity <= existCountity && requiredQuantity != 0 {
                    if !Defaults[.logged]{
                        self.performSegue(withIdentifier: "goLogin", sender: self)
                    }
                    else{
                        // Edit Code => Add to cart first and then move to Addresses List
                        
                        self.startLoading()
                        self.productModelView?.addToCartApi(price: rowData?.price ?? "0", count: requiredQuantity /* => required quantity */ ) { (data) in
                            
                            self.stopLoading()
                            
                            if let dataModel = data as? BaseResponse{
                                
                                if dataModel.message == "تم الاضافة للسلة" {
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let myAddressVC = storyBoard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressViewController
                                    myAddressVC.catsProductObject = rowData
                                    myAddressVC.numberOfProducts = requiredQuantity
                                    Defaults[.numberOfProducts] = "\(requiredQuantity)"
                                    self.navigationController?.pushViewController(myAddressVC, animated: true)
                                    //performSegue(withIdentifier: "directBuy", sender: self)
                                }
                            }
                        }
                    }
                }
                else{
                    self.showDefaultAlert(title: "noEnoughQuantity".localized(), message: nil)
                }
            }
            
            if let newPrice = rowData?.newPrice{
                if let oldPrice = rowData?.price{
                    if Double(newPrice) == Double(oldPrice){
                        cell2?.oldPrice.isHidden = true
                    } else {
                        cell2?.oldPrice.isHidden = false
                        cell2?.oldPrice.text = "\(oldPrice) \("currency".localized())"
                    }
                }
            }
            return cell2!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == catsCollectionShow {
            let rowData = categories![indexPath.row]
            
            Defaults[.subCategoryId]        = ""
            Defaults[.subCategoryName]      = ""
            Defaults[.parentCategoryId]     = rowData.id ?? ""
            
            // Move to Products List related to selected Category
            performSegue(withIdentifier: "catProductsSeg", sender: self) // New Code
        }
        else if collectionView == offersCollectionShow {
            let rowData = offers![indexPath.row]
            
            if rowData.slider_type == "link"{
                let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
                viewController.url = (rowData.link)!
                Defaults[.dismissed] = true
                self.present(viewController, animated: true, completion: nil)
            }
            else{
                Defaults[.prodId] = rowData.link ?? ""
                performSegue(withIdentifier: "showFromHome", sender: self)
            }
        }
        else {
            var rowData : CatsProducts?
            if collectionView == topProductsCollectionShow {
                rowData = topProducts?[indexPath.row]
            }
            else if collectionView == newProductsCollectionShow {
                rowData = newProducts?[indexPath.row]
            }
            else if collectionView == selectedProductsCollectionShow {
                rowData = selectedProducts?[indexPath.row]
            }
            
            Defaults[.prodId] = rowData?.id ?? ""
            performSegue(withIdentifier: "showFromHome", sender: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == selectedProductsCollectionShow {
            if indexPath.row == selectedProducts!.count - 1 && enableLoadMore {
               print("Reach to last item")

                // Load Selected Products Data From API
                startLoadingSelectedProducts()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.image = nil
        verticalIndicator?.backgroundColor = appThemeColor
    }
    
}

extension HomeViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var itemsPerRow     :CGFloat
        var hardCodedPadding:CGFloat
        var itemWidth       :CGFloat = 0
        var itemHeight      :CGFloat = 0
        
        if collectionView == catsCollectionShow {
            itemsPerRow         = 4
            hardCodedPadding    = 8
            itemWidth           = ( (catsCollectionShow.frame.size.width - 20) / itemsPerRow ) - hardCodedPadding
            itemHeight          = 90
        }
        else if collectionView == offersCollectionShow {
            itemsPerRow         = 1.1
            hardCodedPadding    = 10
            itemWidth           = ( (offersCollectionShow.frame.size.width - 24) / itemsPerRow ) - hardCodedPadding
            itemHeight          = 124
        }
        else if collectionView == topProductsCollectionShow || collectionView == newProductsCollectionShow  || collectionView == selectedProductsCollectionShow {
            itemsPerRow         = 2
            hardCodedPadding    = 4
            itemWidth           = ( (selectedProductsCollectionShow.frame.size.width - 16) / itemsPerRow ) - hardCodedPadding
            itemHeight          = 274
        }
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
}

/*
extension HomeViewController : UITableViewDelegate { }

extension HomeViewController : UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return categories[section]
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
//    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
//        header.textLabel?.font = UIFont(name: "YourFontname", size: 14.0)
//        header.textLabel?.textAlignment = NSTextAlignment.right
//        header.backgroundView?.backgroundColor = .clear
//    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cellHeight = UITableViewAutomaticDimension
//        if dashboardList?.cats
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoryRow
        cell.tableCellHeaderLbl.text = categories?.data![indexPath.row].name
        cell.categories = categories
        cell.section = indexPath.row
        subCategoryId = cell.subCategoryId
        cell.moreBtn?.tag = indexPath.row//Int(categories?.data![indexPath.row].id ?? "0")!
        return cell
    }
    

    
}
*/
