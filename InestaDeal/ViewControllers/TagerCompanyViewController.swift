//
//  TagerCompanyViewController.swift
//  InestaDeal
//
//  Created by Waleed on 4/13/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyUserDefaults
class TagerCompanyViewController: UIViewController {

    @IBOutlet weak var conditionButton: UIButton!
    @IBOutlet weak var conditionView: UIView!
    @IBOutlet weak var myCompaniesView: UIView!
    @IBOutlet weak var myCompaniesButton: UIButton!
    @IBOutlet weak var menuView : UIView!
    var modelView: TagercompanyTableModelView?
    var subCategoryId: String?
    var companyId: String?
    var flagSegue: Bool = false
    var companies: CompanyResponse?
    {
        didSet{
            companiesTableView.reloadData()
            print("companiescount: \(companies?.data?.count)")
        }
    }
    

    @IBAction func allCompaniesTapped(sender:UIButton){
            conditionWebView.isHidden = true
            companiesTableView.isHidden = false
            myCompaniesView.isHidden = false
            conditionView.isHidden = true
            conditionButton.setTitleColor(UIColor(red: 76/255, green: 76/255, blue: 76/255, alpha: 1.0), for: .normal)
            myCompaniesButton.setTitleColor(UIColor(red: 174/255, green: 43/255, blue: 0/255, alpha: 1.0), for: .normal)
        
    }
    
    @IBAction func CompanyConditionTapped(sender: UIButton){
        conditionWebView.isHidden = false
        companiesTableView.isHidden = true
        myCompaniesView.isHidden = true
        conditionView.isHidden = false
        myCompaniesButton.setTitleColor(UIColor(red: 76/255, green: 76/255, blue: 76/255, alpha: 1.0), for: .normal)
        conditionButton.setTitleColor(UIColor(red: 174/255, green: 43/255, blue: 0/255, alpha: 1.0), for: .normal)
    }
    
    @IBOutlet weak var conditionWebView: UIWebView!
    @IBOutlet weak var companiesTableView: UITableView!
    
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBar(isBack: true)
        localizeUI()
        activity.center = view.center
        activity.color = #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
        conditionView.isHidden = true
        companiesTableView.contentInset.bottom = 30
        conditionButton.setTitleColor(UIColor.darkGray, for: .normal)
        menuView.roundCorners(corners: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner], radius: 25.0)
        guard let url2 = URL(string: "https://www.instadeal.co/conditions_company.html?lang=\(Defaults[.langId])") else { return }
        
        conditionWebView.loadRequest((URLRequest(url: url2)))
        companiesTableView.tableFooterView = UIView()
        modelView = TagercompanyTableModelView(scene: self)
        
        if flagSegue
        {
            modelView?.getUserCompaniesApi(successClosure: { [weak self] (data) in
                if let dataModel = data as? CompanyResponse{
                    self?.companies = dataModel
                    self?.activity.stopAnimating()
                    self?.activity.isHidden = true
                }
            })
        }
        else{
            modelView?.getCompaniesApi(successClosure: { [weak self] (data) in
                if let dataModel = data as? CompanyResponse{
                    self?.companies = dataModel
                    self?.activity.stopAnimating()
                    self?.activity.isHidden = true
                }
            })
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func localizeUI() {
        myCompaniesButton.setTitle("companies".localized(), for: .normal)
        conditionButton.setTitle("Terms & Conditions".localized(), for: .normal)
    }
    
}

extension TagerCompanyViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Defaults[.companyId] = companies?.data![indexPath.row].id ?? ""
        Defaults[.tagerCompanyId] = companies?.data![indexPath.row].id ?? ""
        Defaults[.deliveryFees] = companies?.data![indexPath.row].delivery_price ?? ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let destinationVC = segue.destination as! TagerCompanyProductsViewController
            destinationVC.companyId = companyId
    }
}

extension TagerCompanyViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return companies?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyTableCell") as! CompanyTableCell
        
        cell.mainPhoto.sd_setImage(with: URL(string: companies?.data![indexPath.row].photo1 ?? ""), placeholderImage: #imageLiteral(resourceName: "iconApp"))
        cell.logo.sd_setImage(with: URL(string: companies?.data![indexPath.row].photo ?? ""), placeholderImage:  #imageLiteral(resourceName: "iconApp"))
        
        if indexPath.row == 0 {
            cell.topView.isHidden = true
            cell.bottomView.isHidden = true
        }else {
            cell.topView.isHidden = false
            cell.bottomView.isHidden = true
        }
        
        if  indexPath.row  == (companies?.data!.count)! - 1 {
            cell.bottomView.isHidden = false
        }
        
    
        if  indexPath.row  != ((companies?.data!.count)! - 1), indexPath.row != 0{
            cell.topCompanyName.text = companies?.data![indexPath.row - 1].name ?? ""
            cell.topDeliveryLbl.text = companies?.data![indexPath.row - 1].delivery_price ?? "0"
//            cell.topDeliveryLbl.text = "\(companies?.data![indexPath.row - 1].deliveryPrice ?? 0)"
            cell.topDeliveryTimeLbl.text = companies?.data![indexPath.row - 1].delivery_time ?? ""
        }else {
            cell.bottomCompanyName.text = companies?.data![indexPath.row].name ?? ""
            cell.bottomDeliveryLbl.text = companies?.data![indexPath.row].delivery_price ?? "0"
//            cell.bottomDeliveryLbl.text = "\(companies?.data![indexPath.row].deliveryPrice ?? 0 )"
            cell.bottomDeliveryTimeLbl.text = companies?.data![indexPath.row].delivery_time ?? ""
        }
        
        if  (indexPath.row  == (companies?.data!.count)! - 1), (companies?.data!.count)! > 1 {
            cell.topCompanyName.text = companies?.data![indexPath.row - 1].name ?? ""
            cell.topDeliveryLbl.text = companies?.data![indexPath.row - 1].delivery_price ?? "0"
//            cell.topDeliveryLbl.text = "\(companies?.data![indexPath.row - 1].deliveryPrice ?? 0 )"
            cell.topDeliveryTimeLbl.text = companies?.data![indexPath.row - 1].delivery_time ?? ""
        }
        
        if companies?.data![indexPath.row].gmmaly == "1" {
            cell.gmmalyView.isHidden = false
        } else {
             cell.gmmalyView.isHidden = true
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let companies = companies?.data{
            if indexPath.row  == companies.count - 1 {
                return CGFloat(206)
                
            }else {
                return CGFloat(206-36)
                
            }
        }
        return CGFloat(206-36)
    }
    
    
}
