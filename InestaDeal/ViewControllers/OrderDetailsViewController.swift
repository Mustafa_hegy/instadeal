//
//  OrderDetailsViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/17/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults
class OrderDetailsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
   
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var ordersTable: UITableView!
    
    @IBOutlet weak var popUpBtnCall: UIButton!
    @IBOutlet weak var popUpBtnPrepare: UIButton!
    @IBOutlet weak var popUpBtnDelivery: UIButton!
    @IBOutlet weak var popUpBtnFinished: UIButton!
    @IBOutlet weak var popUpSeperator1: UIView!
    @IBOutlet weak var popUpSeperator2: UIView!
    @IBOutlet weak var popUpSeperator3: UIView!
    
    @IBOutlet weak var headValueLable1: UILabel!
    @IBOutlet weak var headValueLable2: UILabel!
    @IBOutlet weak var headValueLable3: UILabel!
    @IBOutlet weak var headValueLable4: UILabel!
    @IBOutlet weak var headValueLable5: UILabel!
    @IBOutlet weak var headValueLable6: UILabel!
    @IBOutlet weak var headValueLable7: UILabel!
    
    @IBOutlet weak var headTitleLbl1: UILabel!
    @IBOutlet weak var headTitleLbl2: UILabel!
    @IBOutlet weak var headTitleLbl3: UILabel!
    @IBOutlet weak var headTitleLbl4: UILabel!
    @IBOutlet weak var headTitleLbl5: UILabel!
    @IBOutlet weak var headTitleLbl6: UILabel!
    @IBOutlet weak var headTitleLbl7: UILabel!
    @IBOutlet weak var showMapBtn: UIButton!
    
    @IBOutlet weak var selectOptionLbl: UILabel!
    @IBOutlet weak var wantToDoLbl: UILabel!
    
    var index : IndexPath?
    let activity            = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var companyId           = ""
    var orderId             = ""
    var orderDetails        : OrderDetailsResponse?
    {
        didSet{
            ordersTable.reloadData()
            setHeadValues()
        }
    }
    
    @IBAction func contactClient(_ sender: UIButton) {
        if let url = URL(string: "tel://\(orderDetails?.data?.client_mobile ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func dismissPopupView(_ sender: UITapGestureRecognizer) {
        popUpView.isHidden = true
        
        // Show all Pop Up Buttons
        popUpBtnCall.isHidden       = false
        popUpBtnPrepare.isHidden    = false
        popUpBtnDelivery.isHidden   = false
        popUpBtnFinished.isHidden   = false
        popUpSeperator1.isHidden    = false
        popUpSeperator3.isHidden    = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // if browsing screen as a tager
//        if companyId != "0" {
            localizeUI()
//        }
        popUpView.isHidden = true
        addNavBar(isBack: true)
        // Do any additional setup after loading the view.
        loadDataThroughInternet()
    }

    func localizeUI(){
        popUpBtnCall.setTitle("call".localized(), for: .normal)
        popUpBtnPrepare.setTitle("prepareProduct".localized(), for: .normal)
        popUpBtnDelivery.setTitle("deliveryService".localized(), for: .normal)
        popUpBtnFinished.setTitle("deliveryDone".localized(), for: .normal)
        showMapBtn.setTitle("showMap".localized(), for: .normal)
        selectOptionLbl.text    = "L_selectOption".localized()
        wantToDoLbl.text        = "L_wantToDo".localized()
        headTitleLbl1.text    = "orderNumber".localized()
        headTitleLbl2.text    = "deliveryCost".localized()
        headTitleLbl3.text    = "totalCost".localized()
        headTitleLbl4.text    = "orderDateTime".localized()
        headTitleLbl5.text    = "payType".localized()
        headTitleLbl6.text    = "address".localized()
        headTitleLbl7.text    = "totalInvoice".localized()
        
        
        if Defaults[.langId] == "1" {
            headTitleLbl1.textAlignment     = .left
            headTitleLbl2.textAlignment     = .left
            headTitleLbl3.textAlignment     = .left
            headTitleLbl4.textAlignment     = .left
            headTitleLbl5.textAlignment     = .left
            headTitleLbl6.textAlignment     = .left
            headTitleLbl7.textAlignment     = .left
            headValueLable1.textAlignment   = .left
            headValueLable2.textAlignment   = .left
            headValueLable3.textAlignment   = .left
            headValueLable4.textAlignment   = .left
            headValueLable5.textAlignment   = .left
            headValueLable6.textAlignment   = .left
            headValueLable7.textAlignment   = .left
        } else if Defaults[.langId] == "2" {
            headTitleLbl1.textAlignment     = .right
            headTitleLbl2.textAlignment     = .right
            headTitleLbl3.textAlignment     = .right
            headTitleLbl4.textAlignment     = .right
            headTitleLbl5.textAlignment     = .right
            headTitleLbl6.textAlignment     = .right
            headTitleLbl7.textAlignment     = .right
            headValueLable1.textAlignment   = .right
            headValueLable2.textAlignment   = .right
            headValueLable3.textAlignment   = .right
            headValueLable4.textAlignment   = .right
            headValueLable5.textAlignment   = .right
            headValueLable6.textAlignment   = .right
            headValueLable7.textAlignment   = .right
        }
    }
    
    func loadDataThroughInternet () {
        getOrderDetailsApi { (data) in
            
            if let dataModel = data as? OrderDetailsResponse{
                
                self.orderDetails = dataModel
            }
        }
    }
    @IBAction func prepareProduct(_ sender: Any) {
        setOrderPrepareApi { (data) in
            
            self.showDefaultAlert(title: "preparing".localized(), message: nil)
            self.popUpView.isHidden = true
        }
    }
    
    @IBAction func deliveryService(_ sender: Any) {
        setOrderDeliveryApi { (data) in
            
            self.showDefaultAlert(title: "delivering".localized(), message: nil)
            self.popUpView.isHidden = true
        }
    }
    @IBAction func deliverIsFinish(_ sender: UIButton) {
        setOrderDoneApi { (data) in
            
            self.showDefaultAlert(title: "orderDeliverd".localized(), message: nil)
            self.popUpView.isHidden = true
        }
        
    }
    
    @IBAction func btnShowMapPressed(_ sender: Any) {
        if let UrlNavigation = URL.init(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if orderDetails?.data?.lat != nil && orderDetails?.data?.lng != nil {
                    if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(orderDetails?.data?.lat ?? 0.0),\(orderDetails?.data?.lng ?? 0.0)&directionsmode=driving") {
                        UIApplication.shared.open(urlDestination, options: [:], completionHandler: nil)
                    }
                }
            }
            else {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser()
                
            }
        }
        else
        {
            NSLog("Can't use comgooglemaps://");
            self.openTrackerInBrowser()
        }
        
    }
    
    func openTrackerInBrowser(){
        if orderDetails?.data?.lat != nil && orderDetails?.data?.lng != nil {
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(orderDetails?.data?.lat ?? 0.0),\(orderDetails?.data?.lng ?? 0.0)&directionsmode=driving") {
                UIApplication.shared.open(urlDestination, options: [:], completionHandler: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getOrderDetailsApi(successClosure: @escaping SuccessClosure){
        print("getorderApi: https://www.instadeal.co/home/getorder/\(orderId)\(companyId == "0" ? "" : "/" + companyId)")
        // Add Activity Indicator
        startLoading()
        Alamofire.request("https://www.instadeal.co/home/getorder/\(orderId)\(companyId == "0" ? "" : "/" + companyId)?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = OrderDetailsResponse(JSON: data)
                    successClosure(dataModel)
                    
                    self.stopLoading()
                }
            }
        }
        
    }
    
    func setOrderDoneApi(successClosure: @escaping SuccessClosure){
        
        // Add Activity Indicator
        startLoading()
        Alamofire.request("https://www.instadeal.co/home/setOrderDone/\((orderDetails?.data?.products?[(index?.row)!].id)!)/\(companyId)?lang=\(Defaults[.langId])", method: .get).response { response in
            
            
            if let json = response.response {
                //                if let data = json as? [String : Any]{
                //                    let dataModel = OrdersResponse(JSON: data)
                successClosure(json)
                self.stopLoading()
                
                self.loadDataThroughInternet()
                //                }
                //            }
            }
        }
        
    }
    
    func setOrderDeliveryApi(successClosure: @escaping SuccessClosure){
        
        // Add Activity Indicator
        startLoading()
        Alamofire.request("https://www.instadeal.co/home/setDeliveryOrder/\((orderDetails?.data?.products?[(index?.row)!].id)!)?lang=\(Defaults[.langId])", method: .get).response { response in
            
            if let json = response.response {
//                if let data = json as? [String : Any]{
//                    let dataModel = OrdersResponse(JSON: data)
                successClosure(json)
                self.stopLoading()
                
                self.loadDataThroughInternet()
//                }
//            }
            }
        }
    }
    
    func setOrderPrepareApi(successClosure: @escaping SuccessClosure){
        
        // Add Activity Indicator
        startLoading()
        Alamofire.request("https://www.instadeal.co/home/setReadyOrder/\((orderDetails?.data?.products?[(index?.row)!].id)!)?lang=\(Defaults[.langId])", method: .get).response { response in
            
            if let json = response.response {
//                if let data = json as? [String : Any]{
//                    let dataModel = OrdersResponse(JSON: data)
                successClosure(json)
                self.stopLoading()
                
                self.loadDataThroughInternet()
//                }
//            }
            }
        }
    }

    func setOrderProductCancelApi(productObject: OrderProduct? ,successClosure: @escaping SuccessClosure){
        // Add Activity Indicator
        startLoading()
        Alamofire.request("https://www.instadeal.co/home/CancelOrder/\((productObject?.id)!)/\((productObject?.status)!)?lang=\(Defaults[.langId])", method: .get).response { response in
            
            
            if let json = response.response {
                //                if let data = json as? [String : Any]{
                //                    let dataModel = OrdersResponse(JSON: data)
                successClosure(json)
                self.stopLoading()
                //                }
                //            }
            }
        }
    }
    
    //MARL: - Table Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (orderDetails?.data?.products?.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell: TagerOrdersCells = tableView.dequeueReusableCell(withIdentifier: "TagerOrdersCells", for: indexPath) as! TagerOrdersCells
        
        let rowData                     = orderDetails?.data?.products?[indexPath.row]
        cell.dateTime.text              = rowData?.date!
        cell.itemImage.sd_setImage(with: URL(string: (rowData?.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "iconApp"))
        cell.itemTitle.text             = rowData?.product_name
        cell.currentStatusLable.text    = "\(rowData?.status_text ?? "")\n\(rowData?.price ?? "") \("currency".localized())"
        cell.headTitlesLbl.text         = "\("status".localized()) :\n\("price".localized()) :"
        cell.qtyTitleLbl.text           = "quantity".localized()
        cell.quantityLable.text         = rowData?.count
        cell.sellerTitleLBl.text        = "seller".localized()
        cell.companyName.text           = rowData?.company_name
        cell.deliverTimeTitleLbl.text   = "deliveryTime".localized()
        cell.deliveryTime.text          = rowData?.deliveryTime
        cell.deliverByTitleLbl.text     = "deliverBy".localized()
        cell.deliveryBy.text            = rowData?.gamm3 == "1" ? "instaGmma3ly".localized() : "direct".localized()
        
        if !((rowData?.options!.isEmpty)!) {
            for option in (rowData?.options!)! {
                cell.itemOptionTitle.text! += "\(cell.itemOptionTitle.text!.isEmpty ? "" : "\n")\(option.option_name!) : \(option.option_value!)"
                cell.itemOptionValue.text! += "\(cell.itemOptionValue.text!.isEmpty ? "" : "\n")\(option.option_price_txt!)"
                
                if Defaults[.langId] == "1" {
                    cell.itemOptionTitle.textAlignment           = .left
                    cell.itemOptionValue.textAlignment           = .right
                } else if Defaults[.langId] == "2" {
                    cell.itemOptionTitle.textAlignment           = .right
                    cell.itemOptionValue.textAlignment           = .left
                }
            }
        }
        
        // Stepper Status
        setStepperStatusForView(cell: cell, status: rowData?.status, statusText: rowData?.status_text)
        
        if companyId == "0" {
            // Order Details For User
            
            if rowData?.status == "1" ||  rowData?.status == "2"
            {
                // if 1    = "قيد التنفيذ"
                // if 2    = "تجهيز المنتج"
                
                cell.btnChangeStatus.isEnabled          = true
                cell.btnChangeStatus.backgroundColor    = .groupTableViewBackground
                cell.btnChangeStatus.titleForNormal     = "cancelOrder".localized()
                cell.cancelOrderProduct = {
                    
                    self.showTwoActionsAlert(title: "cancelOrderConfirm".localized(),
                                             firstActionButtontitle: "yes".localized(),
                                             secondActionButtontitle:"no".localized(),
                                             mesage: nil,
                                             actionClosure: {
                                                // Cancel Product Order
                                                self.setOrderProductCancelApi(productObject: rowData, successClosure: { (data) in
                                                    rowData?.status         = "5"
                                                    rowData?.status_text    = "canceled".localized()
                                                    tableView.reloadData()
                                                })
                    })
                }
            }
            else if rowData?.status == "5" ||  rowData?.status == "6"
            {
                // if 5    = "ملغي"
                // if 6    = "ملغي"
                cell.btnChangeStatus.isEnabled          = false
                cell.btnChangeStatus.backgroundColor    = .clear
                cell.btnChangeStatus.titleForNormal     = "canceled".localized()
            }
            else {
                cell.btnChangeStatus.isHidden   = true
            }
            
        }
        else {
            // Order Details For Tager
            cell.btnChangeStatus.isHidden   = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if companyId != "0" {
            // Screen is opened from tager store orders button
            index = indexPath
            popUpView.isHidden  = false
            
            let rowData         = orderDetails?.data?.products?[indexPath.row]
            Defaults[.orderId]  = rowData?.id ?? ""
    //        companyName.text    = "( اسم الشركة :" + ((rowData?.company_name) ?? "") + " )"
            setPopUpActionsFor(status: rowData?.status)
        }
    }

    // MARK: - Class Custom Methods
    func setHeadValues () {
        headValueLable1.text    = orderDetails?.data?.id
        headValueLable2.text    = "\(orderDetails?.data?.deliveryCost ?? "0" ) \("currency".localized())"
        headValueLable3.text    = "\(orderDetails?.data?.sum ?? "0") \("currency".localized())"
        headValueLable4.text    = "\((orderDetails?.data?.odate)!) \((orderDetails?.data?.otime)!)"
        headValueLable5.text    = orderDetails?.data?.pay
        headValueLable6.text    = orderDetails?.data?.address
        
        let totalInvoice        = NSString(string: orderDetails?.data?.deliveryCost ?? "0").floatValue + NSString(string: orderDetails?.data?.sum ?? "0").floatValue
        headValueLable7.text    =  "\(totalInvoice) \("currency".localized())"
    }
    private func startLoading() {
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
    }
    
    private func stopLoading() {
        self.activity.stopAnimating()
        self.activity.isHidden = true
    }
    
    func setPopUpActionsFor(status:String?){
        /*
         if 1    = "قيد التنفيذ"
         if 2    = "تجهيز المنتج"
         if 3    = "خدمة التوصيل"
         if 4    = "تم التسليم"
         if 5    = "ملغي"
         if 6    = "ملغي"
         if 7    = "مسترجع"
         if 8    = "فاشل"
         if 9    = "مرفوض"
         if 10   = "ملغي من الادارة"
         */
        
        switch status {
        case "2":
            popUpBtnPrepare.isHidden    = true
            popUpSeperator2.isHidden    = true
            
        case "3":
            popUpBtnPrepare.isHidden    = true
            popUpBtnDelivery.isHidden   = true
            popUpSeperator2.isHidden    = true
            popUpSeperator3.isHidden    = true
            
        case "4":
            popUpBtnPrepare.isHidden    = true
            popUpBtnDelivery.isHidden   = true
            popUpBtnFinished.isHidden   = true
            popUpSeperator2.isHidden    = true
            popUpSeperator3.isHidden    = true
            
        case "5","6","7","8","9","10":
            popUpBtnCall.isHidden       = true
            popUpBtnPrepare.isHidden    = true
            popUpBtnDelivery.isHidden   = true
            popUpBtnFinished.isHidden   = true
            popUpSeperator1.isHidden    = true
            popUpSeperator2.isHidden    = true
            popUpSeperator3.isHidden    = true
            
        default: break
            
        }
    }
}
