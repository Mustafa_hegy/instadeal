//
//  VideoCell.swift
//  TwoDirectionalScroller
//
//  Created by Robert Chen on 7/11/15.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit

class CategoryCell : UICollectionViewCell {
    
    @IBOutlet weak var subCategoryLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}
