//
//  SelectAreaViewController.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 5/16/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire
import SwiftyUserDefaults
import AVKit
import AVFoundation

//import MediaPlayer

class SelectAreaViewController: UIViewController {

    @IBOutlet weak var selectAreaLbl: UILabel!
    @IBOutlet weak var selectAreaBtn: UIButton!
    @IBOutlet weak var showAll: UIButton!
    @IBOutlet weak var suggestedOffers: UILabel!
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var videoPlayerView: UIView!
    
    var imgsArray       = [AlamofireSource]()
    var imgsTypes       = [String]()
    var sliderModel     : SlidersResponse?
//    var player          : AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        // Do any additional setup after loading the view.
        
            // add tap action to slider image
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
            slideShowView.addGestureRecognizer(gestureRecognizer)
            
            // Set Slider Options
            slideShowView.pageControl.pageIndicatorTintColor =  UIColor(hex: 0xF08944)
            slideShowView.contentScaleMode  = .scaleAspectFill
            slideShowView.circular          = true
            slideShowView.slideshowInterval = 5.0
            
            // Call slider offers api
            getOfferssApi {[weak self] (data) in
                if let dataModel = data as? SlidersResponse{
                    print("dattta:\(dataModel)")
                    for index in dataModel.data ?? []{
                        print("index:\(index.photo ?? "")")
                        self!.imgsArray.append(AlamofireSource(urlString: index.photo ?? "")!)
                        self!.sliderModel = dataModel
                    }
                    self!.slideShowView.setImageInputs(self!.imgsArray)
                }
            }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // Call method to setup AVPlayer & AVPlayerLayer to play video
//        self.setupAVPlayer()
//        playVideo()
    }
    // MARK: - Class Custom Methods
    func localizeUI(){
        showAll.setTitle("showAll".localized(), for: .normal)
        selectAreaBtn.setTitle("selectArea".localized(), for: .normal)
        selectAreaLbl.text      = "selectArea".localized()
        suggestedOffers.text    = "suggestedOffers".localized()
        
        if Defaults[.langId] == "1" {
            suggestedOffers.textAlignment   = .left
        } else if Defaults[.langId] == "2" {
            suggestedOffers.textAlignment   = .right
        }
    }
    
    func setupAVPlayer() {
        let path = Bundle.main.path(forResource: "Intro", ofType:"mp4")
        let videoURL = URL(fileURLWithPath: path!)
        let avAssets = AVAsset(url:  videoURL) // Create assets to get duration of video.
        let player = AVPlayer(url: videoURL)
        let playerLayer = AVPlayerLayer(player: player)
//        playerLayer.frame = self.videoPlayerView.bounds
        
        playerLayer.backgroundColor = .none
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
//        playerLayer.zPosition = -1
        
        self.videoPlayerView.layer.addSublayer(playerLayer)
        player.play()
        
        player.addPeriodicTimeObserver(forInterval: CMTime(seconds: 1, preferredTimescale: 1) , queue: .main) { [weak self] time in
            
            if time == avAssets.duration {
                if Defaults[.areaId] != "" {
                    self?.performSegue(withIdentifier: "tabBarSegue", sender: "home")
                }else{
                    self?.videoPlayerView.isHidden  = true
                }
            }
        }
    }
    
    /*
    var moviePlayer: MPMoviePlayerController?
    func playVideo() {
        let path = Bundle.main.path(forResource: "Intro", ofType:"mp4")
        let url = URL(fileURLWithPath: path!)
        moviePlayer = MPMoviePlayerController(contentURL: url)
        if let player = moviePlayer {
            player.view.frame = self.view.bounds
            player.controlStyle = .none
            player.prepareToPlay()
            player.scalingMode = .aspectFill
            
            NotificationCenter.default.addObserver(self, selector: "movieFinished", name:
                NSNotification.Name.MPMoviePlayerPlaybackDidFinish, object: nil)
                
            self.view.addSubview(player.view)
        }
    }
    
    @objc func movieFinished() {
        moviePlayer!.view.removeFromSuperview()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.MPMoviePlayerPlaybackDidFinish, object: nil)
        
        if Defaults[.areaId] != "" {
            self.performSegue(withIdentifier: "tabBarSegue", sender: "home")
        }else{
            self.videoPlayerView.isHidden  = true
        }
    }
    */
    
    // MARK: - Class Actions
    @IBAction func btnSelectArea(_ sender: Any) {
        performSegue(withIdentifier: "areasListSegue", sender: "home")
    }
    
    @IBAction func btnAllOffers(_ sender: Any) {
        performSegue(withIdentifier: "areasListSegue", sender: "allOffer")
    }
    
    @objc func didTap() {
        if sliderModel?.data?[slideShowView.currentPage].slider_type == "link"{
            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
            viewController.url = (sliderModel?.data?[slideShowView.currentPage].link)!
            Defaults[.dismissed] = true
            self.present(viewController, animated: true, completion: nil)
        }
        else{
            Defaults[.prodId] = sliderModel?.data?[slideShowView.currentPage].link ?? ""
//            performSegue(withIdentifier: "showFromHome", sender: self)
            
            performSegue(withIdentifier: "areasListSegue", sender: "productDetails")
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "areasListSegue" {
            let destinationView             = segue.destination as! AreasListViewController
            destinationView.nextViewFlag    = (sender as! String)
        }
        if segue.identifier == "tabBarSegue" {
            let destinationView             = segue.destination as! UITabBarController
            let destinationTab              = (sender as! String)
            
            if destinationTab == "home" {
                destinationView.selectedIndex   = 0
            }
        }
    }

    // MARK: - Api Connection
    func getOfferssApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getFirstAd?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = SlidersResponse(JSON: data)
                    
                    successClosure(dataModel)
                }
            }
        }
    }
}
