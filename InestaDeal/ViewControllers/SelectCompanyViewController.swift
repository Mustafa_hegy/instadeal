//
//  SelectCompanyViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/19/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import SwiftyUserDefaults

class SelectCompanyViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    let activity        = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var modelView: companyTableModelView?
    @IBOutlet weak var companiesTableView: UITableView!
    @IBOutlet weak var backButtonAction: UIButton!
    var values : [String] = []
    
    var companies: [CompanyResponse]?{
        didSet{
            companiesTableView.reloadData()
        }
    }
    
    var selectedCells = [Int : [String]]()
    var selectedCompsStrings = [Int : [String]]()
    var selectedIndexpathes = [IndexPath]()

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SelectCategoryCell = tableView.dequeueReusableCell(withIdentifier: "SelectCategoryCell", for: indexPath) as! SelectCategoryCell
        
        cell.categoryLbl.text = companies?[indexPath.row].name ?? ""
        
        cell.imgIcon.sd_setImage(with: URL(string:companies?[indexPath.row].photo ?? ""), placeholderImage: nil)
        
        if Defaults[.langId] == "1" {
            cell.categoryLbl.textAlignment = .left
        } else if Defaults[.langId] == "2" {
            cell.categoryLbl.textAlignment = .right
        }
        
        if let _ = selectedCells[indexPath.row]{
            if (selectedCells[indexPath.row]?.contains(companies?[indexPath.row].id  ?? ""))!{
                
                cell.accessoryType = .checkmark
            }
                
            else{
                cell.accessoryType = .none
            }
        }
//        cell.accessoryType = self.selectedCells.contains{ $0.contains(ind) }

        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
//
//        Defaults[.catSelectedAddProduct] = categories?.data?[indexPath.section].childs![indexPath.row].name ?? ""
//
        Defaults[.compId] = companies?[indexPath.row].id ?? ""
        
        
//        if let dictIndex = selectedCells![indexPath.section]{
        
        if let _ = selectedCells[indexPath.row]{
            if (selectedCells[indexPath.row]?.contains(companies?[indexPath.row].id ?? ""))!{
                
                selectedCells[indexPath.row]?.removeAll(companies?[indexPath.row].id ?? "")
                selectedCompsStrings[indexPath.row]?.removeAll(companies?[indexPath.row].name ?? "")
            }
            else{
                selectedCells[indexPath.row]?.append(companies?[indexPath.row].id ?? "")
                selectedCompsStrings[indexPath.row]?.append(companies?[indexPath.row].name ?? "")
            }
        }
        else{
            selectedCells[indexPath.row] = [companies?[indexPath.row].id ?? ""]
            selectedCompsStrings[indexPath.row] = [companies?[indexPath.row].name ?? ""]
        }
//        if self.selectedCells.contains(indexPath.row) {
//            self.selectedCells.remove(at: self.selectedCells.index(of: indexPath.row)!)
////            values.remove(at: <#T##Int#>)
//        } else {
//            self.selectedCells.append(indexPath.row)
////            values.append(categories?.data?[indexPath.section].childs![indexPath.row].id ?? "")
//        }
        if Defaults[.search] {
            Defaults[.compSelectedAddProduct] = companies?[indexPath.row].name ?? ""
            
            Defaults[.compId] = companies?[indexPath.row].id ?? ""
            self.navigationController?.popViewController()
            
        }

        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        var compsStrings = ""
        var compsIds = ""
        Defaults[.flagAddProd] = false
        super.viewWillDisappear(animated)
        for arr in selectedCompsStrings {
           compsStrings += arr.value.joined(separator: ",") + ","
        }
        if compsStrings != ""
        { compsStrings.remove(at: compsStrings.index(before: compsStrings
            .endIndex))
            
        }
        
        Defaults[.selectedCompsString] = compsStrings
        
        for arr in selectedCells {
            compsIds += arr.value.joined(separator: "-") + "-"
        }
        if compsIds != ""
        {
            compsIds.remove(at: compsIds.index(before: compsIds
            .endIndex))
        }
        Defaults[.selectedCompsIds] = compsIds
        
//        Defaults[.selectedCatsArr] = selectedCells as? [Int : [String]]
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonAction.setTitle("back".localized(), for: .normal)
        
//        addNavBarCart(isBack: true)
        getCompaniesApi(successClosure: { [weak self] (data) in
            
            if let dataModel = data as? CompanyResponse{
                
                self?.companies = dataModel.data
                print("companies:\(dataModel.data)")
                
            }
        })

        // Do any additional setup after loading the view.
    }

    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getCompaniesApi(successClosure: @escaping SuccessClosure){
        // Add Activity Indicator
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
        
        let params: Parameters = [
            "lang": "\(Defaults[.langId])" ,
            "area": "\(Defaults[.areaId])"
        ]
        
        print("from getCompaniesApi \(Defaults[.subCategoryId])")
        
        Alamofire.request("https://www.instadeal.co/home/getCompanies/\(Defaults[.subCategoryId])?lang=\(Defaults[.langId])", method: .get, parameters: params).responseJSON { response in
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyResponse(JSON: data)
                    successClosure(dataModel)
                }
                self.activity.stopAnimating()
                self.activity.isHidden = true
            }
        }
    }
}
