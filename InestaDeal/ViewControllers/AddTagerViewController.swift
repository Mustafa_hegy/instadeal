//
//  SettingsViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/14/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults


class AddTagerViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UIGestureRecognizerDelegate, UITextFieldDelegate {
    @IBOutlet weak var timePicker: UIDatePicker!
    
    @IBOutlet weak var bankHeight: NSLayoutConstraint!
    
    @IBOutlet weak var gammalyButton: UIButton!
    @IBOutlet weak var bankNameBottomConst: NSLayoutConstraint!
    var bankNameBottomConstTemp: NSLayoutConstraint!
    @IBOutlet weak var collectBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var fullNameTopConstraint: NSLayoutConstraint!
    var fullNameTopConstraintTemp: NSLayoutConstraint!
    @IBOutlet weak var minorConstraint: NSLayoutConstraint!
    @IBOutlet weak var fullNameHeight: NSLayoutConstraint!
    @IBOutlet weak var ibanHeight: NSLayoutConstraint!
    @IBOutlet weak var popTableHeight: NSLayoutConstraint!
    
    var popUpIsCollect = false
    var collectType = 0
    var catId: String?
    
    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var companyNameEn: UITextField!
    @IBOutlet weak var containgView: UIView!
    @IBOutlet weak var payTable: UITableView!
    @IBOutlet weak var tagerName: UITextField!
    @IBOutlet weak var chooseCats: UIButton!
    @IBOutlet weak var payType: UIButton!
    @IBOutlet weak var linkField: UITextField!
    @IBOutlet weak var deliveryPrice: UITextField!
    @IBOutlet weak var deliveryTime: UITextField!
    @IBOutlet weak var deliveryTimeFrom: UITextField!
    @IBOutlet weak var deliveryTimeTo: UITextField!
    @IBOutlet weak var collectBtn: UIButton!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var headLbl: UILabel!
    @IBOutlet weak var comments: UITextField!
    @IBOutlet weak var commentsEn: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var startTime: UITextField!
    @IBOutlet weak var endTime: UITextField!
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var iban: UITextField!
    @IBOutlet weak var bankName: UITextField!
    
    @IBOutlet weak var sendBtnAction: UIButton!
    @IBOutlet weak var gmma3lySubscribe: UILabel!
    
    enum CashTypeValue: Int {
        case cash = 1
        case cashAndKNet = 3
    }
    
    var cashSendData: CashTypeValue?
    var datePickerView:UIDatePicker = UIDatePicker()
    
    var isCash = false
    var gammaly = "0"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if !popUpIsCollect{
            return 2
        }
        return 3
    }
    
    @IBAction func selectGammalyButtonTapped(_ sender: Any) {
        if gammaly == "0"{
            gammalyButton.setBackgroundImage(#imageLiteral(resourceName: "mark"), for: .normal)
            gammaly = "1"
        } else {
            gammalyButton.setBackgroundImage(#imageLiteral(resourceName: "emptyCircle"), for: .normal)
            gammaly = "0"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ShowAddressTableViewCell = tableView.dequeueReusableCell(withIdentifier: "payCellPopUp", for: indexPath) as! ShowAddressTableViewCell
        if !popUpIsCollect{
            if indexPath.row == 0{
                cell.addressName.text = "cash".localized()
//                isCash = true
//                bankHeight.constant = 0
//                ibanHeight.constant = 0
//                fullNameHeight.constant = 0
//                bankName.isHidden = true
//                iban.isHidden = true
//                fullName.isHidden = true
//                view.layoutSubviews()
//                view.layoutSubviews()
            }
            if indexPath.row == 2{
                cell.addressName.text = "knet".localized()
            }
            if indexPath.row == 1{
                cell.addressName.text = "cash&knet".localized()
//                isCash = false
//                bankHeight.constant = 40
//                ibanHeight.constant = 40
//                fullNameHeight.constant = 40
//                view.layoutSubviews()
//                view.layoutSubviews()
//                bankName.isHidden = false
//                iban.isHidden = false
//                fullName.isHidden = false
            }
        }
        else{
            if indexPath.row == 0{
                cell.addressName.text = "weekly".localized()
            }
            if indexPath.row == 1{
                cell.addressName.text = "twoWeek".localized()
            }
            if indexPath.row == 2{
                cell.addressName.text = "monthly".localized()
            }
        }
        
        return cell
    }
    
    @IBAction func termsGesture(_ sender: UITapGestureRecognizer) {
        let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
        viewController.url = "https://www.instadeal.co/conditions_company.html"
        self.navigationController?.show(viewController, sender: self)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        containgView.isHidden = true
        if !popUpIsCollect{
            if indexPath.row == 0{
                payType.titleLabel?.text = "cash".localized()
                payType.setTitle("cash".localized(), for: .normal)
                
                cashSendData = CashTypeValue.cash
                isCash = true
                bankName.isHidden = true
                collectBtn.isHidden = true
                fullName.isHidden = true
                iban.isHidden = true
                fullNameTopConstraint.isActive = false
                bankNameBottomConst.isActive = false
                
            }
            if indexPath.row == 2{
                payType.titleLabel?.text = "knet".localized()
            }
            if indexPath.row == 1{
                payType.titleLabel?.text = "cash&knet".localized()
                payType.setTitle("cash&knet".localized(), for: .normal)
                cashSendData = CashTypeValue.cashAndKNet
                isCash = false
                bankName.isHidden = false
                fullName.isHidden = false
                collectBtn.isHidden = false
                iban.isHidden = false
                fullNameTopConstraint = fullNameTopConstraintTemp
                bankNameBottomConst = bankNameBottomConstTemp
                bankNameBottomConst.isActive = true
                fullNameTopConstraint.isActive = true
                
                view.layoutSubviews()
//                bankHeight.constant = 40
//                ibanHeight.constant = 40
//                fullNameHeight.constant = 40
                
            }
        }
        else{
            if indexPath.row == 0{
                
                collectBtn.setTitle("weekly".localized(), for: .normal)
                
                
            }
            if indexPath.row == 1{
                
                collectBtn.setTitle("twoWeek".localized(), for: .normal)
            }
            if indexPath.row == 2{
                
                collectBtn.setTitle("monthly".localized(), for: .normal)
                
            }
            collectType = indexPath.row + 1
        }
        
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view?.isDescendant(of: payTable))!{
            
            return false
        }
        return true
    }
    
    @IBAction func gestureAction(_ sender: UITapGestureRecognizer) {
        containgView.isHidden = true
        view.endEditing(true)
    }
    
    @IBAction func chooseCatsAction(_ sender: UIButton) {
        let viewController: SelectCategoryViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
        
        self.navigationController?.show(viewController, sender: self)
    }
    @IBAction func payAction(_ sender: UIButton) {
        popUpIsCollect = false
        containgView.isHidden = false
        
        payType.setTitleColor(.gray, for: .normal)
        payTable.reloadData()
    }
    
    @IBAction func collectMoney(_ sender: UIButton) {
        popUpIsCollect = true
        containgView.isHidden = false
        
        sender.setTitleColor(.gray, for: .normal)
        payTable.reloadData()
    }
    
    var timeField: UITextField?
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == startTime || textField == endTime{
//            timeField = textField
//            textField.inputView = timePicker
//            view.endEditing(true)
//        }
//    }
    @IBAction func timePickerAction(_ sender: UIDatePicker) {
        
    }
    
    @objc func datePickerValueChanged(){
//        let date = Date()
        let dateAsString = datePickerView.date.timeString()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss a"
        let date = dateFormatter.date(from: dateAsString)
        
        dateFormatter.dateFormat = "hh:mm a"
        let date24 = dateFormatter.string(from: date!)
        
        startTime.text = date24
    }
    @objc func datePickerValueChangedEnd(){
        //        let date = Date()
        
        
        let dateAsString = datePickerView.date.timeString()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss a"
        let date = dateFormatter.date(from: dateAsString)
        
        dateFormatter.dateFormat = "hh:mm a"
        let date24 = dateFormatter.string(from: date!)
        
        endTime.text = date24
        
        
    }
    @IBAction func startTimeTextFieldClick(_ sender: UITextField) {
       datePickerView = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.time
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        
        
    }
    @IBAction func endTimeTextFieldClick(_ sender: UITextField) {
        datePickerView = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.time
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChangedEnd), for: UIControlEvents.valueChanged)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        
        bankNameBottomConstTemp = bankNameBottomConst
        fullNameTopConstraintTemp = fullNameTopConstraint
        deliveryPrice.keyboardType = .decimalPad
        Defaults[.selectedCatstring] = ""
        Defaults[.selectedCatIds] = ""
        payTable.tableFooterView = UIView()
        headLbl.text = "addTagerHint1".localized()
        
        headLbl.text = headLbl.text! + "addTagerHint2".localized()
        
        let linkAttributes: [NSAttributedStringKey: Any] = [
            .link: NSURL(string: "https://www.instadeal.co/conditions.html")!,
            .foregroundColor: UIColor.black
        ]
        
        let attributedString = NSMutableAttributedString(string: headLbl.text ?? "")
        
        // Set the 'click here' substring to be the link
        
        let startRang = Defaults[.langId] == "1" ? 140 : 133
        let changeRang = Defaults[.langId] == "1" ? 20 : 14
        attributedString.setAttributes(linkAttributes, range: NSMakeRange(startRang, changeRang))
        headLbl.attributedText = attributedString
        addNavBar(isBack: true)
        
        // Do any additional setup after loading the view.
    }
    
    func localizeUI(){
        sendBtnAction.setTitle("add".localized(), for: .normal)
        payType.setTitle("payType".localized(), for: .normal)
        collectBtn.setTitle("collectMoney".localized(), for: .normal)
        chooseCats.setTitle("chooseCategory".localized(), for: .normal)
        companyName.placeholder         = "companyName".localized() + " " + "inArabic".localized()
        companyNameEn.placeholder       = "companyName".localized() + " " + "inEnglish".localized()
        tagerName.placeholder           = "tagerName".localized()
        mobileNumber.placeholder        = "mobileNumber".localized()
        fullName.placeholder            = "fullName".localized()
        iban.placeholder                = "iban".localized()
        bankName.placeholder            = "bankName".localized()
        startTime.placeholder           = "workHoursStart".localized()
        endTime.placeholder             = "workHoursEnd".localized()
        deliveryTime.placeholder        = "deliveryPeriod".localized()
        deliveryTimeFrom.placeholder    = "deliveryPeriodFrom".localized()
        deliveryTimeTo.placeholder      = "deliveryPeriodTo".localized()
        deliveryPrice.placeholder       = "deliveryCost".localized()
        address.placeholder             = "address".localized()
        linkField.placeholder           = "instagram".localized()
        comments.placeholder            = "notes".localized() + " " + "inArabic".localized()
        commentsEn.placeholder          = "notes".localized() + " " + "inEnglish".localized()
        gmma3lySubscribe.text           = "gmma3lySubscribe".localized()
        
        companyName.textAlignment               = .natural
        companyNameEn.textAlignment             = .natural
        comments.textAlignment                   = .natural
        commentsEn.textAlignment                 = .natural
        if Defaults[.langId] == "1" {
            payType.contentHorizontalAlignment      = .left
            collectBtn.contentHorizontalAlignment   = .left
            chooseCats.contentHorizontalAlignment   = .left
            tagerName.textAlignment         = .left
            mobileNumber.textAlignment      = .left
            fullName.textAlignment          = .left
            iban.textAlignment              = .left
            bankName.textAlignment          = .left
            startTime.textAlignment         = .left
            endTime.textAlignment           = .left
            deliveryTime.textAlignment      = .left
            deliveryTimeFrom.textAlignment  = .left
            deliveryTimeTo.textAlignment    = .left
            deliveryPrice.textAlignment     = .left
            address.textAlignment           = .left
            linkField.textAlignment         = .left
        } else if Defaults[.langId] == "2" {
            payType.contentHorizontalAlignment      = .right
            collectBtn.contentHorizontalAlignment   = .right
            chooseCats.contentHorizontalAlignment   = .right
            tagerName.textAlignment         = .right
            mobileNumber.textAlignment      = .right
            fullName.textAlignment          = .right
            iban.textAlignment              = .right
            bankName.textAlignment          = .right
            startTime.textAlignment         = .right
            endTime.textAlignment           = .right
            deliveryTime.textAlignment      = .right
            deliveryTimeFrom.textAlignment  = .right
            deliveryTimeTo.textAlignment    = .right
            deliveryPrice.textAlignment     = .right
            address.textAlignment           = .right
            linkField.textAlignment         = .right
        }
    }
    
    override func viewWillLayoutSubviews() {
        if popUpIsCollect{
            popTableHeight.constant = 135
        }
        else{
            popTableHeight.constant = 90
        }
    }
    override func viewDidLayoutSubviews() {
        if popUpIsCollect{
            popTableHeight.constant = 135
        }
        else{
            popTableHeight.constant = 90
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if popUpIsCollect{
            popTableHeight.constant = 135
        }
        else{
            popTableHeight.constant = 90
        }
        view.layoutSubviews()
        if Defaults[.selectedCatstring] != ""{
            chooseCats.setTitle(Defaults[.selectedCatstring], for: .normal)
            chooseCats.setTitleColor(.gray, for: .normal)
        }
        else{
            chooseCats.setTitle("chooseCategory".localized(), for: .normal)
            chooseCats.setTitleColor(.lightGray, for: .normal)
        }
        
        if payType.titleLabel?.text == "payType".localized() {
            
            payType.setTitleColor(.lightGray, for: .normal)
        }
        else{
            payType.setTitleColor(.gray, for: .normal)
        }
        if collectBtn.titleLabel?.text == "collectMoney".localized() {
            
            collectBtn.setTitleColor(.lightGray, for: .normal)
        }
        else{
            collectBtn.setTitleColor(.gray, for: .normal)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func catsBtnAction(_ sender: UIButton) {
    }
    @IBAction func sendBtnAction(_ sender: UIButton) {
        if companyName.isEmpty          ||
            companyNameEn.isEmpty       ||
            tagerName.isEmpty           ||
            mobileNumber.isEmpty        ||
            startTime.isEmpty           ||
            endTime.isEmpty             ||
            deliveryTimeFrom.isEmpty    ||
            deliveryTimeTo.isEmpty      ||
            address.isEmpty             ||
            linkField.isEmpty
        {
            //  deliveryPrice.isEmpty || comments.isEmpty ||
            
            self.showDefaultAlert(title: "allFieldsRequired".localized(), message: nil)
        }
        else if payType.titleLabel?.text == "payType".localized(){
            
            self.showDefaultAlert(title: "selectPayType".localized(), message: nil)
            payType.setTitleColor(.lightGray, for: .normal)
        }
        else if payType.titleLabel?.text == "chooseCategory".localized() {
            
            self.showDefaultAlert(title: "categoryRequired".localized(), message: nil)
        }
        else{
            payType.setTitleColor(.gray, for: .normal)
            addTagerApi { [weak self] (data) in
                
//                if let dataModel = data as? BaseResponse{
                
                self?.showDefaultAlert(title: "successSendRequest".localized(), message: nil, actionBlock: {
                        
                        self?.navigationController?.popViewController()
                    })
                    
//                }
            }
        }
        
    }

    func addTagerApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "name_ar" : companyName.text ?? "",
            "name_en" : companyNameEn.text ?? "",
            "owner" : tagerName.text ?? "",
            "mobile" : mobileNumber.text ?? "",
            "cats" : "\(Defaults[.selectedCatIds])",
            "work_from" : startTime.text ?? "",
            "work_to" : endTime.text ?? "",
            "address" : address.text ?? "",
//            "delivery_time" : deliveryTime.text ?? "",
            "delivery_time_from" : deliveryTimeFrom.text ?? "",
            "delivery_time_to" : deliveryTimeTo.text ?? "",
            "delivery_price" : Int(deliveryPrice.text ?? "") ?? 0,
            "client" : Defaults[.userId] ,
            "instagram" : linkField.text ?? "",
            "payment" : "\(cashSendData?.rawValue ?? 1)",
            "notes_ar" : comments.text ?? "",
            "notes_en" : commentsEn.text ?? "",
            "iban" : iban.text ?? "",
            "account_name" : fullName.text ?? "",
            "bank" : bankName.text ?? "",
            "collect" : "\(collectType)",
            "gamma3": gammaly
        ]
        
        
        Alamofire.request("https://www.instadeal.co/home/addCompany?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
        }
        
    }
    
}
