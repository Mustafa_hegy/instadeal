//
//  AreasListViewController.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 5/19/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class AreasListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet var areasTableView    : UITableView!
    
    let activity                    = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var nextViewFlag                : String?
    var areasListResponse           : AreasResponse?
    {
        didSet{
            areasTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Add nav bar => addNavBarCart for nav bar with back button and without cart button
        addNavBarCart(isBack: true)
        
        // Add Activity Indicator
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
        
        // Load Data From API
        getAreasApi(successClosure: {  (data) in
            if let dataModel = data as? AreasResponse{
                self.areasListResponse = dataModel
            }
        })
        
    }
    
    @objc func handelExpendClose(button : UIButton) {
        let section = button.tag
        
        // Set All index pathes of rows for a section to be deleted
        var indexPaths = [IndexPath]()
        for row in (areasListResponse?.data?[section].child!.indices)! {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        let isExpanded = areasListResponse?.data?[section].isExpanded
        areasListResponse?.data?[section].isExpanded    = !isExpanded!
        
        if isExpanded! {
            areasTableView.deleteRows(at: indexPaths, with: .fade)
        }
        else{
            areasTableView.insertRows(at: indexPaths, with: .fade)
        }
        areasTableView.reloadData()
    }
    
    // MARK: - TableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return areasListResponse?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !(areasListResponse?.data?[section].isExpanded)! {
            return 0
        }
        
        return areasListResponse?.data?[section].child?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView: areaCell  = tableView.dequeueReusableCell(withIdentifier: "SectionCell") as! areaCell
        let sectionItem                 = areasListResponse?.data?[section]
        
        if (areasListResponse?.data?.count)! > 0 {
            headerView.titleLbl.text        = sectionItem?.name
            headerView.cellButton.tag       = section
            headerView.cellButton.addTarget(self, action: #selector(handelExpendClose), for: .touchUpInside)
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: areaCell    = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! areaCell
        
        let row                     = areasListResponse?.data?[indexPath.section].child?[indexPath.row]
        cell.titleLbl.text          = row?.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row                     = areasListResponse?.data?[indexPath.section].child?[indexPath.row]
        // Cash selected area
        Defaults[.areaId]           = row?.id ?? "0"
        Defaults[.areaName]         = row?.name ?? ""
        
        if nextViewFlag == "home" {
            performSegue(withIdentifier: "tabBarSegue", sender: "home")
        } else if nextViewFlag == "allOffer" {
            performSegue(withIdentifier: "tabBarSegue", sender: "offers")
        } else if nextViewFlag == "productDetails" {
            performSegue(withIdentifier: "productDetailsSegue", sender: nil)
        } else if nextViewFlag == "productsList" {
            self.dismiss(animated: true, completion: nil)
        }
    }

    // MARK: - Api Connection
    func getAreasApi(successClosure: @escaping SuccessClosure){
        Alamofire.request("https://www.instadeal.co/home/getAreaWithSub?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = AreasResponse(JSON: data)
                    successClosure(dataModel)
                }
            }
            
            self.activity.stopAnimating()
            self.activity.isHidden = true
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "tabBarSegue" {
            let destinationView             = segue.destination as! UITabBarController
            let destinationTab              = (sender as! String)
            
            if destinationTab == "home" {
                destinationView.selectedIndex   = 0
            }
            else if destinationTab == "offers" {
                destinationView.selectedIndex   = 2
            }
        }
    }
    /*
var player: AVPlayer?
    private func loadVideo() {
        
        //this line is important to prevent background music stop
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        } catch { }
        
        let path = NSBundle.mainBundle().pathForResource("your path", ofType:"mp4")
        
        player = AVPlayer(URL: NSURL(fileURLWithPath: path!))
        let playerLayer = AVPlayerLayer(player: player)
        
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.zPosition = -1
        
        self.view.layer.addSublayer(playerLayer)
        
        player?.seekToTime(kCMTimeZero)
        player?.play()
    }
 */
}
