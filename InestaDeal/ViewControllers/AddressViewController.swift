//
//  AddressViewController.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/16/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyUserDefaults

class AddressViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var addressNameTextField: TextField!
    @IBOutlet weak var phoneNumberTextField: TextField!
    @IBOutlet weak var areaTextField: TextField!
    @IBOutlet weak var blockTextField: TextField!
    @IBOutlet weak var streetTextField: TextField!
    @IBOutlet weak var houseTextField: TextField!
    @IBOutlet weak var gadaTextField: TextField!
    @IBOutlet weak var roomtTextField: TextField!
    @IBOutlet weak var floorTextField: TextField!
    @IBOutlet weak var notesTextField: TextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nextButton:UIButton!
    
    var confirm = false
    var edit = false
    var editedData : GetAddress?
    var name = ""
    var street = ""
    var city = ""
    var country = ""
    var areaId = ""
    var lat = 0.0
    var lon = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        handleTabBarVisibility(hide: true)
        
        self.activityIndicator.isHidden = true
        addNavBar(isBack: true)
        
        // Set Default Browsing Selected Area
        areaTextField.text = Defaults[.areaName]
        areaId = Defaults[.areaId]
        
        if confirm{
//            areaTextField.text = city
            streetTextField.text = "\(street),\(city),\(country)"
        }
        
        if edit{
            print("edited data -> \(String(describing: editedData?.name))")
            nextButton.setTitle("update".localized(), for: .normal)
            if let edited = editedData{
                addressNameTextField.text = edited.name
                phoneNumberTextField.text = edited.mobile!
                streetTextField.text = edited.street
                blockTextField.text = edited.block
                houseTextField.text = edited.house
                gadaTextField.text = edited.gada
                notesTextField.text = edited.notes
                roomtTextField.text = edited.room
                floorTextField.text = edited.floor
                areaTextField.text = edited.area
                areaId = edited.area_id!
                lat = Double(edited.lat!)!
                lon = Double(edited.lng!)!
            }
        
        }
        
        
        print("lat: \(lat)")
        print("lon: \(lon)")
            let span:MKCoordinateSpan = MKCoordinateSpanMake(2.0, 2.0)
            let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.lat, self.lon)
            let region : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
            map.setRegion(region, animated: true)
            let annotation = MKPointAnnotation()
            annotation.coordinate.latitude = self.lat
            annotation.coordinate.longitude = self.lon
            map.addAnnotation(annotation)
            
        
    }
    
    
    func localizeUI(){
        nextButton.setTitle("next".localized(), for: .normal)
        addressNameTextField.placeholder    = "addressName".localized() + " * "
        phoneNumberTextField.placeholder    = "recieverPhoneNumber".localized() + " * "
        areaTextField.placeholder           = "area".localized()
        blockTextField.placeholder          = "block".localized() + " * "
        streetTextField.placeholder         = "street".localized()
        houseTextField.placeholder          = "house".localized()
        gadaTextField.placeholder           = "gada".localized()
        roomtTextField.placeholder          = "room".localized()
        floorTextField.placeholder          = "floor".localized()
        notesTextField.placeholder          = "notes".localized()
        
        
        if Defaults[.langId] == "1" {
            addressNameTextField.textAlignment  = .left
            phoneNumberTextField.textAlignment  = .left
            areaTextField.textAlignment         = .left
            blockTextField.textAlignment        = .left
            streetTextField.textAlignment       = .left
            houseTextField.textAlignment        = .left
            gadaTextField.textAlignment         = .left
            roomtTextField.textAlignment        = .left
            floorTextField.textAlignment        = .left
            notesTextField.textAlignment        = .left
        } else if Defaults[.langId] == "2" {
            addressNameTextField.textAlignment  = .right
            phoneNumberTextField.textAlignment  = .right
            areaTextField.textAlignment         = .right
            blockTextField.textAlignment        = .right
            streetTextField.textAlignment       = .right
            houseTextField.textAlignment        = .right
            gadaTextField.textAlignment         = .right
            roomtTextField.textAlignment        = .right
            floorTextField.textAlignment        = .right
            notesTextField.textAlignment        = .right
        }
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        let (isValid, name, phone,area ,block,street,gada,house,floor,room,notes) = validateInput()
        view.endEditing(true)
        if isValid{
            self.activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            if !edit{
                let params: Parameters = [
                    "name": name ,
                    "street": street ,
                    "house": house ,
                    "gada": gada ,
                    "mobile": phone ,
                    "desc": notes ,
                    "block": block ,
                    "client": Defaults[.userId] ,
                    "area": area ,
                    "floor": floor ,
                    "room": room,
                    "lat": self.lat,
                    "lng": self.lon,
                    "area_id": self.areaId
                ]
                
                addAddressApi(params: params) { (data) in
                    if let dataJson = data as? [String : Any]{
                        let dataModel = BaseResponse(JSON: dataJson)
                        if let model = dataModel{
                            if model.result == "true"{
                                self.activityIndicator.stopAnimating()
                                self.activityIndicator.isHidden = true
                                self.showDefaultAlert(title: model.message, message: nil, actionBlock: {
                                    if let dat = dataJson as? [String : Any]{
                                        Defaults[.addressId] = dataJson["data"] as! String
                                    }
                                    for controller in self.navigationController?.viewControllers ?? [] {
                                        if controller.isKind(of: MyAddressViewController.self) {
                                            self.navigationController!.popToViewController(controller, animated: true)
                                            break
                                        }
                                    }
                                    
                                    
                                })
                            }
                        }
                    }
                }
            }else{
                
                let params: Parameters = [
                    "name": name ,
                    "street": street ,
                    "house": house ,
                    "gada": gada ,
                    "mobile": phone ,
                    "desc": notes ,
                    "block": block ,
                    "client": Defaults[.userId] ,
                    "area": area ,
                    "floor": floor ,
                    "room": room,
                    "lat": editedData!.lat!,
                    "lng": editedData!.lng!,
                    "id": String(editedData!.id!),
                    "area_id": self.areaId
                ]
                
                editAddressApi(params: params) { (data) in
                    if let dataJson = data as? [String : Any]{
                        let dataModel = BaseResponse(JSON: dataJson)
                        if let model = dataModel{
                            if model.result == "true"{
                                self.activityIndicator.stopAnimating()
                                self.activityIndicator.isHidden = true
                                self.showDefaultAlert(title: model.message, message: nil, actionBlock: {
                                    if let dat = dataJson as? [String : Any]{
                                        Defaults[.addressId] = dataJson["data"] as! String
                                    }
                                    for controller in self.navigationController?.viewControllers ?? [] {
                                        if controller.isKind(of: MyAddressViewController.self) {
                                            self.navigationController!.popToViewController(controller, animated: true)
                                            break
                                        }
                                    }
                                    
                                })
                            }
                        }
                    }
                }
            }
        }else{
            self.showDefaultAlert(title: "برجاء ملئ جميع الحقول المطلوبة", message: nil, actionBlock: {
            })
        }
    }
    
    func addAddressApi(params:Parameters,successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/addAddress?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.httpBody , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        successClosure(data)
                    }
                }
                
        }
        
    }
    
    
    func editAddressApi(params:Parameters,successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/EditAddress?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.httpBody , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        successClosure(data)
                    }
                }
                
        }
        
    }
    
    func validateInput() -> (Bool,String,String,String,String,String,String,String,String,String,String){
        if addressNameTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
        else if phoneNumberTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
        else if areaTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
        else if blockTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
            /*
        else if streetTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
        else if gadaTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
        else if houseTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
        else if floorTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
        else if roomtTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
        else if notesTextField.text == ""{
            return (false,"","","","","","","","","","")
        }
        */
        else{
            return (true,addressNameTextField.text!,phoneNumberTextField.text!,areaTextField.text!,blockTextField.text!,streetTextField.text!,gadaTextField.text!,houseTextField.text!,floorTextField.text!,roomtTextField.text!,notesTextField.text!)
        }
    }
}

