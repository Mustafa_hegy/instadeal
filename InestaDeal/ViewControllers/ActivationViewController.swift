//
//  ActivationViewController.swift
//  InestaDeal
//
//  Created by Waleed on 6/10/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class ActivationViewController: UIViewController, UITextFieldDelegate{

    @IBAction func maxLengthAction(_ sender: UITextField) {
        if activationCode.text?.count == 6{
            
            sendCodeApi(successClosure: { (data) in
               
                    if let res = data as? Int{
                        if res == 1{
                        self.performSegue(withIdentifier: "goFromActivationToLogin", sender: self)
                    }
                    else{
                            self.showDefaultAlert(title: "correctActivationCodeRequired".localized(), message: nil)
                    }
                }
                
            })
        }
    }

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var resendCodeAction: UIButton!
    @IBOutlet weak var activatePhoneButton: UIButton!
    
    @IBOutlet weak var activationCode: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        localizeUI()
        if Defaults[.langId] == "1" {
            backButton.setImage( #imageLiteral(resourceName: "blackArrow") , for: .normal)
        } else if Defaults[.langId] == "2" {
            backButton.setImage( #imageLiteral(resourceName: "blackArrowAr") , for: .normal)
        }
    }

    func localizeUI(){
        resendCodeAction.setTitle("resendActivationCode".localized(), for: .normal)
        activatePhoneButton.setTitle("activatePhone".localized(), for: .normal)
        activationCode.placeholder     = "activationCode".localized()
    }
    
    @IBAction func resendCodeAction(_ sender: UIButton) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendCodeApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "code": activationCode.text ?? ""
        ]
        
        Alamofire.request("https://www.instadeal.co/home/activeaccount/\(Defaults[.userId])?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let res = json as? Int{
                       
                        successClosure(res)
                    }
                }
                
        }
        
    }


}
