//
//  SelectLanguageViewController.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 5/23/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Localize_Swift

class SelectLanguageViewController: UIViewController, UITableViewDelegate , UITableViewDataSource , UIGestureRecognizerDelegate {

    @IBOutlet weak var containgView             : UIView!
    @IBOutlet weak var langTable                : UITableView!
    @IBOutlet weak var selectLangTitleLabel: UILabel!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var selectedLanguageLable    : UILabel!  // 1 for english 2 for arabic
    @IBOutlet weak var selectLangHeadLabel: UILabel!
    @IBOutlet weak var arrowIcon: UIImageView!
    
    // Arabic Lang is always auto Selected
    var selectedLangueId: String = "2"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        localizeUI()
        // Add nav bar => addNavBarCart for nav bar with back button and without cart button
         addNavBar(isBack: true)
    }

    // MARK: - Class Custom Methods
    func localizeUI(){
        enterButton.setTitle("enter".localized(), for: .normal)
        selectLangHeadLabel.text    = "selectLangHead".localized()
        selectLangTitleLabel.text   = "selectLangTitle".localized()
        

        if Defaults[.langId] == "1" {
            arrowIcon.image = #imageLiteral(resourceName: "blackArrow")
            selectLangHeadLabel.textAlignment   = .left
            selectedLanguageLable.textAlignment = .right
        } else if Defaults[.langId] == "2" {
            arrowIcon.image = #imageLiteral(resourceName: "blackArrowAr")
            selectLangHeadLabel.textAlignment   = .right
            selectedLanguageLable.textAlignment = .left
        }
    }
    
    // MARK: - Class Actions
    @IBAction func selectLangButton(_ sender: Any) {
        
        self.containgView.isHidden = false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view?.isDescendant(of: langTable))!{
            
            return false
        }
        return true
    }
    
    @IBAction func gestureAction(_ sender: UITapGestureRecognizer) {
        containgView.isHidden = true
        view.endEditing(true)
    }
    
    @IBAction func enterButton(_ sender: Any) {
        // Cash selected Lang
        Defaults[.langId]           = selectedLangueId
        
        // Set layout directions
        if selectedLangueId == "1"{
            // English Language Selected
            UIView.appearance().semanticContentAttribute = .forceRightToLeft //Design is created for Ar =>default RTL :(
            Localize.setCurrentLanguage("en")
        }
        else if selectedLangueId == "2"{
            // Arabic Language Selected
            UIView.appearance().semanticContentAttribute = .forceLeftToRight //Design is created for Ar =>default RTL :(
            Localize.setCurrentLanguage("ar")
        }
        
        // Move To TagerCompanyViewController
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tagerEnter") as! TagerCompanyViewController
        (viewController as! TagerCompanyViewController).flagSegue = true
        self.navigationController?.show(viewController, sender: self)
    }
    
    // MARK: - TableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ShowAddressTableViewCell = tableView.dequeueReusableCell(withIdentifier: "langCellPopUp", for: indexPath) as! ShowAddressTableViewCell
        
            if indexPath.row == 0{
                cell.addressName.text = "English Language"
            }
            if indexPath.row == 1{
                cell.addressName.text = "اللغة العربية"
            }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        containgView.isHidden = true
            if indexPath.row == 0{
                selectedLanguageLable.text = "English Language"
                selectedLangueId = "1"
            }
            if indexPath.row == 1{
                selectedLanguageLable.text = "اللغة العربية"
                selectedLangueId = "2"
            }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
