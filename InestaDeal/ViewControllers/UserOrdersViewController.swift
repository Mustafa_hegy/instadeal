//
//  OrdersViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/17/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults
import Cosmos
import TCPickerView
// linkViewController
class UserOrdersViewController: UIViewController , UITableViewDelegate , UITableViewDataSource, UIGestureRecognizerDelegate,TCPickerViewOutput
{

    let activity            = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    @IBOutlet weak var doneDeliver: UIView!
    @IBOutlet weak var topBtnViewConst: NSLayoutConstraint!
    @IBOutlet weak var sendCommentTopConst: NSLayoutConstraint!
    @IBOutlet weak var starsHeightConst: NSLayoutConstraint!
    @IBOutlet weak var sendCommentBtn: UIButton!
    @IBOutlet weak var headLbl: UILabel!
    @IBOutlet weak var comment: UITextField!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var starsView: CosmosView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var ordersTable: UITableView!
    
    var companyId = ""
    var orderId = ""
    var clientName = ""
    var clientEmail = ""
    var clientMobile = ""
    var temptopBtnViewConst: CGFloat = 0.0
    var isRate = true
    var index : IndexPath?
    var selectedTager:Int?
    var orders: OrdersResponse?
    {
        didSet{
            ordersTable.reloadData()
        }
    }
    
    func rateTagerApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "company_id": companyId ,
            "client": Defaults[.userId] ,
            "rate": Int(starsView.rating) ,
            "comment": comment.text ?? ""
        ]
        
        
        Alamofire.request("https://www.instadeal.co/home/addRate?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }

    @IBAction func cancelBtnAction(_ sender: UIButton) {
        popUpView.isHidden = true
    }
    @IBAction func rateBtnAction(_ sender: UIButton) {
        
        if isRate{
            rateTagerApi { (data) in
                
                if let dataModel = data as? BaseResponse{
                    
                    self.showDefaultAlert(title: dataModel.message, message: nil)
                    self.popUpView.isHidden = true
                }
            }
        }
        else{
            sendApi(companyId: companyId, mobile: clientMobile, comment: comment.text ?? "", name: clientName, email: clientEmail, successClosure: { (data) in
                
                if let _ = data as? BaseResponse{
                    self.showDefaultAlert(title: "تم ارسال الشكوي بنجاح", message: nil, actionBlock: {
                        self.popUpView.isHidden = true
                    })
                }
            })
        }
        
        
    }
    @IBAction func rateTagerAction(_ sender: UIButton) {
        starsHeightConst.constant = 27
//        topBtnViewConst.constant = CGFloat(temptopBtnViewConst)
        topBtnViewConst.constant = temptopBtnViewConst 
        view.layoutIfNeeded()
        isRate = !isRate
        starsView.isHidden = false
        popUpView.isHidden = false
        ratingView.isHidden = false
        optionsView.isHidden = true
        headLbl.text = "تقيم التاجر"
        sendCommentBtn.setTitle("تقيم التاجر", for: .normal)
        
    }
    
    @IBAction func sendCommentAction(_ sender: UIButton) {
        isRate = !isRate
        starsHeightConst.constant = 0
//        topBtnViewConst.constant = CGFloat(temptopBtnViewConst)
        topBtnViewConst.constant = temptopBtnViewConst + 27
        view.layoutIfNeeded()
        starsView.isHidden = true
        popUpView.isHidden = false
        ratingView.isHidden = false
        optionsView.isHidden = true
        headLbl.text = clientName
        sendCommentBtn.setTitle("إضافة ملاحظة", for: .normal)
    }
    
    @IBAction func showOrderDetails(_ sender: UIButton) {
        
         let linkViewController: LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
        print("orderLink: https://www.instadeal.co/home/showOrders/\(orderId)/\(companyId).html")
        linkViewController.url = "https://www.instadeal.co/home/showOrders/\(orderId)/\(companyId).html"
        
        self.navigationController?.show(linkViewController, sender: sender)
    }
    
    @IBAction func contactClient(_ sender: UIButton) {
        if let url = URL(string: "tel://\(orders?.data![(index?.row)!].client_mobile ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func dismissPopupView(_ sender: UITapGestureRecognizer) {
        
        popUpView.isHidden = true
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        temptopBtnViewConst = topBtnViewConst.constant
        popUpView.isHidden = true
        addNavBar(isBack: true)
        ratingView.isHidden = true
        getCompanyOrdersApi { (data) in
            
            if let dataModel = data as? OrdersResponse{
                
                self.orders = dataModel
              
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view?.isDescendant(of: starsView))!{
            
            return false
        }
        if (touch.view?.isDescendant(of: ratingView))!{
            return false
        }
        return true
    }
    func sendApi(companyId: String ,mobile: String , comment: String , name: String , email:String ,successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "company_id": companyId,
            "mobile": mobile ,
            "text": comment ,
            "name": name ,
            "email": ""
        ]
        
        
        Alamofire.request("https://www.instadeal.co/home/addContact?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
    func getCompanyOrdersApi(successClosure: @escaping SuccessClosure){
        // Add Activity Indicator
        startLoading()
        print("getUserOrdersApi: https://www.instadeal.co/home/getUserOrders/\(Defaults[.userId])")
        Alamofire.request("https://www.instadeal.co/home/getUserOrders/\(Defaults[.userId])?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = OrdersResponse(JSON: data)
                    successClosure(dataModel)
                    self.stopLoading()
                }
            }
        }
        
    }

    //MARL: - Table Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (orders?.data?.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        index = indexPath
        let cell: TagerOrdersCells  = tableView.dequeueReusableCell(withIdentifier: "TagerOrdersCells", for: indexPath) as! TagerOrdersCells
        
        let rowData                     = orders?.data![indexPath.row]
        cell.dateTime.text              = rowData?.date!
        cell.itemImage.sd_setImage(with: URL(string: (rowData?.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "iconApp"))
        cell.itemTitle.text             = rowData?.product_name
        cell.currentStatusLable.text    = "\(rowData?.status_text ?? "")\n\(rowData?.price ?? "") \("currency".localized()) \n\(rowData?.deliv_text ?? "")\n\(rowData?.deliveryTime ?? "")"
        cell.quantityLable.text         = rowData?.count
        
        // Stepper Status
        setStepperStatusForView(cell: cell, status: rowData?.status, statusText: rowData?.status_text)
        
        cell.moveToOrderDetails = {
            self.performSegue(withIdentifier: "OrderDetailsSegue", sender: rowData)
        }
        
        /*
        // Old Code
        cell.deliverDoneView.isHidden = true
        cell.companyName.text = orders?.data![indexPath.row].company_name
        cell.orderNumber.text = orders?.data![indexPath.row].id
        cell.totalPrice.text = (orders?.data![indexPath.row].sum ?? "0.0") + " KD"
        cell.dateTime.text = (orders?.data![indexPath.row].odate ?? "") + " " + (orders?.data![indexPath.row].otime ?? "")
        cell.isUserInteractionEnabled = true
        if orders?.data![indexPath.row].status == "2"{
            cell.deliverDoneView.isHidden = false
            //cell.isUserInteractionEnabled = false
        }
        
        if orders?.data![indexPath.row].gammaly == "0"{
            cell.gammalyView.isHidden = true
            cell.deliveryCost.text = (orders?.data![indexPath.row].deliveryCost ?? "0.0") + " KD"
        } else {
            cell.gammalyView.isHidden = false
            let deliveryCost = Double(orders?.data![indexPath.row].deliveryCost ?? "0.0") ?? 0.0
            cell.deliveryCost.text =   "\(deliveryCost)" + " KD" + " جمع لى "
        }
         */
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        popUpView.isHidden = false
//        ratingView.isHidden = true
//        optionsView.isHidden = false
//        clientEmail = orders?.data![indexPath.row].client_email ?? ""
//        clientName = orders?.data![indexPath.row].company_name ?? ""
//        clientMobile = orders?.data![indexPath.row].client_mobile ?? ""
//        companyId = orders?.data![indexPath.row].id ?? ""
//        companyName.text = "( اسم الشركة :" + ((orders?.data![indexPath.row].company_name) ?? "") + " )"
        /*
         // Old Code
        let selectedOrder = orders?.data![indexPath.row].companies!
        orderId = orders?.data![indexPath.row].id ?? ""
        var picker: TCPickerViewInput = TCPickerView()
        
        picker.title = "التجار"
        
        let values = selectedOrder!.map { TCPickerView.Value(title: $0.name ?? "") }
        picker.values = values
        picker.delegate = self
        picker.selection = .single

        picker.completion = { (selectedIndexes) in
            for i in selectedIndexes {
                print(values[i].title)
                self.popUpView.isHidden = false
                self.ratingView.isHidden = true
                self.optionsView.isHidden = false
                //self.clientEmail =
                self.clientName = selectedOrder![i].name ?? ""
                self.clientMobile = selectedOrder![i].mobile ?? ""
                self.companyId = selectedOrder![i].id ?? ""
                self.companyName.text = "( اسم الشركة :" + (selectedOrder![i].name ?? "") + " )"
            }
        }
        picker.closeAction = {
            print("Handle close action here")
        }
        picker.show()
        */
    }
    
    func pickerView(_ pickerView: TCPickerViewInput, didSelectRowAtIndex index: Int) {
        print("Uuser select row at index: \(index)")
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "OrderDetailsSegue" {
            let destinationView             = segue.destination as! OrderDetailsViewController
            let rowData                     = sender as! Order
            destinationView.orderId         = rowData.order_id!
            destinationView.companyId       = "0"
        }
    }
    
    private func startLoading() {
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
    }
    
    private func stopLoading() {
        self.activity.stopAnimating()
        self.activity.isHidden = true
    }
}
