//
//  CategoryRow.swift
//  TwoDirectionalScroller
//
//  Created by Robert Chen on 7/11/15.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit
import SwifterSwift
import SwiftyUserDefaults

class CategoryRow : UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableCellHeaderLbl: UILabel!
    
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var moreLabel: UILabel!
    
    var subCategoryId: String?
    var categories: CategoriesResponse?
    {
        didSet{
            moreLabel?.text = "more".localized()
            collectionView.reloadData()
            collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    var section: Int = 0
}

extension CategoryRow : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("counts:\(categories?.data![self.section].childs?.count)")
        return categories?.data![self.section].childs?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
        cell.imageView.sd_setImage(with: URL(string: (categories?.data![section].childs![indexPath.row].photo) ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.subCategoryLbl.text = categories?.data![section].childs![indexPath.row].name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        if Defaults[.logged]{
        // Move to Companies List related to selected Category
//            self.parentViewController?.performSegue(withIdentifier: "companySeg", sender: self) // Basic Code
        
        // Move to Products List related to selected Category
        self.parentViewController?.performSegue(withIdentifier: "catProductsSeg", sender: self) // New Code
//        }
//        else{
//            self.parentViewController?.performSegue(withIdentifier: "goLogin", sender: self)
        
//        }
       subCategoryId = categories?.data![section].childs![indexPath.row].id
        Defaults[.subCategoryId] = subCategoryId ?? ""
        Defaults[.subCategoryName] = categories?.data![section].childs![indexPath.row].name ?? ""
        Defaults[.parentCategoryId] = (categories?.data![section].childs![indexPath.row].parent)!
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.image = nil
        verticalIndicator?.backgroundColor = appThemeColor
    }
    
}

extension CategoryRow : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
}

