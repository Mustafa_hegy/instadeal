//
//  FiltersViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/28/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class FiltersViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var searchInBtn: UIButton!
    @IBOutlet weak var to: UITextField!
    @IBOutlet weak var from: UITextField!
    @IBOutlet weak var company: UITextField!
    @IBOutlet weak var deliveryType: UITextField!
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var searchHint: UILabel!
    @IBOutlet weak var searchHint2: UILabel!
    @IBOutlet weak var sortBySegment: UISegmentedControl!
    
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    var results = [OffersResponse]()
    var selectedDeliveryType : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        
        Defaults[.compSelectedAddProduct] = ""
//        addNavBar(isBack: true)
  
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Defaults[.search] = false
        if Defaults[.compSelectedAddProduct] == ""{
            
        }else{
            self.company.text = Defaults[.compSelectedAddProduct]
        }
        self.view.endEditing(true)
        self.company.endEditing(true)
    }
    
    func localizeUI(){
        searchButton.setTitle("filter".localized(), for: .normal)
//        categoryButton.setTitle("selectCategory".localized(), for: .normal)
        searchHint.text         = "sortBy".localized()
        searchHint2.text        = "selectPrice".localized()
        pageTitleLabel.text     = "productsfilter".localized()
        from.placeholder        = "priceFrom".localized()
        to.placeholder          = "priceTo".localized()
        company.placeholder     = "company".localized()
        deliveryType.placeholder     = "chooseDeliveryType".localized()

        sortBySegment.setTitle("lowestPrice".localized(), forSegmentAt: 0)
        sortBySegment.setTitle("highestPrice".localized(), forSegmentAt: 1)
        
        if Defaults[.langId] == "1" {
            searchHint.textAlignment    = .left
            searchHint2.textAlignment   = .left
            company.textAlignment       = .left
            deliveryType.textAlignment  = .left
        } else if Defaults[.langId] == "2" {
            searchHint.textAlignment    = .right
            searchHint2.textAlignment   = .right
            company.textAlignment       = .right
            deliveryType.textAlignment  = .right
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 10{
            
        }
        
    }
    
    @IBAction func companyBtnAction(_ sender: UIButton) {
        Defaults[.search] = true
        performSegue(withIdentifier: "companiesSegue", sender: self)
        self.view.endEditing(true)
        
    }
    
    @IBAction func deliveryTypeBtnAction(_ sender: Any) {
        
        self.showThreeActionsAlert(title: "selectDeliveryType".localized(),
                                   firstActionButtontitle: "cancel".localized(),
                                   secondActionButtontitle: "direct".localized(),
                                   thirdActionButtontitle:"gmma3ly".localized(),
                                   message: nil,
                                   onCancel: {},
                                   secondActionClosure: {
                                    self.deliveryType.text = "direct".localized()
                                    self.selectedDeliveryType = 0
        }) {
            self.deliveryType.text = "gmma3ly".localized()
            self.selectedDeliveryType = 1
            
        }
        
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        self.view.endEditing(true)
        performSegue(withIdentifier: "unwindFilterSegue", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goResults"{
            
            if let nextVC = segue.destination as? SearchResultViewController{
                nextVC.results = self.results
            }
        }
    }

    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
