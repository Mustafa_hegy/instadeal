//
//  MyLocationViewController.swift
//  InestaDeal
//
//  Created by Ibrahim Salah on 3/15/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MyLocationViewController: UIViewController, CLLocationManagerDelegate , MKMapViewDelegate {
    @IBOutlet weak var map : MKMapView!
    let manager = CLLocationManager()
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    var name = ""
    var street = ""
    var city = ""
    var country = ""
    var lat = 0.0
    var lon = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        handleTabBarVisibility(hide: true)
        
        addNavBar(isBack: true)
        map.delegate    = self
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        confirmButton.isEnabled = false
        
        if let latitude = manager.location?.coordinate.latitude , let longitude = manager.location?.coordinate.longitude{
            let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
            let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
            let region : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
            map.setRegion(region, animated: true)
            let annotation = MKPointAnnotation()
            annotation.coordinate = (manager.location?.coordinate)!
            map.addAnnotation(annotation)
            self.lon = longitude
            self.lat = latitude
            decodeYourLocation(latitude: latitude, longitude: longitude)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleTabBarVisibility(hide: true)
    }
    
    func localizeUI(){
        confirmButton.setTitle("confirm".localized(), for: .normal)
        skipButton.setTitle("skip".localized() , for: .normal)
        currentLocationLabel.text   = "determineYourLocation".localized()
    }
    
    func decodeYourLocation (latitude:CLLocationDegrees, longitude:CLLocationDegrees){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            if error != nil {
                print(error)
                
            }else{
                // Place details
                var placeMark: CLPlacemark!
                placeMark = placemarks?[0]
                if let placemark = placeMark.addressDictionary {
                    // Location name
                    if let locationName = placemark["Name"] as? NSString {
                        self.name = locationName as String
                    }
                    // Street address
                    if let s = placemark["Thoroughfare"] as? NSString {
                         self.street = s as String
                    }
                    // City
                    if let c = placemark["City"] as? NSString {
                        self.city = c as String
                    }
                    // Country
                    if let count = placemark["Country"] as? NSString {
                        self.country = count as String
                    }
                    self.lat = latitude
                    self.lon = longitude
                    print("My location: \(self.name) \(self.street) \(self.city) \(self.country)")
                    self.confirmButton.isEnabled = true
                    self.currentLocationLabel.text = "\(self.street) \(self.city) \(self.country)"
                   //(self.name)
                }
            }
            
            
        })
    }
    
    @IBAction func confirmButtonTapped(sender:UIButton){
        performSegue(withIdentifier: "confirm", sender: nil)
    }
    @IBAction func escapeButtonTapped(Sender:UIButton){
        performSegue(withIdentifier: "EscapeSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "confirm"{
            if let addressVC = segue.destination as? AddressViewController{
                addressVC.confirm = true
                addressVC.name = self.name
                addressVC.street = self.street
                addressVC.city = self.city
                addressVC.country = self.country
                addressVC.lat = self.lat
                addressVC.lon = self.lon
            }
        }
        
        
        if segue.identifier == "EscapeSegue"{
            if let addressVC = segue.destination as? AddressViewController{
                addressVC.lat = self.lat
                addressVC.lon = self.lon
            }
        }
    }
    
    // MARK: MapKit Delegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation  {
            return nil
        }
        
        let reuseId = "pin"
        var pav:MKPinAnnotationView?
        if (pav == nil)
        {
            pav = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pav?.isDraggable = true
            pav?.canShowCallout = true;
        }
        else
        {
            pav?.annotation = annotation;
        }
        
        return pav;
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        if newState == .ending {
            
            lon = (view.annotation?.coordinate.longitude)!
            lat = (view.annotation?.coordinate.latitude)!
            
            print("lat: \(lat)")
            print("lon: \(lon)")
            
            decodeYourLocation(latitude: lat, longitude: lon)
        }
    }
}
