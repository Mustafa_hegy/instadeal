//
//  SelectCategoryViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/19/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import SwiftyUserDefaults

class SelectCategoryViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return categories?.data?.count ?? 0
    }
    var selectedCells = [Int : [String]]()
    var selectedCatsStrings = [Int : [String]]()
    var selectedIndexpathes = [IndexPath]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return categories?.data?[section].childs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = catsHeaderView()
        _ = headerView.view
        headerView.headerLbl.text = categories?.data?[section].name ?? ""
        
        if Defaults[.langId] == "1" {
            headerView.headerLbl.textAlignment  = .left
        } else if Defaults[.langId] == "2" {
            headerView.headerLbl.textAlignment  = .right
        }
        
        return headerView.view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SelectCategoryCell = tableView.dequeueReusableCell(withIdentifier: "SelectCategoryCell", for: indexPath) as! SelectCategoryCell
        
        cell.categoryLbl.text = categories?.data?[indexPath.section].childs![indexPath.row].name
        
        cell.imgIcon.sd_setImage(with: URL(string: categories?.data?[indexPath.section].childs![indexPath.row].photo ?? ""), placeholderImage: nil)
        
        if Defaults[.langId] == "1" {
            cell.categoryLbl.textAlignment = .left
        } else if Defaults[.langId] == "2" {
            cell.categoryLbl.textAlignment = .right
        }
        
        if let _ = selectedCells[indexPath.section]{
            if (selectedCells[indexPath.section]?.contains(categories?.data?[indexPath.section].childs![indexPath.row].id ?? ""))!{
                
                cell.accessoryType = .checkmark
            }
                
            else{
                cell.accessoryType = .none
            }
        }
//        cell.accessoryType = self.selectedCells.contains{ $0.contains(ind) }

        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
//
//        Defaults[.catSelectedAddProduct] = categories?.data?[indexPath.section].childs![indexPath.row].name ?? ""
//
        Defaults[.catId] = categories?.data?[indexPath.section].childs![indexPath.row].id ?? ""
        
        
//        if let dictIndex = selectedCells![indexPath.section]{
        
        if let _ = selectedCells[indexPath.section]{
            if (selectedCells[indexPath.section]?.contains(categories?.data?[indexPath.section].childs![indexPath.row].id ?? ""))!{
                
                selectedCells[indexPath.section]?.removeAll(categories?.data?[indexPath.section].childs![indexPath.row].id ?? "")
                 selectedCatsStrings[indexPath.section]?.removeAll(categories?.data?[indexPath.section].childs![indexPath.row].name ?? "")
                

            }
            else{
                selectedCells[indexPath.section]?.append(categories?.data?[indexPath.section].childs![indexPath.row].id ?? "")
                selectedCatsStrings[indexPath.section]?.append(categories?.data?[indexPath.section].childs![indexPath.row].name ?? "")
                
            }
        }
        else{
              selectedCells[indexPath.section] = [categories?.data?[indexPath.section].childs![indexPath.row].id ?? ""]
            selectedCatsStrings[indexPath.section] = [categories?.data?[indexPath.section].childs![indexPath.row].name ?? ""]
        }
//        if self.selectedCells.contains(indexPath.row) {
//            self.selectedCells.remove(at: self.selectedCells.index(of: indexPath.row)!)
////            values.remove(at: <#T##Int#>)
//        } else {
//            self.selectedCells.append(indexPath.row)
////            values.append(categories?.data?[indexPath.section].childs![indexPath.row].id ?? "")
//        }
        if Defaults[.search] {
            Defaults[.catSelectedAddProduct] = categories?.data?[indexPath.section].childs![indexPath.row].name ?? ""
            
            Defaults[.catId] = categories?.data?[indexPath.section].childs![indexPath.row].id ?? ""
            self.navigationController?.popViewController()
            
        }

        tableView.reloadData()
    }
    
    @IBOutlet weak var categoriesTable: UITableView!
    var values : [String] = []
    var categories: CategoriesResponse?
    {
        didSet{
            categoriesTable.reloadData()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        var catsStrings = ""
        var catsIds = ""
        Defaults[.flagAddProd] = false
        super.viewWillDisappear(animated)
        for arr in selectedCatsStrings {
           catsStrings += arr.value.joined(separator: ",") + ","
        }
        if catsStrings != ""
        { catsStrings.remove(at: catsStrings.index(before: catsStrings
            .endIndex))
            
        }
        
        Defaults[.selectedCatstring] = catsStrings
        
        for arr in selectedCells {
            catsIds += arr.value.joined(separator: ",") + ","
        }
        if catsIds != ""
        {
            catsIds.remove(at: catsIds.index(before: catsIds
            .endIndex))
        }
        Defaults[.selectedCatIds] = catsIds
        
//        Defaults[.selectedCatsArr] = selectedCells as? [Int : [String]]
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        addNavBar(isBack: true)
        getCategoriesApi(successClosure: {  (data) in
            
            if let dataModel = data as? CategoriesResponse{
                
                self.categories = dataModel
                
            }
            
        })

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getCategoriesApi(successClosure: @escaping SuccessClosure){
        var tempC = ""
        if Defaults[.flagAddProd] {
            tempC = "?c=\(Defaults[.companyId])&lang=\(Defaults[.langId])"
        }
        else{
            tempC = "?lang=\(Defaults[.langId])"
        }
        Alamofire.request("https://www.instadeal.co/home/getCatsWithSub/\(tempC)", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CategoriesResponse(JSON: data)
                    
                    successClosure(dataModel)
                    Defaults[.flagAddProd] = false
                }
            }
        }
    }

}
