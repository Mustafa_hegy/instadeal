//
//  MenuViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/11/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults
import Localize_Swift
import LiveChat

class MenuViewController: UIViewController , UIGestureRecognizerDelegate {

    @IBOutlet weak var containingView: UIView!
    @IBOutlet weak var supportTable: UITableView!
    @IBOutlet weak var headHeight: NSLayoutConstraint!
    @IBOutlet weak var menuHead: UIImageView!
    @IBOutlet weak var board: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var menuTable: UITableView!
    var logged: Bool = false
    var supportModel : GetSupportResponse?
    {
        didSet{
            supportTable.reloadData()
        }
    }
    var results = [OffersResponse]()
    {
        didSet{
            
        }
    }
    
//    ["الرئيسية","عن التطبيق","ملفي الشخصي","جمع لي","طلباتي","طلب إضافة تاجر او محل","المفضلة","اعلن لدينا","الدعم الفني","أدعو أصدقائك","شروط وأحكام","اتصل بنا"]
    var labelsArray: [String] = [
        "home".localized(),
        "aboutApp".localized(),
        "myProfile".localized(),
        "gmma3ly".localized(),
        "myOrders".localized(),
        "addCompany".localized(),
        "favorite".localized(),
        "advWithUs".localized(),
        "support".localized(),
        "inviteFriends".localized(),
        "terms&Conditions".localized(),
        "contactUs".localized()
    ]
    
    let iconsArray: [UIImage] = [#imageLiteral(resourceName: "homeIconMenu") , #imageLiteral(resourceName: "about") , #imageLiteral(resourceName: "myAccount") , #imageLiteral(resourceName: "gmma3lyIcon") , #imageLiteral(resourceName: "myOrders") , #imageLiteral(resourceName: "joinUs") , #imageLiteral(resourceName: "emptyHart") , #imageLiteral(resourceName: "advIcon") , #imageLiteral(resourceName: "myOrders") , #imageLiteral(resourceName: "export") , #imageLiteral(resourceName: "term") , #imageLiteral(resourceName: "contactUs") , #imageLiteral(resourceName: "bell"), #imageLiteral(resourceName: "language") ]
    //, #imageLiteral(resourceName: "newFace") , #imageLiteral(resourceName: "twitternew") , #imageLiteral(resourceName: "instanew") , #imageLiteral(resourceName: "web")
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        labelsArray.append("notifications".localized())
        labelsArray.append("appLang".localized())
//        labelsArray.append("فيس بوك")
//        labelsArray.append("تويتر")
//        labelsArray.append("انستجرام")
//        labelsArray.append("الموقع الإلكتروني")
        supportTable.tableFooterView = UIView()
        self.tabBarController?.tabBar.layer.zPosition = 0
        board.isHidden = true
        headHeight.constant = 150
        if Defaults[.logged]{
            loginBtn.setTitle("logout".localized(), for: .normal)
            if Defaults[.userType] != "user" {
                board.isHidden = false
                headHeight.constant = 200
            }
        }
        getSupportApi { (data) in
            
            if let dataModel = data as? GetSupportResponse{
                self.supportModel = dataModel
                self.supportTable.reloadData()
            }
        }
//        addNavBar(isBack: true)
        // Do any additional setup after loading the view.
    }
    

    func localizeUI(){
        loginBtn.setTitle("login".localized(), for: .normal)
        board.setTitle("sellarDashboard".localized(), for: .normal)
    }
    
    
    @IBAction func exitContaingView(_ sender: UITapGestureRecognizer) {
        containingView.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        containingView.isHidden = true
        view.layoutSubviews()
        view.layoutIfNeeded()
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view?.isDescendant(of: supportTable))!{
            
            return false
        }
        return true
    }
    @IBAction func tagerBoardAction(_ sender: UIButton) {
        // Old Code : move to tager compny view controller
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tagerEnter") as! TagerCompanyViewController
        (viewController as! TagerCompanyViewController).flagSegue = true
        self.navigationController?.show(viewController, sender: self)
        
        /*
        // Edited Code : Move to select language view controller
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectLanguageViewController") as! SelectLanguageViewController
        self.navigationController?.show(viewController, sender: self)
         */
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.hidesBottomBarWhenPushed = false
        if segue.identifier == "goFavs"{
            if let VC = segue.destination as? SearchResultViewController{
//                VC.results = self.results
                VC.flag = false
                
            }
        }
        
    }

    @IBAction func loginBtnAction(_ sender: UIButton) {
        
        if Defaults[.logged]{
            Defaults[.logged] = false
            Defaults[.userId] = ""
            navigationController?.popViewController()
        }
        else{
            performSegue(withIdentifier: "logOutSeg", sender: sender)
            Defaults[.logged] = false
        }
       
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getFavApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getMyFav/\(Defaults[.userId] )?lang=\(Defaults[.langId])&area=\(Defaults[.areaId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = GetOffersResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
    func getSupportApi(successClosure: @escaping SuccessClosure){
        
        Alamofire.request("https://www.instadeal.co/home/getSupport?lang=\(Defaults[.langId])", method: .get).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = GetSupportResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
}



extension MenuViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == supportTable{
            if let url = URL(string: "tel://\(self.supportModel?.data?[indexPath.row].mobile ?? "")"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        else{
        switch indexPath.row {
            
        case 0:
            let viewController: HomeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.show(viewController, sender: self)
        case 1:
            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
            viewController.url = "https://www.instadeal.co/about.html?lang=\(Defaults[.langId])"
            self.navigationController?.show(viewController, sender: self)
        case 2:
            let viewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "myAccount") as! UIViewController
           
            self.navigationController?.show(viewController, sender: self)
        case 3:
            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
            viewController.url = "http://instadeal.co/gamm3.html?lang=\(Defaults[.langId])"
            self.navigationController?.show(viewController, sender: self)
        case 4:
            let viewController: UserOrdersViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "userOrdersVC") as! UserOrdersViewController
            
           self.navigationController?.view.backgroundColor = .white
            self.navigationController?.show(viewController, sender: self)
        case 5:
            if !Defaults[.logged]{
                performSegue(withIdentifier: "logOutSeg", sender: self)
            }
            else{
                performSegue(withIdentifier: "goTager", sender: self)
                
            }
        case 6:
            self.performSegue(withIdentifier: "goFavs", sender: self)
//            getFavApi(successClosure: { (data) in
//
//                if let dataModel = data as? GetOffersResponse{
//
//                    self.results = dataModel.data ?? []
//
//                }
//            })
        case 7:
            let viewController: ContactUsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            
            self.navigationController?.show(viewController, sender: self)
        case 8:
//            containingView.isHidden = false
//            supportTable.reloadData()
            
//            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
//            viewController.url = "https://direct.lc.chat/10893612/"
//            self.navigationController?.show(viewController, sender: self)
            
            //Presenting chat:
            if !Defaults[.logged]{
                LiveChat.name = Defaults[.userName] // User name and email can be provided if known
                LiveChat.email = Defaults[.userEmail]
            }
            LiveChat.presentChat()
            
        case 9:
            let textToShare = "InstaDeal Application. \n https://itunes.apple.com/us/app/instadeal/id1409473459?mt=8"
            
            if let myWebsite = NSURL(string: "") {
                let objectsToShare = [textToShare, myWebsite] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                //New Excluded Activities Code
                activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                //
                
                activityVC.popoverPresentationController?.sourceView = self.menuTable
                self.present(activityVC, animated: true, completion: nil)
            }
            
        case 10:
            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
            viewController.url = "https://www.instadeal.co/conditions.html?lang=\(Defaults[.langId])"
            self.navigationController?.show(viewController, sender: self)
            
        case 11:
            let viewController: ContactUsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            
            self.navigationController?.show(viewController, sender: self)
        case 12:
                if !Defaults[.logged]{
                    performSegue(withIdentifier: "logOutSeg", sender: self)
                }
                else{
                    performSegue(withIdentifier: "goNoti", sender: self)
                    
            }
        case 13:
            if Defaults[.langId] == "1" {
                // Arabic Language Selected
                UIView.appearance().semanticContentAttribute = .forceLeftToRight //Design is created for Ar =>default RTL :(
                
                Defaults[.langId]       = "2"
                Localize.setCurrentLanguage("ar")
            } else if Defaults[.langId] == "2" {
                // English Language Selected
                UIView.appearance().semanticContentAttribute = .forceRightToLeft //Design is created for Ar =>default RTL :(
                Defaults[.langId]       = "1"
                Localize.setCurrentLanguage("en")
            }
            
            let viewController:MainTabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarRoot") as! MainTabBarController
            let appDelegate = UIApplication.shared.delegate
            appDelegate?.window??.rootViewController = viewController
            
        case 14:
            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
            viewController.url = "https://www.facebook.com/InstaDeal.co"
            self.navigationController?.show(viewController, sender: self)
        case 15:
            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
            viewController.url = "https://twitter.com/InstaDealco"
            self.navigationController?.show(viewController, sender: self)
        case 16:
            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
            viewController.url = "https://www.instagram.com/instadeal.co"
            self.navigationController?.show(viewController, sender: self)
        case 17:
            let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
            viewController.url = "https://www.instadeal.co"
            self.navigationController?.show(viewController, sender: self)
        default:
            break
        }
        }
    }
}

extension MenuViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == supportTable{
            return supportModel?.data?.count ?? 0
        }
        
        return iconsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == supportModel{
            return 45
        }
        return 44
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == supportTable{
            let cell: ShowAddressTableViewCell = tableView.dequeueReusableCell(withIdentifier: "payCellPopUp", for: indexPath) as! ShowAddressTableViewCell
            cell.addressName.text = supportModel?.data?[indexPath.row].mobile ?? ""
            return cell
        }
        else{
            
            let cell: MenuCell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
            cell.cellIconImg.image = iconsArray[indexPath.row]
            cell.cellLabel.text = labelsArray[indexPath.row]
            
            if Defaults[.langId] == "1" {
                cell.cellLabel.textAlignment = .left
            } else if Defaults[.langId] == "2" {
                cell.cellLabel.textAlignment = .right
            }
            return cell
        }
        return UITableViewCell()
    }
    
    
    
}
