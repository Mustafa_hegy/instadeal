//
//  AddProductViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/11/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults
import AlamofireImage
import SDWebImage
import ImagePicker
import TLPhotoPicker



class AddProductViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImagePickerDelegate, TLPhotosPickerViewControllerDelegate {
    var selectedAssets = [TLPHAsset]()
    @IBOutlet weak var maxAmmount: UITextField!
    @IBOutlet weak var imageScroll: UIScrollView!
    
    var imagePicker = ImagePickerController()
     let imagePickerController = UIImagePickerController()
    var pickerIsCamera = true
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
            selectedImgsArr = images
        
        switch self.selectedImgsArr.count
        {
        case 1 :
            self.img1.isHidden = false
            self.img1.image = self.selectedImgsArr[0]
        case 2 :
            self.img1.isHidden = false
            self.img1.image = self.selectedImgsArr[0]
            self.img2.isHidden = false
            self.img2.image = self.selectedImgsArr[1]
        case 3 :
            self.img1.isHidden = false
            self.img1.image = self.selectedImgsArr[0]
            self.img2.isHidden = false
            self.img2.image = self.selectedImgsArr[1]
            self.img3.isHidden = false
            self.img3.image = self.selectedImgsArr[2]
        case 4 :
            self.img1.isHidden = false
            self.img1.image = self.selectedImgsArr[0]
            self.img2.isHidden = false
            self.img2.image = self.selectedImgsArr[1]
            self.img3.isHidden = false
            self.img3.image = self.selectedImgsArr[2]
            self.img4.isHidden = false
            self.img4.image = self.selectedImgsArr[3]
        case 5 :
            self.img1.isHidden = false
            self.img1.image = self.selectedImgsArr[0]
            self.img2.isHidden = false
            self.img2.image = self.selectedImgsArr[1]
            self.img3.isHidden = false
            self.img3.image = self.selectedImgsArr[2]
            self.img4.isHidden = false
            self.img4.image = self.selectedImgsArr[3]
            self.img5.isHidden = false
            self.img5.image = self.selectedImgsArr[4]
        case 6 :
            self.img1.isHidden = false
            self.img1.image = self.selectedImgsArr[0]
            self.img2.isHidden = false
            self.img2.image = self.selectedImgsArr[1]
            self.img3.isHidden = false
            self.img3.image = self.selectedImgsArr[2]
            self.img4.isHidden = false
            self.img4.image = self.selectedImgsArr[3]
            self.img5.isHidden = false
            self.img5.image = self.selectedImgsArr[4]
            self.img6.isHidden = false
            self.img6.image = self.selectedImgsArr[5]
        default:
            break
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    var configuration = Configuration()
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var chooseDepartBtn: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var advPrice: UITextField!
    @IBOutlet weak var advDiscount: UITextField!
    @IBOutlet weak var advDesc: UITextField!
    
    @IBOutlet weak var img6: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var advName: UITextField!
    var publicImageURL = [URL]()
    var imgsArrayUrls = [String]()
    var selectedImgsArr = [UIImage]()
    var product: ProductDetailsResponse?
    var flagEdit: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        imagePickerController.delegate = self
        imagePicker.delegate = self
        Defaults[.search] = true
         Defaults[.flagAddProd] = true
        Defaults[.selectedCatstring] = ""
        handleTabBarVisibility(hide: true)
        
        configuration.allowMultiplePhotoSelection = true
//        imagePicker = ImagePickerController(configuration: configuration)
            if flagEdit{
                addBtn.setTitle("edit".localized(), for: .normal)
                getProductDetailsApi(successClosure: { (data) in
                    
                    if let dataModel = data as? ProductDetailsResponse{
                        self.product = dataModel
                        if dataModel.photo != "" {
                            self.img1.sd_setImage(with: URL(string: dataModel.photo ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                            self.img1.isHidden = false
                            
                        }
                        if dataModel.photo1 != "" {
                             self.img2.sd_setImage(with: URL(string: dataModel.photo1 ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                            self.img2.isHidden = false
                        }
                        if dataModel.photo2 != "" {
                             self.img3.sd_setImage(with: URL(string: dataModel.photo2 ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                            self.img3.isHidden = false
                        }
                        if dataModel.photo3 != "" {
                            self.img4.sd_setImage(with: URL(string: dataModel.photo3 ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                            self.img4.isHidden = false
                        }
                        if dataModel.photo4 != "" {
                            self.img5.sd_setImage(with: URL(string: dataModel.photo4 ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                            self.img5.isHidden = false
                        }
                        if dataModel.photo6 != "" {
                            self.img6.sd_setImage(with: URL(string: dataModel.photo6 ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                            self.img6.isHidden = false
                        }
                        print("Discount: \(dataModel.discount)")
                        self.advName.text = dataModel.name
                        self.maxAmmount.text = dataModel.counts
                        self.advPrice.text = dataModel.price
                        self.advDiscount.text = dataModel.discount
                        let catsStr = Defaults[.selectedCatstring] == "" ? dataModel.cat_name : Defaults[.selectedCatstring]
                        self.chooseDepartBtn.setTitle(catsStr, for: .normal)
                        let catsIdStr = Defaults[.catId] == "" ? dataModel.cat_id : Defaults[.catId]
                        Defaults[.catId] = catsIdStr ?? ""
                        self.advDesc.text = dataModel.FeedData
                    }
                })
                
                
            }
        addNavBar(isBack: true)
        imagePicker.delegate = self
        img1.isHidden = true
        img2.isHidden = true
        img3.isHidden = true
        img4.isHidden = true
        img5.isHidden = true
        img6.isHidden = true
        // Do any add   itional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Defaults[.flagAddProd] = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Defaults[.flagAddProd] = true
        if !flagEdit{
            
            if Defaults[.selectedCatstring] != ""{
                chooseDepartBtn.setTitle(Defaults[.selectedCatstring], for: .normal)
            }
            else{
                chooseDepartBtn.setTitle("chooseCategory".localized(), for: .normal)
            }
        }
        else{
            
            if Defaults[.selectedCatstring] != ""{
                chooseDepartBtn.setTitle(Defaults[.selectedCatstring], for: .normal)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func localizeUI(){
        addBtn.setTitle("add".localized(), for: .normal)
        chooseDepartBtn.setTitle("chooseCategory".localized(), for: .normal)
        advName.placeholder         = "advName".localized()
        advPrice.placeholder        = "price".localized()
        advDiscount.placeholder     = "discount".localized()
        maxAmmount.placeholder      = "maxAmount".localized()
        advDesc.placeholder         = "advDetails".localized()
    }
    
    @IBAction func chooseDepAction(_ sender: UIButton) {
        
        let viewController: SelectCategoryViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
        
        self.navigationController?.show(viewController, sender: self)
    }
    @IBAction func addProduct(_ sender: UIButton) {
        
        var photoIndex = 0
        var photoIndexStr = ""
        
        if !flagEdit{
            if pickerIsCamera{
                
                uploadProductApi { (data) in
                    
                    if let dataModel = data as? addProductResponse{
                        
                        if dataModel.result == "true"{
                            Defaults[.uploadedProdId] = dataModel.id
                            
                            for image in self.selectedImgsArr{
                                self.progressBar.isHidden = false
                                switch self.selectedImgsArr.count
                                {
                                case 1 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedImgsArr[0]
                                case 2 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedImgsArr[0]
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedImgsArr[1]
                                case 3 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedImgsArr[0]
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedImgsArr[1]
                                    self.img3.isHidden = false
                                    self.img3.image = self.selectedImgsArr[2]
                                case 4 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedImgsArr[0]
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedImgsArr[1]
                                    self.img3.isHidden = false
                                    self.img3.image = self.selectedImgsArr[2]
                                    self.img4.isHidden = false
                                    self.img4.image = self.selectedImgsArr[3]
                                case 5 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedImgsArr[0]
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedImgsArr[1]
                                    self.img3.isHidden = false
                                    self.img3.image = self.selectedImgsArr[2]
                                    self.img4.isHidden = false
                                    self.img4.image = self.selectedImgsArr[3]
                                    self.img5.isHidden = false
                                    self.img5.image = self.selectedImgsArr[4]
                                case 6 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedImgsArr[0]
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedImgsArr[1]
                                    self.img3.isHidden = false
                                    self.img3.image = self.selectedImgsArr[2]
                                    self.img4.isHidden = false
                                    self.img4.image = self.selectedImgsArr[3]
                                    self.img5.isHidden = false
                                    self.img5.image = self.selectedImgsArr[4]
                                    self.img6.isHidden = false
                                    self.img6.image = self.selectedImgsArr[5]
                                default:
                                    break
                                }
                                
                                if photoIndex > 0{
                                    photoIndexStr = String(photoIndex)
                                }
                                else{
                                    photoIndexStr = ""
                                }
                                
                                DispatchQueue.global(qos: .background).sync {
                                    self.uploadProductPicsApi(with: "photo\(photoIndexStr)", image: image, successClosure: { (response) in
                                        if photoIndex == self.selectedImgsArr.count - 1{
                                            DispatchQueue.main.sync {
                                                //                                        self.showDefaultAlert(title: "تم إضافة الاعلان", message: nil)
                                            }
                                            
                                        }
                                    })
                                }
                                photoIndex += 1
                            }
                            
                            self.showDefaultAlert(title:"adminReviewAddProductAlert".localized(), message: nil, actionBlock: {
                                if self.progressBar.isHidden{
                                    self.navigationController?.popViewController()
                                }
                            })
                        }
                    }
                }
            }
            else{
                uploadProductApi { (data) in
                    
                    if let dataModel = data as? addProductResponse{
                        
                        if dataModel.result == "true"{
                            Defaults[.uploadedProdId] = dataModel.id
                            
                            for image in self.selectedAssets{
                                self.progressBar.isHidden = false
                                switch self.selectedAssets.count
                                {
                                case 1 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedAssets[0].fullResolutionImage
                                case 2 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedAssets[0].fullResolutionImage
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedAssets[1].fullResolutionImage
                                case 3 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedAssets[0].fullResolutionImage
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedAssets[1].fullResolutionImage
                                    self.img3.isHidden = false
                                    self.img3.image = self.selectedAssets[2].fullResolutionImage
                                case 4 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedAssets[0].fullResolutionImage
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedAssets[1].fullResolutionImage
                                    self.img3.isHidden = false
                                    self.img3.image = self.selectedAssets[2].fullResolutionImage
                                    self.img4.isHidden = false
                                    self.img4.image = self.selectedAssets[3].fullResolutionImage
                                case 5 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedAssets[0].fullResolutionImage
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedAssets[1].fullResolutionImage
                                    self.img3.isHidden = false
                                    self.img3.image = self.selectedAssets[2].fullResolutionImage
                                    self.img4.isHidden = false
                                    self.img4.image = self.selectedAssets[3].fullResolutionImage
                                    self.img5.isHidden = false
                                    self.img5.image = self.selectedAssets[4].fullResolutionImage
                                case 6 :
                                    self.img1.isHidden = false
                                    self.img1.image = self.selectedAssets[0].fullResolutionImage
                                    self.img2.isHidden = false
                                    self.img2.image = self.selectedAssets[1].fullResolutionImage
                                    self.img3.isHidden = false
                                    self.img3.image = self.selectedAssets[2].fullResolutionImage
                                    self.img4.isHidden = false
                                    self.img4.image = self.selectedAssets[3].fullResolutionImage
                                    self.img5.isHidden = false
                                    self.img5.image = self.selectedAssets[4].fullResolutionImage
                                    self.img6.isHidden = false
                                    self.img6.image = self.selectedAssets[5].fullResolutionImage
                                default:
                                    break
                                }
                                
                                
                                if photoIndex > 0{
                                    photoIndexStr = String(photoIndex)
                                }
                                else{
                                    photoIndexStr = ""
                                }
                                
                                DispatchQueue.global(qos: .background).sync {
                                    self.uploadProductPicsApi(with: "photo\(photoIndexStr)", image: image.fullResolutionImage!, successClosure: { (response) in
                                        if photoIndex == self.selectedImgsArr.count - 1{
                                            DispatchQueue.main.sync {
                                                //                                        self.showDefaultAlert(title: "تم إضافة الاعلان", message: nil)
                                            }
                                            
                                        }
                                    })
                                }
                                photoIndex += 1
                            }
                            
                            self.showDefaultAlert(title: "adminReviewAddProductAlert".localized(), message: nil, actionBlock: {
                                if self.progressBar.isHidden{
                                    self.navigationController?.popViewController()
                                }
                            })
                        }
                    }
                }
            }
        }
        else{
            if pickerIsCamera{
                updateProductApi(successClosure: { (data) in
                    for image in self.selectedImgsArr{
                        self.progressBar.isHidden = false
                        switch self.selectedImgsArr.count
                        {
                        case 1 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedImgsArr[0]
                        case 2 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedImgsArr[0]
                            self.img2.isHidden = false
                            self.img2.image = self.selectedImgsArr[1]
                        case 3 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedImgsArr[0]
                            self.img2.isHidden = false
                            self.img2.image = self.selectedImgsArr[1]
                            self.img3.isHidden = false
                            self.img3.image = self.selectedImgsArr[2]
                        case 4 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedImgsArr[0]
                            self.img2.isHidden = false
                            self.img2.image = self.selectedImgsArr[1]
                            self.img3.isHidden = false
                            self.img3.image = self.selectedImgsArr[2]
                            self.img4.isHidden = false
                            self.img4.image = self.selectedImgsArr[3]
                        case 5 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedImgsArr[0]
                            self.img2.isHidden = false
                            self.img2.image = self.selectedImgsArr[1]
                            self.img3.isHidden = false
                            self.img3.image = self.selectedImgsArr[2]
                            self.img4.isHidden = false
                            self.img4.image = self.selectedImgsArr[3]
                            self.img5.isHidden = false
                            self.img5.image = self.selectedImgsArr[4]
                        case 6 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedImgsArr[0]
                            self.img2.isHidden = false
                            self.img2.image = self.selectedImgsArr[1]
                            self.img3.isHidden = false
                            self.img3.image = self.selectedImgsArr[2]
                            self.img4.isHidden = false
                            self.img4.image = self.selectedImgsArr[3]
                            self.img5.isHidden = false
                            self.img5.image = self.selectedImgsArr[4]
                            self.img6.isHidden = false
                            self.img6.image = self.selectedImgsArr[5]
                        default:
                            break
                        }
                        
                        
                        if photoIndex > 0{
                            photoIndexStr = String(photoIndex)
                        }
                        else{
                            photoIndexStr = ""
                        }
                        
                        DispatchQueue.global(qos: .background).sync {
                            self.uploadProductPicsApi(with: "photo\(photoIndexStr)", image: image, successClosure: { (response) in
                                if photoIndex == self.selectedImgsArr.count - 1{
                                    DispatchQueue.main.sync {
                                        
                                    }
                                    
                                }
                                //                             self.showDefaultAlert(title: "تم تعديل المنتج بنجاح", message: nil)
                            })
                        }
                        photoIndex += 1
                    }
                    
                    self.showDefaultAlert(title: "adminReviewUpdateAlert".localized(), message: nil, actionBlock: {
                        if self.progressBar.isHidden{
                            self.navigationController?.popViewController()
                        }
                    })
                })
            }
            else
            {
                
                updateProductApi(successClosure: { (data) in
                    for image in self.selectedAssets{
                        self.progressBar.isHidden = false
                        switch self.selectedAssets.count
                        {
                        case 1 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedAssets[0].fullResolutionImage
                        case 2 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedAssets[0].fullResolutionImage
                            self.img2.isHidden = false
                            self.img2.image = self.selectedAssets[1].fullResolutionImage
                        case 3 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedAssets[0].fullResolutionImage
                            self.img2.isHidden = false
                            self.img2.image = self.selectedAssets[1].fullResolutionImage
                            self.img3.isHidden = false
                            self.img3.image = self.selectedAssets[2].fullResolutionImage
                        case 4 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedAssets[0].fullResolutionImage
                            self.img2.isHidden = false
                            self.img2.image = self.selectedAssets[1].fullResolutionImage
                            self.img3.isHidden = false
                            self.img3.image = self.selectedAssets[2].fullResolutionImage
                            self.img4.isHidden = false
                            self.img4.image = self.selectedAssets[3].fullResolutionImage
                        case 5 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedAssets[0].fullResolutionImage
                            self.img2.isHidden = false
                            self.img2.image = self.selectedAssets[1].fullResolutionImage
                            self.img3.isHidden = false
                            self.img3.image = self.selectedAssets[2].fullResolutionImage
                            self.img4.isHidden = false
                            self.img4.image = self.selectedAssets[3].fullResolutionImage
                            self.img5.isHidden = false
                            self.img5.image = self.selectedAssets[4].fullResolutionImage
                        case 6 :
                            self.img1.isHidden = false
                            self.img1.image = self.selectedAssets[0].fullResolutionImage
                            self.img2.isHidden = false
                            self.img2.image = self.selectedAssets[1].fullResolutionImage
                            self.img3.isHidden = false
                            self.img3.image = self.selectedAssets[2].fullResolutionImage
                            self.img4.isHidden = false
                            self.img4.image = self.selectedAssets[3].fullResolutionImage
                            self.img5.isHidden = false
                            self.img5.image = self.selectedAssets[4].fullResolutionImage
                            self.img6.isHidden = false
                            self.img6.image = self.selectedAssets[5].fullResolutionImage
                        default:
                            break
                        }
                        
                        
                        if photoIndex > 0{
                            photoIndexStr = String(photoIndex)
                        }
                        else{
                            photoIndexStr = ""
                        }
                        
                        DispatchQueue.global(qos: .background).sync {
                            self.uploadProductPicsApi(with: "photo\(photoIndexStr)", image: image.fullResolutionImage!, successClosure: { (response) in
                                if photoIndex == self.selectedImgsArr.count - 1{
                                    DispatchQueue.main.sync {
                                        
                                    }
                                    
                                }
                                //                             self.showDefaultAlert(title: "تم تعديل المنتج بنجاح", message: nil)
                            })
                        }
                        photoIndex += 1
                    }
                    
                    self.showDefaultAlert(title: "adminReviewProductUpdateImageAlert".localized(), message: nil, actionBlock: {
                        if self.progressBar.isHidden{
                            self.navigationController?.popViewController()
                        }
                    })
                    
                    
                })
                
            }
        }
    }
    @IBAction func pickImage(_ sender: UIButton) {
       
        let alert = UIAlertController(title: "Choose type" , message: "Select image" , preferredStyle: .actionSheet);

        let photos = UIAlertAction(title : "Camera" , style: .default , handler: { (alert: UIAlertAction ) in

            self.openCamera()

        })

        let videos = UIAlertAction(title : "Gallery" , style: .default , handler: { (alert: UIAlertAction ) in

            self.openGallery()

        })



        let cancel = UIAlertAction(title : "Cancel" , style: .default , handler: nil)

        alert.addAction(photos)

        alert.addAction(videos)

        alert.addAction(cancel)

        present(alert, animated: true , completion: nil)

    }
    

    
//
    func openCamera () {
        pickerIsCamera = true
        if UIImagePickerController.isSourceTypeAvailable(.camera){

           
            imagePicker.topView.flashButton.isHidden = false
            imagePicker.topView.rotateCamera.isHidden = false
            imagePicker.bottomContainer.isHidden = false
            imagePicker.delegate = self
            imagePicker.topView.isHidden = false
            
            imagePicker.imageLimit = 6
            
//            imagePicker.cameraAvailable()
            imagePicker.cameraNotAvailable()
            present(imagePicker, animated: true, completion: nil)
        }else{

            print("Camera not awailable")

        }
    }
    
    func openGallery() {

        let viewController = TLPhotosPickerViewController()
        viewController.delegate = self
        var configure = TLPhotosPickerConfigure()
        pickerIsCamera = false
        //configure.nibSet = (nibName: "CustomCell_Instagram", bundle: Bundle.main) // If you want use your custom cell..
        self.present(viewController, animated: true, completion: nil)

    }
    
    //TLPhotosPickerViewControllerDelegate
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        // use selected order, fullresolution image
        self.selectedAssets = withTLPHAssets
        
        switch self.selectedAssets.count
        {
        case 1 :
            self.img1.isHidden = false
            self.img1.image = self.selectedAssets[0].fullResolutionImage
        case 2 :
            self.img1.isHidden = false
            self.img1.image = self.selectedAssets[0].fullResolutionImage
            self.img2.isHidden = false
            self.img2.image = self.selectedAssets[1].fullResolutionImage
        case 3 :
            self.img1.isHidden = false
            self.img1.image = self.selectedAssets[0].fullResolutionImage
            self.img2.isHidden = false
            self.img2.image = self.selectedAssets[1].fullResolutionImage
            self.img3.isHidden = false
            self.img3.image = self.selectedAssets[2].fullResolutionImage
        case 4 :
            self.img1.isHidden = false
            self.img1.image = self.selectedAssets[0].fullResolutionImage
            self.img2.isHidden = false
            self.img2.image = self.selectedAssets[1].fullResolutionImage
            self.img3.isHidden = false
            self.img3.image = self.selectedAssets[2].fullResolutionImage
            self.img4.isHidden = false
            self.img4.image = self.selectedAssets[3].fullResolutionImage
        case 5 :
            self.img1.isHidden = false
            self.img1.image = self.selectedAssets[0].fullResolutionImage
            self.img2.isHidden = false
            self.img2.image = self.selectedAssets[1].fullResolutionImage
            self.img3.isHidden = false
            self.img3.image = self.selectedAssets[2].fullResolutionImage
            self.img4.isHidden = false
            self.img4.image = self.selectedAssets[3].fullResolutionImage
            self.img5.isHidden = false
            self.img5.image = self.selectedAssets[4].fullResolutionImage
        case 6 :
            self.img1.isHidden = false
            self.img1.image = self.selectedAssets[0].fullResolutionImage
            self.img2.isHidden = false
            self.img2.image = self.selectedAssets[1].fullResolutionImage
            self.img3.isHidden = false
            self.img3.image = self.selectedAssets[2].fullResolutionImage
            self.img4.isHidden = false
            self.img4.image = self.selectedAssets[3].fullResolutionImage
            self.img5.isHidden = false
            self.img5.image = self.selectedAssets[4].fullResolutionImage
            self.img6.isHidden = false
            self.img6.image = self.selectedAssets[5].fullResolutionImage
        default:
            break
        }
        
    }
    
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    }
 
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            img1.contentMode = .scaleAspectFit
            img1.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }

    func uploadProductApi( successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "company_id": Defaults[.companyId] ,
            "price": advPrice.text ?? "" ,
            "name": advName.text ?? "" ,
            "text": advDesc.text ?? "",
            "cat" : Defaults[.catId],
            "counts" : maxAmmount.text ?? "",
            "discount": advDiscount.text ?? ""
        ]
        
        Alamofire.request("https://www.instadeal.co/home/addProduct/\(Defaults[.userId])?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = addProductResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
        }
    }
    
    func updateProductApi( successClosure: @escaping SuccessClosure){
        let params: Parameters = [
            "company_id": Defaults[.companyId] ,
            "price": advPrice.text ?? "" ,
            "name": advName.text ?? "" ,
            "text": advDesc.text ?? "",
            "cat" : Defaults[.catId],
            "counts" : maxAmmount.text ?? "",
            "discount" : advDiscount.text ?? ""
        ]
        
        Alamofire.request("https://www.instadeal.co/home/updateProduct/\(Defaults[.tagerProdId])?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = addProductResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
        
    }
    
    func getProductDetailsApi(successClosure: @escaping SuccessClosure){
        
        print("EditProduct: https://www.instadeal.co/home/getProductForEdit/\(Defaults[.tagerProdId])?lang=\(Defaults[.langId])")
        Alamofire.request("https://www.instadeal.co/home/getProductForEdit/\(Defaults[.tagerProdId])?lang=\(Defaults[.langId])", method: .get ,  encoding: JSONEncoding.default)
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = ProductDetailsResponse(JSON: data)
                        Defaults[.catId] = dataModel?.cat_id ?? ""
                        
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
    
    func uploadProductPicsApi(with name: String , image: UIImage , successClosure: @escaping SuccessClosure){
 
        Alamofire.upload(multipartFormData:{ multipartFormData in
            if let imageData = UIImageJPEGRepresentation(image, 0.6)?.base64EncodedData() {
                multipartFormData.append(imageData, withName: name)}
 
            
        }, to:"https://www.instadeal.co/home/updateProduct2/\(Defaults[.uploadedProdId] )",
                         method:.post,
                         headers: [:],
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.uploadProgress(closure: { (progress) in
            
                                    self.progressBar.isHidden = false
                                    self.progressBar.setProgress(Float(progress.fractionCompleted), animated: true)
                                    
                                })
                                upload.responseString { response in
                                    debugPrint(response)
                                    if response.error == nil{
                                    
                                    }
                                    successClosure(response)
                                    self.progressBar.isHidden = true
                                    self.showDefaultAlert(title:"adminReviewAddProductAlert".localized(), message: nil, actionBlock: {
                                        self.navigationController?.popViewController()
                                    })
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                            }
        })
        

    }
}

