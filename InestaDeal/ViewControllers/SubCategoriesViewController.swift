//
//  SubCategoriesViewController.swift
//  InestaDeal
//
//  Created by Waleed on 4/13/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class SubCategoriesViewController: UIViewController {

    var modelView: SubCategoriesModelView?
    var parentCategoryId: String?
    var categoryName: String?
    @IBOutlet weak var tableCellHeaderLbl: UILabel!
    var categories: CategoriesResponse?
    {
        didSet{
            subCategoriesView.reloadData()
            
        }
    }
    var section: Int = 0
    
    @IBOutlet weak var subCategoriesView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleTabBarVisibility(hide: true)
        
        UIView.transition(with: tabBarController!.tabBar, duration: 0.7, options: .transitionCrossDissolve, animations: nil)
        
        print("parentCategoryId\(parentCategoryId ?? "")")
        subCategoriesView.transform = CGAffineTransform(scaleX: -1, y: 1)
        modelView = SubCategoriesModelView(scene: self)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableCellHeaderLbl.text = categoryName
        
        modelView?.getSubCategoriesApi(successClosure: { [unowned self] (data) in
            
            if let dataModel = data as? CategoriesResponse{
                
                self.categories = dataModel
                
                print(dataModel.data![0])
            }
            
        })
        addNavBar(isBack: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "companySegInternal"{
            
            let destinationVC = segue.destination as! CompanyViewController
            destinationVC.subCategoryId = Defaults[.subCategoryId]
        }
        if segue.identifier == "catProductsSeg"{
            
            let destinationVC = segue.destination as! ProductsListViewController
            destinationVC.subCategoryId = Defaults[.subCategoryId]
        }
    }
    
}

extension SubCategoriesViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        Defaults[.subCategoryId] = categories?.data![indexPath.row].id ?? ""
        
        // Move to Products List related to selected Category
        self.performSegue(withIdentifier: "catProductsSeg", sender: self) // New Code
        
    }
}
extension SubCategoriesViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return categories?.data!.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subCatCell", for: indexPath) as! CategoryCell
        cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
        cell.imageView.sd_setImage(with: URL(string: (categories?.data![indexPath.row].photo) ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.subCategoryLbl.text = categories?.data![indexPath.row].name
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.backgroundColor = appThemeColor
    }
    
}

extension SubCategoriesViewController : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 10
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = 90.0
        return CGSize(width: itemWidth, height: 90.0)
    }
    
}
