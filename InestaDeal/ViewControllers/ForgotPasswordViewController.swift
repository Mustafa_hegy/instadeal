//
//  ForgotPasswordViewController.swift
//  
//
//  Created by Waleed on 6/6/18.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var sendPasswordAction: UIButton!
    @IBOutlet weak var contactIcon: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
         let paddingView = UIView(frame: CGRect(x: 0,y:  0,width: contactIcon.size.width + 20, height: emailTextField.size.height))
        
        if Defaults[.langId] == "1" {
            backButton.setImage( #imageLiteral(resourceName: "blackArrow") , for: .normal)
            emailTextField.textAlignment    = .left
            
            emailTextField.leftView = paddingView
            emailTextField.leftViewMode = UITextFieldViewMode.always
        } else if Defaults[.langId] == "2" {
            backButton.setImage( #imageLiteral(resourceName: "blackArrowAr") , for: .normal)
            emailTextField.textAlignment    = .right
            
            emailTextField.rightView = paddingView
            emailTextField.rightViewMode = UITextFieldViewMode.always
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func localizeUI(){
        forgetPasswordButton.setTitle("forgetPassword".localized(), for: .normal)
        sendPasswordAction.setTitle("sendPassword".localized(), for: .normal)
        emailTextField.placeholder             = "email".localized()
    }
    
    @IBAction func sendPasswordAction(_ sender: UIButton) {
        getPassswordApi { (data) in
            if let dataModel = data as? BaseResponse{
                self.showDefaultAlert(title: dataModel.message, message: nil)
            }
        }
    }
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func getPassswordApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "email": emailTextField.text ?? ""
           
        ]
        
        
        Alamofire.request("https://www.instadeal.co/home/ForgetPass?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = BaseResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }

}
