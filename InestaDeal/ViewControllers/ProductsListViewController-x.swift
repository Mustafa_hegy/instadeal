//
//  CompanyViewController.swift
//  InestaDeal
//
//  Created by Waleed on 4/13/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyUserDefaults
class CompanyViewController: UIViewController {
    
    @IBOutlet weak var gmmalyButton: UIButton!
    @IBOutlet weak var gmmalyView: UIView!
    @IBOutlet weak var allCompaniesView: UIView!
    @IBOutlet weak var allcompaniesButton: UIButton!
    @IBOutlet weak var menuView : UIView!
    var modelView: companyTableModelView?
    var subCategoryId: String?
    var companyId: String?
    var flagSegue: Bool = false
    var companies: [CompanyResponse]?{
        didSet{
            companiesTableView.reloadData()
        }
    }
    var allCompanies :[CompanyResponse]?
    
    
    @IBOutlet weak var companiesTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBar(isBack: true)
        //activity
        let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activity.center = view.center
        activity.color = #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
        
  
        menuView.roundCorners(corners: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner], radius: 25.0)
        gmmalyView.isHidden = true
        gmmalyButton.setTitleColor(UIColor.darkGray, for: .normal)
        Defaults[.deliveryFees] = ""

        companiesTableView.tableFooterView = UIView()
        companiesTableView.contentInset.bottom = 30
        modelView = companyTableModelView(scene: self)
        
        if flagSegue
        {
            modelView?.getUserCompaniesApi(successClosure: { [weak self] (data) in
                
                if let dataModel = data as? CompanyResponse{
                    
                    self?.companies = dataModel.data
                    self?.allCompanies = self?.companies
                    print("companies:\(dataModel.data)")
                    
                }
            })
        }
        else{
            modelView?.getCompaniesApi(successClosure: { [weak self] (data) in
                
                if let dataModel = data as? CompanyResponse{
                    
                    self?.companies = dataModel.data
                    self?.allCompanies = self?.companies
                    activity.stopAnimating()
                    activity.isHidden = true
                    print("companies:\(dataModel.data)")
                    
                }
            })
        }
        
        // Do any additional setup after loading the view.
    }
    @IBAction func gmmalyCompanyTapped(sender: UIButton){
        if let companies = companies{
            var gmmalyCompanies = [CompanyResponse]()
            for company in companies{
                if company.gmmaly == "1"{
                    gmmalyCompanies.append(company)
                }
            }
            allCompaniesView.isHidden = true
            gmmalyView.isHidden = false
            allcompaniesButton.setTitleColor(UIColor(red: 76/255, green: 76/255, blue: 76/255, alpha: 1.0), for: .normal)
            gmmalyButton.setTitleColor(UIColor(red: 174/255, green: 43/255, blue: 0/255, alpha: 1.0), for: .normal)
            self.companies = gmmalyCompanies
        }
    }
    
    @IBAction func allCompaniesTapped(sender:UIButton){
        if let companies = self.allCompanies{
            self.companies = companies
            allCompaniesView.isHidden = false
            gmmalyView.isHidden = true
            gmmalyButton.setTitleColor(UIColor(red: 76/255, green: 76/255, blue: 76/255, alpha: 1.0), for: .normal)
            allcompaniesButton.setTitleColor(UIColor(red: 174/255, green: 43/255, blue: 0/255, alpha: 1.0), for: .normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension CompanyViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Defaults[.companyId] = companies?[indexPath.row].id ?? ""
        
        Defaults[.deliveryFees] = companies?[indexPath.row].delivery_price ?? ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? CompnyProductsViewController{
        
            destinationVC.companyId = companyId
        }
    }
}

extension CompanyViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return companies?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyTableCell") as! CompanyTableCell
        print("MainPhoto: \(companies?[indexPath.row].photo1)")
        print("Logo: \(companies?[indexPath.row].photo)")
        cell.mainPhoto.sd_setImage(with: URL(string: companies?[indexPath.row].photo1 ?? ""), placeholderImage: nil)
        cell.logo.sd_setImage(with: URL(string: companies?[indexPath.row].photo ?? ""), placeholderImage: nil)
        if indexPath.row == 0 {
            cell.topView.isHidden = true
            cell.bottomView.isHidden = true
        }else {
            cell.topView.isHidden = false
            cell.bottomView.isHidden = true
        }
        
        if  indexPath.row  == companies!.count - 1 {
            cell.bottomView.isHidden = false
        }
        
    
        
        if  indexPath.row  != (companies!.count - 1), indexPath.row != 0{
            cell.topCompanyName.text = companies?[indexPath.row - 1].name ?? ""
            cell.topDeliveryLbl.text = (companies?[indexPath.row - 1].delivery_price ?? "0") + " د.ك"
            cell.topDeliveryTimeLbl.text = companies?[indexPath.row - 1].delivery_time ?? ""
        }else {
            cell.bottomCompanyName.text = companies?[indexPath.row].name ?? ""
            cell.bottomDeliveryLbl.text = (companies?[indexPath.row].delivery_price ?? "0") + " د.ك"
            cell.bottomDeliveryTimeLbl.text = companies?[indexPath.row].delivery_time ?? ""
        }
        
        if  indexPath.row  == companies!.count - 1 , (companies!.count) > 1 {
            cell.topCompanyName.text = companies?[indexPath.row - 1].name ?? ""
            cell.topDeliveryLbl.text = (companies?[indexPath.row - 1].delivery_price ?? "0") + " د.ك"
            cell.topDeliveryTimeLbl.text = companies?[indexPath.row - 1].delivery_time ?? ""
        }

        
        if companies?[indexPath.row].gmmaly == "1"{
            cell.gmmalyView.isHidden = false
        }else{
            cell.gmmalyView.isHidden = true
        }

        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let companies = companies{
            if indexPath.row  == companies.count - 1 {
                return CGFloat(206)

            }else {
                return CGFloat(206-36)

            }
        }
        return CGFloat(206-36)
    }
    
    
}
