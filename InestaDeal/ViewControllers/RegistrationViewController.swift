//
//  RegistrationViewController.swift
//  InestaDeal
//
//  Created by Waleed on 4/14/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class RegistrationViewController: UIViewController {
    

    @IBOutlet weak var backButton: UIButton!

    //MARK:- Registration///
    
    @IBOutlet weak var bottomLinks: UIStackView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var mobileField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    //MARK:- Login///
    @IBOutlet weak var registrationForm: UIView!
    @IBOutlet weak var registerTabView: UIView!
    @IBOutlet weak var loginTabView: UIView!
    @IBOutlet weak var registerTabBtn: UIButton!
    @IBOutlet weak var enterTabBtn: UIButton!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var whoWeAreButton: UIButton!
    @IBOutlet weak var newRegisterButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        //        self.hidesBottomBarWhenPushed = true
        
        handleTabBarVisibility(hide: true)
//        self.modalPresentationStyle = .fullScreen
        
//        tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.layer.zPosition = -1
        registrationForm.isHidden = true
        // Do any additional setup after loading the view.
        
        // Login Cradintials
//        mobileTextField.text    = "98758330"
//        passwordTextField.text  = "98758330"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Defaults[.logged] {
            performSegue(withIdentifier: "goMyAccount", sender: self)
        }
    }
    
    func localizeUI(){
        registerTabBtn.setTitle("register".localized(), for: .normal)
        registerButton.setTitle("register".localized(), for: .normal)
        newRegisterButton.setTitle("register".localized(), for: .normal)
        enterTabBtn.setTitle("enter".localized(), for: .normal)
        loginButton.setTitle("login".localized(), for: .normal)
        forgetPasswordButton.setTitle("forgetPassword".localized(), for: .normal)
        termsButton.setTitle("Terms & Conditions".localized(), for: .normal)
        whoWeAreButton.setTitle("whoWeAre".localized(), for: .normal)
        
        mobileTextField.placeholder        = "mobile".localized()
        mobileField.placeholder            = "mobile".localized()
        passwordTextField.placeholder      = "password".localized()
        passwordField.placeholder          = "password".localized()
        emailField.placeholder             = "email".localized()
        nameField.placeholder              = "name".localized()
        
        if Defaults[.langId] == "1" {
            backButton.setImage( #imageLiteral(resourceName: "blackArrow") , for: .normal)
            mobileTextField.textAlignment       = .left
            mobileField.textAlignment           = .left
            passwordTextField.textAlignment     = .left
            passwordField.textAlignment         = .left
            emailField.textAlignment            = .left
            nameField.textAlignment             = .left
        } else if Defaults[.langId] == "2" {
            backButton.setImage( #imageLiteral(resourceName: "blackArrowAr") , for: .normal)
            mobileTextField.textAlignment       = .right
            mobileField.textAlignment           = .right
            passwordTextField.textAlignment     = .right
            passwordField.textAlignment         = .right
            emailField.textAlignment            = .right
            nameField.textAlignment             = .right
        }
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func tabBtnAction(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            loginTabView.isHidden = false
            registerTabView.isHidden = true
            enterTabBtn.setTitleColor(appThemeColor, for: .normal)
            registerTabBtn.setTitleColor(.white, for: .normal)
            bottomLinks.isHidden = false
            registrationForm.isHidden = true
        default:
            loginTabView.isHidden = true
            registerTabView.isHidden = false
            bottomLinks.isHidden = true
            registrationForm.isHidden = false
            registerTabBtn.setTitleColor(appThemeColor, for: .normal)
            enterTabBtn.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        //        if navigationController != nil{
        //        self.navigationController?.popViewController()
        //        }else{
        //            self.navigationController?.popToRootViewController(animated: true)
        //        }
        if !Defaults[.logged]{
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func endEditingGest(_ sender: UITapGestureRecognizer) {
        
        view.endEditing(true)
    }
    @IBAction func loginBtnAction(_ sender: UIButton) {
        
        loginApi { (model) in
            if let dataModel = model as? RegistrationResponse{
                
                
                self.showDefaultAlert(title: dataModel.message ?? "", message: nil, actionBlock: {
                    if dataModel.result == "true"{
                        Defaults[.userId] = dataModel.id ?? ""
                        Defaults[.logged] = true
                        Defaults[.userType] = dataModel.utype ?? ""
                        if dataModel.status == 0{
                            self.showDefaultAlert(title: "incorrectPhoneOrPass".localized(), message: nil)
                        }
                    }
                    if dataModel.status == 1{
                        self.performSegue(withIdentifier: "goHome", sender: sender)
                    }
                    if dataModel.status == 2{
                        self.showDefaultAlert(title: "deactivatedMembership".localized(), message: nil)
                    }
                    if dataModel.status == 3{
                        Defaults[.userId] = dataModel.id ?? ""
                        self.performSegue(withIdentifier: "goFromRegsterToActivation", sender: sender)
                    }   
                    
                    
                })
                
                
                
            }
            
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.hidesBottomBarWhenPushed = true
    }
    @IBAction func registerAction(_ sender: UIButton) {
        if nameField.isEmpty || emailField.isEmpty || mobileField.isEmpty || passwordField.isEmpty{
            self.showDefaultAlert(title: "allFieldsRequired".localized(), message: nil)
        }
        else{
            registerApi { (model) in
                if let dataModel = model as? RegistrationResponse{
                    
                    if dataModel.result == "true"{
                        Defaults[.userId] = dataModel.id ?? ""
                        
                        Defaults[.userType] = dataModel.utype ?? ""
//                        self.showDefaultAlert(title: "شكرا لانضمامك الينا", message: nil, actionBlock: {
//                            self.performSegue(withIdentifier: "goHome", sender: self)
//                        })
                        self.performSegue(withIdentifier: "goFromRegsterToActivation", sender: self)
                        
                    }
                    else{
                        self.showDefaultAlert(title: dataModel.message, message: nil)
                    }
                    
                }
                
            }
        }
        
    }
    
    func registerApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "name": nameField.text ?? "",
            "email": emailField.text ?? "",
            "mobile": mobileField.text ?? "",
            "password": passwordField.text ?? ""
        ]
        
        
        Alamofire.request("https://www.instadeal.co/home/addAccount?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = RegistrationResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
                
        }
        
    }
    
    func loginApi(successClosure: @escaping SuccessClosure){
        let params: Parameters = [
            "mobile": mobileTextField.text ?? "",
            "password": passwordTextField.text ?? ""
        ]
        Alamofire.request("https://www.instadeal.co/home/loginUser?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                if let json = response.result.value {
                    if let data = json as? [String : Any]{
                        let dataModel = RegistrationResponse(JSON: data)
                        successClosure(dataModel!)
                    }
                }
        }
        
    }
    
    @IBAction func conditionAction(_ sender: UIButton) {
        Defaults[.dismissed] = true
        let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
        viewController.url = "https://www.instadeal.co/conditions.html?lang=\(Defaults[.langId])"
        self.present(viewController, animated: true, completion: nil)
    }
    @IBAction func aboutUsaction(_ sender: UIButton) {
        Defaults[.dismissed] = true
        let viewController:LinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkViewController") as! LinkViewController
        viewController.url = "https://www.instadeal.co/about.html?lang=\(Defaults[.langId])"
        self.present(viewController, animated: true, completion: nil)
    }
    
    
}
