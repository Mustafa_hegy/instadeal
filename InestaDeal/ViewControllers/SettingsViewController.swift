//
//  SettingsViewController.swift
//  InestaDeal
//
//  Created by Waleed on 5/14/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyUserDefaults

class SettingsViewController: UIViewController, UITableViewDelegate , UITableViewDataSource , UIGestureRecognizerDelegate , UITextFieldDelegate {

    @IBOutlet weak var payTable: UITableView!
    @IBOutlet weak var comments: UITextField!
    @IBOutlet weak var address: UITextField!
    
    var gammaly = "0"
    
    var collectType = 0
    enum CashTypeValue: Int {
        case cash = 1
        case cashAndKNet = 3
    }
    
    var cashSendData: CashTypeValue?
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view?.isDescendant(of: payTable))!{
            
            return false
        }
        return true
    }
    
    @IBAction func gestureAction(_ sender: UITapGestureRecognizer) {
        containgView.isHidden = true
        view.endEditing(true)
    }
    var popUpIsCollect = false
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !popUpIsCollect{
            return 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ShowAddressTableViewCell = tableView.dequeueReusableCell(withIdentifier: "payCellPopUp", for: indexPath) as! ShowAddressTableViewCell
        if !popUpIsCollect{
            if indexPath.row == 0{
                cell.addressName.text = "cash".localized()
            }
            if indexPath.row == 2{
                cell.addressName.text = "knet".localized()
            }
            if indexPath.row == 1{
                cell.addressName.text = "cash&knet".localized()
            }
        }
        else{
            if indexPath.row == 0{
                cell.addressName.text = "weekly".localized()
            }
            if indexPath.row == 1{
                cell.addressName.text = "twoWeek".localized()
            }
            if indexPath.row == 2{
                cell.addressName.text = "monthly".localized()
            }
        }
        return cell
    }
    
    @IBOutlet weak var containgView: UIView!

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        containgView.isHidden = true
        if !popUpIsCollect{
            if indexPath.row == 0{
                catsBtn.titleLabel?.text = "cash".localized()
                catsBtn.setTitle("cash".localized(), for: .normal)
                
                cashSendData = CashTypeValue.cash
            }
            if indexPath.row == 2{
                catsBtn.titleLabel?.text = "knet".localized()
            }
            if indexPath.row == 1{
                catsBtn.titleLabel?.text = "cash&knet".localized()
                catsBtn.setTitle("cash&knet".localized(), for: .normal)
                cashSendData = CashTypeValue.cashAndKNet
            }
        }
        else{
            if indexPath.row == 0{
                collectBtn.setTitle("weekly".localized(), for: .normal)
            }
            if indexPath.row == 1{
                
                collectBtn.setTitle("twoWeek".localized(), for: .normal)
            }
            if indexPath.row == 2{
                
                collectBtn.setTitle("monthly".localized(), for: .normal)
                
            }
            collectType = indexPath.row + 1
        }
        
    }
    
    @IBOutlet weak var companyName: UITextField!
    
    @IBOutlet weak var gammalyButton: UIButton!
    
    @IBOutlet weak var collectBtn: UIButton!
    @IBOutlet weak var bankName: UITextField!
    @IBOutlet weak var iban: UITextField!
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var tagerName: UITextField!
    @IBOutlet weak var categories: UIButton!
    
    @IBOutlet weak var linkField: UITextField!
    @IBOutlet weak var details: UITextField!
    @IBOutlet weak var deliveryPrice: UITextField!
    @IBOutlet weak var deliveryTime: UITextField!
    @IBOutlet weak var deliveryTimeFrom: UITextField!
    @IBOutlet weak var deliveryTimeTo: UITextField!
    
    @IBOutlet weak var toLbl: UITextField!
    @IBOutlet weak var fromLbl: UITextField!
    @IBOutlet weak var catsBtn: UIButton!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var headLbl: UILabel!
    @IBOutlet weak var sendBtnAction: UIButton!
    @IBOutlet weak var gmma3lySubscribe: UILabel!
    
    @IBOutlet weak var popTableHeight: NSLayoutConstraint!
    var companyId = ""
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Defaults[.selectedCatstring] != ""{
           
            categories.setTitle(Defaults[.selectedCatstring], for: .normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
        Defaults[.search] = false
        Defaults[.selectedCatstring] = ""
        Defaults[.selectedCatIds] = ""
        Defaults[.flagAddProd] = false
        headLbl.text = ""
        deliveryPrice.delegate  = self
        addNavBar(isBack: true)
        getCompanyDetailsApi { [weak self] (data) in
                
                if let dataModel = data as? CompanyDetailsResponse{
                    self?.companyId = dataModel.id ?? ""
                    self?.companyName.text = dataModel.name
                    self?.mobileNumber.text = dataModel.mobile
                    self?.catsBtn.setTitle(dataModel.pay_name, for: .normal)
                    self?.fromLbl.text = dataModel.workFrom
                    self?.toLbl.text = dataModel.workTo
//                    self?.deliveryTime.text = dataModel.delivery_time
                    self?.deliveryTimeFrom.text = dataModel.delivery_time_from
                    self?.deliveryTimeTo.text = dataModel.delivery_time_to
//                    self?.deliveryPrice.text = dataModel.delivery_price
                    self?.linkField.text = dataModel.instgram
                    self?.details.text = dataModel.feedData
                    self?.fullName.text = dataModel.account_name
                    self?.iban.text = dataModel.iban
                    self?.bankName.text = dataModel.bank
                    var tempCollect = ""
                    if dataModel.collect == "1"{
                        tempCollect = "weekly".localized()
                    }
                    if dataModel.collect == "2"{
                        tempCollect = "twoWeek".localized()
                    }
                    if dataModel.collect == "3"{
                        tempCollect = "monthly".localized()
                    }
                    self?.collectBtn.setTitle(tempCollect, for: .normal)
                    self?.categories.setTitle(dataModel.catsName, for: .normal)
                    
                    self?.gammaly = dataModel.gamm3!
                    if self?.gammaly == "0"{
                        self?.gammalyButton.setBackgroundImage(#imageLiteral(resourceName: "emptyCircle"), for: .normal)
                    } else {
                        self?.gammalyButton.setBackgroundImage(#imageLiteral(resourceName: "mark"), for: .normal)
                    }
                    
                }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        if popUpIsCollect{
            popTableHeight.constant = 135
        }
        else{
            popTableHeight.constant = 90
        }
    }
    override func viewDidLayoutSubviews() {
        if popUpIsCollect{
            popTableHeight.constant = 135
        }
        else{
            popTableHeight.constant = 90
        }
    }

    func localizeUI(){
        sendBtnAction.setTitle("update".localized(), for: .normal)
        catsBtn.setTitle("payType".localized(), for: .normal)
        collectBtn.setTitle("collectMoney".localized(), for: .normal)
        categories.setTitle("chooseCategory".localized(), for: .normal)
        companyName.placeholder     = "companyName".localized()
        mobileNumber.placeholder    = "mobileNumber".localized()
        fullName.placeholder        = "fullName".localized()
        iban.placeholder            = "iban".localized()
        bankName.placeholder        = "bankName".localized()
        fromLbl.placeholder         = "workHoursStart".localized()
        toLbl.placeholder           = "workHoursEnd".localized()
        deliveryTime.placeholder    = "deliveryPeriod".localized()
        deliveryTimeFrom.placeholder = "deliveryPeriodFrom".localized()
        deliveryTimeTo.placeholder  = "deliveryPeriodTo".localized()
        deliveryPrice.placeholder   = "deliveryCost".localized()
        linkField.placeholder       = "instagram".localized()
        details.placeholder         = "notes".localized()
        gmma3lySubscribe.text       = "gmma3lySubscribe".localized()
        deliveryPrice.text          = "deliveryCost".localized()
        
        if Defaults[.langId] == "1" {
            catsBtn.contentHorizontalAlignment      = .left
            collectBtn.contentHorizontalAlignment   = .left
            categories.contentHorizontalAlignment   = .left
            companyName.textAlignment               = .left
            mobileNumber.textAlignment              = .left
            fullName.textAlignment                  = .left
            iban.textAlignment                      = .left
            bankName.textAlignment                  = .left
            fromLbl.textAlignment                   = .left
            toLbl.textAlignment                     = .left
            deliveryTime.textAlignment              = .left
            deliveryTimeFrom.textAlignment          = .left
            deliveryTimeTo.textAlignment            = .left
            deliveryPrice.textAlignment             = .left
            linkField.textAlignment                 = .left
            details.textAlignment                   = .left
        } else if Defaults[.langId] == "2" {
            catsBtn.contentHorizontalAlignment      = .right
            collectBtn.contentHorizontalAlignment   = .right
            categories.contentHorizontalAlignment   = .right
            companyName.textAlignment               = .right
            mobileNumber.textAlignment              = .right
            fullName.textAlignment                  = .right
            iban.textAlignment                      = .right
            bankName.textAlignment                  = .right
            fromLbl.textAlignment                   = .right
            toLbl.textAlignment                     = .right
            deliveryTime.textAlignment              = .right
            deliveryTimeFrom.textAlignment          = .right
            deliveryTimeTo.textAlignment            = .right
            deliveryPrice.textAlignment             = .right
            linkField.textAlignment                 = .right
            details.textAlignment                   = .right
        }
    }
    @IBAction func selectGammalyButtonTapped(_ sender: Any) {
        if gammaly == "0"{
            gammalyButton.setBackgroundImage(#imageLiteral(resourceName: "mark"), for: .normal)
            gammaly = "1"
        } else {
            gammalyButton.setBackgroundImage(#imageLiteral(resourceName: "emptyCircle"), for: .normal)
            gammaly = "0"
        }
    }
    
    @IBAction func collectMoney(_ sender: UIButton) {
        popUpIsCollect = true
        payTable.reloadData {
            if self.popUpIsCollect{
                self.popTableHeight.constant = 135
            }
            else{
                self.popTableHeight.constant = 90
            }
            self.containgView.isHidden = false
            
        }
        
    }
    @IBAction func categoriesActions(_ sender: UIButton) {
    }
    @IBAction func catsBtnAction(_ sender: UIButton) {
        popUpIsCollect = false
        payTable.reloadData {
            if self.popUpIsCollect{
                self.popTableHeight.constant = 135
            }
            else{
                self.popTableHeight.constant = 90
            }
            self.containgView.isHidden = false
        }
        
    }
    @IBAction func sendBtnAction(_ sender: UIButton) {
        updateSettingsApi { (data) in
//            self.navigationController?.popViewController()
            self.showDefaultAlert(title: "adminReviewUpdateAlert".localized(), message: nil, actionBlock: {
                    self.navigationController?.popViewController()
            })
        }
        
    }

    func updateSettingsApi(successClosure: @escaping SuccessClosure){
        
        let params: Parameters = [
            "name": companyName.text ?? "" ,
            "mobile": mobileNumber.text ?? "" ,
            "instagram": linkField.text ?? "" ,
            "notes": details.text ?? "",
            "work_from": fromLbl.text ?? "",
            "work_to": toLbl.text ?? "",
//            "delivery_time": deliveryTime.text ?? "",
            "delivery_time_from" : deliveryTimeFrom.text ?? "",
            "delivery_time_to" : deliveryTimeTo.text ?? "",
//            "delivery_price": deliveryPrice.text ?? "",  ===>>>>>>>>>>>>Changed
            "account_name":   fullName.text ?? "",
            "iban": iban.text ?? "",
            "bank": bankName.text ?? "",
            "payment": cashSendData?.rawValue ?? 1,
            "collect": collectType,
            "cats": "\(Defaults[.selectedCatIds])",
            "gamma3": gammaly
 
        ]
        
        
        Alamofire.request("http://instadeal.co/home/editCompany/\(companyId)?lang=\(Defaults[.langId])", method: .post, parameters: params,  encoding: URLEncoding.default , headers: ["Content-Type": "application/x-www-form-urlencoded"])
            .responseJSON { response in
                if let json = response.result.value {
                        successClosure(json)
                }
                
        }
        
    }
    
    func getCompanyDetailsApi(successClosure: @escaping SuccessClosure){
        let params: Parameters = [
            "area": "\(Defaults[.areaId])"
        ]
        
//        Alamofire.request("https://www.instadeal.co/home/getCompany/\(Defaults[.tagerCompanyId] )/0", method: .get, parameters: params).responseJSON
        Alamofire.request("https://www.instadeal.co/home/getCompany/\(Defaults[.tagerCompanyId] )?lang=\(Defaults[.langId])", method: .get, parameters: params).responseJSON { response in
            
            
            if let json = response.result.value {
                if let data = json as? [String : Any]{
                    let dataModel = CompanyDetailsResponse(JSON: data)
                    //                    print("companyId:\(self.scene.companyId)")
                    successClosure(dataModel)
                }
            }
        }
        
    }
    
    // MARK: - UITextFieldDelegate
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == deliveryPrice {
            // Move to Areas Shipping Cost view
            let viewController: AreasShippingCostViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AreasShippingCostViewController") as! AreasShippingCostViewController
            
            viewController.companyId    = Defaults[.tagerCompanyId]
            self.navigationController?.show(viewController, sender: self)
            
            return false
        }
        return true
    }
    
}
