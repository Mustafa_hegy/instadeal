//
//  ProductsListViewController.swift
//  InestaDeal
//
//  Created by Muhammad Mrzok on 8/2/19.
//  Copyright © 2019 Waleed. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyUserDefaults

class ProductsListViewController: UIViewController ,UIPopoverPresentationControllerDelegate, UIScrollViewDelegate {

    @IBOutlet weak var menuView             : UIView!
    @IBOutlet weak var gmmalyButton         : UIButton!
    @IBOutlet weak var gmmalyView           : UIView!
    @IBOutlet weak var gmmalySelectionIcon: UIImageView!
    @IBOutlet weak var allProductsView      : UIView!
    @IBOutlet weak var allProductsButton    : UIButton!
    @IBOutlet weak var companiesButton      : UIButton!
    @IBOutlet weak var filterButton         : UIButton!
    @IBOutlet weak var productsCountLabel   : UILabel!
    @IBOutlet weak var collectionShow       : UICollectionView!
    @IBOutlet weak var filtersUpperView: UIView!
    @IBOutlet weak var filtersUpperViewAutoLayoutConstraintHeight: NSLayoutConstraint!
    
    @IBOutlet weak var catsCollectionView: UICollectionView!
    private var lastContentOffset: CGFloat = 0
    
    var modelView       : ProductsListModelView?
    var productModelView: ProductModelView? = ProductModelView()
    var subCategoryId   : String?
    var companyId       : String?
    var filterParams    = ""
    let activity        = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var limit           = 10
    var start           = 0
    var isFilterList    : Bool  = false
    var enableLoadMore  : Bool  = false
    var gmma3lyProducts : Bool  = false
    var isAllCatProducts  : Bool  = false
//    var productObject:ProductDetailsResponse?
    
    var products: [CatsProducts]?{
//    var products: [ProductDetailsResponse]?{
        didSet{
            collectionShow.reloadData()
        }
    }
    
    var gmmalyProducts : [CatsProducts]?{
//    var gmmalyProducts : [ProductDetailsResponse]?{
        didSet{
            collectionShow.reloadData()
        }
    }
    
    var allProducts :[CatsProducts]?
//    var allProducts :[ProductDetailsResponse]?
    
    var parentCategoryId: String?
    var categories:CategoriesResponse?{
        didSet{
            catsCollectionView.reloadData()
            
            if Defaults[.langId] == "1" {
                filtersUpperView.semanticContentAttribute       = .forceLeftToRight
                catsCollectionView.semanticContentAttribute     = .forceLeftToRight
                catsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
            } else if Defaults[.langId] == "2" {
                filtersUpperView.semanticContentAttribute       = .forceRightToLeft
                catsCollectionView.semanticContentAttribute     = .forceRightToLeft
                catsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeUI()
//        addNavBar(isBack: true)
        parentCategoryId = Defaults[.parentCategoryId]
        //activity
        startLoading()
        
        menuView.roundCorners(corners: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner], radius: 25.0)
        menuView.setShadow()
        gmmalyView.isHidden = true
        gmmalyButton.setTitleColor(UIColor.darkGray, for: .normal)
        Defaults[.deliveryFees] = ""
        
        modelView = ProductsListModelView(scene: self)
        
        // Do any additional setup after loading the view.
        
//        startLoadDataFromApi()
        
        // Load Cats data
        modelView?.getSubCategoriesApi(successClosure: { [unowned self] (data) in
            
            if let dataModel = data as? CategoriesResponse{
                
                self.categories = dataModel
                
                print(dataModel.data![0])
            }
            
            self.stopLoading()
        })
        isAllCatProducts = true
        startLoadParentCategoryProductsDataFromApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addNavBarProductList(isBack: true)
    }
    
    func localizeUI(){
//        gmmalyButton.setTitle("gmma3ly".localized(), for: .normal)
//        allProductsButton.setTitle("all".localized(), for: .normal)
        companiesButton.setTitle("brwoseCompanies".localized(), for: .normal)
        filterButton.setTitle("filter".localized(), for: .normal)
//        productsCountLabel.text = "productsCount".localized()
        
        if Defaults[.langId] == "1" {
            collectionShow.semanticContentAttribute = .forceLeftToRight
        } else if Defaults[.langId] == "2" {
            collectionShow.semanticContentAttribute = .forceRightToLeft
        }
    }

    func startLoadDataFromApi(){
        // Add Activity Indicator
        startLoading()
        
        if gmma3lyProducts {
            if isAllCatProducts {
                // Load gmma3lyProducts Data From API for Parent SubCategory
                modelView?.getGamm3ParentCatProductsApi( successClosure: { [weak self] (data) in
                    // Remove Activity Indicator
                    self?.stopLoading()
                    
                    if let dataModel = data as? CompanyCatsResponse {
                        
                        if (dataModel.data?.count)! > 0 {
                            self?.gmmalyProducts = dataModel.data
                            self?.products = self?.gmmalyProducts
                        }
                    }
                })
            }
            else {
                // Load gmma3lyProducts Data From API for Selected SubCategory
                modelView?.getGamm3SubCatsProductsApi( successClosure: { [weak self] (data) in
                    // Remove Activity Indicator
                    self?.stopLoading()
                    
                    if let dataModel = data as? CompanyCatsResponse {
                        
                        if (dataModel.data?.count)! > 0 {
                            self?.gmmalyProducts = dataModel.data
                            self?.products = self?.gmmalyProducts
                        }
                    }
                })
            }
        }
        else if !isAllCatProducts {
            // Load Sub Category Products Data From API
            modelView?.getCatsProductsApi(limit:limit,start:start,filterParameters: filterParams,successClosure: { [weak self] (data) in
                // Remove Activity Indicator
                self?.stopLoading()
                
                if let dataModel = data as? CompanyCatsResponse {
                    
                    if (dataModel.data?.count)! > 0 {
                        self?.allProducts == nil ? self?.allProducts = dataModel.data : self?.allProducts?.append(contentsOf: dataModel.data!)
                        
                        self?.products = self?.allProducts
                        
                        if (dataModel.data?.count)! >= self!.limit {
                            self?.start             = self!.start + self!.limit
                            self?.enableLoadMore    = true
                        } else {
                            self?.enableLoadMore    = false
                        }
//                        self?.productsCountLabel.text = "\(self?.products?.count ?? 0) \("product".localized())"
                    }
                }
                
            })
        }
    }
    
    func startLoadParentCategoryProductsDataFromApi() {
        // Add Activity Indicator
        startLoading()
        
        // Load All Category Products Data From API
        modelView?.getParentCatProductsApi(limit:limit,start:start,filterParameters: filterParams,successClosure: { [weak self] (data) in
            // Remove Activity Indicator
            self?.stopLoading()
            
            if let dataModel = data as? CompanyCatsResponse {
                
                if (dataModel.data?.count)! > 0 {
                    self?.allProducts == nil ? self?.allProducts = dataModel.data : self?.allProducts?.append(contentsOf: dataModel.data!)
                    
                    self?.products = self?.allProducts
                    
                    if (dataModel.data?.count)! >= self!.limit {
                        self?.start             = self!.start + self!.limit
                        self?.enableLoadMore    = true
                    } else {
                        self?.enableLoadMore    = false
                    }
                    
//                    self?.productsCountLabel.text = "\(self?.products?.count ?? 0) \("product".localized())"
                }
            }
            
        })
    }
    
    private func startLoading() {
        activity.center = view.center
        activity.color =  #colorLiteral(red: 0.9405409098, green: 0.5390258431, blue: 0.2667022943, alpha: 1)
        activity.startAnimating()
        view.addSubview(activity)
    }
    
    private func stopLoading() {
        self.activity.stopAnimating()
        self.activity.isHidden = true
    }
    
    @IBAction func gmmalyCompanyTapped(sender: UIButton){
        gmma3lyProducts = true
        products?.removeAll()
//        if self.gmmalyProducts != nil {
//            self.products                   = self.gmmalyProducts
//            self.productsCountLabel.text    = "\(self.products?.count ?? 0) \("product".localized())"
//        } else {
                // Load gmma3lyProducts Data From API
                startLoadDataFromApi()
//        }
        gmmalySelectionIcon.image = #imageLiteral(resourceName: "filledCircle")
//        allProductsView.isHidden        = true
//        gmmalyView.isHidden             = false
//        allProductsButton.setTitleColor(UIColor(red: 76/255, green: 76/255, blue: 76/255, alpha: 1.0), for: .normal)
//        gmmalyButton.setTitleColor(UIColor(red: 174/255, green: 43/255, blue: 0/255, alpha: 1.0), for: .normal)
        
        /*
         // This code is imp;emented if the allproducts list have both gmma3ly and not gmma3ly products so extract gmma3ly from the list and show it
        if let productsList = allProducts{
//            var gmmalyProducts = [CatsProducts]()
            for product in productsList{
                if product.gmmaly == "1"{
                    gmmalyProducts.append(product)
                }
            }
            
            self.products                   = gmmalyProducts
            self.productsCountLabel.text    = "\(self.products?.count ?? 0) \("product".localized())"
            
            allProductsView.isHidden        = true
            gmmalyView.isHidden             = false
            allProductsButton.setTitleColor(UIColor(red: 76/255, green: 76/255, blue: 76/255, alpha: 1.0), for: .normal)
            gmmalyButton.setTitleColor(UIColor(red: 174/255, green: 43/255, blue: 0/255, alpha: 1.0), for: .normal)
        }
         */
    }
    
    @IBAction func allProductsTapped(sender:UIButton){
        gmma3lyProducts = false
        products?.removeAll()
        if let productsList = self.allProducts{
            self.products                   = productsList
            self.productsCountLabel.text    = "\(self.products?.count ?? 0) \("product".localized())"
            
            allProductsView.isHidden        = false
            gmmalyView.isHidden             = true
            gmmalyButton.setTitleColor(UIColor(red: 76/255, green: 76/255, blue: 76/255, alpha: 1.0), for: .normal)
            allProductsButton.setTitleColor(UIColor(red: 174/255, green: 43/255, blue: 0/255, alpha: 1.0), for: .normal)
        }
    }
    
    @IBAction func browseCompaniesTapped(_ sender: Any) {
        if Defaults[.subCategoryId] == "" {
            self.showDefaultAlert(title: "categoryRequired".localized(), message: nil)
        } else {
            self.performSegue(withIdentifier: "companySeg", sender: self)
        }
    }
    
    @IBAction func filterTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "filtersSegue", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     // Scroll delegate functions to hide filtersupperview
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        lastContentOffset = collectionShow.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /*
        // Hide View when scrolling
        if lastContentOffset > scrollView.contentOffset.y {
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                self?.filtersUpperView.alpha = 1.0
                self?.filtersUpperView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self?.filtersUpperViewAutoLayoutConstraintHeight.constant   = 45
                }, completion: nil)
        } else if lastContentOffset < scrollView.contentOffset.y {
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                self?.filtersUpperView.alpha = 0
                self?.filtersUpperView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self?.filtersUpperViewAutoLayoutConstraintHeight.constant   = 0
                }, completion: nil)
        }
 */
    }
    
    @IBAction func unwindToProductListView(_ unwindSegue: UIStoryboardSegue) {
        if unwindSegue.identifier == "unwindFilterSegue" {
            let sourceViewController = unwindSegue.source as! FiltersViewController
            // Use data from the view controller which initiated the unwind segue
            
            // Set the current view is filter result view & Clear all previous data
            products?.removeAll()
            gmmalyProducts?.removeAll()
            allProducts?.removeAll()
            
            start           = 0
            isFilterList    = true
            enableLoadMore  = false
            gmma3lyProducts = false
            
            // ga=<gamma3>&user=<user_id>&comps=<companies>&price_from=<start_price>&price_to=<end_price>
            filterParams = "ga=\(sourceViewController.selectedDeliveryType)&user=\(Defaults[.userId])&comps=\(Defaults[.selectedCompsIds])&price_from=\(sourceViewController.from.text ?? "")&price_to=\(sourceViewController.to.text ?? "")"
            
            if isAllCatProducts {
                startLoadParentCategoryProductsDataFromApi()
            }
            else {
                startLoadDataFromApi()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.hidesBottomBarWhenPushed = false
        if segue.identifier == "companySeg"{
            let destinationVC = segue.destination as! CompanyViewController
            destinationVC.subCategoryId = subCategoryId
        }
        else if segue.identifier == "filtersSegue"{
            let destinationVC = segue.destination as! UINavigationController
            destinationVC.modalPresentationStyle    = .popover
//            destinationVC.modalTransitionStyle      = .crossDissolve
            destinationVC.preferredContentSize      = CGSize(width: screenWidth, height: screenHeight )
            destinationVC.popoverPresentationController?.delegate = self
            destinationVC.popoverPresentationController?.sourceRect = filterButton.frame // CGRect(x:  0 , y: screenHeight/3, width: 0, height: 0)
            destinationVC.popoverPresentationController?.sourceView = filterButton // self.view
            destinationVC.popoverPresentationController?.permittedArrowDirections = .up
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension ProductsListViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.catsCollectionView {
            return CGSize(width: 120, height: 35)
        }
        else {
            return CGSize(width: (collectionView.frame.width - 4) / 2, height: 274)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}

extension ProductsListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.catsCollectionView{
            return (categories?.data!.count ?? 0 ) + 1
        }
        return products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.catsCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as? CategoryCell
            
            if indexPath.row == 0{
                cell?.imageView.isHidden    = true
                cell?.subCategoryLbl.text   = "all".localized()
                if Defaults[.subCategoryId] == "" {
                    cell?.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.568627451, blue: 0.1921568627, alpha: 1)
                    cell?.subCategoryLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    if Defaults[.langId] == "1" {
                        catsCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
                    } else if Defaults[.langId] == "2" {
                        catsCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .right)
                    }
                }
            }
            else{
                cell?.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9294117647, blue: 0.9647058824, alpha: 1)
                cell?.subCategoryLbl.textColor =  #colorLiteral(red: 0.1921568627, green: 0.1921568627, blue: 0.1921568627, alpha: 1)
                cell?.imageView.isHidden    = false
                
                let rowData  = categories?.data![indexPath.row - 1]
                cell?.contentView.transform = CGAffineTransform(scaleX: -1, y: -1)
                
                if rowData?.file == "" { // rowData.photo!.isEmptyrowData.photo!.isEmpty
                    cell?.imageView.image = #imageLiteral(resourceName: "instaLogo")
                } else {
                    cell?.imageView.sd_setImage(with: URL(string: (rowData?.file) ?? ""), placeholderImage: #imageLiteral(resourceName: "instaLogo"))
                }
                cell?.subCategoryLbl.text = rowData?.name
            }
            
            cell?.contentView.transform = CGAffineTransform(scaleX: 1, y: 1)
            return cell!
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as? CatsProductCell

            let rowData  = products![indexPath.row]
            cell?.productHead.text  = rowData.name
            cell?.delivery.text     = "\(rowData.newPrice ?? 0) \("currency".localized())"

            if rowData.photo == "" { // rowData.photo!.isEmptyrowData.photo!.isEmpty
                cell?.productImage.image = #imageLiteral(resourceName: "instaLogo")
            } else {
                cell?.productImage.sd_setImage(with: URL(string: (rowData.photo) ?? ""), placeholderImage: #imageLiteral(resourceName: "instaLogo"))
            }

            cell?.addCartBtn.tag    = indexPath.row

            print("price: \(rowData.newPrice)")

            if let fav = rowData.favChecked {
                if fav == 1 {
                    cell?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                } else {
                    cell?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                }
            }
            
            if let discount = Int(rowData.discount ?? "0") {
                if discount > 0 {
                    cell?.discountPercentage.isHidden   = false
                    cell?.discountPercentage.text       = "\("discountKey".localized()) \(rowData.discount ?? "0")%"
                } else {
                    cell?.discountPercentage.isHidden = true
                }
            }
            
            if let gmma3 = Int(rowData.gmmaly ?? "0") {
                if gmma3 == 1 {
                    cell?.gmma3lyIcon.isHidden = false
                } else {
                    cell?.gmma3lyIcon.isHidden = true
                }
            }
            
            if let freeShipping = rowData.freeDelivery {
                if freeShipping == 1 {
                    cell?.shippingCostLabel.isHidden = false
                } else {
                    cell?.shippingCostLabel.isHidden = true
                }
            }

            cell?.productFavAction = {
                if Defaults[.logged]{
                    // Load Data From API
                    Defaults[.prodId] = rowData.id ?? ""

                    self.startLoading()
                    self.productModelView?.addToFavApi { (data) in

                        self.stopLoading()

                        if let dataModel = data as? BaseResponse{
                            if dataModel.result == "true"
                            {
                                if dataModel.message == "تمت الاضافة للمفضلة"
                                {
                                    cell?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "filled_heart"), for: .normal)
                                } else {
                                    cell?.favProductButton.setBackgroundImage(#imageLiteral(resourceName: "emptyHart"), for: .normal)
                                }
                            }
                            self.view.layoutSubviews()
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                else{
                    self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
                }
            }

            cell?.productAddCartAction = {
                Defaults[.prodId] = rowData.id ?? ""
                let requiredQuantity: Int   = 1
                let existCountity: Int      = Int(rowData.counts ?? "0") ?? 0
                if requiredQuantity <= existCountity && requiredQuantity != 0 {
                    if !Defaults[.logged]{
                        self.performSegue(withIdentifier: "goLoginFromProdCell", sender: self)
                    }
                    else{
                        // Edit Code => Add to cart first and then move to Addresses List

                        self.startLoading()
                        self.productModelView?.addToCartApi(price: rowData.price ?? "0", count: requiredQuantity /* => required quantity */ ) { (data) in

                            self.stopLoading()

                            if let dataModel = data as? BaseResponse{

                                if dataModel.message == "تم الاضافة للسلة" {
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let myAddressVC = storyBoard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressViewController
                                    myAddressVC.catsProductObject = rowData
                                    myAddressVC.numberOfProducts = requiredQuantity
                                    Defaults[.numberOfProducts] = "\(requiredQuantity)"
                                    self.navigationController?.pushViewController(myAddressVC, animated: true)
                                    //performSegue(withIdentifier: "directBuy", sender: self)
                                }
                            }
                        }
                    }
                }
                else{
                    self.showDefaultAlert(title: "noEnoughQuantity".localized(), message: nil)
                }
            }

            if let newPrice = rowData.newPrice{
                if let oldPrice = rowData.price{
                    if Double(newPrice) == Double(oldPrice){
                        cell?.oldPrice.isHidden = true
                    } else {
                        cell?.oldPrice.isHidden = false
                        cell?.oldPrice.text = "\(oldPrice) \("currency".localized())"
                    }
                }
            }
            return cell!
        }
    }
}

extension ProductsListViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == catsCollectionView{
            
            // be sure to uncheck gmma3ly button and products
            gmma3lyProducts             = false
            gmmalySelectionIcon.image   = #imageLiteral(resourceName: "emptyCircle")
            
            //  be sure to Clear Filters
            filterParams                = ""
            
            let cell = catsCollectionView.cellForItem(at: indexPath) as? CategoryCell
//            for tempCell in collectionView.visibleCells as! [CategoryCell] {
                // do something
                cell?.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.568627451, blue: 0.1921568627, alpha: 1)
                cell?.subCategoryLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//            }
//            cell?.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9294117647, blue: 0.9647058824, alpha: 1)
            
            if indexPath.row == 0{
                if !isAllCatProducts {
                    // clear previous list
                    allProducts?.removeAll()
                    products?.removeAll()
                    start   = 0
                    
                    // Set that the list is parent category products
                    isAllCatProducts = true
                    // Call praducts of parent category
                    startLoadParentCategoryProductsDataFromApi()
                }
                
            }
            else{
                let rowData  = categories?.data![indexPath.row - 1]
//                if catId != Defaults[.subCategoryId] {
                    // clear previous list
                    allProducts?.removeAll()
                    products?.removeAll()
                    start   = 0
//                }
                
                isAllCatProducts = false
                Defaults[.subCategoryId]    = rowData?.id ?? ""
                Defaults[.subCategoryName]  = rowData?.name ?? ""
                startLoadDataFromApi()
            }
        }
        else if collectionView == collectionShow{
            Defaults[.prodId] = ""
            let rowData  = products![indexPath.row]
            Defaults[.prodId] = rowData.id ?? ""
            Defaults[.deliveryPrice] = rowData.deliveryPrice ?? ""
            print("CurrentCompanyDeliveryPrice: \(Defaults[.deliveryPrice])")
            performSegue(withIdentifier: "goProdDetailsSegue", sender: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == catsCollectionView{
            let cell = catsCollectionView.cellForItem(at: indexPath) as? CategoryCell
            cell?.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9294117647, blue: 0.9647058824, alpha: 1)
            cell?.subCategoryLbl.textColor =  #colorLiteral(red: 0.1921568627, green: 0.1921568627, blue: 0.1921568627, alpha: 1)
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == collectionShow{
            if indexPath.row == allProducts!.count - 1  && enableLoadMore && !gmma3lyProducts {  //numberofitem count
                if isAllCatProducts {
                    startLoadParentCategoryProductsDataFromApi()
                }
                else {
                    startLoadDataFromApi()
                }
            }
        }
    }
}

