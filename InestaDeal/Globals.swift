//
//  Globals.swift
//  InestaDeal
//
//  Created by Waleed on 4/11/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import SwifterSwift

public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}

// Screen height.
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}

public var appThemeColor: UIColor {
    return UIColor(hex: 0xFF9000)!
}
