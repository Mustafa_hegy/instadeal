    //
//  AppDelegate.swift
//  InestaDeal
//
//  Created by Waleed on 3/31/18.
//  Copyright © 2018 Waleed. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SwiftyUserDefaults
//import OneSignal
import Localize_Swift
//import AVKit
//import AVFoundation
import Firebase
import UserNotifications
import LiveChat

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , LiveChatDelegate {

    var window: UIWindow?
//    var player: AVPlayer?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Thread.sleep(forTimeInterval: 3.0)
        // Override point for customization after application launch.
        UIApplication.shared.statusBarStyle = .lightContent
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedStringKey.font:UIFont(name: "Droid Arabic Kufi", size: 9)]
        appearance.setTitleTextAttributes(attributes ?? [:], for: .normal)
        
        IQKeyboardManager.shared().isEnabled = true
        Defaults[.dismissed] = false
        /*
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId:"3aec0ac7-09d4-43ec-8fd5-4c48eb58cd6e", //"43187a97-e081-4266-a462-f21b39e44510", => Second //"9d5c6178-0190-43cb-ae8d-670f65ad8ee2", => First
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        */
        // Set Google Maps => AIzaSyArTbhPANo1vuVJPhO9UJHkjLE8uCeKF88
        
        // Initialize LIVECHAT
        LiveChat.licenseId  = "10893612"
        LiveChat.delegate   = self // Set self as delegate
        
        
        if Defaults[.langId] == nil || Defaults[.langId] == "" {
            // App is NOT set and lang is NOT selected ... set languge as device lang
            var preferredLanguage : String = Bundle.main.preferredLocalizations.first!
            Localize.setCurrentLanguage(preferredLanguage)
            
            // Cash selected Lang
            if preferredLanguage == "en" {
                Defaults[.langId]   = "1"
                UIView.appearance().semanticContentAttribute = .forceRightToLeft //Design is created for Ar =>default RTL :(
            } else if preferredLanguage == "ar" {
                Defaults[.langId]   = "2"
                UIView.appearance().semanticContentAttribute = .forceLeftToRight //Design is created for Ar =>default RTL :(
            }
        }
        else {
            // App is set and lang is selected
            if Defaults[.langId] == "1" {
                Localize.setCurrentLanguage("en")
                // English Direction
                UIView.appearance().semanticContentAttribute = .forceRightToLeft //Design is created for Ar =>default RTL :(
            } else if Defaults[.langId] == "2" {

                Localize.setCurrentLanguage("ar")
                // Arabic Direction
                UIView.appearance().semanticContentAttribute = .forceLeftToRight //Design is created for Ar =>default RTL :(
            }
        }
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        // [END register_for_notifications]
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
                
                // Set Token for Push Notification => Empty
                Defaults[.PNToken] = ""
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
//                self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
                
                // Set Token for Push Notification => FCM Token
                Defaults[.PNToken] = result.token
            }
        }
        
        // [START subscribe_topic]
        Messaging.messaging().subscribe(toTopic: "InstaDeal") { error in
            print("Subscribed to InstaDeal topic")
        }
        // [END subscribe_topic]
        
        /*
        // Call method to setup AVPlayer & AVPlayerLayer to play video
//        self.setupAVPlayer()
        let path                    = Bundle.main.path(forResource: "Intro", ofType:"mp4")
        let videoURL                = URL(fileURLWithPath: path!)
//        let avAssets                = AVAsset(url:  videoURL) // Create assets to get duration of video.
        player                      = AVPlayer(url: videoURL)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,object: player?.currentItem)

        let playerViewController    = AVPlayerViewController()
        window?.rootViewController  = playerViewController
        playerViewController.player = player
        playerViewController.view.backgroundColor   = .clear
        playerViewController.showsPlaybackControls  = false
        playerViewController.videoGravity           = AVLayerVideoGravity.resizeAspectFill.rawValue
        
        player?.play()
        */
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

        // MARK: - Class Custom Methods
    @objc func continueWithLaunch() {
        print("Intro Splash Video Reached to THE END")
      //Change the window rootView controller to your base controller (e.g. tabbar controller)
        if Defaults[.areaId] != "" {
            let controller  = HomeViewController()
            window?.rootViewController  = controller
        }else{
            let controller  = SelectAreaViewController()
            window?.rootViewController  = controller
        }
    }
    
    @objc func playerItemDidReachEnd(notification: Notification) {
        print("THE END")

        // uncomment the following line for a looping video
        // let p: AVPlayerItem = notification.object as! AVPlayerItem
        // p.seek(to: kCMTimeZero)

        //Change the window rootView controller to your base controller (e.g. tabbar controller)
//          if Defaults[.areaId] != "" {
//              let controller  = HomeViewController()
//              window?.rootViewController  = controller
//          }else{
              let controller  = SelectAreaViewController()
              window?.rootViewController  = controller
//          }

    }
    
    func received(message: LiveChatMessage) {
        print("Received message: \(message.text)")
        // Handle message here
    }
}
    
    // [START ios_10_message_handling]
    @available(iOS 10, *)
    extension AppDelegate : UNUserNotificationCenterDelegate {
        // Receive displayed notifications for iOS 10 devices.
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    willPresent notification: UNNotification,
                                    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            let userInfo = notification.request.content.userInfo
            // With swizzling disabled you must let Messaging know about the message, for Analytics
            // Messaging.messaging().appDidReceiveMessage(userInfo)
            // Print message ID.
            //            if let messageID = userInfo[gcmMessageIDKey] {
            //                print("Message ID: \(messageID)")
            //            }
            // Print full message.
            print(userInfo)
            // Change this to your preferred presentation option
            completionHandler([])
        }
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    didReceive response: UNNotificationResponse,
                                    withCompletionHandler completionHandler: @escaping () -> Void) {
            let userInfo = response.notification.request.content.userInfo
            // Print message ID.
            //            if let messageID = userInfo[gcmMessageIDKey] {
            //                print("Message ID: \(messageID)")
            //            }
            // Print full message.
            print(userInfo)
            completionHandler()
        }
    }
    // [END ios_10_message_handling]
    extension AppDelegate : MessagingDelegate {
        // [START refresh_token]
        func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
            print("Firebase registration token: \(fcmToken)")
            let dataDict:[String: String] = ["token": fcmToken]
            NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
            // TODO: If necessary send token to application server.
            // Note: This callback is fired at each app startup and whenever a new token is generated.
        }
        // [END refresh_token]
        // [START ios_10_data_message]
        // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
        // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
        func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
            print("Received data message: \(remoteMessage.appData)")
        }
        // [END ios_10_data_message]
    }
